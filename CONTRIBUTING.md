## Licensing
This repository is compliant with the [REUSE specification](https://reuse.software/). Please ensure that any files you contribute to the repository meet either
of the following two conditions:
* The file has a license header matching the one specified in the [HEADER](HEADER) file. This way of licensing files is preferred; Use the second option only
  when adding a license header is not possible.
* There is a file named `<filename>.license` in the same directory which matches the license header specified in the [HEADER](HEADER) file. Please adjust the
  SPDX license identifier as necessary.

The licence that should be used depends on the kind of file to be licensed. Please follow this schema when licencing files:
* **Code:** GNU Affero General Public License v3.0 or later (SPDX identifier: `AGPL-3.0-or-later`)
* **Images/other binary resources:** Creative Commons Attribution 4.0 International (SPDX identifier: `CC-BY-4.0`)
* **Trivial files/configurations:** Creative Commons Zero v1.0 Universal (SPDX identifier: `CC0-1.0`)
* **Documentation:** GNU Free Documentation License v1.3 or later (SPDX identifier: `GFDL-1.3-or-later`)

The only exception to the above schema are files for which you/the MobTalker2 team does not own the copyright to. Licences like these should be handled as
described in the REUSE specification.

## Code Style
### Member order
1.  enums (simple)
2.  static attributes (logs first, then private, default, protected, public)
3.  static constructor
4.  static functions (public first)
5.  initializer
6.  constructor
7.  constructor helper methods
8.  getters/setters
9.  other methods (private, default, protected, public)
10. abstract methods
11. overridden methods (hashCode, equals, toString)
12. inner types

This order may be disregarded, for example if the order of initialization is important.

### Member naming ###
Non-public (private, default, protected) attributes MUST be prefixed with an underscore (`_`). Public attributes SHOULD be marked `final`, if possible, and MUST be written in CamelCase. Loggers MUST be named `LOG`.

Apart from the above specifications, standard Java conventions apply.

### Semantic sections ###
Each semantic section (members that can be grouped together) should be separated by comments like this one:

	// ==========================================================

This is more of a visual help for clearer code.
