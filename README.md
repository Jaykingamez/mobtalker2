# MobTalker2
[![REUSE badge](https://api.reuse.software/badge/bitbucket.org/MobTalker2/mobtalker2)](https://api.reuse.software/info/bitbucket.org/MobTalker2/mobtalker2)

A visual novel engine for Minecraft driven by a Lua-like scripting engine, MobTalkerScript.

For more information, visit the [Project Website](http://www.mobtalker.net).


## Compiling
1. Install Git and Gradle
2. Clone the repository
3. Setup the project by following standard Forge procedure
    - 'gradle setupDecompWorkspace'
    - 'gradle eclipse' or 'gradle idea'
4. 'gradle build proguard'


## Contributing
### Creating an issue
If you're not a programmer or aren't comfortable with submitting a Pull Request, 
you can submit an issue report to the tracker.  
That way other contributors can start working on fixing it.

### Submitting a Pull Request
If you want to help the project by fixing bugs or implementing new features, 
you can fork the repository and submit a Pull Request.

MobTalker2 uses [git-flow](https://www.atlassian.com/git/workflows#!workflow-gitflow). 
Use properly named feature branches for your PRs.

1. Fork the repository (always from develop, NEVER from master)
2. Setup the project as shown above
3. Create a new feature branch and make your changes
4. (Optional) Squash your commits
5. Submit your PR

To increase the chances that your PR gets accepted, follow these basic rules

* Use git-flow feature branches
* Include the license header on top of your files (easily achieved by running 'gradle licenseFormat')
* Follow the general coding style of the rest of the project (e.g. spaces instead of tabs)

## Maintainers
* mezzodrinker <contact@mobtalker.net>

## License
This work is licensed under multiple licenses. In general, the following schema applies:

* **Code:** GNU Affero General Public License v3.0 or later
* **Documentation:** GNU Free Documentation License v1.3 or later
* **Images/other binary resources:** Creative Commons Attribution 4.0 International
* **Trivial files/configurations:** Creative Commons Zero v1.0 Universal

Please note that each file has a license header (or file called `<filename>.license`) which contains SPDX identifiers for the licenses used in the file.