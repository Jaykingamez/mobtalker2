/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

import static net.minecraftforge.fml.relauncher.Side.*;

import java.math.RoundingMode;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.SideOnly;

import com.google.common.math.DoubleMath;

@SideOnly( CLIENT )
public class MGButton extends MGComponent
{
    public static final int _colorEnabled = 0x00E0E0E0;
    public static final int _colorDisabled = 0xFFA0A0A0;
    public static final int _colorHover = 0x00FFFFA0;
    
    protected static final ResourceLocation _buttonTexture = new ResourceLocation( "textures/gui/widgets.png" );
    
    // ========================================
    
    protected String _caption;
    
    // ========================================
    
    {
        enableMouseEvents( true );
    }
    
    // ========================================
    
    @Override
    public void doDraw()
    {
        bindTexture( _buttonTexture );
        
        int vOffset = 20;
        if ( _isMouseInside )
        {
            vOffset *= 2;
        }
        
        Rectangle b = getBounds();
        
        int leftWidth = DoubleMath.roundToInt( b.getWidth() / 2D, RoundingMode.FLOOR );
        int rightWidth = DoubleMath.roundToInt( b.getWidth() / 2D, RoundingMode.CEILING );
        
        GlStateManager.color( 1.0f, 1.0f, 1.0f );
        
        int v = 46 + vOffset;
        drawTextureScaled( b.getLeft(), b.getTop(), leftWidth, b.getHeight(), 0, v, leftWidth, 20 );
        drawTextureScaled( b.getCenterX(), b.getTop(), rightWidth, b.getHeight(), 200 - rightWidth, v, rightWidth, 20 );
        drawCenteredString( _caption, b.getCenterX(), b.getTop() + ( ( b.getHeight() - 8 ) / 2 ), _colorEnabled );
    }
    
    // ========================================
    
    public String getCaption()
    {
        return _caption;
    }
    
    public void setCaption( String caption )
    {
        _caption = caption;
    }
    
}
