/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

import static net.minecraftforge.fml.relauncher.Side.*;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.mobtalker.mobtalker2.client.renderer.texture.MobTalkerTexture;

import org.lwjgl.opengl.GL11;

@SideOnly( CLIENT )
public class MGTexture extends MGComponent
{
    protected ResourceLocation _location;
    protected MobTalkerTexture _texture;
    
    // ========================================
    
    {}
    
    // ========================================
    
    protected void bindTexture()
    {
        bindTexture( _location );
    }
    
    @Override
    protected void doDraw()
    {
        if ( _texture == null )
            return;
        
        bindTexture();
        
        GlStateManager.enableBlend();
        GlStateManager.blendFunc( GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA );
        GlStateManager.color( 1.0f, 1.0f, 1.0f, 1.0f );
        
        drawTextureScaled( getBounds(), 0, 0 );
        
        GlStateManager.disableBlend();
    }
    
    // ========================================
    
    public ResourceLocation getLocation()
    {
        return _location;
    }
    
    public void setTexture( ResourceLocation location )
    {
        _location = location;
        _texture = null;
        
        MobTalkerTexture texture = new MobTalkerTexture( location );
        if ( !getTextureManager().loadTexture( location, texture ) )
            return;
        
        _texture = texture;
    }
}
