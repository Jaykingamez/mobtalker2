/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.*;

@SideOnly( Side.CLIENT )
public class MGFrame extends MGContainer
{
    protected final MGTexture _backgroundTexture;
    protected final MGRectangle _backgroundShape;
    protected final MGEdgeTexture _edgeTexture;
    protected int _edgeSize;
    
    // ========================================
    
    public MGFrame()
    {
        _backgroundTexture = new MGTexture();
        _backgroundTexture.setName( "bgTexture" );
        _backgroundTexture.setParent( this );
        _backgroundTexture.setLevel( -2 );
        _backgroundTexture.setVisible( false );
        add( _backgroundTexture );
        
        _backgroundShape = new MGRectangle();
        _backgroundShape.setName( "bgColor" );
        _backgroundShape.setParent( this );
        _backgroundShape.setLevel( -2 );
        _backgroundShape.setVisible( false );
        add( _backgroundShape );
        
        _edgeTexture = new MGEdgeTexture();
        _edgeTexture.setName( "edgeTexture" );
        _edgeTexture.setParent( this );
        _edgeTexture.setLevel( -1 );
        add( _edgeTexture );
        
        setBackgroundPadding( 0, 0, 0, 0 );
        setEdgeSize( 0 );
    }
    
    // ========================================
    
    @Override
    public void doDraw()
    {
        super.doDraw();
    }
    
    // ========================================
    
    public void setBackgroundTexture( String resourcePath )
    {
        setBackgroundTexture( new ResourceLocation( resourcePath ) );
    }
    
    public void setBackgroundTexture( ResourceLocation resource )
    {
        _backgroundShape.setVisible( false );
        _backgroundTexture.setTexture( resource );
        _backgroundTexture.setVisible( true );
    }
    
    public void setBackgroundColor( float r, float g, float b, float a )
    {
        setBackgroundColor( new Color( r, g, b, a ) );
    }
    
    public void setBackgroundColor( Color color )
    {
        _backgroundTexture.setVisible( false );
        _backgroundShape.setColor( color );
        _backgroundShape.setVisible( true );
    }
    
    public void setBackgroundPadding( int left, int top, int right, int bottom )
    {
        _backgroundTexture.clearAllAnchors();
        _backgroundTexture.addAnchor( Alignment.TOPLEFT, Alignment.TOPLEFT, this, new Point( -left, -top ) );
        _backgroundTexture.addAnchor( Alignment.BOTTOMRIGHT, Alignment.BOTTOMRIGHT, this, new Point( right, bottom ) );
        
        _backgroundShape.clearAllAnchors();
        _backgroundShape.addAnchor( Alignment.TOPLEFT, Alignment.TOPLEFT, this, new Point( -left, -top ) );
        _backgroundShape.addAnchor( Alignment.BOTTOMRIGHT, Alignment.BOTTOMRIGHT, this, new Point( right, bottom ) );
    }
    
    // ========================================
    
    public void setEdgeTexture( String resourcePath )
    {
        setEdgeTexture( new ResourceLocation( resourcePath ) );
    }
    
    public void setEdgeTexture( ResourceLocation resource )
    {
        _edgeTexture.setTexture( resource );
    }
    
    public void setEdgeSize( int edgeSize )
    {
        _edgeSize = edgeSize;
        _edgeTexture.clearAllAnchors();
        _edgeTexture.addAnchor( Alignment.TOPLEFT, Alignment.TOPLEFT, this, new Point( -edgeSize, -edgeSize ) );
        _edgeTexture.addAnchor( Alignment.BOTTOMRIGHT, Alignment.BOTTOMRIGHT, this, new Point( edgeSize, edgeSize ) );
    }
}
