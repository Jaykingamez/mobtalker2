/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

import static net.minecraftforge.fml.relauncher.Side.*;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly( CLIENT )
public abstract class MouseListenerAdapter implements IMouseListener
{
    @Override
    public void mouseClicked( MGComponent sender, int x, int y, int button )
    {}
    
    @Override
    public void mouseUp( MGComponent sender, int x, int y )
    {}
    
    @Override
    public void mouseMoved( MGComponent sender, int x, int y )
    {}
    
    @Override
    public void mouseEntered( MGComponent sender )
    {}
    
    @Override
    public void mouseLeft( MGComponent sender )
    {}
}
