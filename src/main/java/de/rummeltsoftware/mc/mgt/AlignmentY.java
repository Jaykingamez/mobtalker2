/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

import java.math.RoundingMode;

import com.google.common.math.DoubleMath;

public enum AlignmentY
{
 TOP
 {
     @Override
     public int getCoordinate( Rectangle r )
     {
         return r.getTop();
     }
     
     @Override
     public void setCoordinate( MutableRectangle r, int y )
     {
         r.setTop( y );
     }
     
     @Override
     public void setHeigth( MutableRectangle r, int heigth )
     {
         r.setBottom( r.getTop() + heigth );
     }
 },
 
 CENTER
 {
     @Override
     public int getCoordinate( Rectangle r )
     {
         return r.getCenterY();
     }
     
     @Override
     public void setCoordinate( MutableRectangle r, int y )
     {
         r.setCenterY( y );
     }
     
     @Override
     public void setHeigth( MutableRectangle r, int heigth )
     {
         int currentHeight = r.getCenterY();
         heigth = DoubleMath.roundToInt( heigth / 2d, RoundingMode.CEILING );
         r.setTop( currentHeight - heigth );
         r.setBottom( currentHeight + heigth );
     }
 },
 
 BOTTOM
 {
     @Override
     public int getCoordinate( Rectangle r )
     {
         return r.getBottom();
     }
     
     @Override
     public void setCoordinate( MutableRectangle r, int y )
     {
         r.setBottom( y );
     }
     
     @Override
     public void setHeigth( MutableRectangle r, int heigth )
     {
         r.setTop( r.getBottom() - heigth );
     }
 };
    
    // ========================================
    
    public abstract int getCoordinate( Rectangle r );
    
    public abstract void setCoordinate( MutableRectangle r, int y );
    
    /**
     * Sets the height of a rectangle relative from this side
     */
    public abstract void setHeigth( MutableRectangle r, int heigth );
}
