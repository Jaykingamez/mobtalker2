/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

import static net.minecraftforge.fml.relauncher.Side.*;

import java.math.RoundingMode;
import java.util.*;

import net.minecraft.client.gui.FontRenderer;
import net.minecraftforge.fml.relauncher.SideOnly;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Strings;
import com.google.common.math.DoubleMath;

@SideOnly( CLIENT )
public class MGTextArea extends MGComponent
{
    private int _lineCount;
    private int _lineHeight;
    private int _lineSpacing;
    private boolean _dropShadow;
    
    private String _text;
    private AlignmentX _textAlignment;
    protected final List<String> _lines;
    
    private boolean _enableScrolling;
    private int _scrollingSpeed;
    private boolean _isScrolling;
    private int _scrollingRow;
    private int _scrollingCol;
    
    // ========================================
    
    public MGTextArea()
    {
        _lineHeight = getFontRenderer().FONT_HEIGHT;
        _lines = new ArrayList<String>( 1 );
        _textAlignment = AlignmentX.LEFT;
        
        setWidth( 1 );
        setLineCount( 1 );
    }
    
    // ========================================
    
    @Override
    protected void doDraw()
    {
        int yOffset = 0;
        
        int linesToDisplay = Math.min( ( _isScrolling ? _scrollingRow : _lineCount ), _lines.size() );
        
        FontRenderer renderer = getFontRenderer();
        
        for ( int i = 0; i < linesToDisplay; i++ )
        {
            String line = _lines.get( i );
            
            int left = _textAlignment.getCoordinate( getBounds() );
            if ( _textAlignment != AlignmentX.LEFT )
            {
                int lineWidth = renderer.getStringWidth( line );
                if ( _textAlignment == AlignmentX.CENTER )
                    left -= DoubleMath.roundToInt( lineWidth / 2.0D, RoundingMode.CEILING );
                else if ( _textAlignment == AlignmentX.RIGHT )
                    left -= lineWidth;
            }
            
            if ( _isScrolling && ( i == ( linesToDisplay - 1 ) ) )
            {
                line = line.substring( 0, _scrollingCol );
            }
            
            renderer.drawString( line, left, getBounds().getTop() + yOffset, 0xFFFFFFFF, _dropShadow );
            yOffset += _lineHeight;
        }
    }
    
    @Override
    public void doLayout( Dimensions screenDim )
    {
        super.doLayout( screenDim );
        
        _lines.clear();
        
        if ( Strings.isNullOrEmpty( _text ) )
            return;
        
        int width = Math.max( getBounds().getWidth(), getMinStringWidth( _text ) );
        
        @SuppressWarnings( "unchecked" )
        List<String> formattedLines = getFontRenderer().listFormattedStringToWidth( _text, width );
        for ( int i = 0; i < formattedLines.size(); i++ )
        {
            _lines.add( formattedLines.get( i ) );
        }
        if ( _enableScrolling )
        {
            _scrollingRow = 1;
            _scrollingCol = 1;
            _isScrolling = true;
        }
    }
    
    private static int getMinStringWidth( String s )
    {
        FontRenderer renderer = getFontRenderer();
        
        int result = 1;
        for ( String word : StringUtils.split( s, ' ' ) )
        {
            result = Math.max( result, renderer.getStringWidth( word ) );
        }
        
        return result;
    }
    
    @Override
    public void update()
    {
        if ( !_isScrolling || _lines.isEmpty() )
            return;
        
        int rowLength = _lines.get( _scrollingRow - 1 ).length();
        if ( ( _scrollingCol += _scrollingSpeed ) > rowLength )
        {
            _scrollingRow++;
            _scrollingCol = 1;
        }
        
        if ( _scrollingRow > Math.min( _lineCount, _lines.size() ) )
        {
            _isScrolling = false;
        }
    }
    
    // ========================================
    
    public String getText()
    {
        return _text;
    }
    
    public AlignmentX getTextAlignment()
    {
        return _textAlignment;
    }
    
    public int getLineCount()
    {
        return _lineCount;
    }
    
    public int getLineSpacing()
    {
        return _lineSpacing;
    }
    
    public int getLineHeight()
    {
        return _lineHeight;
    }
    
    public boolean getDropShadow()
    {
        return _dropShadow;
    }
    
    public boolean isTextScrollingEnabled()
    {
        return _enableScrolling;
    }
    
    public int getTextScrollingSpeed()
    {
        return _scrollingSpeed;
    }
    
    // ========================================
    
    public void setText( String text )
    {
        _text = text;
        invalidate();
    }
    
    public void setTextAlignment( AlignmentX textAlignment )
    {
        _textAlignment = textAlignment;
    }
    
    public void setLineCount( int count )
    {
        _lineCount = count;
        super.setHeight( ( count * _lineHeight ) - _lineSpacing );
        
        if ( _text != null )
        {
            setText( _text );
        }
    }
    
    public void setLineSpacing( int spacing )
    {
        _lineSpacing = spacing;
        _lineHeight = getFontRenderer().FONT_HEIGHT + _lineSpacing;
        super.setHeight( ( _lineCount * _lineHeight ) - _lineSpacing );
    }
    
    public void setDropShadow( boolean dropShadow )
    {
        _dropShadow = dropShadow;
    }
    
    @Override
    public void setHeight( int height )
    {
        throw new UnsupportedOperationException( "Cannot set the height of an text area directly. Use setLineCount and setLineSpacing instead." );
    }
    
    public void enableTextScrolling( boolean enable )
    {
        _enableScrolling = enable;
        _isScrolling = enable;
    }
    
    public void setTextScrollingSpeed( int speed )
    {
        _scrollingSpeed = speed;
    }
    
    public boolean isTextScrolling()
    {
        return _isScrolling;
    }
    
    public void finishTextScrolling()
    {
        _isScrolling = false;
    }
    
    public void resetTextScrolling()
    {
        _scrollingRow = 1;
        _scrollingCol = 1;
        _isScrolling = _enableScrolling;
    }
}
