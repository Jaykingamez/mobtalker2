/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

public class Dimensions
{
    private final int _w;
    private final int _h;
    
    // ========================================
    
    public Dimensions( int w, int h )
    {
        _w = w;
        _h = h;
    }
    
    // ========================================
    
    public int getWidth()
    {
        return _w;
    }
    
    public int getHeight()
    {
        return _h;
    }
    
    // ========================================
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = ( prime * result ) + _h;
        result = ( prime * result ) + _w;
        return result;
    }
    
    @Override
    public boolean equals( Object obj )
    {
        if ( this == obj )
        {
            return true;
        }
        if ( obj == null )
        {
            return false;
        }
        if ( getClass() != obj.getClass() )
        {
            return false;
        }
        Dimensions other = (Dimensions) obj;
        if ( _h != other._h )
        {
            return false;
        }
        if ( _w != other._w )
        {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString()
    {
        return "Dimension [Width=" + _w + ", Height=" + _h + "]";
    }
}
