/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

import java.math.RoundingMode;

import com.google.common.math.DoubleMath;

public class Rectangle
{
    public static final Rectangle Normal = new Rectangle( 0, 0, 0, 0 );
    
    // ========================================
    
    protected int _left;
    protected int _right;
    protected int _top;
    protected int _bottom;
    
    // ========================================
    
    public Rectangle( int left, int right, int top, int bottom )
    {
        _left = left;
        _right = right;
        _top = top;
        _bottom = bottom;
    }
    
    // ========================================
    
    public int getWidth()
    {
        return _right - _left;
    }
    
    public int getHeight()
    {
        return _bottom - _top;
    }
    
    // ========================================
    
    public int getLeft()
    {
        return _left;
    }
    
    public int getTop()
    {
        return _top;
    }
    
    public int getCenterX()
    {
        return _left + DoubleMath.roundToInt( getWidth() / 2d, RoundingMode.CEILING );
    }
    
    public int getCenterY()
    {
        return _top + DoubleMath.roundToInt( getHeight() / 2d, RoundingMode.CEILING );
    }
    
    public int getBottom()
    {
        return _bottom;
    }
    
    public int getRight()
    {
        return _right;
    }
    
    // ========================================
    
    public boolean contains( int x, int y )
    {
        return ( _left <= x ) && ( x <= _right ) && ( _top <= y ) && ( y <= _bottom );
    }
    
    // ========================================
    
    @Override
    public String toString()
    {
        return String.format( "Rectangle [Left=%s, Right=%s, Top=%s, Bottom=%s, Width=%s, Height=%s]",
                              getLeft(), getRight(), getTop(), getBottom(), getWidth(), getHeight() );
    }
    
}