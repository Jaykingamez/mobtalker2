/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

import net.minecraftforge.fml.relauncher.*;

@SideOnly( Side.CLIENT )
public class MGRectangle extends MGComponent
{
    private Color _color;
    
    // ========================================
    
    {
        _color = Color.White;
    }
    
    // ========================================
    
    @Override
    protected void doDraw()
    {
        drawRectangle( getBounds(), _color.getRf(), _color.getGf(), _color.getBf(), _color.getAf() );
    }
    
    // ========================================
    
    public void setColor( Color c )
    {
        _color = c;
    }
}
