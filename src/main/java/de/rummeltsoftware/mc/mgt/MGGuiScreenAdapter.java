/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

import static net.minecraftforge.fml.relauncher.Side.*;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly( CLIENT )
public class MGGuiScreenAdapter extends GuiScreen
{
    private final MGDrawable _drawable;
    private final boolean _pauseGame;
    
    private boolean _initialized;
    
    // ========================================
    
    public MGGuiScreenAdapter( MGDrawable drawable, boolean pauseGame )
    {
        _drawable = drawable;
        _pauseGame = pauseGame;
    }
    
    // ========================================
    
    @Override
    public boolean doesGuiPauseGame()
    {
        return _pauseGame;
    }
    
    @Override
    public void initGui()
    {
        if ( !_initialized )
        {
            _drawable.init();
            _initialized = true;
        }
        
        _drawable.invalidate();
        _drawable.layout( new Dimensions( width, height ) );
    }
    
    @Override
    public void drawScreen( int mouseX, int mouseY, float par3 )
    {
        _drawable.onMouseUpdate( mouseX, mouseY );
        _drawable.draw();
    }
    
    @Override
    public void updateScreen()
    {
        _drawable.update();
    }
    
    @Override
    protected void mouseClicked( int x, int y, int mouseButton )
    {
        _drawable.onMouseClicked( x, y, mouseButton );
    }
    
    @Override
    protected void mouseReleased( int x, int y, int state )
    {
        _drawable.onMouseUp( x, y );
    }
    
    @Override
    protected void keyTyped( char keyChar, int keyCode )
    {
        _drawable.onKeyPressed( keyChar, keyCode );
    }
    
    @Override
    public void onGuiClosed()
    {
        _drawable.onClosed();
    }
}
