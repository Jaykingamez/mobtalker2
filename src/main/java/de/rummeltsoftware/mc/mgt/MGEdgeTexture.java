/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

import static net.minecraftforge.fml.relauncher.Side.*;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraftforge.fml.relauncher.SideOnly;

import org.lwjgl.opengl.GL11;

@SideOnly( CLIENT )
public class MGEdgeTexture extends MGTexture
{
    @Override
    protected void doDraw()
    {
        if ( _texture == null )
            return;
        
        Rectangle b = getBounds();
        Dimensions textureSize = _texture.getSize();
        int tileSize = 8;
        
        bindTexture();
        
        GlStateManager.enableBlend();
        GlStateManager.blendFunc( GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA );
        GlStateManager.color( 1.0f, 1.0f, 1.0f, 1.0f );
        
        // Corners
        drawTextureScaled( b.getLeft(), b.getTop(), b.getLeft() + tileSize, b.getTop() + tileSize,
                           4 * 32, 0, 32, 32, textureSize );
        drawTextureScaled( b.getRight() - tileSize, b.getTop(), b.getRight(), b.getTop() + tileSize,
                           5 * 32, 0, 32, 32, textureSize );
        drawTextureScaled( b.getLeft(), b.getBottom() - tileSize, b.getLeft() + tileSize, b.getBottom(),
                           6 * 32, 0, 32, 32, textureSize );
        drawTextureScaled( b.getRight() - tileSize, b.getBottom() - tileSize, b.getRight(), b.getBottom(),
                           7 * 32, 0, 32, 32, textureSize );
        
        // Sides
        drawTextureScaled( b.getLeft(), b.getTop() + tileSize, b.getLeft() + tileSize, b.getBottom() - tileSize,
                           0 * 32, 0, 32, 32, textureSize );
        drawTextureScaled( b.getRight() - tileSize, b.getTop() + tileSize, b.getRight(), b.getBottom() - tileSize,
                           1 * 32, 0, 32, 32, textureSize );
        drawTextureScaled( b.getLeft() + tileSize, b.getTop(), b.getRight() - tileSize, b.getTop() + tileSize,
                           2 * 32, 0, 32, 32, textureSize );
        drawTextureScaled( b.getLeft() + tileSize, b.getBottom() - tileSize, b.getRight() - tileSize, b.getBottom(),
                           3 * 32, 0, 32, 32, textureSize );
        
        GlStateManager.disableBlend();
    }
}
