/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2;

import static net.minecraftforge.fml.relauncher.Side.*;
import net.minecraftforge.fml.relauncher.SideOnly;

public class Constants
{
    @SideOnly( CLIENT )
    public static class Client
    {
        public static final String PREFIX_ITEM = "item." + MobTalker2.ID + ":";
        public static final String PREFIX_RESOURCE = MobTalker2.ID + ":";
    }
}
