/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.network;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.*;
import net.minecraftforge.fml.common.network.NetworkRegistry.TargetPoint;
import net.minecraftforge.fml.common.network.simpleimpl.*;
import net.minecraftforge.fml.relauncher.Side;
import net.mobtalker.mobtalker2.MobTalker2;
import net.mobtalker.mobtalker2.client.network.ClientMessageHandler;
import net.mobtalker.mobtalker2.common.network.message.*;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;

public class NetworkManager
{
    private static final MobTalkerLog LOG = new MobTalkerLog( "NetworkManager" );
    private static final SimpleNetworkWrapper CHANNEL;
    
    static
    {
        CHANNEL = NetworkRegistry.INSTANCE.newSimpleChannel( MobTalker2.CHANNEL );
    }
    
    // ========================================
    
    public static void load()
    {
        registerServerMessage( MessageShowText.class, MessageIDs.ShowText );
        registerServerMessage( MessageShowMenu.class, MessageIDs.ShowMenu );
        registerServerMessage( MessageShowSprite.class, MessageIDs.ShowSprite );
        registerServerMessage( MessageShowScene.class, MessageIDs.ShowScene );
        registerServerMessage( MessageHideTexture.class, MessageIDs.HideTexture );
        
        registerClientMessage( MessageCancelInteraction.class, MessageIDs.InteractionCancel );
        registerClientMessage( MessageShowTextResponse.class, MessageIDs.ShowTextResponse );
        registerClientMessage( MessageShowMenuResponse.class, MessageIDs.ShowMenuResponse );
        
        registerServerMessage( MessageGuiConfig.class, MessageIDs.GuiConfig );
    }
    
    @SuppressWarnings( "unchecked" )
    private static <REQ extends ServerToClientMessage> void registerServerMessage( Class<REQ> messageClass, int discriminator )
    {
        try
        {
            Class<ClientMessageHandler.Delegator<REQ>> handlerClass;
            handlerClass = (Class<ClientMessageHandler.Delegator<REQ>>) Class.forName( messageClass.getName() + "$Handler" );
            
            CHANNEL.registerMessage( handlerClass, messageClass, discriminator, Side.CLIENT );
        }
        catch ( Exception ex )
        {
            LOG.fatal( ex, "Failed to register message type '%s'", messageClass.getSimpleName() );
        }
    }
    
    @SuppressWarnings( "unchecked" )
    private static <REQ extends ClientToServerMessage> void registerClientMessage( Class<REQ> messageClass, int discriminator )
    {
        try
        {
            Class<ServerMessageHandler.Delegator<REQ>> handlerClass;
            handlerClass = (Class<ServerMessageHandler.Delegator<REQ>>) Class.forName( messageClass.getName() + "$Handler" );
            
            CHANNEL.registerMessage( handlerClass, messageClass, discriminator, Side.SERVER );
        }
        catch ( Exception ex )
        {
            LOG.fatal( ex, "Failed to register message type '%s'", messageClass.getSimpleName() );
        }
    }
    
    // ========================================
    
    public static void sendToAll( IMessage message )
    {
        CHANNEL.sendToAll( message );
    }
    
    public static void sendTo( IMessage message, EntityPlayerMP player )
    {
        CHANNEL.sendTo( message, player );
    }
    
    public static void sendToAllAround( IMessage message, TargetPoint point )
    {
        CHANNEL.sendToAllAround( message, point );
    }
    
    public static void sendToDimension( IMessage message, int dimensionId )
    {
        CHANNEL.sendToDimension( message, dimensionId );
    }
    
    public static void sendToServer( IMessage message )
    {
        CHANNEL.sendToServer( message );
    }
}
