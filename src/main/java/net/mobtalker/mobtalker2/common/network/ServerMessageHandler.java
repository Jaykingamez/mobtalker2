/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.network;

import java.util.*;
import java.util.concurrent.ExecutionException;

import net.minecraft.entity.player.*;
import net.minecraftforge.fml.common.network.simpleimpl.*;
import net.mobtalker.mobtalker2.common.network.message.*;
import net.mobtalker.mobtalker2.server.interaction.*;
import net.mobtalker.mobtalker2.util.EntityUtil;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;

import com.google.common.base.Throwables;
import com.google.common.collect.Maps;

public class ServerMessageHandler
{
    private static final MobTalkerLog LOG = new MobTalkerLog( "ServerMessageHandler" );
    
    private static ServerMessageHandler instance = new ServerMessageHandler();
    
    public static ServerMessageHandler instance()
    {
        return instance;
    }
    
    // ========================================
    
    private final Map<EntityPlayerMP, RemoteCallResult> _calls;
    
    private ServerMessageHandler()
    {
        _calls = Maps.newHashMap();
    }
    
    // ========================================
    
    private RemoteCallResult createFuture( EntityPlayerMP player, RemoteCallMessage msg )
    {
        synchronized ( _calls )
        {
            RemoteCallResult future = _calls.remove( player );
            
            if ( ( future != null ) && !future.isCancelled() )
            {
                LOG.error( "Overriding an already pending call for player '%s'! THIS IS A BUG! PLEASE FILL AN ISSUE WITH THE LOG!",
                           EntityUtil.getName( player ) );
            }
            
            future = new RemoteCallResult( msg.getResponseType() );
            _calls.put( player, future );
            
            return future;
        }
    }
    
    // ========================================
    
    public void sendShowText( EntityPlayerMP player, String name, String text, boolean isLast )
        throws Exception
    {
        MessageShowText msg = new MessageShowText().setName( name )
                                                   .setText( text )
                                                   .setLast( isLast );
        
        RemoteCallResult result = createFuture( player, msg );
        
        NetworkManager.sendTo( msg, player );
        
        try
        {
            result.get();
        }
        catch ( ExecutionException ex )
        {
            Throwable cause = Throwables.getRootCause( ex );
            Throwables.propagateIfPossible( cause );
            throw (Exception) cause;
        }
    }
    
    public int sendShowMenu( EntityPlayerMP player, String caption, List<String> options )
        throws Exception
    {
        MessageShowMenu msg = new MessageShowMenu().setCaption( caption )
                                                   .setChoices( options );
        
        RemoteCallResult result = createFuture( player, msg );
        
        NetworkManager.sendTo( msg, player );
        
        MessageShowMenuResponse response;
        try
        {
            response = (MessageShowMenuResponse) result.get();
        }
        catch ( ExecutionException ex )
        {
            Throwable cause = Throwables.getRootCause( ex );
            Throwables.propagateIfPossible( cause );
            throw (Exception) cause;
        }
        
        return response.getChoice();
    }
    
    public static void sendShowSprite( EntityPlayerMP player, String group, String path, String position, int offsetX, int offsetY )
    {
        NetworkManager.sendTo( new MessageShowSprite().setGroup( group )
                                                      .setPath( path )
                                                      .setPosition( position )
                                                      .setOffsetX( offsetX )
                                                      .setOffsetY( offsetY ),
                               player );
    }
    
    public static void sendShowScene( EntityPlayerMP player, String path, String mode )
    {
        NetworkManager.sendTo( new MessageShowScene().setPath( path )
                                                     .setMode( mode ),
                               player );
    }
    
    public static void sendHideTexture( EntityPlayerMP player, String group )
    {
        NetworkManager.sendTo( new MessageHideTexture().setGroup( group ),
                               player );
    }
    
    public static void sendCloseInteraction( EntityPlayerMP player )
    {
        if ( player.openContainer != player.inventoryContainer )
        {
            player.closeScreen();
        }
    }
    
    public static void sendGuiConfig( EntityPlayerMP player, String config )
    {
        NetworkManager.sendTo( new MessageGuiConfig().setConfig( config ),
                               player );
    }
    
    // ========================================
    
    public void cancelCall( EntityPlayer player )
    {
        synchronized ( _calls )
        {
            RemoteCallResult future = _calls.remove( player );
            
            if ( future != null )
                future.cancel( true );
        }
    }
    
    public void cancelAllCalls()
    {
        synchronized ( _calls )
        {
            for ( RemoteCallResult future : _calls.values() )
            {
                future.cancel( true );
            }
            
            _calls.clear();
        }
    }
    
    // ========================================
    
    public void onRemoteCallResult( EntityPlayerMP player, ClientToServerMessage response )
    {
        RemoteCallResult future;
        synchronized ( _calls )
        {
            future = _calls.remove( player );
        }
        
        if ( future == null )
        {
            LOG.error( "Fetched remote call response for a non-exisiting call: %s",
                       response.getClass().getTypeName() );
            return;
        }
        if ( future.isCancelled() )
        {
            LOG.error( "Fetched remote call response for a canceled call: %s",
                       response.getClass().getTypeName() );
            return;
        }
        
        if ( !future.getResponseType().equals( response.getClass() ) )
            throw new IllegalStateException( "Remote call response has wrong type. Expected '"
                                             + future.getResponseType().getName()
                                             + "', got '"
                                             + response.getClass().getName()
                                             + "'." );
        
        try
        {
            future.set( response, null );
        }
        catch ( Exception ex )
        {
            future.set( null, ex );
        }
    }
    
    public void onCancelInteraction( EntityPlayerMP player, MessageCancelInteraction packet )
    {
        InteractionRegistry.instance().cancelInteraction( player );
        cancelCall( player );
    }
    
    // ========================================
    
    public static ServerInteractionHandler getInteractionHandler()
    {
        return null;
    }
    
    // ========================================
    
    public static abstract class Delegator<REQ extends ClientToServerMessage>
        implements IMessageHandler<REQ, ServerToClientMessage>
    {}
    
    public static abstract class RemoteCallResultDelegator<REQ extends ClientToServerMessage>
        implements IMessageHandler<REQ, ServerToClientMessage>
    {
        @Override
        public ServerToClientMessage onMessage( REQ message, MessageContext ctx )
        {
            ServerMessageHandler.instance().onRemoteCallResult( ctx.getServerHandler().playerEntity, message );
            return null;
        }
    }
}
