/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.network.message;

import static net.minecraftforge.fml.relauncher.Side.*;

import io.netty.buffer.ByteBuf;

import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.mobtalker.mobtalker2.client.network.ClientMessageHandler;

public class MessageShowText extends RemoteCallMessage
{
    private String _name;
    private String _text;
    private boolean _isLast;
    
    // ========================================
    
    public String getName()
    {
        return _name;
    }
    
    public String getText()
    {
        return _text;
    }
    
    public boolean isLast()
    {
        return _isLast;
    }
    
    public MessageShowText setName( String name )
    {
        _name = name;
        return this;
    }
    
    public MessageShowText setText( String text )
    {
        _text = text;
        return this;
    }
    
    public MessageShowText setLast( boolean isLast )
    {
        _isLast = isLast;
        return this;
    }
    
    // ========================================
    
    @Override
    public void fromBytes( ByteBuf buf )
    {
        _name = readString( buf );
        _text = readString( buf );
        _isLast = buf.readBoolean();
    }
    
    @Override
    public void toBytes( ByteBuf buf )
    {
        writeString( buf, _name );
        writeString( buf, _text );
        buf.writeBoolean( _isLast );
    }
    
    @Override
    public Class<? extends ClientToServerMessage> getResponseType()
    {
        return MessageShowTextResponse.class;
    }
    
    // ========================================
    
    public static class Handler extends ClientMessageHandler.Delegator<MessageShowText>
    {
        @Override
        public ClientToServerMessage onMessage( MessageShowText message, MessageContext ctx )
        {
            if ( !checkThread( message, ctx ) )
                return null;
            
            ClientMessageHandler.instance().onShowText( message );
            return null;
        }
    }
}
