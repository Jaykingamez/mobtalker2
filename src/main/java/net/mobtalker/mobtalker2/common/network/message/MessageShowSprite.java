/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.network.message;

import static net.minecraftforge.fml.relauncher.Side.*;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.mobtalker.mobtalker2.client.network.ClientMessageHandler;

public class MessageShowSprite extends ServerToClientMessage
{
    private String _group;
    private String _path;
    
    private String _position;
    private int _offsetX;
    private int _offsetY;
    
    // ========================================
    
    public String getGroup()
    {
        return _group;
    }
    
    public MessageShowSprite setGroup( String group )
    {
        _group = group;
        return this;
    }
    
    public String getPath()
    {
        return _path;
    }
    
    public MessageShowSprite setPath( String path )
    {
        _path = path;
        return this;
    }
    
    public String getPosition()
    {
        return _position;
    }
    
    public MessageShowSprite setPosition( String position )
    {
        _position = position;
        return this;
    }
    
    public int getOffsetX()
    {
        return _offsetX;
    }
    
    public MessageShowSprite setOffsetX( int offsetX )
    {
        _offsetX = offsetX;
        return this;
    }
    
    public int getOffsetY()
    {
        return _offsetY;
    }
    
    public MessageShowSprite setOffsetY( int offsetY )
    {
        _offsetY = offsetY;
        return this;
    }
    
    // ========================================
    
    @Override
    public void fromBytes( ByteBuf buf )
    {
        _group = readString( buf );
        _path = readString( buf );
        _position = readString( buf );
        _offsetX = buf.readShort();
        _offsetY = buf.readShort();
    }
    
    @Override
    public void toBytes( ByteBuf buf )
    {
        writeString( buf, _group );
        writeString( buf, _path );
        writeString( buf, _position );
        buf.writeShort( _offsetX );
        buf.writeShort( _offsetY );
    }
    
    // ========================================
    
    public static class Handler extends ClientMessageHandler.Delegator<MessageShowSprite>
    {
        @Override
        public ClientToServerMessage onMessage( MessageShowSprite message, MessageContext ctx )
        {
            if ( !checkThread( message, ctx ) )
                return null;
            
            ClientMessageHandler.instance().onShowSprite( message );
            return null;
        }
    }
}
