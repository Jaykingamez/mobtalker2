/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common;

import static com.google.common.base.Preconditions.*;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Set;

import com.google.common.collect.Sets;

import net.minecraftforge.common.config.*;
import net.minecraftforge.fml.client.event.ConfigChangedEvent.OnConfigChangedEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.mobtalker.mobtalker2.MobTalker2;
import net.mobtalker.mobtalker2.common.entity.EntityType;

public class MobTalkerConfig
{
    public static final Charset CHARSET = Charset.forName( "UTF-8" );
    
    private static Configuration _config;
    private static boolean _debug;
    private static Property _disabledEntities;
    private static Set<EntityType> _disabledEntityTypes;
    private static Property _textScrollingSpeed;
    
    /**
     * @deprecated as of 0.7.7-alpha. Use {@link net.mobtalker.mobtalker2.client.gui.settings.ChoiceFrameSettings.ChoicesSettings
     * ChoiceFrameSettings.Choices.PerPage} instead.
     */
    @Deprecated
    private static Property _menuOptionsPerPage;
    
    // ========================================
    
    public static void init( File configFile )
    {
        checkState( _config == null, "A config is already loaded" );
        checkNotNull( configFile );
        
        _config = new Configuration( configFile, true );
        
        load();
        save();
        
        FMLCommonHandler.instance().bus().register( new ChangeHandler() );
    }
    
    public static void load()
    {
        loadGeneral();
        loadGui();
    }
    
    private static void loadGeneral()
    {
        Property debug = get( Configuration.CATEGORY_GENERAL, "Debug", false, "Whether debug output should be done or not\n"
                                                                              + "WARNING! This will clutter up your log and decrease overall user experience" );
        
        _debug = debug.getBoolean();
        
        _disabledEntities = get( Configuration.CATEGORY_GENERAL, "DisabledEntities",
                                 new String[] {},
                                 "Entities players should not be able to interact with\n"
                                 + "For example: Creeper or WitherSkeleton" );
        
        String[] entityTypeNames = _disabledEntities.getStringList();
        _disabledEntityTypes = Sets.newHashSetWithExpectedSize( entityTypeNames.length );
        for ( String entityTypeName : entityTypeNames )
        {
            _disabledEntityTypes.add( EntityType.forName( entityTypeName ) );
        }
    }
    
    private static void loadGui()
    {
        _textScrollingSpeed = get( "gui", "TextScrollingSpeed", 4,
                                   "Scrolling speed of the dialog text" );
        _menuOptionsPerPage = get( "gui", "MenuOptionsPerPage", 5,
                                   "How many options are displayed per page for choice menus" );
    }
    
    // ========================================
    
    public static void save()
    {
        if ( _config.hasChanged() )
        {
            _config.save();
        }
    }
    
    // ========================================
    
    public static Configuration getConfiguration()
    {
        return _config;
    }
    
    // ========================================
    
    public static boolean isDebugEnabled()
    {
        return _debug;
    }
    
    public static Set<EntityType> getDisabledEntityTypes()
    {
        return _disabledEntityTypes;
    }
    
    public static int getTextScrollingSpeed()
    {
        return _textScrollingSpeed.getInt();
    }
    
    /**
     * @return Number of options that should be displayed per menu page.
     * @deprecated as of 0.7.7-alpha. Use {@link net.mobtalker.mobtalker2.client.gui.settings.ChoiceFrameSettings.ChoicesSettings
     * ChoiceFrameSettings.Choices.PerPage} instead.
     */
    @Deprecated
    public static int getMenuOptionsPerPage()
    {
        return _menuOptionsPerPage.getInt();
    }
    
    // ========================================
    
    private static Property get( String category, String key, int defaultValue, String comment )
    {
        return _config.get( category, key, defaultValue, comment );
    }
    
    private static Property get( String category, String key, boolean defaultValue, String comment )
    {
        return _config.get( category, key, defaultValue, comment );
    }
    
//    private static Property get( String category, String key, double defaultValue, String comment )
//    {
//        return _config.get( category, key, defaultValue, comment );
//    }

//    private static Property get( String category, String key, String defaultValue, String comment )
//    {
//        return _config.get( category, key, defaultValue, comment );
//    }
    
    private static Property get( String category, String key, String[] defaultValue, String comment )
    {
        return _config.get( category, key, defaultValue, comment );
    }
    
    // ========================================
    
    public static final class ChangeHandler
    {
        ChangeHandler()
        {}
        
        @SubscribeEvent
        public void onConfigurationChanged( OnConfigChangedEvent event )
        {
            if ( !event.modID.equals( MobTalker2.ID ) )
                return;
            
            load();
        }
    }
}
