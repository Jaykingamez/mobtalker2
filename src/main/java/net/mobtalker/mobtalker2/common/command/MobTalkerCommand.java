/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.apache.commons.lang3.Validate.validState;

public class MobTalkerCommand extends CommandBase
{
    static final MobTalkerLog LOG = new MobTalkerLog( "Command" );
    
    // ========================================
    
    private final List<IMobTalkerSubCommand> _subCommands = new ArrayList<>();
    
    // ========================================
    
    private static MobTalkerCommand instance;
    
    public static MobTalkerCommand instance()
    {
        validState( isInitialized(), "MobTalkerCommand has not yet been instantiated" );
        return instance;
    }
    
    public static boolean isInitialized()
    {
        return instance != null;
    }
    
    // ========================================
    
    public MobTalkerCommand()
    {
        validState( !isInitialized(), "MobTalkerCommand has already been instantiated" );
        instance = this;
        _subCommands.add( new MainCommand() );
        _subCommands.add( new SavedVariablesCommand() );
        //TODO _subCommands.add( new DebugCommand() );
        //TODO _subCommands.add( new ReloadCommand() );
    }
    
    // ========================================
    
    List<IMobTalkerSubCommand> getSubCommands()
    {
        return Collections.unmodifiableList( _subCommands );
    }
    
    // ========================================
    
    @Override
    public String getCommandName()
    {
        return "mobtalker";
    }
    
    @Override
    public String getCommandUsage( ICommandSender sender )
    {
        return "/mobtalker <subcommand> [arguments...]";
    }
    
    @Override
    public void processCommand( ICommandSender sender, String[] args )
            throws CommandException
    {
        LOG.debug( "Received command: /mobtalker %s", String.join( " ", args ) );
        
        boolean inString = false;
        StringBuilder sb = new StringBuilder();
        List<String> arguments = new ArrayList<>();
        for ( String arg : args )
        {
            if ( inString )
            {
                sb.append( " " );
                if ( arg.endsWith( "\"" ) )
                {
                    sb.append( arg, 0, arg.length() - 1 );
                    arguments.add( sb.toString() );
                    sb.setLength( 0 );
                    inString = false;
                }
                else
                {
                    sb.append( arg );
                }
            }
            else if ( arg.startsWith( "\"" ) )
            {
                if ( arg.endsWith( "\"" ) )
                {
                    arguments.add( arg );
                }
                else
                {
                    inString = true;
                    sb.append( arg, 1, arg.length() );
                }
            }
            else
            {
                arguments.add( arg );
            }
        }
        
        if ( sb.length() > 0 || inString )
        {
            arguments.add( sb.toString() );
        }
        args = arguments.toArray( new String[0] );
        
        for ( IMobTalkerSubCommand subCommand : _subCommands )
        {
            if ( subCommand.handle( sender, args ) )
            {
                break;
            }
        }
    }
    
    // ========================================
    
    @Override
    public String toString()
    {
        return getClass().getSimpleName();
    }
    
    @Override
    public int hashCode()
    {
        return getClass().hashCode();
    }
    
    @Override
    public boolean equals( Object obj )
    {
        return getClass().isInstance( obj );
    }
}
