/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.command;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;

import java.util.Arrays;

public abstract class AbstractMobTalkerSubCommand implements IMobTalkerSubCommand
{
    protected static MobTalkerLog LOG = MobTalkerCommand.LOG;
    
    // ========================================
    
    protected abstract boolean handleSubCommand( ICommandSender sender, String[] args )
            throws CommandException;
    
    public abstract String getSubCommandName();
    
    public String[] getSubCommandAliases()
    {
        return new String[0];
    }
    
    @Override
    public boolean handle( ICommandSender sender, String[] args )
            throws CommandException
    {
        String subCommand = get( args, 0 );
        if ( !getSubCommandName().equalsIgnoreCase( subCommand )
             && Arrays.stream( getSubCommandAliases() ).noneMatch( e -> e.equalsIgnoreCase( subCommand ) ) )
        {
            return false;
        }
        
        return handleSubCommand( sender, Arrays.copyOfRange( args, 1, args.length ) );
    }
    
    // ========================================
    
    /**
     * Helper function for optional command arguments. Will try to retrieve the argument at position {@code index} and fall back to {@code null} if the index is
     * not valid for the given arguments array.
     *
     * @param args  Array from which the element at the given index should be returned
     * @param index Index whose element should be returned
     *
     * @return The element at index {@code index}, or {@code null} if {@code index} is not valid.
     */
    protected static <T> T get( T[] args, int index )
    {
        if ( index < 0 || index >= args.length )
        {
            return null;
        }
        return args[index];
    }
    
    // ========================================
    
    @Override
    public String toString()
    {
        return getClass().getSimpleName();
    }
    
    @Override
    public int hashCode()
    {
        return getClass().hashCode();
    }
    
    @Override
    public boolean equals( Object obj )
    {
        return getClass().isInstance( obj );
    }
}
