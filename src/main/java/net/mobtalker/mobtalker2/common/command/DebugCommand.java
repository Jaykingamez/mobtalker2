/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.command;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.IChatComponent;

import static net.minecraft.util.EnumChatFormatting.GOLD;
import static net.minecraft.util.EnumChatFormatting.RESET;
import static net.mobtalker.mobtalker2.util.ChatMessageHelper.coloredText;

public final class DebugCommand extends AbstractMobTalkerSubCommand
{
    // /mobtalker debug <script>
    
    private final String[] _aliases = { "dbg", getSubCommandName() };
    
    // ========================================
    
    @Override
    public String getSubCommandName()
    {
        return "debug";
    }
    
    @Override
    public String[] getSubCommandAliases()
    {
        return _aliases;
    }
    
    @Override
    protected boolean handleSubCommand( ICommandSender sender, String[] args )
            throws CommandException
    {
        // TODO implement /mobtalker debug
        return false;
    }
    
    @Override
    public IChatComponent getCommandUsage()
    {
        IChatComponent command = coloredText( "/mobtalker " + String.join( "|", getSubCommandAliases() ) + "\n", GOLD );
    
        // TODO command help
        IChatComponent description = coloredText( "  <empty description>", RESET );
        command.appendSibling( description );
    
        return command;
    }
}
