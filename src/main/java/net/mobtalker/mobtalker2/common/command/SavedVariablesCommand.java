/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.command;

import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IChatComponent;
import net.mobtalker.mobtalker2.server.storage.IVariableStorage;
import net.mobtalker.mobtalker2.server.storage.VariableStorageRegistry;
import net.mobtalker.mobtalkerscript.v3.value.MtsValue;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static net.minecraft.util.EnumChatFormatting.*;
import static net.mobtalker.mobtalker2.util.ChatMessageHelper.*;
import static org.apache.commons.lang3.StringUtils.isBlank;

public final class SavedVariablesCommand extends AbstractMobTalkerSubCommand
{
    private final String[] _aliases = { "sv", "savedvars", getSubCommandName() };
    
    // ========================================
    
    /** Helper function for sending saved variables uniformly. */
    private static void sendSavedVariables( EntityLivingBase sender, Map<String, MtsValue> savedVariables )
    {
        if ( savedVariables == null || savedVariables.isEmpty() )
        {
            sendMessageToEntity( sender, "No saved variables.", RED );
            return;
        }
        
        List<String> names = savedVariables.keySet().stream().sorted().collect( Collectors.toList() );
        for ( String name : names )
        {
            MtsValue value = savedVariables.get( name );
            
            IChatComponent message = coloredText( name, GRAY );
            message.appendSibling( new ChatComponentText( " = " ) );
            message.appendSibling( formatMtsValue( value ) );
            message.appendText( ";" );
            sender.addChatMessage( message );
        }
    }
    
    /** Helper function for sending script identifiers uniformly. */
    private static void sendAvailableScriptIdentifiers( EntityLivingBase sender, Collection<String> identifiers )
    {
        sendAvailableScriptIdentifiers( sender, identifiers, RESET );
    }
    
    /** Helper function for sending script identifiers uniformly. */
    private static void sendAvailableScriptIdentifiers( EntityLivingBase sender, Collection<String> identifiers, EnumChatFormatting color )
    {
        sender.addChatMessage( formatAvailableScriptIdentifiers( identifiers, color ) );
    }
    
    private static IChatComponent formatAvailableScriptIdentifiers( Collection<String> identifiers, EnumChatFormatting color )
    {
        IChatComponent message = coloredText( "Available script identifiers are:", color );
        identifiers.stream().sorted().forEachOrdered( ( e ) -> {
            IChatComponent c = new ChatComponentText( "\n    " );
            c.appendText( e );
            message.appendSibling( c );
        } );
        return message;
    }
    
    // ========================================
    
    public void list( EntityLivingBase sender, String identifier )
    {
        LOG.debug( "Received 'savedvariables list' command with identifier=%s", identifier );
        
        if ( "?".equalsIgnoreCase( identifier ) || "help".equalsIgnoreCase( identifier ) )
        {
            listHelp( sender, null );
            return;
        }
        
        IVariableStorage storage = VariableStorageRegistry.instance().getOrCreateVariableStorage( sender );
        if ( isBlank( identifier ) || "all".equalsIgnoreCase( identifier ) )
        {
            // List all saved variables for all scripts
            Map<String, Map<String, MtsValue>> savedVariablesPerPlayer = storage.getAllSavedVariables();
            if ( savedVariablesPerPlayer == null || savedVariablesPerPlayer.isEmpty() )
            {
                sendSavedVariables( sender, null );
            }
            else
            {
                for ( Map.Entry<String, Map<String, MtsValue>> e : savedVariablesPerPlayer.entrySet() )
                {
                    IChatComponent message = new ChatComponentText( "Saved variables for script " );
                    message.appendSibling( coloredText( e.getKey(), UNDERLINE ) );
                    message.appendText( " and player " );
                    message.appendSibling( coloredText( sender.getCommandSenderName(), UNDERLINE ) );
                    message.appendText( ":" );
                    sender.addChatMessage( message );
                    
                    sendSavedVariables( sender, e.getValue() );
                }
            }
        }
        else
        {
            // List saved variables for script with identifier
            sendSavedVariables( sender, storage.getSavedVariables( identifier ) );
        }
    }
    
    public void listHelp( EntityLivingBase sender, String identifier )
    {
        IChatComponent message = new ChatComponentText( "" );
        
        message.appendSibling( coloredText( "/mobtalker %1$s list\n/mobtalker %1$s list all\n", GOLD, getSubCommandName() ) );
        message.appendText( "  Lists all saved variables for all scripts." );
        
        message.appendText( "\n\n" );
        
        message.appendSibling( coloredText( "/mobtalker %1$s list ?\n/mobtalker %1$s list help\n", GOLD, getSubCommandName() ) );
        message.appendText( "  Shows this help." );
        
        message.appendText( "\n\n" );
        
        message.appendSibling( coloredText( "/mobtalker %s list <identifier>\n", GOLD, getSubCommandName() ) );
        message.appendText( "  Lists all saved variables for one specific script." );
        
        IVariableStorage storage = VariableStorageRegistry.instance().getVariableStorage( sender );
        Set<String> identifiers = storage == null ? null : storage.getIdentifiers();
        if ( identifiers != null && !identifiers.isEmpty() )
        {
            message.appendText( "\n\n" );
            message.appendSibling( formatAvailableScriptIdentifiers( identifiers, RESET ) );
        }
        
        sender.addChatMessage( message );
    }
    
    // ========================================
    
    public void clear( EntityLivingBase sender, String identifier, String variables )
    {
        LOG.debug( "Received 'savedvariables clear' command with identifier=%s; variableName=%s", identifier, variables );
        
        if ( "?".equalsIgnoreCase( identifier ) || "help".equalsIgnoreCase( identifier ) || isBlank( identifier ) )
        {
            clearHelp( sender );
            return;
        }
        
        IVariableStorage storage = VariableStorageRegistry.instance().getOrCreateVariableStorage( sender );
        Map<String, Map<String, MtsValue>> allSavedVariables = storage.getAllSavedVariables();
        
        // Check if the script identifier is valid
        if ( !allSavedVariables.containsKey( identifier ) )
        {
            sendMessageToEntity( sender, "Script with identifier '%s' does not exist or hasn't saved any variables yet.", RED, identifier );
            sendAvailableScriptIdentifiers( sender, allSavedVariables.keySet(), RED );
            return;
        }
        
        if ( isBlank( variables ) || "all".equalsIgnoreCase( variables ) )
        {
            LOG.debug( "Resetting all saved variables for player '%s' and script with identifier '%s'", sender.getCommandSenderName(), identifier );
            
            // Reset all saved variables for script
            storage.setSavedVariables( identifier, new HashMap<>() );
            
            IChatComponent message = coloredText( "All saved variables for player '%s' and script with identifier '%s' were cleared.", DARK_GREEN,
                                                  sender.getCommandSenderName(), identifier );
            sender.addChatMessage( message );
            return;
        }
        
        // Reset specific saved variables for script
        String[] variableNames = variables.split( "," );
        LOG.debug( "Resetting the following variables for player '%s' and script with identifier '%s': %s", sender.getCommandSenderName(), identifier,
                   String.join( ", ", variableNames ) );
        
        List<String> successful = new ArrayList<>();
        List<String> notPresent = new ArrayList<>();
        Map<String, MtsValue> savedVariables = new HashMap<>( allSavedVariables.get( identifier ) );
        for ( String variable : variableNames )
        {
            if ( savedVariables.remove( variable ) != null )
            {
                successful.add( variable );
            }
            else
            {
                notPresent.add( variable );
            }
        }
        storage.setSavedVariables( identifier, savedVariables );
        
        if ( successful.size() > 0 )
        {
            IChatComponent message = new ChatComponentText( format( "The following variables were reset successfully for player '%s':\n",
                                                                    sender.getCommandSenderName() ) );
            for ( String variable : successful )
            {
                IChatComponent c = new ChatComponentText( "    " );
                c.appendSibling( coloredText( variable, DARK_GREEN ) );
                c.appendText( "\n" );
                message.appendSibling( c );
            }
            sender.addChatMessage( message );
        }
        
        if ( notPresent.size() > 0 )
        {
            IChatComponent message = new ChatComponentText( format( "The following variables were not present and could not be reset for player '%s':\n",
                                                                    sender.getCommandSenderName() ) );
            for ( String variable : notPresent )
            {
                IChatComponent c = new ChatComponentText( "    " );
                c.appendSibling( coloredText( variable, RED ) );
                c.appendText( "\n" );
                message.appendSibling( c );
            }
            sender.addChatMessage( message );
        }
        
        if ( successful.size() + notPresent.size() <= 0 )
        {
            sendSavedVariables( sender, null );
        }
    }
    
    public void clearHelp( EntityLivingBase sender )
    {
        IChatComponent message = new ChatComponentText( "" );
        
        message.appendSibling( coloredText( "/mobtalker %1$s clear\n/mobtalker %1$s clear ?\n/mobtalker %1$s clear help\n", GOLD, getSubCommandName() ) );
        message.appendText( "  Shows this help." );
        
        message.appendText( "\n\n" );
        
        message.appendSibling( coloredText( "/mobtalker %1$s clear <identifier>\n/mobtalker %1$s clear <identifier> all\n", GOLD, getSubCommandName() ) );
        message.appendText( "  Clears all saved variables for a script." );
        
        message.appendText( "\n\n" );
        
        message.appendSibling( coloredText( "/mobtalker %s clear <identifier> <variable>,<variable>,...\n", GOLD, getSubCommandName() ) );
        message.appendText( "  Clears only the given saved variables for a script." );
        
        IVariableStorage storage = VariableStorageRegistry.instance().getVariableStorage( sender );
        Set<String> identifiers = storage == null ? null : storage.getIdentifiers();
        if ( identifiers != null && !identifiers.isEmpty() )
        {
            message.appendText( "\n\n" );
            message.appendSibling( formatAvailableScriptIdentifiers( identifiers, RESET ) );
        }
        
        sender.addChatMessage( message );
    }
    
    // ========================================
    
    public void migrate( EntityLivingBase sender, String identifierFrom, String identifierTo )
    {
        LOG.debug( "Received 'savedvariables migrate' command with identifierFrom=%s; identifierTo=%s", identifierFrom, identifierTo );
        
        if ( "?".equalsIgnoreCase( identifierFrom ) || "help".equalsIgnoreCase( identifierFrom ) || isBlank( identifierFrom ) )
        {
            migrateHelp( sender );
            return;
        }
        
        IVariableStorage storage = VariableStorageRegistry.instance().getOrCreateVariableStorage( sender );
        Map<String, Map<String, MtsValue>> allSavedVariables = storage.getAllSavedVariables();
        
        // check if both identifiers are valid
        if ( !allSavedVariables.containsKey( identifierFrom ) )
        {
            sendMessageToEntity( sender, "Script with identifier '%s' does not exist or hasn't saved any variables yet.", RED, identifierFrom );
            sendAvailableScriptIdentifiers( sender, allSavedVariables.keySet(), RED );
            return;
        }
        if ( !allSavedVariables.containsKey( identifierTo ) )
        {
            sendMessageToEntity( sender, "Script with identifier '%s' does not exist or hasn't saved any variables yet.", RED, identifierTo );
            sendAvailableScriptIdentifiers( sender, allSavedVariables.keySet(), RED );
            return;
        }
        
        LOG.debug( "Migrating saved variables from identifier '%s' to identifier '%s' for entity '%s'", identifierFrom, identifierTo,
                   sender.getCommandSenderName() );
        
        // migrate saved variables
        Map<String, MtsValue> from = allSavedVariables.get( identifierFrom );
        Map<String, MtsValue> to = new HashMap<>( allSavedVariables.get( identifierTo ) );
        to.putAll( from );
        
        // save changes
        storage.setSavedVariables( identifierTo, to );
        
        sendMessageToEntity( sender, "Saved variables were migrated from '%s' to '%s'.", RESET, identifierFrom, identifierTo );
    }
    
    public void migrateHelp( EntityLivingBase sender )
    {
        IChatComponent message = new ChatComponentText( "" );
        
        message.appendSibling( coloredText( "/mobtalker %1$s migrate\n/mobtalker %1$s migrate ?\n/mobtalker %1$s migrate help\n", GOLD, getSubCommandName() ) );
        message.appendText( "  Shows this help." );
        
        message.appendText( "\n\n" );
        
        message.appendSibling( coloredText( "/mobtalker %1$s migrate <identifier from> <identifier to>\n", GOLD, getSubCommandName() ) );
        message.appendText( "  Migrates saved variables from one script identifier to another." );
        
        IVariableStorage storage = VariableStorageRegistry.instance().getVariableStorage( sender );
        Set<String> identifiers = storage == null ? null : storage.getIdentifiers();
        if ( identifiers != null && !identifiers.isEmpty() )
        {
            message.appendText( "\n\n" );
            message.appendSibling( formatAvailableScriptIdentifiers( identifiers, RESET ) );
        }
        
        sender.addChatMessage( message );
    }
    
    // ========================================
    
    public void help( EntityLivingBase sender, String action )
    {
        IChatComponent message = new ChatComponentText( "" );
        
        if ( StringUtils.isNotBlank( action ) )
        {
            message.appendSibling( coloredText( "Unknown action '%s'.\n", RED, action ) );
        }
        
        message.appendSibling( coloredText( "/mobtalker %1$s\n/mobtalker %1$s ?\n/mobtalker %1$s help\n", GOLD, getSubCommandName() ) );
        message.appendText( "  Shows this help." );
        
        message.appendText( "\n\n" );
        
        message.appendSibling( coloredText( "/mobtalker %s clear <identifier> [variables]\n", GOLD, getSubCommandName() ) );
        message.appendText( "  Clears saved variables for a script." );
        
        message.appendText( "\n\n" );
        
        message.appendSibling( coloredText( "/mobtalker %s list [identifier]\n", GOLD, getSubCommandName() ) );
        message.appendText( "  Lists saved variables for all scripts or one specific script." );
        
        sender.addChatMessage( message );
    }
    
    // ========================================
    
    @Override
    public String getSubCommandName()
    {
        return "savedvariables";
    }
    
    @Override
    public String[] getSubCommandAliases()
    {
        return _aliases;
    }
    
    @Override
    public boolean handleSubCommand( ICommandSender sender, String[] args )
    {
        EntityLivingBase entity;
        if ( !( sender instanceof EntityLivingBase ) )
        {
            Entity commandSenderEntity = sender.getCommandSenderEntity();
            if ( !( commandSenderEntity instanceof EntityLivingBase ) )
            {
                // Command sender must be a living entity
                return false;
            }
            else
            {
                entity = (EntityLivingBase) commandSenderEntity;
            }
        }
        else
        {
            entity = (EntityLivingBase) sender;
        }
        
        String action = get( args, 0 );
        String parameter = get( args, 1 );
        String parameter2 = get( args, 2 );
        
        LOG.debug( "Received 'savedvariables' command from %s with arguments: ", entity.getCommandSenderName(), Arrays.toString( args ) );
        
        if ( "list".equalsIgnoreCase( action ) )
        {
            list( entity, parameter );
            return true;
        }
        
        if ( "clear".equalsIgnoreCase( action ) )
        {
            clear( entity, parameter, parameter2 );
            return true;
        }
        
        if ( "migrate".equalsIgnoreCase( action ) )
        {
            migrate( entity, parameter, parameter2 );
            return true;
        }
        
        if ( "help".equalsIgnoreCase( action ) || "?".equalsIgnoreCase( action ) )
        {
            help( entity, null );
            return true;
        }
        
        help( entity, action );
        return true;
    }
    
    @Override
    public IChatComponent getCommandUsage()
    {
        IChatComponent command = coloredText( "/mobtalker %s\n", GOLD, String.join( "\n/mobtalker ", getSubCommandAliases() ) );
        IChatComponent description = coloredText( "  Manages and lists saved variables for the current player. Use ", RESET );
        description.appendSibling( coloredText( "/mobtalker %s help", GRAY, getSubCommandName() ) );
        description.appendText( " for more information." );
        command.appendSibling( description );
        
        return command;
    }
}
