/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.command;

import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IChatComponent;

import static net.minecraft.util.EnumChatFormatting.GOLD;
import static net.minecraft.util.EnumChatFormatting.RESET;
import static net.mobtalker.mobtalker2.util.ChatMessageHelper.coloredText;

public final class MainCommand implements IMobTalkerSubCommand
{
    @Override
    public boolean handle( ICommandSender sender, String[] args )
    {
        if ( args.length > 0 && !"help".equalsIgnoreCase( args[0] ) && !"?".equalsIgnoreCase( args[0] ) )
        {
            return false;
        }
        
        if ( sender.sendCommandFeedback() )
        {
            IChatComponent message = MobTalkerCommand.instance().getSubCommands().stream().map( IMobTalkerSubCommand::getCommandUsage )
                                                     .reduce( new ChatComponentText( "" ), ( a, b ) -> {
                                                         if ( a.getUnformattedText().length() > 0 )
                                                         {
                                                             a.appendText( "\n\n" );
                                                         }
                                                         a.appendSibling( b );
                                                         return a;
                                                     } );
            sender.addChatMessage( message );
        }
        
        return true;
    }
    
    @Override
    public IChatComponent getCommandUsage()
    {
        IChatComponent command = coloredText( "/mobtalker\n/mobtalker ?\n/mobtalker help\n", GOLD );
        IChatComponent description = coloredText( "  Shows this help.", RESET );
        command.appendSibling( description );
        
        return command;
    }
    
    // ========================================
    
    @Override
    public String toString()
    {
        return getClass().getSimpleName();
    }
    
    @Override
    public int hashCode()
    {
        return getClass().hashCode();
    }
    
    @Override
    public boolean equals( Object obj )
    {
        return getClass().isInstance( obj );
    }
}
