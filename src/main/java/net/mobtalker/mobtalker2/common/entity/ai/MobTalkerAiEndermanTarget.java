/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.ai;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.player.EntityPlayer;
import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;

public class MobTalkerAiEndermanTarget extends MobTalkerAiNearestAttackableTarget<EntityEnderman>
{
    private EntityPlayer _starer;
    
    private int _stareCountdown;
    private int _teleportTimer;
    
    public MobTalkerAiEndermanTarget( IInteractionAdapter adapter )
    {
        super( EntityEnderman.class, adapter );
    }
    
    @Override
    public boolean shouldExecute()
    {
        if ( !super.shouldExecute() )
            return false;
        
        _starer = (EntityPlayer) _target;
        _target = null;
        return true;
    }
    
    @Override
    public boolean continueExecuting()
    {
        if ( _starer != null )
        {
            EntityEnderman entity = _entity;
            if ( !entity.shouldAttackPlayer( _starer ) )
                return false;
            
            entity.isAggressive = true;
            entity.faceEntity( _starer, 10.0F, 10.0F );
            return true;
        }
        else
        {
            return super.continueExecuting();
        }
    }
    
    @Override
    public void startExecuting()
    {
        _stareCountdown = 5;
        _teleportTimer = 0;
    }
    
    @Override
    public void resetTask()
    {
        _starer = null;
        _entity.setScreaming( false );
        
        IAttributeInstance attribute = _entity.getEntityAttribute( SharedMonsterAttributes.movementSpeed );
        attribute.removeModifier( EntityEnderman.attackingSpeedBoostModifier );
        
        super.resetTask();
    }
    
    @Override
    public void updateTask()
    {
        if ( _starer != null )
        {
            if ( _stareCountdown-- < 0 )
            {
                _target = _starer;
                _starer = null;
                super.startExecuting();
                
                _entity.playSound( "mob.endermen.stare", 1.0F, 1.0F );
                _entity.setScreaming( true );
                
                IAttributeInstance attribute = _entity.getEntityAttribute( SharedMonsterAttributes.movementSpeed );
                attribute.applyModifier( EntityEnderman.attackingSpeedBoostModifier );
            }
        }
        else
        {
            if ( _target != null )
            {
                if ( ( _target instanceof EntityPlayer ) && _entity.shouldAttackPlayer( (EntityPlayer) _target ) )
                {
                    if ( _entity.getDistanceSqToEntity( _target ) < 16.0D )
                    {
                        _entity.teleportRandomly();
                    }
                }
                
                _teleportTimer = 0;
            }
            else if ( ( _entity.getDistanceSqToEntity( _target ) > 256.0D )
                      && ( _teleportTimer++ > 30 ) && _entity.teleportToEntity( _target ) )
            {
                _teleportTimer = 0;
            }
            
            super.updateTask();
        }
    }
}