/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity;

import java.util.UUID;

public class AttackRecord
{
    private final UUID _attacker;
    private final long _timeOfAttack;
    private final float _damageCaused;
    
    // ========================================
    
    public AttackRecord( UUID attacker, long timeOfAttack, float damageCaused )
    {
        _attacker = attacker;
        _timeOfAttack = timeOfAttack;
        _damageCaused = damageCaused;
    }
    
    // ========================================
    
    public UUID getAttacker()
    {
        return _attacker;
    }
    
    public long getTimeOfAttack()
    {
        return _timeOfAttack;
    }
    
    public float getDamageCaused()
    {
        return _damageCaused;
    }
}
