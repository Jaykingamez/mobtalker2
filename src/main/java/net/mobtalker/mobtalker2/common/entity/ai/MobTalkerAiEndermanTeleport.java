/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.ai;

import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.util.BlockPos;
import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;

/**
 * TODO
 */
public class MobTalkerAiEndermanTeleport extends MobTalkerAiBase<EntityEnderman>
{
    public MobTalkerAiEndermanTeleport( IInteractionAdapter adapter )
    {
        super( EntityEnderman.class, adapter );
    }
    
    @Override
    public boolean shouldExecute()
    {
        if ( _adapter.isActive() )
            return false;
        if ( !_entity.worldObj.isDaytime() )
            return false;
        
        float f = _entity.getBrightness( 1.0F );
        if ( ( f > 0.5F ) && _entity.worldObj.canSeeSky( new BlockPos( _entity ) )
             && ( ( _entity.getRNG().nextFloat() * 30.0F ) < ( ( f - 0.4F ) * 2.0F ) ) )
        {
            _entity.setAttackTarget( null );
            _entity.setScreaming( false );
            _entity.isAggressive = false;
            _entity.teleportRandomly();
        }
        
        return false;
    }
    
    @Override
    public boolean continueExecuting()
    {
        return false;
    }
}
