/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.ai;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIBase;
import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;

import static org.apache.commons.lang3.Validate.isTrue;
import static org.apache.commons.lang3.Validate.notNull;

public abstract class MobTalkerAiBase<T extends EntityLiving> extends EntityAIBase
{
    static final MobTalkerLog LOG = new MobTalkerLog( "AI" );
    
    // ========================================
    
    protected final IInteractionAdapter _adapter;
    protected final T _entity;
    
    // ========================================
    
    protected MobTalkerAiBase( Class<? extends T> cls, IInteractionAdapter adapter )
    {
        EntityLiving entity = adapter.getEntity();
        notNull(entity, "Interaction adapters must have an assigned entity");
        isTrue(cls.isAssignableFrom( entity.getClass() ), "Can't register AI class for %s on a %s entity", cls.getName(), entity.getClass().getName());
        
        _adapter = adapter;
        _entity = cls.cast(entity);
    }
}
