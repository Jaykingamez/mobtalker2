/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.MultimapBuilder;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.*;
import net.minecraft.entity.passive.*;
import net.minecraft.entity.player.EntityPlayerMP;
import net.mobtalker.mobtalker2.util.fp.Either;
import net.mobtalker.mobtalker2.util.fp.Option;

import java.util.HashMap;
import java.util.Optional;
import java.util.function.Predicate;

public abstract class EntityType
{
    private final EntityType _parent;
    private final String _key;
    private final String _displayName;
    
    // ========================================
    
    public EntityType( String key, String displayName, EntityType parent )
    {
        _parent = parent;
        _key = key;
        _displayName = displayName;
    }
    
    // ========================================
    
    public boolean hasParent()
    {
        return _parent != null;
    }
    
    public EntityType getParent()
    {
        return _parent;
    }
    
    public String getKey()
    {
        return _key;
    }
    
    public String getDisplayName()
    {
        return _displayName;
    }
    
    public abstract boolean matches( Entity entity );
    
    @Override
    public String toString()
    {
        return "type:" + getKey();
    }
    
    // ========================================
    
    private static final ListMultimap<Class<?>, EntityType> TYPES_BY_CLASS;
    private static final HashMap<String, EntityType> TYPES_BY_NAME;
    
    // Wildcard
    public static final EntityType All;
    
    // Monsters
    public static final EntityType Creature;
    
    public static final EntityType Blaze;
    public static final EntityType Creeper;
    public static final EntityType Enderman;
    public static final EntityType Endermite;
    public static final EntityType Ghast;
    public static final EntityType Guardian;
    public static final EntityType IronGolem;
    public static final EntityType MagmaCube;
    public static final EntityType Skeleton;
    public static final EntityType Slime;
    public static final EntityType Snowman;
    public static final EntityType Spider;
    public static final EntityType Witch;
    public static final EntityType Zombie;
    public static final EntityType ZombiePigman;
    
    // Sub-Monsters
    public static final EntityType CaveSpider;
    public static final EntityType ElderGuardian;
    public static final EntityType WitherSkeleton;
    
    // Animals
    public static final EntityType Animal;
    
    public static final EntityType Chicken;
    public static final EntityType Cow;
    public static final EntityType Horse;
    public static final EntityType Ocelot;
    public static final EntityType Pig;
    public static final EntityType Rabbit;
    public static final EntityType Sheep;
    public static final EntityType Wolf;
    
    // Sub-Animals
    public static final EntityType Mooshroom;
    
    // Others
    public static final EntityType Player;
    public static final EntityType Villager;
    
    // ========================================
    
    static
    {
        TYPES_BY_CLASS = MultimapBuilder.hashKeys( 25 ).arrayListValues( 2 ).build();
        TYPES_BY_NAME = Maps.newHashMapWithExpectedSize( 25 );
        
        // Wildcard
        All = register( EntityLivingBase.class, "*", "Any", Predicates::Any );
        
        // Monsters
        Creature = register( EntityCreature.class, "Creature" );
        
        Blaze = register( EntityBlaze.class, "Blaze", Creature );
        Creeper = register( EntityCreeper.class, "Creeper", Creature );
        Enderman = register( EntityEnderman.class, "Enderman", Creature );
        Endermite = register( EntityEndermite.class, "Endermite", Creature );
        Ghast = register( EntityGhast.class, "Ghast", Creature );
        Guardian = register( EntityGuardian.class, "Guardian", Creature );
        IronGolem = register( EntityIronGolem.class, "IronGolem", "Iron Golem", Creature );
        MagmaCube = register( EntityMagmaCube.class, "MagmaCube", "Magma Cube", Creature );
        Skeleton = register( EntitySkeleton.class, "Skeleton", Creature );
        Slime = register( EntitySlime.class, "Slime", Creature );
        Snowman = registerFirst( EntitySnowman.class, "SnowGolem", "Snow Golem", Creature );
        Spider = register( EntitySpider.class, "Spider", Creature );
        Witch = register( EntityWitch.class, "Witch", Creature );
        Zombie = register( EntityZombie.class, "Zombie", Creature );
        ZombiePigman = registerFirst( EntityPigZombie.class, "ZombiePigman", "Zombie Pigman", Creature );
        
        // Sub-Monsters
        CaveSpider = registerFirst( EntityCaveSpider.class, "CaveSpider", "Cave Spider", Spider );
        ElderGuardian = registerFirst( EntityGuardian.class, "ElderGuardian", "Elder Guardian", Guardian, Predicates::ElderGuardian );
        WitherSkeleton = registerFirst( EntitySkeleton.class, "WitherSkeleton", "Wither Skeleton", Creature, Predicates::WitherSkeleton );
        
        // Animals
        Animal = register( EntityAnimal.class, "Animal" );
        
        Chicken = register( EntityChicken.class, "Chicken", Animal );
        Cow = register( EntityCow.class, "Cow", Animal );
        Horse = register( EntityHorse.class, "Horse", Animal );
        Ocelot = register( EntityOcelot.class, "Ocelot", Animal );
        Pig = register( EntityPig.class, "Pig", Animal );
        Rabbit = register( EntityRabbit.class, "Rabbit", Animal );
        Sheep = register( EntitySheep.class, "Sheep", Animal );
        Wolf = register( EntityWolf.class, "Wolf", Animal );
        
        // Sub-Animals
        Mooshroom = registerFirst( EntityMooshroom.class, "Mooshroom", Cow );
        
        // Others
        Player = register( EntityPlayerMP.class, "Player" );
        Villager = register( EntityVillager.class, "Villager" );
    }
    
    // ========================================
    
    public static EntityType register( Class<? extends Entity> entityClass, EntityType entityType )
    {
        TYPES_BY_CLASS.put( entityClass, entityType );
        TYPES_BY_NAME.put( entityType.getKey(), entityType );
        return entityType;
    }
    
    public static EntityType registerFirst( Class<? extends Entity> entityClass, EntityType entityType )
    {
        TYPES_BY_CLASS.get( entityClass ).add( 0, entityType );
        TYPES_BY_NAME.put( entityType.getKey(), entityType );
        return entityType;
    }
    
//    public static EntityType registerBefore( Class<? extends Entity> entityClass, EntityType entityType, EntityType other )
//    {
//        List<EntityType> classList = TYPES_BY_CLASS.get( entityClass );
//        int i = Math.max( classList.indexOf( other ), 0 );
//
//        classList.add( i, entityType );
//        TYPES_BY_NAME.put( entityType.getKey(), entityType );
//        return entityType;
//    }
    
    // ========================================
    
    public static EntityType register( Class<? extends Entity> entityClass )
    {
        return register( entityClass, getEntityKey( entityClass ) );
    }
    
    public static EntityType register( Class<? extends Entity> entityClass, String name )
    {
        return register( entityClass, name, name, All );
    }
    
    public static EntityType register( Class<? extends Entity> entityClass, String name, String displayName )
    {
        return register( entityClass, name, displayName, All );
    }
    
    public static EntityType register( Class<? extends Entity> entityClass, String name, EntityType parent )
    {
        return register( entityClass, name, name, parent );
    }
    
    public static EntityType register( Class<? extends Entity> entityClass, String name, String displayName, EntityType parent )
    {
        SimpleEntityType type = new SimpleEntityType( entityClass, name, displayName, parent );
        register( entityClass, type );
        return type;
    }
    
    public static EntityType register( Class<? extends Entity> entityClass, String name, Predicate<Entity> matcher )
    {
        return register( entityClass, name, name, All, matcher );
    }
    
    public static EntityType register( Class<? extends Entity> entityClass, String name, String displayName, Predicate<Entity> matcher )
    {
        ComplexEntityType type = new ComplexEntityType( name, displayName, All, matcher );
        register( entityClass, type );
        return type;
    }
    
    public static EntityType register( Class<? extends Entity> entityClass, String name, String displayName, EntityType parent,
                                       Predicate<Entity> matcher )
    {
        ComplexEntityType type = new ComplexEntityType( name, displayName, parent, matcher );
        register( entityClass, type );
        return type;
    }
    
    // ========================================
    
    public static EntityType registerFirst( Class<? extends Entity> entityClass )
    {
        return registerFirst( entityClass, getEntityKey( entityClass ) );
    }
    
    public static EntityType registerFirst( Class<? extends Entity> entityClass, String name )
    {
        return registerFirst( entityClass, name, name );
    }
    
    public static EntityType registerFirst( Class<? extends Entity> entityClass, String name, String displayName )
    {
        return registerFirst( entityClass, name, displayName, All );
    }
    
    public static EntityType registerFirst( Class<? extends Entity> entityClass, String name, EntityType parent )
    {
        return registerFirst( entityClass, name, name, parent );
    }
    
    public static EntityType registerFirst( Class<? extends Entity> entityClass, String name, String displayName, EntityType parent )
    {
        SimpleEntityType type = new SimpleEntityType( entityClass, name, displayName, parent );
        registerFirst( entityClass, type );
        return type;
    }
    
    public static EntityType registerFirst( Class<? extends Entity> entityClass, String name, Predicate<Entity> matcher )
    {
        return registerFirst( entityClass, name, name, matcher );
    }
    
    public static EntityType registerFirst( Class<? extends Entity> entityClass, String name, String displayName, Predicate<Entity> matcher )
    {
        return registerFirst( entityClass, name, displayName, All, matcher );
    }
    
    public static EntityType registerFirst( Class<? extends Entity> entityClass, String name, String displayName, EntityType parent,
                                            Predicate<Entity> matcher )
    {
        ComplexEntityType type = new ComplexEntityType( name, displayName, parent, matcher );
        registerFirst( entityClass, type );
        return type;
    }
    
    // ========================================
    
    public static EntityType of( EntityLivingBase entity )
    {
        Class<?> c = entity.getClass();
        do
        {
            if ( TYPES_BY_CLASS.containsKey( c ) )
            {
                for ( EntityType type : TYPES_BY_CLASS.get( c ) )
                    if ( type.matches( entity ) )
                        return type;
            }
            else
            {
                c = c.getSuperclass();
            }
        }
        while ( !Entity.class.equals( c ) );
        
        return register( entity.getClass() );
    }
    
    public static Either<String, EntityType> tryForName( String name )
    {
        EntityType type = TYPES_BY_NAME.get( name );
        if ( type != null )
        {
            return Either.right( type );
        }
        
        Option<Class<? extends Entity>> optEntityClass = getEntityClass( name );
        if ( !optEntityClass.isPresent() )
        {
            return Either.left( String.format( "unknown entity type name '%s'", name ) );
        }
        
        Class<? extends Entity> entityClass = optEntityClass.get();
        Option<EntityType> optParent = getParentByClass( entityClass );
        if ( !optParent.isPresent() )
        {
            return Either.left( String.format( "invalid entity class '%s' for entity type name '%s'", entityClass.getName(), name ) );
        }
        
        EntityType parent = optParent.get();
        return Either.right( register( entityClass, name, parent ) );
    }
    
    public static EntityType forName( String name )
    {
        Either<String, EntityType> result = tryForName( name );
        if ( result.isLeft() )
        {
            throw new IllegalArgumentException( result.left().get() );
        }
        return result.right().get();
    }
    
    private static Option<EntityType> getParentByClass( Class<? extends Entity> cls )
    {
        if ( EntityAnimal.class.isAssignableFrom( cls ) )
        {
            return Option.of( EntityType.Animal );
        }
        if ( EntityCreature.class.isAssignableFrom( cls ) )
        {
            return Option.of( EntityType.Creature );
        }
        if ( EntityLivingBase.class.isAssignableFrom( cls ) )
        {
            return Option.of( EntityType.All );
        }
        
        return Option.empty();
    }
    
    @SuppressWarnings( "unchecked" )
    private static Option<Class<? extends Entity>> getEntityClass( String name )
    {
        Class<? extends Entity> entityClass = (Class<? extends Entity>) EntityList.stringToClassMapping.get( name );
        if (entityClass == null) return Option.empty();
        return Option.of( entityClass );
    }
    
    private static String getEntityKey( Class<? extends Entity> entityClass )
    {
        return (String) EntityList.classToStringMapping.get( entityClass );
    }
    
    // ========================================
    
    public static EntityType[] values()
    {
        return TYPES_BY_NAME.values().toArray( new EntityType[0] );
    }
    
    // ========================================
    
    private static final class Predicates
    {
        public static boolean Any( Entity e )
        {
            return true;
        }
        
        public static boolean ElderGuardian( Entity e )
        {
            return ( e instanceof EntityGuardian ) && ( ( (EntityGuardian) e ).isElder() );
        }
        
        public static boolean WitherSkeleton( Entity e )
        {
            return ( e instanceof EntitySkeleton ) && ( ( (EntitySkeleton) e ).getSkeletonType() == 1 );
        }
    }
}
