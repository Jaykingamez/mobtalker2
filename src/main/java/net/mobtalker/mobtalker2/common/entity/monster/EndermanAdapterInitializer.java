/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.monster;

import static net.mobtalker.mobtalker2.common.entity.ai.EntityAiTaskUtil.*;
import net.minecraft.entity.ai.*;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.EnderTeleportEvent;
import net.minecraftforge.fml.common.eventhandler.*;
import net.mobtalker.mobtalker2.common.entity.EntityType;
import net.mobtalker.mobtalker2.common.entity.ai.*;
import net.mobtalker.mobtalker2.server.interaction.*;
import net.mobtalker.mobtalker2.server.script.lib.EntityReaction;

public class EndermanAdapterInitializer implements IInteractionAdapterInitializer<EntityEnderman>
{
    static
    {
        MinecraftForge.EVENT_BUS.register( new TeleportHandler() );
    }
    
    // ========================================
    
    @Override
    public void initialize( IInteractionAdapter adapter, EntityEnderman entity,
                            EntityAITasks tasks, EntityAITasks targetTasks )
    {
        // Tasks
        addLast( tasks, 0,
                 new MobTalkerAiInteractPlayer( adapter ) );
        addLast( tasks, 1,
                 new MobTalkerAiAvoidEntity( adapter, 1.5D, 1.2D, 0.8D, 4.0D,
                                             PredicateExplodingCreeper.Instance ) );
        addLast( tasks, 1,
                 new MobTalkerAiAvoidEntity( adapter, 1.5D, 1.2D, 0.8D, 6.0D,
                                             new PredicateReactionScared( adapter ) ) );
        
        // Target Tasks
        replaceIndex( targetTasks, 0,
                      EntityAIHurtByTarget.class,
                      new MobTalkerAiHurtByTarget( adapter, false ) );
        replaceIndex( targetTasks, 1,
                      EntityAINearestAttackableTarget.class,
                      new MobTalkerAiEndermanTarget( adapter ) );
        
        // Reactions
        adapter.setReaction( EntityReaction.HOSTILE, EntityType.Player );
        adapter.setReaction( EntityReaction.HOSTILE, EntityType.Endermite );
    }
    
    // ========================================
    
    public static class TeleportHandler
    {
        @SubscribeEvent( priority = EventPriority.LOWEST )
        public void onEnderTeleportEvent( EnderTeleportEvent event )
        {
            if ( !( event.entityLiving instanceof EntityEnderman ) )
                return;
            
            if ( InteractionRegistry.instance().isInteracting( (EntityEnderman) event.entityLiving ) )
            {
                event.setCanceled( true );
            }
        }
    }
}
