/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity;

import net.minecraft.entity.Entity;

public interface EntityMatcher
{
    boolean matches( Entity entity );
    
    // ========================================
    
    public static class Type implements EntityMatcher
    {
        private final EntityType _type;
        
        public Type( EntityType type )
        {
            _type = type;
        }
        
        @Override
        public boolean matches( Entity entity )
        {
            return _type.matches( entity );
        }
    }
    
    // ========================================
    
}
