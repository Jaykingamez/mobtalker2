/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.ai;

import java.util.*;

import net.minecraft.entity.*;
import net.minecraft.entity.player.EntityPlayer;
import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;

import com.google.common.base.Predicate;

public class MobTalkerAiNearestAttackableTarget<T extends EntityLiving> extends MobTalkerAiTargetBase<T>
{
    private final Predicate<EntityLivingBase> _filter;
    private final EntityDistanceSorter _sorter;
    
    /**
     * Higher values reduce the chance of searching for a target
     */
    private final int _targetChance;
    
    /**
     * The target that was chosen.
     */
    protected EntityLivingBase _target;
    
    // ========================================
    
    public MobTalkerAiNearestAttackableTarget( Class<? extends T> cls, IInteractionAdapter adapter )
    {
        super( cls, adapter, true, false, false );
        setMutexBits( 0b0001 );
        
        _filter = new SneakingOrInvisibleFilter( adapter );
        _sorter = new EntityDistanceSorter( _entity );
        _targetChance = 0;
    }
    
    // ========================================
    
    @SuppressWarnings( "unchecked" )
    private List<EntityLivingBase> getSurroundingEntities()
    {
        double max = _adapter.getFollowRange();
        return _entity.worldObj.getEntitiesWithinAABB( EntityLivingBase.class,
                                                       _entity.getEntityBoundingBox().expand( max, 4.0D, max ),
                                                       _filter );
    }
    
    private EntityLivingBase getNearestEntity()
    {
        List<EntityLivingBase> targets = getSurroundingEntities();
        if ( targets.isEmpty() )
            return null;
        
        Collections.sort( targets, _sorter );
        
        for ( EntityLivingBase target : targets )
        {
            if ( isValidTarget( target ) )
                return target;
        }
        
        return null;
    }
    
    // ========================================
    
    @Override
    public boolean shouldExecute()
    {
        if ( ( _targetChance > 0 ) && ( _entity.getRNG().nextInt( _targetChance ) > 0 ) )
            return false;
        
        EntityLivingBase entity = getNearestEntity();
        
        if ( entity == null )
            return false;
        
        _target = entity;
        return true;
    }
    
    @Override
    public void startExecuting()
    {
        super.startExecuting();
        _entity.setAttackTarget( _target );
    }
    
    // ========================================
    
    public static class SneakingOrInvisibleFilter implements Predicate<EntityLivingBase>
    {
        private final IInteractionAdapter _adapter;
        
        public SneakingOrInvisibleFilter( IInteractionAdapter adapter )
        {
            _adapter = adapter;
        }
        
        @Override
        public boolean apply( EntityLivingBase entity )
        {
            if ( entity == null )
                return false;
            
            if ( entity instanceof EntityPlayer )
            {
                EntityPlayer player = (EntityPlayer) entity;
                if ( player.isSpectator() )
                    return false;
                
                double range = _adapter.getFollowRange();
                if ( player.isSneaking() )
                    range *= 0.8D;
                if ( player.isInvisible() )
                    range *= 0.7F * Math.max( player.getArmorVisibility(), 0.1F );
                
                return _adapter.getEntity().getDistanceToEntity( player ) <= range;
            }
            
            return true;
        }
    }
}
