/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.ai;

import net.minecraft.entity.EntityLiving;
import net.mobtalker.mobtalker2.server.interaction.*;

public class MobTalkerAiInteractPlayer extends MobTalkerAiBase<EntityLiving>
{
    public MobTalkerAiInteractPlayer( IInteractionAdapter adapter )
    {
        super( EntityLiving.class, adapter );
        setMutexBits( 3 );
    }
    
    // ========================================
    
    @Override
    public boolean isInterruptible()
    {
        return false;
    }
    
    // ========================================
    
    @Override
    public boolean shouldExecute()
    {
        return _adapter.isActive()
               && _adapter.canInteractWith( _adapter.getPartner() );
    }
    
    @Override
    public void startExecuting()
    {
        _entity.getNavigator().clearPathEntity();
        if (_entity.isInWater())
        {
            _entity.getJumpHelper().setJumping();
        }
    }
    
    @Override
    public void resetTask()
    {
        // An adapter is only active here if this task's continue check fails.
        if ( _adapter.isActive() )
        {
            LOG.debug( "Cancelling still active adapter" );
            InteractionRegistry.instance().cancelInteraction( _adapter );
        }
        else
        {
            LOG.trace( "Adapter is already inactive" );
        }
    }
    
    @Override
    public void updateTask()
    {
        _entity.getLookHelper()
               .setLookPositionWithEntity( _adapter.getPartner(),
                                           10.0F, _entity.getVerticalFaceSpeed() );
    }
}