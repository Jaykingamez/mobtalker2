/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity;

import java.util.*;
import java.util.Map.Entry;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.nbt.*;
import net.minecraftforge.common.util.Constants.NBT;
import net.mobtalker.mobtalker2.server.script.lib.EntityReaction;

import com.google.common.collect.Maps;

public class ReactionList
{
    private final HashMap<UUID, EntityReaction> _entities;
    private final HashMap<EntityType, EntityReaction> _types;
    
    // ========================================
    
    public ReactionList()
    {
        _entities = Maps.newHashMapWithExpectedSize( 1 );
        _types = Maps.newHashMapWithExpectedSize( 4 );
    }
    
    // ========================================
    
    public EntityReaction getDefault()
    {
        return EntityReaction.NEUTRAL;
    }
    
    public EntityReaction get( EntityLivingBase entity )
    {
        EntityReaction reaction;
        
        reaction = _entities.get( entity.getPersistentID() );
        if ( reaction != null )
            return reaction;
        
        return get( EntityType.of( entity ) );
    }
    
    public EntityReaction get( EntityType type )
    {
        EntityReaction reaction = _types.get( type );
        if ( reaction != null )
            return reaction;
        
        return getDefault();
    }
    
    public void set( EntityReaction reaction, EntityLivingBase entity )
    {
        _entities.put( entity.getPersistentID(), reaction );
    }
    
    public void set( EntityReaction reaction, EntityType type )
    {
        _types.put( type, reaction );
    }
    
    public void reset( EntityLivingBase entity )
    {
        _entities.remove( entity.getPersistentID() );
    }
    
    public void reset( EntityType type )
    {
        _types.remove( type );
    }
    
    // ========================================
    
    public NBTTagCompound serialize()
    {
        NBTTagCompound store = new NBTTagCompound();
        
        { // Individual entities
            NBTTagList list = new NBTTagList();
            for ( Entry<UUID, EntityReaction> entry : _entities.entrySet() )
            {
                NBTTagCompound entryStore = new NBTTagCompound();
                entryStore.setString( "Entity", entry.getKey().toString() );
                entryStore.setString( "Reaction", entry.getValue().toString() );
                list.appendTag( entryStore );
            }
            store.setTag( "Entities", list );
        }
        { // Entity types
            NBTTagList list = new NBTTagList();
            for ( Entry<EntityType, EntityReaction> entry : _types.entrySet() )
            {
                NBTTagCompound entryStore = new NBTTagCompound();
                entryStore.setString( "Type", entry.getKey().getKey() );
                entryStore.setString( "Reaction", entry.getValue().toString() );
                list.appendTag( entryStore );
            }
            store.setTag( "Types", list );
        }
        
        return store;
    }
    
    public void deserialize( NBTTagCompound store )
    {
        if ( store.hasKey( "Entities", NBT.TAG_LIST ) )
        {
            NBTTagList list = store.getTagList( "Entities", NBT.TAG_COMPOUND );
            for ( int i = 0; i < list.tagCount(); i++ )
            {
                NBTTagCompound entryStore = list.getCompoundTagAt( i );
                _entities.put( UUID.fromString( entryStore.getString( "Entity" ) ),
                               EntityReaction.valueOf( entryStore.getString( "Reaction" ) ) );
            }
        }
        
        if ( store.hasKey( "Types", NBT.TAG_LIST ) )
        {
            NBTTagList list = store.getTagList( "Types", NBT.TAG_COMPOUND );
            for ( int i = 0; i < list.tagCount(); i++ )
            {
                NBTTagCompound entryStore = list.getCompoundTagAt( i );
                _types.put( EntityType.forName( entryStore.getString( "Type" ) ),
                            EntityReaction.valueOf( entryStore.getString( "Reaction" ) ) );
            }
        }
    }
}
