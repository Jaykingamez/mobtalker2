/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity;

import net.minecraft.entity.*;

public interface EntityAttribute
{
    boolean has( Entity entity );
    
    // ========================================
    
    public static final EntityAttribute IsChild = new EntityAttribute()
    {
        @Override
        public boolean has( Entity entity )
        {
            return ( entity instanceof EntityLivingBase ) && ( (EntityLivingBase) entity ).isChild();
        }
    };
    
    // ========================================
}
