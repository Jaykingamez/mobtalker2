/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity;

import java.util.function.Predicate;

import net.minecraft.entity.Entity;

public class ComplexEntityType extends EntityType
{
    private final Predicate<Entity> _matcher;
    
    // ========================================
    
    public ComplexEntityType( String key, String displayName, EntityType parent, Predicate<Entity> matcher )
    {
        super( key, displayName, parent );
        _matcher = matcher;
    }
    
    // ========================================
    
    @Override
    public boolean matches( Entity entity )
    {
        return _matcher.test( entity );
    }
}
