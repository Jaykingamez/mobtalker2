/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.ai;

import net.minecraft.entity.*;
import net.minecraft.entity.player.EntityPlayer;
import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;

import org.apache.commons.lang3.StringUtils;

public abstract class MobTalkerAiTargetBase<T extends EntityLiving> extends MobTalkerAiBase<T>
{
    /**
     * If targets out of sight are valid.
     */
    protected final boolean _checkSight;
    /**
     * If only targets that can be reached should be considered.
     */
    protected final boolean _checkPath;
    /**
     * If invulnerable entities (players) should be attacked.
     */
    protected final boolean _checkInvulnerable;
    
    /**
     * Delay before an out of sight target is no longer valid
     */
    private int _outOfSightDelay;
    
    // ========================================
    
    public MobTalkerAiTargetBase( Class<? extends T> cls,
                                  IInteractionAdapter adapter,
                                  boolean checkSight,
                                  boolean checkPath,
                                  boolean checkInvulnerable )
    {
        super( cls, adapter );
        _checkSight = checkSight;
        _checkPath = checkPath;
        _checkInvulnerable = checkInvulnerable;
    }
    
    // ========================================
    
    protected double getFollowRangeSq()
    {
        double d = _adapter.getFollowRange();
        return d * d;
    }
    
    // ========================================
    
    private static boolean isInvulnerable( EntityLivingBase target )
    {
        return ( target instanceof EntityPlayer ) && ( (EntityPlayer) target ).capabilities.disableDamage;
    }
    
    private boolean checkOwner( EntityLivingBase target )
    {
        if ( !( _entity instanceof IEntityOwnable ) )
            return true;
        
        IEntityOwnable entityOwnable = (IEntityOwnable) _entity;
        
        // Has an owner?
        if ( StringUtils.isEmpty( entityOwnable.getOwnerId() ) )
            return true;
        
        // Is target our owner?
        if ( target == entityOwnable.getOwner() )
            return false;
        
        // If the target is owned by our owner
        if ( ( target instanceof IEntityOwnable ) )
        {
            String targetOwner = ( (IEntityOwnable) target ).getOwnerId();
            return targetOwner.equals( entityOwnable.getOwnerId() );
        }
        
        return true;
    }
    
    private boolean isWithinHomeDistance( EntityLivingBase target )
    {
        return ( _entity instanceof EntityCreature )
               && ( (EntityCreature) _entity ).isWithinHomeDistanceFromPosition( target.getPosition() );
    }
    
    private boolean canEasilyReach( EntityLivingBase target )
    {
        // TODO Implement checks for path (_checkPath)
        return true;
    }
    
    private boolean canSee( EntityLivingBase target )
    {
        return _entity.getEntitySenses().canSee( target );
    }
    
    protected boolean isValidTarget( EntityLivingBase target )
    {
        if ( ( target == null ) || target.equals( _entity ) || !target.isEntityAlive() )
            return false;
        if ( !_entity.canAttackClass( target.getClass() ) )
            return false;
        if ( !_checkInvulnerable && isInvulnerable( target ) )
            return false;
        if ( !checkOwner( target ) )
            return false;
        if ( !isWithinHomeDistance( target ) )
            return false;
        if ( _checkPath && !canEasilyReach( target ) )
            return false;
        if ( _checkSight && !canSee( target ) )
            return false;
        
        return _adapter.shouldAttack( target );
    }
    
    // ========================================
    
    private boolean canStillSee( EntityLivingBase target )
    {
        if ( canSee( target ) )
        {
            _outOfSightDelay = 0;
            return true;
        }
        else if ( _outOfSightDelay < 60 )
        {
            _outOfSightDelay++;
            return true;
        }
        
        return false;
    }
    
    protected boolean isStillValidTarget( EntityLivingBase target )
    {
        if ( ( target == null ) || !target.isEntityAlive() )
        {
            LOG.info( "target is null or dead" );
            return false;
        }
        if ( !_checkInvulnerable && isInvulnerable( target ) )
        {
            LOG.info( "target is invulnerable" );
            return false;
        }
        if ( _entity.getDistanceSqToEntity( target ) > getFollowRangeSq() )
        {
            LOG.info( "target is out of follow range" );
            return false;
        }
        if ( _checkSight && !canStillSee( target ) )
        {
            LOG.info( "target is out of sight" );
            return false;
        }
        
        return _adapter.shouldAttack( target );
    }
    
    // ========================================
    
    @Override
    public abstract boolean shouldExecute();
    
    @Override
    public boolean continueExecuting()
    {
        return isStillValidTarget( _entity.getAttackTarget() );
    }
    
    @Override
    public void startExecuting()
    {
        LOG.debug( this.getClass().getSimpleName() + " Start" );
        _outOfSightDelay = 0;
    }
    
    @Override
    public void resetTask()
    {
        LOG.debug( this.getClass().getSimpleName() + " Reset" );
        _entity.setAttackTarget( null );
    }
    
}