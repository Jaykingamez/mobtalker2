/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.ai;

import java.util.*;

import net.minecraft.entity.ai.*;
import net.minecraft.entity.ai.EntityAITasks.EntityAITaskEntry;

public class EntityAiTaskUtil
{
    private static EntityAITaskEntry createEntry( EntityAITasks tasks, int priority, EntityAIBase task )
    {
        return tasks.new EntityAITaskEntry( priority, task ); // This syntax hurts my eyes
    }
    
    // ========================================
    
    /**
     * Shifts all priorities that are equal or higher than <code>from</code> by <code>by</code>.
     */
    public static void shiftPriorities( EntityAITasks tasks, int from, int by )
    {
        @SuppressWarnings( "unchecked" )
        List<EntityAITaskEntry> taskEntries = tasks.taskEntries;
        for ( int i = 0; i < taskEntries.size(); i++ )
        {
            EntityAITaskEntry entry = taskEntries.get( i );
            if ( entry.priority >= from )
            {
                entry.priority += by;
            }
        }
    }
    
    // ========================================
    
    /**
     * Inserts a task with the specified priority and shifts priorities of other tasks up if the specified priority is already
     * occupied.
     */
    public static void insert( EntityAITasks tasks, int priority, EntityAIBase task )
    {
        @SuppressWarnings( "unchecked" )
        List<EntityAITaskEntry> taskEntries = tasks.taskEntries;
        
        int i = 0;
        boolean needsShifting = false;
        for ( ; i < taskEntries.size(); i++ )
        {
            EntityAITaskEntry entry = taskEntries.get( i );
            
            if ( priority <= entry.priority )
            {
                taskEntries.add( Math.max( i, 0 ), createEntry( tasks, priority, task ) );
                needsShifting = ( priority == entry.priority );
                
                break;
            }
        }
        
        i++;
        if ( needsShifting )
        {
            for ( ; i < taskEntries.size(); i++ )
            {
                EntityAITaskEntry entry = taskEntries.get( i );
                entry.priority++;
            }
        }
    }
    
    /**
     * Inserts a new task at the index one lower than the first occurrence of the specified task type.
     */
    public static void insertBefore( EntityAITasks tasks, Class<? extends EntityAIBase> taskType, EntityAIBase task )
    {
        @SuppressWarnings( "unchecked" )
        List<EntityAITaskEntry> taskEntries = tasks.taskEntries;
        for ( int i = 0; i < taskEntries.size(); i++ )
        {
            EntityAITaskEntry entry = taskEntries.get( i );
            
            if ( entry.action.getClass().equals( taskType ) )
            {
                insert( tasks, entry.priority, task );
                return;
            }
        }
        
        throw new IllegalArgumentException( "task type '" + taskType.getSimpleName() + "' not found" );
    }
    
    /**
     * Inserts a new task at the index one lower than the first occurrence of the specified task type.
     */
    public static void insertAfter( EntityAITasks tasks, Class<? extends EntityAIBase> taskType, EntityAIBase task )
    {
        @SuppressWarnings( "unchecked" )
        List<EntityAITaskEntry> taskEntries = tasks.taskEntries;
        for ( int i = 0; i < taskEntries.size(); i++ )
        {
            EntityAITaskEntry entry = taskEntries.get( i );
            
            if ( entry.action.getClass().equals( taskType ) )
            {
                insert( tasks, entry.priority + 1, task );
                return;
            }
        }
        
        throw new IllegalArgumentException( "task type '" + taskType.getSimpleName() + "' not found" );
    }
    
    // ========================================
    
    public static void addFirst( EntityAITasks tasks, int priority, EntityAIBase task )
    {
        @SuppressWarnings( "unchecked" )
        List<EntityAITaskEntry> taskEntries = tasks.taskEntries;
        
        for ( int i = 0; i < taskEntries.size(); i++ )
        {
            EntityAITaskEntry entry = taskEntries.get( i );
            if ( priority <= entry.priority )
            {
                taskEntries.add( i, createEntry( tasks, priority, task ) );
                break;
            }
        }
    }
    
    public static void addLast( EntityAITasks tasks, int priority, EntityAIBase task )
    {
        @SuppressWarnings( "unchecked" )
        List<EntityAITaskEntry> taskEntries = tasks.taskEntries;
        
        for ( int i = 0; i < taskEntries.size(); i++ )
        {
            EntityAITaskEntry entry = taskEntries.get( i );
            if ( priority < entry.priority )
            {
                taskEntries.add( Math.max( i - 1, 0 ), createEntry( tasks, priority, task ) );
                break;
            }
        }
    }
    
    // ========================================
    
    public static void replaceIndex( EntityAITasks tasks, int i,
                                     Class<? extends EntityAIBase> taskType, EntityAIBase replacement )
    {
        @SuppressWarnings( "unchecked" )
        List<EntityAITaskEntry> entries = tasks.taskEntries;
        EntityAITaskEntry entry = entries.get( i );
        
        if ( !taskType.isAssignableFrom( entry.action.getClass() ) )
            throw new IllegalArgumentException( "Expected task at index " + i + " to be of type " + taskType.getName()
                                                + ", but was of " + entry.action.getClass().getName() );
        
        entry.action = replacement;
    }
    
    /**
     * Replaces the first task in the tasks list that matches the given class and replaces it with the given replacement task.
     * <p>
     * The replacement gets the exact same priority and position in the task list.
     * <p>
     * Do not use while the replaced task is running.
     */
    public static void replaceFirst( EntityAITasks tasks, Class<? extends EntityAIBase> taskType, EntityAIBase replacement )
    {
        @SuppressWarnings( "unchecked" )
        List<EntityAITaskEntry> taskEntries = tasks.taskEntries;
        for ( int i = 0; i < taskEntries.size(); i++ )
        {
            EntityAITaskEntry entry = taskEntries.get( i );
            
            if ( entry.action.getClass().equals( taskType ) )
            {
                entry.action = replacement;
                return;
            }
        }
        
        throw new IllegalArgumentException( "task type '" + taskType.getSimpleName() + "' not found" );
    }
    
    /**
     * Replaces the first task in the tasks list that matches the given class and replaces it with the given replacement task.
     * Further matches are removed.
     * <p>
     * The replacement gets the exact same priority and position in the task list.
     * <p>
     * Do not use while the replaced task is running.
     */
    public static void replaceAll( EntityAITasks tasks, Class<? extends EntityAIBase> taskType, EntityAIBase replacement )
    {
        @SuppressWarnings( "unchecked" )
        List<EntityAITaskEntry> taskEntries = tasks.taskEntries;
        boolean replaced = false;
        int i = 0;
        for ( ; i < taskEntries.size(); i++ )
        {
            EntityAITaskEntry entry = taskEntries.get( i );
            
            if ( entry.action.getClass().equals( taskType ) )
            {
                entry.action = replacement;
                replaced = true;
                break;
            }
        }
        
        if ( !replaced )
            throw new IllegalArgumentException( "task type '" + taskType.getSimpleName() + "' not found" );
        
        for ( ; i < taskEntries.size(); i++ )
        {
            EntityAITaskEntry entry = taskEntries.get( i );
            
            if ( entry.action.getClass().equals( taskType ) )
            {
                taskEntries.remove( i-- );
            }
        }
    }
    
    // ========================================
    
    public static void removeIndex( EntityAITasks tasks, int i, Class<? extends EntityAIBase> taskType )
    {
        @SuppressWarnings( "unchecked" )
        List<EntityAITaskEntry> entries = tasks.taskEntries;
        EntityAITaskEntry entry = entries.get( i );
        
        if ( !taskType.isAssignableFrom( entry.action.getClass() ) )
            throw new IllegalArgumentException( "Expected task at index " + i + " to be of type " + taskType.getName()
                                                + ", but was of " + entry.action.getClass().getName() );
        
        entries.remove( i );
    }
    
    public static void removeFirst( EntityAITasks tasks, Class<? extends EntityAIBase> taskType )
    {
        @SuppressWarnings( "unchecked" )
        List<EntityAITaskEntry> taskEntries = tasks.taskEntries;
        for ( int i = 0; i < taskEntries.size(); i++ )
        {
            EntityAITaskEntry entry = taskEntries.get( i );
            
            if ( entry.action.getClass().equals( taskType ) )
            {
                taskEntries.remove( i );
                return;
            }
        }
        
        throw new IllegalArgumentException( "task type '" + taskType.getSimpleName() + "' not found" );
    }
    
    public static void removeAll( EntityAITasks tasks, Class<? extends EntityAIBase> taskType )
    {
        @SuppressWarnings( "unchecked" )
        List<EntityAITaskEntry> taskEntries = tasks.taskEntries;
        for ( int i = 0; i < taskEntries.size(); i++ )
        {
            EntityAITaskEntry entry = taskEntries.get( i );
            
            if ( entry.action.getClass().equals( taskType ) )
            {
                taskEntries.remove( i-- );
            }
        }
    }
    
    // ========================================
    
    @SuppressWarnings( "unchecked" )
    public static void sortTasks( EntityAITasks tasks )
    {
        Collections.sort( tasks.taskEntries, TaskEntryComparator.INSTANCE );
    }
    
    private static final class TaskEntryComparator implements Comparator<EntityAITaskEntry>
    {
        public static final TaskEntryComparator INSTANCE = new TaskEntryComparator();
        
        @Override
        public int compare( EntityAITaskEntry a, EntityAITaskEntry b )
        {
            return b.priority - a.priority;
        }
    }
    
    // ========================================
    
    public static void printTasks( EntityAITasks tasks )
    {
        @SuppressWarnings( "unchecked" )
        List<EntityAITaskEntry> taskEntries = tasks.taskEntries;
        
        for ( EntityAITaskEntry entry : taskEntries )
        {
            System.out.println( "Task: " + entry.priority + ", " + entry.action.getClass().getSimpleName() );
        }
    }
}
