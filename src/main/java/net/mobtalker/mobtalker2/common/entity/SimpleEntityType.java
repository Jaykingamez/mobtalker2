/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity;

import net.minecraft.entity.Entity;

class SimpleEntityType extends EntityType
{
    private final Class<? extends Entity> _entityClass;
    
    // ========================================
    
    public SimpleEntityType( Class<? extends Entity> entityClass, String name, String displayName, EntityType parent )
    {
        super( name, displayName, parent );
        _entityClass = entityClass;
    }
    
    // ========================================
    
    public Class<? extends Entity> getEntityClass()
    {
        return _entityClass;
    }
    
    @Override
    public boolean matches( Entity entity )
    {
        return _entityClass.equals( entity.getClass() );
    }
}
