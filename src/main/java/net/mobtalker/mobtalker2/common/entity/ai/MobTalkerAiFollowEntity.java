/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.ai;

import net.minecraft.entity.*;
import net.minecraft.entity.ai.EntityAIFollowOwner;
import net.minecraft.util.*;
import net.minecraft.world.World;
import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;

public class MobTalkerAiFollowEntity extends MobTalkerAiBase<EntityLiving>
{
    private static final double _teleportDistance = 20.0 * 20.0;
    private static final double _teleportDistanceBlocked = 12.0 * 12.0;
    
    private final double _speed;
    private final double _minDistanceSq;
    private final double _maxDistanceSq;
    
    private EntityLivingBase _target;
    private int _updateLimiter;
    
    // ========================================
    
    public MobTalkerAiFollowEntity( IInteractionAdapter adapter, double speed, double minDistance, double maxDistance )
    {
        super( EntityLiving.class, adapter );
        _speed = speed;
        _minDistanceSq = minDistance * minDistance;
        _maxDistanceSq = maxDistance * maxDistance;
        
        setMutexBits( 3 );
    }
    
    // ========================================
    
    @Override
    public boolean shouldExecute()
    {
        // TODO Is a limiter necessary?
        if ( _entity.getRNG().nextInt( 10 ) == 0 )
            return false;
        
        EntityLivingBase target = _adapter.getFollowTarget();
        if ( ( target == null ) || target.isDead )
            return false;
        
        if ( _entity.getDistanceSqToEntity( target ) < _maxDistanceSq )
            return false;
        
        _target = target;
        return true;
    }
    
    @Override
    public boolean continueExecuting()
    {
        if ( _entity.getNavigator().noPath() )
            return false;
        if ( ( _target == null ) || _target.isDead )
            return false;
        
        if ( !_target.getUniqueID().equals( _adapter.getFollowTargetUuid() ) )
            return false;
        
        return _entity.getDistanceSqToEntity( _target ) > _minDistanceSq;
    }
    
    @Override
    public void startExecuting()
    {
        _updateLimiter = 0;
    }
    
    @Override
    public void resetTask()
    {
        _target = null;
        _entity.getNavigator().clearPathEntity();
    }
    
    /**
     * From Minecraft's {@link EntityAIFollowOwner}
     */
    @Override
    public void updateTask()
    {
        _entity.getLookHelper().setLookPositionWithEntity( _target, 10.0F, _entity.getVerticalFaceSpeed() );
        
        if ( --_updateLimiter > 0 )
            return;
        
        _updateLimiter = 10;
        
        double distance = _entity.getDistanceSqToEntity( _target );
        if ( ( distance > _teleportDistance )
             || ( !_entity.getNavigator().tryMoveToEntityLiving( _target, _speed ) && ( distance > _teleportDistanceBlocked ) ) )
        {
            teleportToTarget();
        }
    }
    
    private void teleportToTarget()
    {
        int x = MathHelper.floor_double( _target.posX ) - 2;
        int z = MathHelper.floor_double( _target.posZ ) - 2;
        int y = MathHelper.floor_double( _target.getEntityBoundingBox().minY );
        
        for ( int xOff = 0; xOff <= 4; ++xOff )
        {
            for ( int zOff = 0; zOff <= 4; ++zOff )
            {
                World world = _entity.worldObj;
                
                if ( ( ( xOff < 1 ) || ( zOff < 1 ) || ( xOff > 3 ) || ( zOff > 3 ) )
                     && World.doesBlockHaveSolidTopSurface( world, new BlockPos( x + xOff, y - 1, z + zOff ) )
                     && !world.getBlockState( new BlockPos( x + xOff, y, z + zOff ) ).getBlock().isFullCube()
                     && !world.getBlockState( new BlockPos( x + xOff, y + 1, z + zOff ) ).getBlock().isFullCube() )
                {
                    _entity.getNavigator().clearPathEntity();
                    _entity.setLocationAndAngles( x + xOff + 0.5F, y, z + zOff + 0.5F,
                                                  _entity.rotationYaw, _entity.rotationPitch );
                    return;
                }
            }
        }
    }
}
