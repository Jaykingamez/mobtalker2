/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.ai;

import net.minecraft.entity.*;

import com.google.common.base.Predicate;

public class PredicateAliveLivingEntity implements Predicate<Entity>
{
    public static final PredicateAliveLivingEntity Instance = new PredicateAliveLivingEntity();
    
    private PredicateAliveLivingEntity()
    {}
    
    @Override
    public boolean apply( Entity entity )
    {
        return ( entity != null ) && ( entity instanceof EntityLivingBase ) && entity.isEntityAlive();
    }
}
