/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.ai;

import java.util.Comparator;

import net.minecraft.entity.Entity;

public class EntityDistanceSorter implements Comparator<Entity>
{
    private final Entity _entity;
    
    public EntityDistanceSorter( Entity entity )
    {
        _entity = entity;
    }
    
    @Override
    public int compare( Entity a, Entity b )
    {
        return (int) Math.signum( _entity.getDistanceSqToEntity( a ) - _entity.getDistanceSqToEntity( b ) );
    }
}