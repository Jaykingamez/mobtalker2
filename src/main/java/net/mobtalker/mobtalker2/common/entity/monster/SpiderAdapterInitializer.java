/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.monster;

import static net.mobtalker.mobtalker2.common.entity.ai.EntityAiTaskUtil.*;
import net.minecraft.entity.ai.*;
import net.minecraft.entity.monster.EntitySpider;
import net.mobtalker.mobtalker2.common.entity.EntityType;
import net.mobtalker.mobtalker2.common.entity.ai.*;
import net.mobtalker.mobtalker2.server.interaction.*;
import net.mobtalker.mobtalker2.server.script.lib.EntityReaction;

public class SpiderAdapterInitializer implements IInteractionAdapterInitializer<EntitySpider>
{
    @Override
    public void initialize( IInteractionAdapter adapter, EntitySpider entity,
                            EntityAITasks tasks, EntityAITasks targetTasks )
    {
        // Tasks
        replaceIndex( tasks, 1,
                      EntityAIAvoidEntity.class,
                      new MobTalkerAiAvoidEntity( adapter, 2.0D, 1.5D, 0.8D, 4.0D,
                                                  PredicateExplodingCreeper.Instance ) );
        
        addLast( tasks, 1,
                 new MobTalkerAiInteractPlayer( adapter ) );
        addLast( tasks, 2,
                 new MobTalkerAiAvoidEntity( adapter, 2.0D, 1.5D, 0.8D, 6.0D,
                                             new PredicateReactionScared( adapter ) ) );
        
        // Target Tasks
        replaceIndex( targetTasks, 0,
                      EntityAIHurtByTarget.class,
                      new MobTalkerAiHurtByTarget( adapter, false ) );
        replaceIndex( targetTasks, 1,
                      EntityAINearestAttackableTarget.class,
                      new MobTalkerAiSpiderTarget( adapter ) );
        removeIndex( targetTasks, 2,
                     EntityAINearestAttackableTarget.class );
        
        // Reactions
        adapter.setReaction( EntityReaction.HOSTILE, EntityType.Player );
        adapter.setReaction( EntityReaction.HOSTILE, EntityType.IronGolem );
    }
}
