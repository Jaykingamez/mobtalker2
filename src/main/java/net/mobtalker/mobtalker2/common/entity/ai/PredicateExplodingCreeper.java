/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.ai;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityCreeper;

import com.google.common.base.Predicate;

public class PredicateExplodingCreeper implements Predicate<EntityLivingBase>
{
    public static final PredicateExplodingCreeper Instance = new PredicateExplodingCreeper();
    
    private PredicateExplodingCreeper()
    {}
    
    @Override
    public boolean apply( EntityLivingBase entity )
    {
        return ( entity instanceof EntityCreeper ) && ( ( (EntityCreeper) entity ).getCreeperState() > 0 );
    }
}
