/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.ai;

import net.minecraft.entity.*;
import net.minecraft.entity.monster.EntityPigZombie;
import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;

public class MobTalkerAiZombiePigHurtByTarget extends MobTalkerAiHurtByTarget
{
    public MobTalkerAiZombiePigHurtByTarget( IInteractionAdapter adapter )
    {
        super( adapter, true );
    }
    
    @Override
    protected void askEntityForHelp( EntityCreature helper, EntityLivingBase target )
    {
        super.askEntityForHelp( helper, target );
        
        if ( helper instanceof EntityPigZombie ) // This should never be false, Vanilla checks it anyway, so do we.
        {
            ( (EntityPigZombie) helper ).becomeAngryAt( target );
        }
    }
}
