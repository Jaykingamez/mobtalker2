/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemModelMesher;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.*;
import net.mobtalker.mobtalker2.common.item.ItemMobTalker;

// TODO Find out what this is useful for
//@GameRegistry.ObjectHolder( value = Reference.ID )
public class MobTalkerItems
{
    public static final Item talker = new ItemMobTalker();
    
    // ========================================
    
    public static void register()
    {
        GameRegistry.registerItem( talker, "mobtalker" );
    }
    
    @SideOnly( Side.CLIENT )
    public static void registerRender()
    {
        ItemModelMesher mesher = Minecraft.getMinecraft().getRenderItem().getItemModelMesher();
        
        mesher.register( MobTalkerItems.talker, 0, new ModelResourceLocation( "mobtalker2:mobtalker", "inventory" ) );
    }
}
