/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.storage;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.mobtalker.mobtalker2.MobTalker2;
import net.mobtalker.mobtalker2.server.script.serialization.MtsToNbtSerialization;
import net.mobtalker.mobtalkerscript.v3.value.MtsValue;

import java.util.Map;
import java.util.Set;

import static org.apache.commons.lang3.Validate.notBlank;

public abstract class AbstractEntityPropertiesVariableStorage extends AbstractVariableStorage implements IEntityPropertiesVariableStorage
{
    /** Name used to store the variables as NBT tags */
    private final String _nbtTagName;
    
    // ========================================
    
    protected AbstractEntityPropertiesVariableStorage( String nbtTagName )
    {
        _nbtTagName = String.format( "%s.%s", MobTalker2.ID, notBlank( nbtTagName ) );
    }
    
    // ========================================
    
    /**
     * Serializes the saved variables to an {@link NBTTagCompound}.
     *
     * @return Variables stored in this storage, serialized as {@code NBTTagCompound} object.
     * @implSpec Default implementation will map the saved variables by their script's identifier and return the result as {@code NBTTagCompound} object.
     * Variable values will be serialized with the {@link MtsToNbtSerialization#serialize(Map)} function.
     */
    protected NBTTagCompound serialize()
    {
        Map<String, Map<String, MtsValue>> savedVariablesByScriptIdentifier = getAllSavedVariables();
        if ( savedVariablesByScriptIdentifier == null || savedVariablesByScriptIdentifier.isEmpty() )
        {
            // Prevent empty maps from being saved
            return null;
        }
        
        NBTTagCompound serialized = new NBTTagCompound();
        Set<String> identifiers = savedVariablesByScriptIdentifier.keySet();
        for ( String identifier : identifiers )
        {
            Map<String, MtsValue> savedVariables = savedVariablesByScriptIdentifier.get( identifier );
            if ( savedVariables == null || savedVariables.isEmpty() )
            {
                // Ignore empty maps
                continue;
            }
            
            serialized.setTag( identifier, MtsToNbtSerialization.serialize( savedVariables ) );
        }
        
        return serialized;
    }
    
    /**
     * Deserializes the saved variables from an {@link NBTTagCompound}.
     *
     * @param compound {@code NBTTagCompound} object which contains the serialized saved variables.
     *
     * @implSpec Default implementation will treat keys of the {@code NBTTagCompound} object as script identifiers and their respective values as serialized
     * saved variables. Variable values will be deserialized with the {@link MtsToNbtSerialization#deserialize(NBTTagCompound)} function.
     */
    protected void deserialize( NBTTagCompound compound )
    {
        // NBTTags are built upon a Map<String, ?>, so this cast is safe
        @SuppressWarnings( "unchecked" )
        Set<String> keys = (Set<String>) compound.getKeySet();
        for ( String key : keys )
        {
            NBTBase value = compound.getTag( key );
            if ( !( value instanceof NBTTagCompound ) )
            {
                // ignore invalid values silently
                continue;
            }
            
            setSavedVariables( key, MtsToNbtSerialization.deserialize( (NBTTagCompound) value ) );
        }
    }
    
    // ========================================
    
    /**
     * Returns the NBT tag name which should be used to store and retrieve the serialized saved variables from the entity's NBT data.
     *
     * @return NBT tag name of this variable storage.
     * @implNote If overridden, the returned value of this method should never change for one object.
     */
    protected String getNBTTagName()
    {
        return _nbtTagName;
    }
    
    @Override
    public void loadNBTData( NBTTagCompound compound )
    {
        String tagName = getNBTTagName();
        if ( compound.hasKey( tagName ) )
        {
            deserialize( compound.getCompoundTag( tagName ) );
        }
    }
    
    @Override
    public void saveNBTData( NBTTagCompound compound )
    {
        String tagName = getNBTTagName();
        NBTTagCompound serialized = serialize();
        if ( serialized != null && !serialized.hasNoTags() )
        {
            compound.setTag( tagName, serialized );
        }
    }
    
    // ========================================
    
    /**
     * @inheritDocs
     * @implNote Default implementation of this method does nothing.
     */
    @Override
    public void init( Entity entity, World world ) {}
}
