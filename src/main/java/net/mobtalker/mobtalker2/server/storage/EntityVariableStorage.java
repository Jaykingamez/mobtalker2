/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.storage;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.nbt.NBTTagCompound;
import net.mobtalker.mobtalker2.server.script.serialization.MtsToNbtSerialization;
import net.mobtalker.mobtalkerscript.v3.value.MtsValue;

import java.util.*;

import static org.apache.commons.lang3.Validate.notNull;

public class EntityVariableStorage extends AbstractEntityPropertiesVariableStorage
{
    /** Name used to store the variables as NBT tags */
    private static final String NBT_TAG_NAME = EntityVariableStorage.class.getSimpleName();
    
    /** UUID used to store "global" (= per mob) saved variables */
    public static final UUID GLOBAL = new UUID( 0, 0 );
    
    public static final IEntityVariableStorageFactory FACTORY = EntityVariableStorage::new;
    
    // ========================================
    
    /**
     * Table storing all saved variables by script identifier and partner entity UUID.
     * <dl>
     * <dt>Rows</dt><dd>Script identifiers</dd>
     * <dt>Columns</dt><dd>Partner entity UUIDs</dd>
     * </dl>
     */
    private final Table<String, UUID, Map<String, MtsValue>> _savedVariablesByScriptIdentifierAndPartner = HashBasedTable.create();
    
    // ========================================
    
    public EntityVariableStorage()
    {
        super( NBT_TAG_NAME );
        
        // Overwrite the map from AbstractVariableStorage so that none of its methods need to be overridden.
        // This works because the `Table.column(Object)` method returns a view which updates in both directions.
        setSavedVariablesByScriptIdentifier( column( GLOBAL ) );
    }
    
    // ========================================
    
    /** Helper function for {@link Table#put(Object, Object, Object)} */
    private void put( String identifier, UUID uuid, Map<String, MtsValue> savedVariables )
    {
        _savedVariablesByScriptIdentifierAndPartner.put( identifier, uuid, savedVariables );
    }
    
    /** Helper function for {@link Table#column(Object)} */
    private Map<String, Map<String, MtsValue>> column( UUID uuid )
    {
        return _savedVariablesByScriptIdentifierAndPartner.column( uuid );
    }
    
    /** Helper function for {@link Table#get(Object, Object)} */
    private Map<String, MtsValue> get( String identifier, UUID uuid )
    {
        return _savedVariablesByScriptIdentifierAndPartner.get( identifier, uuid );
    }
    
    // ========================================
    
    @Override
    protected NBTTagCompound serialize()
    {
        if ( _savedVariablesByScriptIdentifierAndPartner.isEmpty() )
        {
            // Don't save an empty table
            return null;
        }
        
        NBTTagCompound serialized = new NBTTagCompound();
        for ( Table.Cell<String, UUID, Map<String, MtsValue>> cell : _savedVariablesByScriptIdentifierAndPartner.cellSet() )
        {
            String identifier = cell.getRowKey();
            UUID uuid = cell.getColumnKey();
            Map<String, MtsValue> savedVariables = cell.getValue();
            if ( savedVariables == null || savedVariables.isEmpty() )
            {
                continue;
            }
            
            NBTTagCompound savedVariablesByPartner = serialized.getCompoundTag( identifier );
            savedVariablesByPartner.setTag( uuid.toString(), MtsToNbtSerialization.serialize( savedVariables ) );
            serialized.setTag( identifier, savedVariablesByPartner );
        }
        
        return serialized;
    }
    
    @Override
    protected void deserialize( NBTTagCompound compound )
    {
        @SuppressWarnings( "unchecked" )
        Set<String> identifiers = compound.getKeySet();
        for ( String identifier : identifiers )
        {
            NBTTagCompound savedVariablesByPartner = compound.getCompoundTag( identifier );
            @SuppressWarnings( "unchecked" )
            Set<String> uuids = savedVariablesByPartner.getKeySet();
            for ( String uuid : uuids )
            {
                NBTTagCompound savedVariables = savedVariablesByPartner.getCompoundTag( uuid );
                setSavedVariables( identifier, UUID.fromString( uuid ), MtsToNbtSerialization.deserialize( savedVariables ) );
            }
        }
    }
    
    // ========================================
    
    /**
     * Returns a set of all script identifiers which have saved variables for the given partner entity.
     * <p>
     * The {@code null} entity will return script identifiers who have saved variables that aren't attached to a partner entity. As a shorthand, the {@link
     * #getIdentifiers()} method can be used instead.
     * </p>
     *
     * @param entity Partner entity for which the script identifiers should be returned
     *
     * @return A set of all script identifiers who have saved variables for the given partner entity.
     * @see #getIdentifiers(UUID)
     */
    public Set<String> getIdentifiers( EntityLivingBase entity )
    {
        return getIdentifiers( entity == null ? GLOBAL : entity.getUniqueID() );
    }
    
    /**
     * Returns a set of all script identifiers which have saved variables for the given partner entity UUID.
     * <p>
     * If all script identifiers who have saved variables that aren't attached to a partner entity should be returned, the {@link #GLOBAL} UUID should be used,
     * as {@code null} is not permitted as {@code uuid} argument. As a shorthand, the {@link #getIdentifiers()} method can be used instead.
     * </p>
     *
     * @param uuid UUID of the partner entity for which the script identifiers should be returned
     *
     * @return A set of all script identifiers who have saved variables for the given partner entity UUID.
     * @see #getIdentifiers(EntityLivingBase)
     */
    public Set<String> getIdentifiers( UUID uuid )
    {
        return column( notNull( uuid ) ).keySet();
    }
    
    /**
     * Returns all variables that have been saved for the given script identifier and partner entity.
     * <p>
     * If {@code null} is specified as value for the {@code entity} parameter, this method will instead return all variables for the script identifier that
     * aren't attached to a partner entity. As a shorthand, the {@link #getSavedVariables(String)} method can be used instead.
     * </p>
     *
     * @param identifier Identifier of the script for which the saved variables should be returned
     * @param entity     Partner entity for which the saved variables should be returned
     *
     * @return A map containing the values of the variables saved for the given script identifier and partner entity, mapped by the variable's name. Should be
     * assumed to be unmodifiable.
     * @see #getSavedVariables(String, UUID)
     */
    public Map<String, MtsValue> getSavedVariables( String identifier, EntityLivingBase entity )
    {
        return getSavedVariables( identifier, entity == null ? GLOBAL : entity.getUniqueID() );
    }
    
    /**
     * Returns all variables that have been saved for the given script identifier and partner entity UUID.
     * <p>
     * If this method should return all saved variables for the given script that aren't attached to a partner entity, the {@link #GLOBAL} UUID should be used,
     * as {@code null} is not permitted as {@code uuid} argument. As a shorthand, the {@link #getSavedVariables(String)} method can be used instead.
     * </p>
     *
     * @param identifier Identifier of the script for which the saved variables should be returned
     * @param uuid       UUID of the partner entity for which the script identifiers should be returned
     *
     * @return A map containing the values of the variables saved for the given script identifier and partner entity UUID, mapped by their respective variables'
     * names. Should be assumed to be unmodifiable.
     * @see #getSavedVariables(String, EntityLivingBase)
     */
    public Map<String, MtsValue> getSavedVariables( String identifier, UUID uuid )
    {
        Map<String, MtsValue> savedVariables = get( identifier, notNull( uuid ) );
        return savedVariables == null ? null : Collections.unmodifiableMap( savedVariables );
    }
    
    /**
     * Clears and sets the values of all saved variables for the given script identifier and partner entity.
     * <p>
     * If the {@code entity} parameter is {@code null}, this method will instead clear and set the values of all saved variables for the script identifier that
     * aren't attached to a partner entity. As a shorthand, the {@link #setSavedVariables(String, Map)} method can be used.
     * </p>
     *
     * @param identifier     Identifier of the script for which the saved variables should be updated
     * @param entity         Partner entity for which the saved variables should be updated
     * @param savedVariables Map of variable values mapped by their respective variables' names
     *
     * @see #setSavedVariables(String, UUID, Map)
     */
    public void setSavedVariables( String identifier, EntityLivingBase entity, Map<String, ? extends MtsValue> savedVariables )
    {
        setSavedVariables( identifier, entity == null ? GLOBAL : entity.getUniqueID(), savedVariables );
    }
    
    /**
     * Clears and sets the values of all saved variables for the given script identifier and partner entity UUID.
     * <p>
     * If this method should instead update all saved variables for the given script identifier that aren't attached to a partner entity, the {@link #GLOBAL}
     * UUID should be used, as {@code null} is not permitted as {@code uuid} argument. As a shorthand, the {@link #setSavedVariables(String, Map)} method can be
     * used instead.
     * </p>
     *
     * @param identifier     Identifier of the script for which the saved variables should be updated
     * @param uuid           UUID of the partner entity for which the saved variables should be updated
     * @param savedVariables Map of variable values mapped by their respective variables' names
     *
     * @see #setSavedVariables(String, EntityLivingBase, Map)
     */
    public void setSavedVariables( String identifier, UUID uuid, Map<String, ? extends MtsValue> savedVariables )
    {
        Map<String, MtsValue> entry = get( identifier, notNull( uuid ) );
        if ( entry == null )
        {
            entry = new HashMap<>( savedVariables.size() );
            put( identifier, uuid, entry );
        }
        else
        {
            entry.clear();
        }
        
        entry.putAll( savedVariables );
    }
}
