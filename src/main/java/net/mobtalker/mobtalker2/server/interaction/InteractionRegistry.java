/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.interaction;

import static java.lang.String.*;
import static net.mobtalker.mobtalker2.util.ChatMessageHelper.*;
import static org.apache.commons.lang3.Validate.notNull;
import static org.apache.commons.lang3.Validate.validState;

import java.util.*;

import net.minecraft.entity.*;
import net.minecraft.entity.player.*;
import net.minecraft.item.ItemStack;
import net.minecraft.util.*;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.FakePlayer;
import net.minecraftforge.event.entity.EntityEvent.EntityConstructing;
import net.minecraftforge.event.entity.*;
import net.minecraftforge.event.entity.living.*;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.player.EntityInteractEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.*;
import net.mobtalker.mobtalker2.*;
import net.mobtalker.mobtalker2.common.MobTalkerConfig;
import net.mobtalker.mobtalker2.common.entity.EntityType;
import net.mobtalker.mobtalker2.common.entity.monster.*;
import net.mobtalker.mobtalker2.common.event.MobTalkerEvent;
import net.mobtalker.mobtalker2.common.item.ItemMobTalker;
import net.mobtalker.mobtalker2.common.network.ServerMessageHandler;
import net.mobtalker.mobtalker2.server.script.*;
import net.mobtalker.mobtalker2.util.*;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;
import net.mobtalker.mobtalkerscript.v3.compiler.MtsSyntaxError;

import com.google.common.collect.*;

public class InteractionRegistry
{
    private static final MobTalkerLog LOG = new MobTalkerLog( "InteractionRegistry" );
    private static InteractionRegistry instance;
    
    public static InteractionRegistry instance()
    {
        validState( instance != null, "No InteractionAdapterFactory is currently loaded" );
        return instance;
    }
    
    public static boolean isLoaded()
    {
        return instance != null;
    }
    
    public static void load()
    {
        validState( instance == null, "Already loaded" );
        
        LOG.debug( "Loading" );
        
        instance = new InteractionRegistry();
        
        instance.registerAdapterInitializer( EntityType.Blaze, new BlazeAdapterInitializer() );
        instance.registerAdapterInitializer( EntityType.Creeper, new CreeperAdapterInitializer() );
        instance.registerAdapterInitializer( EntityType.Enderman, new EndermanAdapterInitializer() );
        instance.registerAdapterInitializer( EntityType.Witch, new WitchAdapterInitializer() );
        
        instance.registerAdapterInitializer( EntityType.Skeleton, new SkeletonAdapterInitializer() );
        instance.registerAdapterInitializer( EntityType.WitherSkeleton, new SkeletonAdapterInitializer() );
        
        instance.registerAdapterInitializer( EntityType.Spider, new SpiderAdapterInitializer() );
        instance.registerAdapterInitializer( EntityType.CaveSpider, new SpiderAdapterInitializer() );
        
        instance.registerAdapterInitializer( EntityType.Zombie, new ZombieAdapterInitializer() );
        
        // TODO Implement and register missing entities
        // instance.registerAdapterInitializer( EntityType.ZombiePigman, new PigZombieAdapterInitializer() );
        
        MinecraftForge.EVENT_BUS.register( instance );
        FMLCommonHandler.instance().bus().register( instance );
    }
    
    public static void unload()
    {
        validState( instance != null, "No InteractionAdapterFactory is currently loaded" );
        
        LOG.debug( "Unloading" );
        
        MinecraftForge.EVENT_BUS.unregister( instance );
        FMLCommonHandler.instance().bus().unregister( instance );
        instance.shutdown();
        
        instance = null;
    }
    
    // ========================================
    
    private IInteractionAdapterFactory _adapterFactory;
    private final Map<EntityType, IInteractionAdapterInitializer<? extends EntityLiving>> _adapterInitializers;
    private final Set<IInteractionAdapter> _pendingInitializations;
    
    private final Set<IInteractionAdapter> _activeAdapters;
    private final Map<IInteractionAdapter, OnInteractionFutureTask> _activeInteractions;
    
    private InteractionRegistry()
    {
        _adapterFactory = new DefaultInteractionAdapterFactory();
        _adapterInitializers = Maps.newHashMap();
        _pendingInitializations = Sets.newIdentityHashSet();
        
        _activeAdapters = Sets.newHashSet();
        _activeInteractions = Maps.newHashMap();
    }
    
    // ========================================
    
    // ========================================
    
    private void shutdown()
    {
        for ( IInteractionAdapter adapter : Lists.newArrayList( _activeAdapters ) )
        {
            try
            {
                cancelInteraction( adapter );
            }
            catch ( RuntimeException ex )
            {
                LOG.error( ex );
            }
        }
    }
    
    // ========================================
    
    public IInteractionAdapterFactory getAdapterFactory()
    {
        return _adapterFactory;
    }
    
    public void setAdapterFactory( IInteractionAdapterFactory factory )
    {
        notNull( factory, "factory can not be null" );
        
        _adapterFactory = factory;
    }
    
    // ========================================
    
    public boolean hasAdapterInitializer( EntityType entityType )
    {
        return _adapterInitializers.containsKey( entityType );
    }
    
    /**
     * Registers an {@link IInteractionAdapterInitializer} that will be used to initialize {@link IInteractionAdapter} for the
     * given internal entity name.
     *
     * @param entityType The entity type to register the initializer for.
     * @param initializer The initializer.
     */
    public void registerAdapterInitializer( EntityType entityType,
                                            IInteractionAdapterInitializer<? extends EntityLiving> initializer )
    {
        notNull( entityType, "entityType can not be null" );
        notNull( initializer, "initializer can not be null" );
        
        _adapterInitializers.put( entityType, initializer );
    }
    
    // ========================================
    
    private static final String ADAPTER_IDENTIFIER = "MobTalkerInteractionAdapter";
    
    /**
     * Retrieves the adapter for the giving entity, if it has one.
     *
     * @param entity The entity to retrieve the adapter for.
     * @return The adapter, or <code>null</code> if there is none.
     */
    public IInteractionAdapter getAdapter( EntityLiving entity )
    {
        return (IInteractionAdapter) entity.getExtendedProperties( ADAPTER_IDENTIFIER );
    }
    
    /**
     * Retrieves the adapter for the entity the given player is interacting with, if any.
     *
     * @param player The interacting player.
     * @return The adapter, or <code>null</code> if there is none.
     */
    public IInteractionAdapter getAdapter( EntityPlayer player )
    {
        notNull( player );
        
        for ( IInteractionAdapter adapter : _activeAdapters )
        {
            if ( player.equals( adapter.getPartner() ) )
                return adapter;
        }
        
        return null;
    }
    
    public boolean isInteracting( EntityLiving entity )
    {
        IInteractionAdapter adapter = getAdapter( entity );
        return ( adapter != null ) && adapter.isActive();
    }
    
    public boolean isInteracting( EntityPlayer player )
    {
        IInteractionAdapter adapter = getAdapter( player );
        return ( adapter != null ) && adapter.isActive();
    }
    
    // ========================================
    
    private boolean canCreateAdapterFor( EntityLiving entity )
    {
        EntityType entityType = EntityType.of( entity );
        
        if ( MobTalkerConfig.getDisabledEntityTypes().contains( entityType ) )
            return false;
        
        if ( !hasAdapterInitializer( entityType ) )
        {
            LOG.info( "Entity '%s' does not have an adapter initializer. Interactions are not enabled.",
                      entityType );
            
            MobTalkerConfig.getDisabledEntityTypes().add( entityType );
            return false;
        }
        
        return true;
    }
    
    /**
     * Creates an {@link IInteractionAdapter} for the given entity and registers it with it.
     * <p>
     * The adapter will not be initialized.
     *
     * @param entity The entity to create and register the adapter for.
     */
    private IInteractionAdapter createAdapter( EntityLiving entity )
    {
        String entityName = EntityUtil.getName( entity );
        
        LOG.trace( "Creating adapter for '%s'", entityName );
        
        IInteractionAdapter adapter = _adapterFactory.create( entity );
        
        String identifier = entity.registerExtendedProperties( ADAPTER_IDENTIFIER, adapter );
        if ( !identifier.equals( ADAPTER_IDENTIFIER ) )
            throw new AssertionError( format( "Unable to register adapter properly, result was '%s'", identifier ) );
        
        _pendingInitializations.add( adapter );
        
        return adapter;
    }
    
    private boolean needsInitialization( IInteractionAdapter adapter )
    {
        return _pendingInitializations.contains( adapter );
    }
    
    @SuppressWarnings( { "rawtypes", "unchecked" } )
    private void initialize( IInteractionAdapter adapter )
    {
        LOG.trace( "Initializing adapter for '%s'", adapter.getEntityName() );
        
        if ( !_pendingInitializations.remove( adapter ) )
            throw new AssertionError( "Tried to initialize adapter for '" + adapter.getEntityName()
                                      + "' that wasn't pending for initialization!" );
        
        IInteractionAdapterInitializer initializer = _adapterInitializers.get( adapter.getEntityType() );
        EntityLiving entity = adapter.getEntity();
        initializer.initialize( adapter, entity, entity.tasks, entity.targetTasks );
        
        adapter.deserialize();
    }
    
    // ========================================
    
    public void startInteraction( EntityLiving entity, EntityPlayerMP player )
        throws InteractionException
    {
        notNull( entity, "entity is null" );
        notNull( player, "player is null" );
        
        IInteractionAdapter adapter = getAdapter( entity );
        
        if ( adapter == null )
        {
            LOG.debug( "Entity '%s' does not have an adapter", EntityUtil.getName( entity ) );
            return;
        }
        
        String entityName = adapter.getDisplayedEntityName();
        String playerName = EntityUtil.getName( player );
        
        // Workaround for slower computers causing race conditions
        if ( _activeAdapters.contains( adapter ) )
        {
            LOG.info( "Suppressing duplicate interaction request. Don't click so fast!" );
            return;
        }
        
        if ( !adapter.canStartInteractionWith( player, 9.0 ) )
        {
            LOG.info( "'%s' cannot start an interaction with '%s'",
                      playerName, entityName );
            return;
        }
        
        if ( !MobTalkerEvent.queryReqestInteraction( adapter, player ) )
        {
            LOG.debug( "Request to start interaction between '%s' and '%s' was denied by an event handler",
                       playerName, entityName );
            return;
        }
        
        LOG.info( "Starting interaction between '%s' and '%s'",
                  playerName, entityName );
        
        InteractionScript script = getScript( adapter );
        
        adapter.setPartner( player );
        _activeAdapters.add( adapter );
        
        openGui( adapter );
        
        OnInteractionFutureTask task = new OnInteractionFutureTask( script );
        
        ScriptExecutor.instance().submit( task );
        _activeInteractions.put( adapter, task );
        
        adapter.onInteractionStart();
        MobTalkerEvent.notifyStartInteraction( adapter );
    }
    
    private InteractionScript getScript( IInteractionAdapter adapter )
        throws InteractionException
    {
        try
        {
            return adapter.getScript();
        }
        catch ( MissingScriptException ex )
        {
            throw new InteractionException( "Interactions for '%s' are enabled but no script was found!",
                                            ex, adapter.getDisplayedEntityName() );
        }
        catch ( MtsSyntaxError ex )
        {
            throw new InteractionException( "The script for '%s' contains syntax errors\n%s",
                                            ex, adapter.getDisplayedEntityName(), ex.getMessage() );
        }
        catch ( Exception ex )
        {
            throw new InteractionException( "Unable to create a script instance for '%s': %s",
                                            ex, adapter.getDisplayedEntityName(), ex.getMessage() );
        }
    }
    
    private void openGui( IInteractionAdapter adapter )
    {
        EntityPlayerMP player = adapter.getPartner();
        InteractionScript script = adapter.getScript();
        
        ServerMessageHandler.sendGuiConfig( player, script.getEntry().getGuiSettings() );
        player.openGui( MobTalker2.instance, 1, player.worldObj, (int) player.posX, (int) player.posY, (int) player.posZ );
    }
    
    public void cancelInteraction( EntityPlayer player )
    {
        cancelInteraction( getAdapter( player ) );
    }
    
    public void cancelInteraction( IInteractionAdapter adapter )
    {
        String entityName = adapter.getDisplayedEntityName();
        
        validState( adapter.isActive(),
                    "Trying to end interaction for '%s' who does not have a partner!",
                    entityName );
        validState( _activeAdapters.contains( adapter ),
                    "Corrupt InteractionRegistry: Adapter for '%s' has interaction partner but is not registered as active",
                    entityName );
        
        ServerMessageHandler.instance().cancelCall( adapter.getPartner() );
        _activeInteractions.get( adapter ).cancel( true );
    }
    
    public void endInteraction( EntityPlayerMP player )
    {
        endInteraction( getAdapter( player ) );
    }
    
    public void endInteraction( IInteractionAdapter adapter )
    {
        String entityName = adapter.getDisplayedEntityName();
        EntityPlayerMP player = adapter.getPartner();
        String playerName = EntityUtil.getName( player );
        
        validState( adapter.isActive(),
                    "Trying to end interaction for '%s' who does not have a partner!",
                    entityName );
        validState( _activeAdapters.contains( adapter ),
                    "Corrupt InteractionRegistry: Adapter for '%s' has interaction partner but is not registered as active",
                    entityName );
        validState( _activeInteractions.get( adapter ).isDone(),
                    "Interaction is not done yet. You have to cancel it to end it forcefully.",
                    entityName );
        
        LOG.info( "Ending interaction between '%s' and '%s'",
                  entityName, playerName );
        
        adapter.clearPartner();
        
        _activeAdapters.remove( adapter );
        _activeInteractions.remove( adapter ).cancel( true );
        
        if ( player.openContainer instanceof ServerInteractionHandler )
        {
            player.closeScreen();
        }
        else
        {
            LOG.error( "Open container should be '%s' since we had an active interaction, but it was '%s'",
                       ServerInteractionHandler.class.getSimpleName(),
                       player.openContainer.getClass().getName() );
        }
        
        adapter.onInteractionEnd();
        MobTalkerEvent.notifyEndInteraction( adapter );
    }
    
    public void endInteraction( EntityLiving entity )
    {
        IInteractionAdapter adapter = getAdapter( entity );
        if ( adapter == null )
        {
            LOG.debug( "Entity '%s' does not have an adapter",
                       EntityUtil.getName( entity ) );
            return;
        }
        
        endInteraction( adapter );
    }
    
    // ========================================
    
    /**
     * Forge Callback.
     * <p>
     * Gets called when a player right clicks an entity.
     */
    @SubscribeEvent( priority = EventPriority.LOWEST )
    public void onEntityInteractEvent( EntityInteractEvent event )
    {
        Entity entity = event.target;
        if ( entity.worldObj.isRemote || !( entity instanceof EntityLiving ) )
            return;
        if ( !( event.entityPlayer instanceof EntityPlayerMP ) )
            return;
        
        // Guard against fake players
        if ( event.entityPlayer instanceof FakePlayer )
            return;
        
        EntityLiving entityLiving = (EntityLiving) entity;
        EntityPlayerMP player = (EntityPlayerMP) event.entityPlayer;
        
        ItemStack holdItemStack = player.getHeldItem();
        if ( ( holdItemStack == null ) || !( holdItemStack.getItem() instanceof ItemMobTalker ) )
            return;
        
        try
        {
            startInteraction( entityLiving, player );
        }
        catch ( InteractionException ex )
        {
            LOG.error( ex.getCause(), ex.getMessage() );
            sendMessageToEntity( player, ex.getMessage(), EnumChatFormatting.RED );
        }
    }
    
    // ========================================
    
    /**
     * Forge callback.
     * <p>
     * Gets called when a new {@link Entity} is created.
     * <p>
     * Checks if this entity is of interest and if it is, creates an adapter for it.
     */
    @SubscribeEvent( priority = EventPriority.LOWEST, receiveCanceled = false )
    public void onEntityConstructing( EntityConstructing event )
    {
        if ( !MinecraftUtil.isServer() || !( event.entity instanceof EntityLiving ) )
            return;
        
        EntityLiving entityLiving = (EntityLiving) event.entity;
        
        if ( canCreateAdapterFor( entityLiving ) )
        {
            createAdapter( entityLiving );
        }
    }
    
    @SubscribeEvent( priority = EventPriority.LOWEST, receiveCanceled = false )
    public void onEntityJoin( EntityJoinWorldEvent event )
    {
        Entity entity = event.entity;
        if ( entity.worldObj.isRemote || !( entity instanceof EntityLiving ) )
            return;
        
        EntityLiving entityLiving = (EntityLiving) entity;
        
        IInteractionAdapter adapter = getAdapter( entityLiving );
        if ( adapter == null )
            return;
        
        if ( needsInitialization( adapter ) )
        {
            initialize( adapter );
        }
        
        adapter.onWorldChanged();
    }
    
    /**
     * Forge callback.
     * <p>
     * Gets called when an entity hurts another.
     * <p>
     * Checks if this entity has an adapter and if it does, invokes
     * {@link IInteractionAdapter#onHitByEntity(EntityLivingBase, DamageSource, float)} on it.
     */
    @SubscribeEvent( priority = EventPriority.LOWEST, receiveCanceled = false )
    public void onLivingAttack( LivingHurtEvent event )
    {
        if ( !( event.entity instanceof EntityLiving ) )
            return;
        
        EntityLiving entity = (EntityLiving) event.entity;
        
        IInteractionAdapter adapter = getAdapter( entity );
        if ( adapter == null )
            return;
        
        DamageSource source = event.source;
        if ( !( source.getSourceOfDamage() instanceof EntityLivingBase ) )
            return;
        
        EntityLivingBase attacker = (EntityLivingBase) source.getSourceOfDamage();
        
        adapter.onHitByEntity( attacker, source, event.ammount );
    }
    
    /**
     * Forge callback.
     * <p>
     * Gets called when an entity dies.
     * <p>
     * Checks if this entity has an adapter and if it does, invokes {@link IInteractionAdapter#onDeath()} on it.
     */
    @SubscribeEvent( priority = EventPriority.LOWEST, receiveCanceled = false )
    public void onEntityDeath( LivingDeathEvent event )
    {
        Entity entity = event.entity;
        if ( entity.worldObj.isRemote || !( entity instanceof EntityLiving ) )
            return;
        
        EntityLiving entityLiving = (EntityLiving) entity;
        
        IInteractionAdapter adapter = getAdapter( entityLiving );
        if ( adapter == null )
            return;
        
        adapter.onDeath();
    }
    
    @SubscribeEvent
    public void onLivingUpdate( LivingUpdateEvent event )
    {
        // TODO Find a solution for keeping the player afloat in water during interaction.
        // The interaction registry isn't available from the client side, but movement is controlled mainly by the client.
        // Maybe move it to the Client GUI Container?
        
//        Entity entity = event.entity;
//        if ( /* entity.worldObj.isRemote || */!( entity instanceof EntityPlayer ) )
//            return;
//
////        ( (EntityLiving) entity ).getJumpHelper().setJumping();
//
//        EntityPlayer player = (EntityPlayer) entity;
//////        if ( !isInteracting( player ) )
//////            return;
////
//        if ( player.isInWater() )
//        {
//            BlockPos pos = entity.worldObj.getPrecipitationHeight( player.getPosition() );
//            double distance = player.posY - pos.getY();
//
//            if ( distance < -1.0 )
//            {
//                player.motionY += 0.05;
//            }
//            else if ( ( distance < -0.75 ) )
//            {
//                player.motionY += 0.03;
//            }
//            else if ( distance < 0.0 )
//            {
//                if ( player.motionY > 0.0 )
//                {
//                    player.motionY = 0.02;
//                }
//            }
//        }
    }
}
