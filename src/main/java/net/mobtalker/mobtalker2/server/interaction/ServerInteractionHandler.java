/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.interaction;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.*;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;

/**
 * Handles the script interactions of an entity.
 */
public class ServerInteractionHandler extends Container
{
    private static final MobTalkerLog LOG = new MobTalkerLog( "InteractionHandler" );
    
    // ========================================
    
    protected final IInteractionAdapter _adapter;
    
    // ========================================
    
    public ServerInteractionHandler( IInteractionAdapter adapter )
    {
        _adapter = adapter;
        
        // Needed because Container does not expect to have no slots and causes an exception otherwise.
        addSlotToContainer( new Slot( new InventoryBasic( "FakeInventory", false, 1 ), 0, -999, -999 ) );
    }
    
    // ========================================
    
    public IInteractionAdapter getAdapter()
    {
        return _adapter;
    }
    
    // ========================================
    
    @Override
    public final boolean canInteractWith( EntityPlayer player )
    {
        return _adapter.getPartner() == player;
    }
    
    @Override
    public void onContainerClosed( EntityPlayer player )
    {
        LOG.debug( "Container closed" );
    }
}
