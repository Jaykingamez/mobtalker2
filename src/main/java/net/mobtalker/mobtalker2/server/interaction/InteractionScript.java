/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.interaction;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.mobtalker.mobtalker2.common.MobTalkerConfig;
import net.mobtalker.mobtalker2.server.resources.scriptpack.InteractionScriptEntry;
import net.mobtalker.mobtalker2.server.resources.scriptpack.config.InteractionScriptConfig;
import net.mobtalker.mobtalker2.server.resources.scriptpack.config.ScriptConfig;
import net.mobtalker.mobtalker2.server.script.lib.EventLib;
import net.mobtalker.mobtalker2.server.script.serialization.MtsToNbtSerialization;
import net.mobtalker.mobtalker2.server.storage.EntityVariableStorage;
import net.mobtalker.mobtalker2.server.storage.IVariableStorage;
import net.mobtalker.mobtalker2.server.storage.PlayerVariableStorage;
import net.mobtalker.mobtalker2.server.storage.VariableStorageRegistry;
import net.mobtalker.mobtalker2.util.EntityUtil;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;
import net.mobtalker.mobtalkerscript.v3.value.MtsClosure;
import net.mobtalker.mobtalkerscript.v3.value.MtsTable;
import net.mobtalker.mobtalkerscript.v3.value.MtsValue;

import java.util.*;
import java.util.concurrent.CancellationException;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;
import static org.apache.commons.lang3.Validate.validState;

/**
 * Abstraction wrapper around {@link MtsClosure} that represents an interaction script.
 */
public class InteractionScript
{
    private static final MobTalkerLog LOG = new MobTalkerLog( "InteractionScript" );
    
    // ========================================
    
    private final InteractionScriptEntry _entry;
    private final IInteractionAdapter _adapter;
    
    private final Map<UUID, Map<String, MtsValue>> _playerSaves = new HashMap<>( 1 );
    
    private final MtsTable _env;
    private final List<MtsClosure> _scripts;
    
    private final EventLib _eventLib;
    private boolean _isLoaded;
    
    // ========================================
    
    public InteractionScript( IInteractionAdapter adapter, InteractionScriptEntry entry,
                              MtsTable env, List<MtsClosure> scripts, EventLib eventLib )
    {
        checkNotNull( entry );
        checkNotNull( adapter );
        checkNotNull( env );
        checkNotNull( scripts );
        checkArgument( !scripts.isEmpty() );
        
        _entry = entry;
        _adapter = adapter;
        
        _env = env;
        _scripts = scripts;
        
        _eventLib = eventLib;
    }
    
    // ========================================
    
    public InteractionScriptEntry getEntry()
    {
        return _entry;
    }
    
    public IInteractionAdapter getAdapter()
    {
        return _adapter;
    }
    
    public MtsTable getEnvironment()
    {
        return _env;
    }
    
    // ========================================
    
    public void onInteraction()
    {
        LOG.debug( "onInteraction" );
        
        loadVariablesPerEntity();
        loadVariablesPerPlayer();
        loadVariablesPerPlayerAndEntity();
        
        try
        {
            if ( !_isLoaded )
            {
                load();
                _eventLib.invoke( EventLib.INTERACTION_START );
            }
            else
            {
                if ( _eventLib.hasHandlerFor( EventLib.INTERACTION_START ) )
                {
                    _eventLib.invoke( EventLib.INTERACTION_START );
                }
                else
                {
                    load();
                }
            }
            
            _eventLib.invoke( EventLib.INTERACTION_END );
        }
        catch ( CancellationException ex )
        {
            LOG.info( "Script execution was cancelled" );
            
            _eventLib.invoke( EventLib.INTERACTION_CANCELLED );
        }
        
        saveVariablesPerPlayerAndEntity();
        saveVariablesPerPlayer();
        saveVariablesPerEntity();
    }
    
    private void load()
    {
        _isLoaded = true;
        for ( MtsClosure script : _scripts )
        {
            script.call();
        }
    }
    
    // ========================================
    
    private IVariableStorage getPlayerVariableStorage()
    {
        EntityPlayer player = getAdapter().getPartner();
        String playerName = EntityUtil.getName( player );
        IVariableStorage s = VariableStorageRegistry.instance().getOrCreateVariableStorage( player );
        
        validState( s != null, "Player '%s' has no variable storage attached to them", playerName );
        assert s != null; // added to prevent IDE warnings
        
        if ( !( s instanceof PlayerVariableStorage ) )
        {
            LOG.warn( "Expected storage attached to player '%s' to be of type %s, got %s instead; This is not necessarily an error", playerName,
                      PlayerVariableStorage.class.getName(), s.getClass().getName() );
        }
        
        return s;
    }
    
    private EntityVariableStorage getEntityVariableStorage()
    {
        EntityLiving entity = getAdapter().getEntity();
        String entityName = getAdapter().getDisplayedEntityName();
        IVariableStorage s = VariableStorageRegistry.instance().getOrCreateVariableStorage( entity );
        
        validState( s != null, "Entity '%s' has no variable storage attached to it", entityName );
        assert s != null; // added to prevent IDE warnings
        
        validState( s instanceof EntityVariableStorage, "Expected storage attached to entity '%s' to be of type %s, got %s instead", entityName,
                    EntityVariableStorage.class.getName(), s.getClass().getName() );
        assert s instanceof EntityVariableStorage; // added to prevent IDE warnings
        
        return (EntityVariableStorage) s;
    }
    
    // ========================================
    
    /** Helper method to load saved variables into {@link #_env} and to prevent code duplication */
    private void loadSavedVariables( Collection<String> variableNames, Map<String, MtsValue> savedVariables )
    {
        if ( savedVariables == null || savedVariables.isEmpty() )
        {
            LOG.debug( "Not loading saved variables: no saved variables" );
            return;
        }
        
        LOG.debug( "Loading saved variables..." );
        for ( String variableName : variableNames )
        {
            MtsValue value = savedVariables.get( variableName );
            if ( value == null )
            {
                LOG.debug( "Not loading saved variable '%s': value not present", variableName );
                continue;
            }
            
            LOG.debug( "Loading saved variable '%s' with value '%s'", variableName, value );
            _env.set( variableName, value );
        }
    }
    
    /** Helper method to write saved variables from {@link #_env} into a dedicated map and to prevent code duplication */
    private Map<String, MtsValue> writeSavedVariables( Collection<String> variableNames )
    {
        if ( variableNames == null || variableNames.isEmpty() )
        {
            LOG.debug( "Not writing saved variables: no variable names specified" );
            return new HashMap<>( 0 );
        }
        
        LOG.debug( "Writing saved variables..." );
        Map<String, MtsValue> values = new HashMap<>( variableNames.size(), 1 );
        for ( String variableName : variableNames )
        {
            MtsValue value = _env.get( variableName );
            if ( value == null )
            {
                LOG.debug( "Not writing saved variable '%s': value not present", variableName );
                continue;
            }
            
            if ( MtsToNbtSerialization.isSerializableValue( value ) )
            {
                LOG.debug( "Writing saved variable '%s' with value '%s'", variableName, value );
                values.put( variableName, value );
            }
            else
            {
                LOG.debug( "Not writing saved variable '%s': value '%s' not serializable", variableName, value );
            }
        }
        
        return values;
    }
    
    // ========================================
    
    private void loadVariablesPerPlayerAndEntity()
    {
        String playerName = EntityUtil.getName( getAdapter().getPartner() );
        String mobName = getAdapter().getDisplayedEntityName();
        LOG.debug( "Loading %s for player '%s' and entity '%s'", InteractionScriptConfig.KEY_SAVEDVARIABLESPERPLAYERANDENTITY, playerName, mobName );
        
        Map<String, MtsValue> savedVariables = getEntityVariableStorage().getSavedVariables( getEntry().getIdentifier(), getAdapter().getPartner() );
        loadSavedVariables( getEntry().getSavedVariablesPerPlayerAndEntity(), savedVariables );
        LOG.debug( "Loaded %s for player '%s' and entity '%s'", InteractionScriptConfig.KEY_SAVEDVARIABLESPERPLAYERANDENTITY, playerName, mobName );
    }
    
    private void saveVariablesPerPlayerAndEntity()
    {
        String playerName = EntityUtil.getName( getAdapter().getPartner() );
        String mobName = getAdapter().getDisplayedEntityName();
        LOG.debug( "Saving %s for player '%s' and entity '%s'", InteractionScriptConfig.KEY_SAVEDVARIABLESPERPLAYERANDENTITY, playerName, mobName );
        
        Map<String, MtsValue> savedVariables = writeSavedVariables( getEntry().getSavedVariablesPerPlayerAndEntity() );
        getEntityVariableStorage().setSavedVariables( getEntry().getIdentifier(), getAdapter().getPartner(), savedVariables );
        LOG.debug( "Saved %s for player '%s' and entity '%s'", InteractionScriptConfig.KEY_SAVEDVARIABLESPERPLAYERANDENTITY, playerName, mobName );
    }
    
    // ========================================
    
    private void loadVariablesPerEntity()
    {
        String entityName = getAdapter().getDisplayedEntityName();
        LOG.debug( "Loading %s for entity '%s'", ScriptConfig.KEY_SAVEDVARIABLESPERENTITY, entityName );
        
        Map<String, MtsValue> savedVariables = getEntityVariableStorage().getSavedVariables( getEntry().getIdentifier() );
        loadSavedVariables( getEntry().getSavedVariablesPerEntity(), savedVariables );
        LOG.debug( "Loaded %s for entity '%s'", ScriptConfig.KEY_SAVEDVARIABLESPERENTITY, entityName );
    }
    
    private void saveVariablesPerEntity()
    {
        String entityName = getAdapter().getDisplayedEntityName();
        LOG.debug( "Saving %s for entity '%s'", ScriptConfig.KEY_SAVEDVARIABLESPERENTITY, entityName );
        
        Map<String, MtsValue> savedVariables = writeSavedVariables( getEntry().getSavedVariablesPerEntity() );
        getEntityVariableStorage().setSavedVariables( getEntry().getIdentifier(), savedVariables );
        LOG.debug( "Saved %s for entity '%s'", ScriptConfig.KEY_SAVEDVARIABLESPERENTITY, entityName );
    }
    
    // ========================================
    
    private Map<String, MtsValue> loadVariablesPerPlayerLegacy( EntityPlayer player )
    {
        String name = EntityUtil.getName( player );
        UUID uuid = player.getUniqueID();
        
        // Check if there's any remaining legacy variables for the given player
        if ( _playerSaves.containsKey( uuid ) )
        {
            // Migrate the variables to the new storage
            Map<String, MtsValue> legacyPlayerVariables = _playerSaves.get( uuid );
            if ( migrateSavedVariablesPerPlayer( player, legacyPlayerVariables ) )
            {
                // Remove the variables from the legacy storage
                if ( !_playerSaves.remove( uuid, legacyPlayerVariables ) )
                {
                    LOG.debug( "Could not remove savedVariablesPerPlayer for player '%s' with UUID '%s' from legacy storage", name, uuid );
                }
            }
            
            return legacyPlayerVariables;
        }
        return new HashMap<>();
    }
    
    private void loadVariablesPerPlayer()
    {
        String playerName = EntityUtil.getName( getAdapter().getPartner() );
        LOG.debug( "Loading %s for player '%s'", InteractionScriptConfig.KEY_SAVEDVARIABLESPERPLAYER, playerName );
        
        Map<String, MtsValue> savedVariables = new HashMap<>( loadVariablesPerPlayerLegacy( getAdapter().getPartner() ) );
        try
        {
            // Load saved variables from new variable storage and overwrite any legacy variables
            savedVariables.putAll( getPlayerVariableStorage().getSavedVariables( getEntry().getIdentifier() ) );
        }
        catch ( IllegalStateException e )
        {
            // Only log exceptions in order to continue loading the legacy saved variables
            LOG.warn( e, "Could get variable storage for player '%s'", playerName );
        }
        
        loadSavedVariables( getEntry().getSavedVariablesPerPlayer(), savedVariables );
        LOG.debug( "Loaded %s for player '%s'", InteractionScriptConfig.KEY_SAVEDVARIABLESPERPLAYER, playerName );
    }
    
    private void saveVariablesPerPlayer()
    {
        String name = EntityUtil.getName( getAdapter().getPartner() );
        LOG.debug( "Saving %s for player '%s'", InteractionScriptConfig.KEY_SAVEDVARIABLESPERPLAYER, name );
        
        Map<String, MtsValue> values = writeSavedVariables( getEntry().getSavedVariablesPerPlayer() );
        getPlayerVariableStorage().setSavedVariables( getEntry().getIdentifier(), values );
        LOG.debug( "Saved %s for player '%s'", InteractionScriptConfig.KEY_SAVEDVARIABLESPERPLAYER, name );
    }
    
    /**
     * Migrates legacy savedVariablesPerPlayer to the "new" variable storage, based on the NBT data.
     * <p>
     * TODO Remove the savedVariablesPerPlayer migration methods eventually
     * </p>
     *
     * @return {@code true} on success, {@code false} on failure.
     */
    private boolean migrateSavedVariablesPerPlayer( String id, NBTTagCompound compound )
    {
        UUID uuid = UUID.fromString( id );
        Map<String, MtsValue> savedVariables = MtsToNbtSerialization.deserialize( compound );
        Entity entity = MinecraftServer.getServer().getEntityFromUuid( uuid );
        if ( entity == null )
        {
            LOG.warn( "Could not find entity with UUID '%s'", uuid );
            LOG.warn( "Failed to migrate legacy variable storage for player with UUID '%s'", id );
            return false;
        }
        
        if ( !( entity instanceof EntityPlayer ) )
        {
            LOG.warn( "Entity with UUID '%s' was '%s', expected a player", uuid, entity );
            LOG.warn( "Failed to migrate legacy variable storage for player with UUID '%s'", id );
            return false;
        }
        
        return migrateSavedVariablesPerPlayer( (EntityPlayer) entity, savedVariables );
    }
    
    /**
     * Migrates the legacy savedVariablesPerPlayer to the "new" variable storage. If, for whatever reason, there already are savedVariablesPerPlayer, this
     * method will only migrate variables that aren't already saved.
     * <p>
     * TODO Remove the savedVariablesPerPlayer migration methods eventually
     * </p>
     *
     * @param player         Player object whose variables should be migrated
     * @param savedVariables The old savedVariablesPerPlayer which should be migrated
     *
     * @return {@code true} on success, {@code false} on failure.
     */
    private boolean migrateSavedVariablesPerPlayer( EntityPlayer player, Map<String, MtsValue> savedVariables )
    {
        String name = EntityUtil.getName( player );
        LOG.debug( "Migrating legacy %s to new variable storage for player '%s'", InteractionScriptConfig.KEY_SAVEDVARIABLESPERPLAYER, name );
        
        IVariableStorage storage = getPlayerVariableStorage();
        if ( MobTalkerConfig.isDebugEnabled() )
        {
            for ( Map.Entry<String, MtsValue> entry : savedVariables.entrySet() )
            {
                LOG.debug( "Migrating variable '%s' with value '%s' for player '%s'", entry.getKey(), entry.getValue(), name );
            }
        }
        
        Map<String, MtsValue> current = storage.getSavedVariables( getEntry().getIdentifier() );
        if ( current != null && !current.isEmpty() )
        {
            Map<String, MtsValue> merged = new HashMap<>();
            merged.putAll( current );
            merged.putAll( savedVariables );
            savedVariables = merged;
        }
        
        try
        {
            storage.setSavedVariables( getEntry().getIdentifier(), savedVariables );
            LOG.info( "Legacy %s for player '%s' have been migrated successfully", InteractionScriptConfig.KEY_SAVEDVARIABLESPERPLAYER, name );
        }
        catch ( RuntimeException e )
        {
            LOG.warn( e, "Failed to migrate legacy variable storage for player '%s'", name );
            return false;
        }
        return true;
    }
    
    // ========================================
    
    public NBTTagCompound serialize()
    {
        NBTTagCompound store = new NBTTagCompound();
        
        // Per Player
        if ( !_playerSaves.isEmpty() )
        {
            LOG.debug( "Serializing %d remaining player saves that haven't yet been migrated", _playerSaves.size() );
            for ( Map.Entry<UUID, Map<String, MtsValue>> entry : _playerSaves.entrySet() )
            {
                Map<String, MtsValue> sv = entry.getValue();
                if ( sv.isEmpty() )
                {
                    continue;
                }
                
                UUID uuid = entry.getKey();
                store.setTag( uuid.toString(), MtsToNbtSerialization.serialize( sv ) );
            }
        }
        
        return store;
    }
    
    public void deserialize( NBTTagCompound store )
    {
        if ( store.hasKey( "Global" ) )
        {
            NBTTagCompound tag = store.getCompoundTag( "Global" );
            Map<String, MtsValue> values = MtsToNbtSerialization.deserialize( tag );
            loadSavedVariables( getEntry().getSavedVariablesPerEntity(), values );
        }
        
        @SuppressWarnings( "unchecked" )
        Set<String> keys = store.getKeySet();
        for ( String tagKey : keys )
        {
            if ( "Global".equals( tagKey ) )
            {
                continue;
            }
            
            NBTBase tag = store.getTag( tagKey );
            if ( !( tag instanceof NBTTagCompound ) )
            {
                // silently ignore invalid or missing values
                continue;
            }
            
            NBTTagCompound compound = (NBTTagCompound) tag;
            if ( !migrateSavedVariablesPerPlayer( tagKey, compound ) )
            {
                Map<String, MtsValue> values = MtsToNbtSerialization.deserialize( compound );
                _playerSaves.put( UUID.fromString( tagKey ), values );
            }
        }
    }
}
