/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.interaction;

import static net.mobtalker.mobtalker2.util.ChatMessageHelper.*;

import java.util.concurrent.*;

import net.minecraft.util.EnumChatFormatting;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;
import net.mobtalker.mobtalkerscript.v3.MtsRuntimeException;

import com.google.common.base.Throwables;

public class OnInteractionFutureTask extends FutureTask<InteractionScript>
{
    private static final MobTalkerLog LOG = new MobTalkerLog( "ScriptFuture" );
    
    // ========================================
    
    private final IInteractionAdapter _adapter;
    
    // ========================================
    
    public OnInteractionFutureTask( InteractionScript script )
    {
        super( new Task( script ), null );
        _adapter = script.getAdapter();
    }
    
    // ========================================
    
    @Override
    protected void done()
    {
        try
        {
            get();
            LOG.debug( "Interaction script ran to completion" );
        }
        catch ( InteractionCanceledException | CancellationException ex )
        {
            LOG.debug( "Interaction script was canceled" );
        }
        catch ( ExecutionException ex )
        {
            Throwable cause = Throwables.getRootCause( ex );
            LOG.warn( cause, "The interaction script threw an exception" );
            
            if ( cause instanceof MtsRuntimeException )
            {
                MtsRuntimeException sre = (MtsRuntimeException) cause;
                sendMessageToEntity( _adapter.getPartner(), sre.createStackTrace(), EnumChatFormatting.RED );
            }
            else
            {
                sendMessageToEntity( _adapter.getPartner(), "The script engine crashed. See the server log for details.",
                                     EnumChatFormatting.RED );
            }
        }
        catch ( Exception ex )
        {
            LOG.warn( ex, "Unexpected exception while running an interaction script" );
        }
        
        InteractionRegistry.instance().endInteraction( _adapter );
    }
    
    // ========================================
    
    private static class Task implements Runnable
    {
        private final InteractionScript _script;
        
        public Task( InteractionScript script )
        {
            assert script != null;
            
            _script = script;
        }
        
        @Override
        public void run()
        {
            _script.onInteraction();
        }
    }
}
