/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.interaction;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAITasks;

public interface IInteractionAdapterInitializer<T extends EntityLiving>
{
    void initialize( IInteractionAdapter adapter, T entity, EntityAITasks tasks, EntityAITasks targetTasks );
}
