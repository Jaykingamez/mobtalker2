/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.interaction;

import net.minecraft.entity.EntityLiving;

/**
 * Creates {@link IInteractionAdapter IInteractionAdapter}.
 */
public interface IInteractionAdapterFactory
{
    
    /**
     * Creates an {@link IInteractionAdapter} for the given entity.
     * 
     * @param entity The entity to create an adapter for.
     * @return An {@link IInteractionAdapter} for the given entity.
     */
    IInteractionAdapter create( EntityLiving entity );
    
}
