/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.interaction;

import static net.mobtalker.mobtalker2.server.script.lib.EntityReaction.*;
import static net.mobtalker.mobtalker2.util.EntityUtil.*;
import static org.apache.commons.lang3.Validate.notNull;
import static org.apache.commons.lang3.Validate.validState;

import java.util.*;

import net.minecraft.entity.*;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.player.*;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.*;
import net.minecraft.world.World;
import net.mobtalker.mobtalker2.MobTalker2;
import net.mobtalker.mobtalker2.common.entity.*;
import net.mobtalker.mobtalker2.server.script.ScriptFactory;
import net.mobtalker.mobtalker2.server.script.lib.EntityReaction;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;

import com.google.common.base.Strings;
import com.google.common.collect.Sets;

public class DefaultInteractionAdapter implements IInteractionAdapter
{
    private static final MobTalkerLog LOG = new MobTalkerLog( "InteractionAdapter" );
    
    // ========================================
    
    private final EntityLiving _entity;
    
    /**
     * Cannot make this final since some entities my not have flags set when they're constructed.
     */
    private EntityType _entityType;
    
    private NBTTagCompound _saveData;
    private NBTTagCompound _scriptSaveData;
    
    private EntityPlayerMP _partner;
    private InteractionScript _script;
    private String _scriptName;
    
    private boolean _allowInWater;
    private boolean _allowInAir;
    private boolean _allowAttackers;
    private boolean _allowMovement;
    private double _maxDistance;
    private float _unforgivingness;
    
    private final ReactionList _reactions;
    private final RecentAttackerList _recentAttackers;
    
    private UUID _followedEntityId;
    
    // ========================================
    
    public DefaultInteractionAdapter( EntityLiving entity )
    {
        _entity = notNull( entity );
        
        _allowInWater = true;
        _allowInAir = false;
        _allowAttackers = false;
        _allowMovement = false;
        _maxDistance = 5.0;
        _unforgivingness = 1.0F;
        
        _reactions = new ReactionList();
        _recentAttackers = new RecentAttackerList( this );
    }
    
    // ========================================
    
    @Override
    public final EntityLiving getEntity()
    {
        return _entity;
    }
    
    @Override
    public EntityType getEntityType()
    {
        return ( _entityType != null ) ? _entityType : ( _entityType = EntityType.of( _entity ) );
    }
    
    @Override
    public String getEntityName()
    {
        return getEntityType().getKey();
    }
    
    @Override
    public String getDisplayedEntityName()
    {
        return _entity.hasCustomName() ? _entity.getCustomNameTag() : getEntityName();
    }
    
    @Override
    public float getUnforgivingness()
    {
        return _unforgivingness;
    }
    
    // ========================================
    
    protected long getWorldTime()
    {
        return _entity.worldObj.getTotalWorldTime();
    }
    
    // ========================================
    
    @Override
    public boolean allowsInWater()
    {
        return _allowInWater;
    }
    
    @Override
    public boolean allowsInAir()
    {
        return _allowInAir;
    }
    
    @Override
    public boolean allowsAttackers()
    {
        return _allowAttackers;
    }
    
    @Override
    public boolean allowsMovement()
    {
        return _allowMovement;
    }
    
    @Override
    public void setAllowInWater( boolean allowInWater )
    {
        _allowInWater = allowInWater;
    }
    
    @Override
    public void setAllowInAir( boolean allowInAir )
    {
        _allowInAir = allowInAir;
    }
    
    @Override
    public void setAllowAttackers( boolean allowAttackers )
    {
        _allowAttackers = allowAttackers;
    }
    
    @Override
    public void setAllowMovement( boolean allowMovement )
    {
        _allowMovement = allowMovement;
    }
    
    // ========================================
    
    @Override
    public EntityPlayerMP getPartner()
    {
        return _partner;
    }
    
    @Override
    public void setPartner( EntityPlayerMP player )
    {
        notNull( player );
        validState( !isActive(), "Cannot set interaction partner while already interacting" );
        
        _partner = player;
    }
    
    @Override
    public void clearPartner()
    {
        _partner = null;
    }
    
    @Override
    public boolean isActive()
    {
        return _partner != null;
    }
    
    // ========================================
    
    @Override
    public String getScriptName()
    {
        return _scriptName;
    }
    
    @Override
    public InteractionScript getScript()
    {
        if ( _script == null )
        {
            _script = createScriptInstance();
            loadScriptData();
        }
        
        return _script;
    }
    
    private InteractionScript createScriptInstance()
    {
        if ( Strings.isNullOrEmpty( _scriptName ) )
            return ScriptFactory.instance().createInstance( this, getEntityType() );
        else
            return ScriptFactory.instance().createInstance( this, _scriptName );
        
    }
    
    private void loadScriptData()
    {
        if ( _scriptSaveData != null )
        {
            LOG.trace( "Reading SavedVariables for '%s'", getDisplayedEntityName() );
            
            try
            {
                _script.deserialize( _scriptSaveData );
            }
            catch ( Exception ex )
            {
                LOG.error( ex, "Unable to read SavedVariables for '%s'", getDisplayedEntityName() );
            }
            
            _scriptSaveData = null;
        }
    }
    
    // ========================================
    
    /**
     * Do not call directly.
     */
    @Override
    public final void init( Entity entity, World world )
    {
        assert _entity.equals( entity );
    }
    
    @Override
    public void onSpawn( World world )
    {}
    
    @Override
    public void onWorldChanged()
    {
        LOG.trace( "OnWorldChanged" );
    }
    
    @Override
    public void onDeath()
    {}
    
    @Override
    public void onHitByEntity( EntityLivingBase attacker, DamageSource source, float amount )
    {
        if ( !( attacker instanceof EntityPlayer ) )
            return;
        
        LOG.debug( "'%s' attacked '%s' for '%.3f'",
                   getName( attacker ), getDisplayedEntityName(), amount );
        
        _recentAttackers.addAttacker( attacker.getPersistentID(), getWorldTime(), amount );
    }
    
    @Override
    public void onInteractionStart()
    {
        
    }
    
    @Override
    public void onInteractionEnd()
    {}
    
    @Override
    public void onInteractionCanceled()
    {}
    
    // ========================================
    // Interaction
    
    @Override
    public boolean canStartInteractionWith( EntityPlayerMP player, double maxDistance )
    {
        notNull( player, "player" );
        
        if ( isActive() )
        {
            validState( !player.equals( _partner ),
                        "'%s' tried to start interaction with '%s' twice",
                        getName( player ), getDisplayedEntityName() );
            
            LOG.debug( "'%s' requested interaction with '%s', who is currently interacting with '%s'",
                       getName( player ), getDisplayedEntityName(), getName( _partner ) );
            
            return false;
        }
        
        if ( !canInteractWith( player ) )
            return false;
        
        if ( !checkDistance( _entity, player, maxDistance ) )
        {
            LOG.info( "'%s' cannot interact with '%s': Out of max starting distance (%.3f)",
                      getName( player ), getDisplayedEntityName(), getDistance( _entity, player ) );
            return false;
        }
        
        return true;
    }
    
    @Override
    public boolean canInteractWith( EntityLivingBase target )
    {
        if ( !_entity.isEntityAlive() )
        {
            LOG.info( "'%s' cannot interact with '%s': Entity not alive",
                      getName( target ), getDisplayedEntityName() );
            return false;
        }
        
        if ( !allowsInWater() && ( _entity.isInWater() || _entity.isInLava() ) )
        {
            LOG.info( "'%s' cannot interact with '%s': Entity is in water",
                      getName( target ), getDisplayedEntityName() );
            return false;
        }
        
        if ( !allowsAttackers() && _entity.velocityChanged )
        {
            LOG.info( "'%s' cannot interact with '%s': Entity velocity changed",
                      getName( target ), getDisplayedEntityName() );
            return false;
        }
        
        if ( !allowsInAir() && !_entity.onGround && ( getGroundDistance() > 1.0 ) )
        {
            LOG.info( "'%s' cannot interact with '%s': Entity is falling (%.3f)",
                      getName( target ), getDisplayedEntityName(), getGroundDistance() );
            return false;
        }
        
        if ( !checkDistance( _entity, target, _maxDistance ) )
        {
            LOG.info( "'%s' cannot interact with '%s': Out of max interaction distance (%.3f)",
                      getName( target ), getDisplayedEntityName(), getDistance( _entity, target ) );
            return false;
        }
        
        if ( target.isInWater() || target.isInLava() )
        {
            LOG.info( "'%s' cannot interact with '%s': Target is in water",
                      getName( target ), getDisplayedEntityName() );
            return false;
        }
        
        return true;
    }
    
    private double getGroundDistance()
    {
        BlockPos pos = _entity.worldObj.getPrecipitationHeight( _entity.getPosition() );
        return _entity.posY - pos.getY();
    }
    
    // ========================================
    // Behavior
    
    @Override
    public EntityReaction getReaction( EntityLivingBase entity )
    {
        return _reactions.get( entity );
    }
    
    @Override
    public EntityReaction getReaction( EntityType type )
    {
        return _reactions.get( type );
    }
    
    @Override
    public void setReaction( EntityReaction reaction, EntityLivingBase entity )
    {
        LOG.debug( "Setting behavior of '%s' towards '%s' to '%s'",
                   getDisplayedEntityName(), getName( entity ), reaction.getName() );
        
        _reactions.set( reaction, entity );
        
        // Clear followed entity if we're setting a reaction for it that doesn't allow it
        if ( ( ( reaction == HOSTILE ) || ( reaction == SCARED ) )
             && entity.getPersistentID().equals( _followedEntityId ) )
        {
            clearFollowedEntity();
        }
    }
    
    @Override
    public void setReaction( EntityReaction reaction, EntityType type )
    {
        LOG.debug( "Setting behavior of '%s' towards '%s's to '%s'",
                   getDisplayedEntityName(), type, reaction.getName() );
        
        _reactions.set( reaction, type );
    }
    
    @Override
    public void resetReaction( EntityLivingBase entity )
    {
        _reactions.reset( entity );
    }
    
    @Override
    public void resetReaction( EntityType type )
    {
        _reactions.reset( type );
    }
    
    // ========================================
    
    @Override
    public double getFollowRange()
    {
        IAttributeInstance attr = _entity.getEntityAttribute( SharedMonsterAttributes.followRange );
        return ( attr == null ) ? 16.0 : attr.getAttributeValue();
    }
    
    @Override
    public boolean shouldAttack( EntityLivingBase target )
    {
        if ( ( target == null ) || target.equals( _partner ) )
            return false;
        
        switch ( getReaction( target ) )
        {
            case HOSTILE:
                return true;
            case NEUTRAL:
                return _recentAttackers.wasRecentlyAttackedBy( target.getPersistentID() );
            default:
                return false;
        }
    }
    
    public void forgivePlayer( EntityPlayerMP player )
    {
        _recentAttackers.forgive( player.getPersistentID() );
    }
    
    public Set<EntityPlayerMP> getRecentAttackers()
    {
        _recentAttackers.cleanup();
        
        Set<EntityPlayerMP> entities = Sets.newHashSetWithExpectedSize( _recentAttackers.count() );
        for ( UUID attackerID : _recentAttackers.getRecentAttackers() )
        {
            EntityPlayerMP player = findPlayerByUUID( attackerID );
            if ( player != null ) // Not a player or player is no longer online
            {
                entities.add( player );
            }
        }
        
        return entities;
    }
    
    // ========================================
    
    @Override
    public EntityLivingBase getFollowTarget()
    {
        if ( _followedEntityId == null )
            return null;
        
        return findEntityByUuid( _entity.worldObj, _followedEntityId );
    }
    
    @Override
    public UUID getFollowTargetUuid()
    {
        if ( _followedEntityId == null )
            return null;
        
        return _followedEntityId;
    }
    
    @Override
    public void setFollowedEntity( EntityLivingBase target )
    {
        notNull( target, "target" );
        
        if ( target.equals( this ) )
            throw new IllegalArgumentException( "entity cannot follow itself" );
        
        EntityReaction reaction = getReaction( target );
        if ( ( reaction == HOSTILE ) || ( reaction == SCARED ) )
            throw new IllegalArgumentException( "reaction '" + reaction.getName() + "' towards '" + getName( target ) + "' does not allow following!" );
        
        LOG.debug( "setting followed entity of '%s' to '%s'",
                   getDisplayedEntityName(), getName( target ) );
        
        _followedEntityId = target.getPersistentID();
    }
    
    @Override
    public void clearFollowedEntity()
    {
        LOG.debug( "clearing followed entity of '%s'",
                   getDisplayedEntityName() );
        
        _followedEntityId = null;
    }
    
    // ========================================
    // Saving
    
    private static final String NBTTAG_REACTIONS = "Reactions";
    private static final String NBTTAG_FOLLOWING = "Following";
    private static final String NBTTAG_SCRIPTNAME = "Script";
    private static final String NBTTAG_SCRIPTDATA = "ScriptData";
    
    @Override
    public void saveNBTData( NBTTagCompound compound )
    {
        LOG.trace( "Serializing adapter for '%s'", getDisplayedEntityName() );
        
        NBTTagCompound store = new NBTTagCompound();
        
        // Reactions
        store.setTag( NBTTAG_REACTIONS, _reactions.serialize() );
        
        // Follow
        if ( _followedEntityId != null )
        {
            store.setString( NBTTAG_FOLLOWING, _followedEntityId.toString() );
        }
        
        // Script name
        if ( _scriptName != null )
        {
            store.setString( NBTTAG_SCRIPTNAME, _scriptName );
        }
        
        // Script
        if ( _script != null )
        {
            store.setTag( NBTTAG_SCRIPTDATA, _script.serialize() );
        }
        
        compound.setTag( MobTalker2.ID, store );
    }
    
    // ========================================
    // Loading
    
    @Override
    public void loadNBTData( NBTTagCompound compound )
    {
        _saveData = compound.hasKey( MobTalker2.ID ) ? compound.getCompoundTag( MobTalker2.ID ) : null;
    }
    
    @Override
    public void deserialize()
    {
        if ( _saveData == null )
            return;
        
        LOG.trace( "Deserializing adapter for '%s'", getDisplayedEntityName() );
        
        // Reactions
        if ( _saveData.hasKey( NBTTAG_REACTIONS ) )
        {
            _reactions.deserialize( _saveData.getCompoundTag( NBTTAG_REACTIONS ) );
        }
        
        // Follow
        if ( _saveData.hasKey( NBTTAG_FOLLOWING ) )
        {
            _followedEntityId = UUID.fromString( _saveData.getString( NBTTAG_FOLLOWING ) );
        }
        
        // Script name
        if ( _saveData.hasKey( NBTTAG_SCRIPTNAME ) )
        {
            _scriptName = _saveData.getString( NBTTAG_SCRIPTNAME );
        }
        
        // Script
        if ( _saveData.hasKey( NBTTAG_SCRIPTDATA ) )
        {
            _scriptSaveData = _saveData.getCompoundTag( NBTTAG_SCRIPTDATA );
        }
        
        _saveData = null;
    }
}
