/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.lib;

import static net.mobtalker.mobtalker2.server.script.lib.ScriptApiConstants.*;

import java.util.Map;
import java.util.Map.Entry;

import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.BlockPos;
import net.minecraft.village.MerchantRecipe;
import net.mobtalker.mobtalker2.util.*;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;
import net.mobtalker.mobtalkerscript.v3.value.*;

public class ScriptApiConverter
{
    static final MobTalkerLog LOG = new MobTalkerLog( "ScriptApiConverter" );
    
    public static MtsTable asTable( ItemStack stack )
    {
        MtsTable result = new MtsTable( 0, 3 );
        result.set( KEY_ITEM_NAME, MtsString.of( ItemUtil.getNameForItem( stack ) ) );
        result.set( KEY_ITEM_META, MtsNumber.of( stack.getItemDamage() ) );
        result.set( KEY_ITEM_COUNT, MtsNumber.of( stack.stackSize ) );
        return result;
    }
    
    public static MtsVarargs asVarargs( ItemStack stack )
    {
        return MtsVarargs.of( MtsString.of( ItemUtil.getNameForItem( stack ) ),
                              MtsNumber.of( stack.getItemDamage() ),
                              MtsNumber.of( stack.stackSize ) );
    }
    
    public static MtsTable asTable( MerchantRecipe recipe )
    {
        MtsTable result = new MtsTable( 0, 2 );
        result.set( KEY_VILLAGER_RECIPE_OFFER, asTable( recipe.getItemToSell() ) );
        
        MtsTable price = new MtsTable( 2, 0 );
        price.add( asTable( recipe.getItemToBuy() ) );
        if ( recipe.getSecondItemToBuy() != null )
        {
            price.add( asTable( recipe.getSecondItemToBuy() ) );
        }
        result.set( KEY_VILLAGER_RECIPE_PRICE, price );
        
        return result;
    }
    
    public static MtsTable asTable( PotionEffect effect )
    {
        MtsTable result = new MtsTable( 0, 3 );
        result.set( KEY_EFFECT_NAME, MtsString.of( PotionUtil.getName( effect ) ) );
        result.set( KEY_EFFECT_DURATION, MtsNumber.of( effect.getDuration() ) );
        result.set( KEY_EFFECT_AMPLIFIER, MtsNumber.of( effect.getAmplifier() ) );
        return result;
    }
    
    @Deprecated
    public static MtsTable asTable( BlockPos pos )
    {
        MtsTable result = new MtsTable( 0, 3 );
        result.set( KEY_BLOCKPOS_X, MtsNumber.of( pos.getX() ) );
        result.set( KEY_BLOCKPOS_Y, MtsNumber.of( pos.getY() ) );
        result.set( KEY_BLOCKPOS_Z, MtsNumber.of( pos.getZ() ) );
        return result;
    }
    
    @Deprecated
    public static MtsVarargs asVarargs( BlockPos pos )
    {
        return MtsVarargs.of( MtsNumber.of( pos.getX() ),
                              MtsNumber.of( pos.getY() ),
                              MtsNumber.of( pos.getZ() ) );
    }
}
