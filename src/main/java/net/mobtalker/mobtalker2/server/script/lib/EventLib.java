/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.lib;

import static net.mobtalker.mobtalkerscript.v3.MtsCheck.*;

import java.util.*;

import net.mobtalker.mobtalkerscript.v3.value.*;
import net.mobtalker.mobtalkerscript.v3.value.userdata.*;

public class EventLib
{
    /**
     * <p>
     * TODO <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Event">MobTalker2 Wiki</a>
     * </p>
     * 
     * Event identifier for when a new interaction is started. The string equivalent of this field is {@code "INTERACTION_START"}.
     * 
     * <p>
     * This <i>native field</i> can be used in scripts with {@code Event.INTERACTION_START}.
     * </p>
     * 
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Event">MobTalker2 Wiki</a>
     */
    @MtsNativeField
    public static final MtsString INTERACTION_START = MtsString.of( "INTERACTION_START" );
    
    /**
     * <p>
     * TODO <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Event">MobTalker2 Wiki</a>
     * </p>
     * 
     * Event identifier for when an interaction was ended regularly. The string equivalent of this field is {@code "INTERACTION_END"}.
     * 
     * <p>
     * This <i>native field</i> can be used in scripts with {@code Event.INTERACTION_END}.
     * </p>
     * 
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Event">MobTalker2 Wiki</a>
     */
    @MtsNativeField
    public static final MtsString INTERACTION_END = MtsString.of( "INTERACTION_END" );
    
    /**
     * <p>
     * TODO <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Event">MobTalker2 Wiki</a>
     * </p>
     * 
     * Event identifier for when an interaction was cancelled due to the player manually aborting the script execution or an exception occurring. The string
     * equivalent of this field is {@code "INTERACTION_CANCELLED"}.
     * 
     * <p>
     * This <i>native field</i> can be used in scripts with {@code Event.INTERACTION_CANCELLED}.
     * </p>
     * 
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Event">MobTalker2 Wiki</a>
     */
    @MtsNativeField
    public static final MtsString INTERACTION_CANCELLED = MtsString.of( "INTERACTION_CANCELLED" );
    
    // ========================================
    
    private final Map<String, MtsValue> _handlers;
    
    // ========================================
    
    public EventLib()
    {
        _handlers = new HashMap<>();
    }
    
    // ========================================
    
    /**
     * Registers a function as event handler for the given event type. If {@link MtsValue#Nil nil} is provided as the event handler, this function will call
     * {@link #unregister(MtsValue)} instead.
     * 
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Event.Register(...)}.
     * </p>
     * 
     * @param argEvent event type to register a handler for
     * @param argHandler handler for the event
     * @see #INTERACTION_START
     * @see #INTERACTION_END
     * @see #INTERACTION_CANCELLED
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Event/Register">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public void register( MtsValue argEvent, MtsValue argHandler )
    {
        if ( argHandler.isNil() )
        {
            unregister( argEvent );
            return;
        }
        
        String event = checkString( argEvent, 0 );
        MtsValue handler = checkType( argHandler, 1, MtsType.FUNCTION );
        
        _handlers.put( event, handler );
    }
    
    /**
     * Unregisters the event handler for the given event type.
     * 
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Event.Unregister(...)}.
     * </p>
     * 
     * @param argEvent event type to unregister the handler for
     * @see #INTERACTION_START
     * @see #INTERACTION_END
     * @see #INTERACTION_CANCELLED
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Event/Unregister">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public void unregister( MtsValue argEvent )
    {
        String event = checkString( argEvent, 0 );
        _handlers.remove( event );
    }
    
    /**
     * Checks if any handler is registered for the given event type.
     * 
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Event.IsRegistered(...)}.
     * </p>
     * 
     * @param argEvent event type to check
     * @return {@code MtsBoolean#True true} if an event handler is registered, {@code MtsBoolean#False false} otherwise.
     * @see #INTERACTION_START
     * @see #INTERACTION_END
     * @see #INTERACTION_CANCELLED
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Event/IsRegistered">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public MtsBoolean isRegistered( MtsValue argEvent )
    {
        String event = checkString( argEvent, 0 );
        return MtsBoolean.of( hasHandlerFor( event ) );
    }
    
    /**
     * Retrieves the handler that is registered for the given event type or {@link MtsValue#Nil nil} if none was registered.
     * 
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Event.GetHandler(...)}.
     * </p>
     * 
     * @param argEvent event type to get the handler for
     * @return The event handler for the given event or {@link MtsValue#Nil nil} if none was registered.
     * @see #INTERACTION_START
     * @see #INTERACTION_END
     * @see #INTERACTION_CANCELLED
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Event/GetHandler">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public MtsValue getHandler( MtsValue argEvent )
    {
        String event = checkString( argEvent, 0 );
        MtsValue handler = _handlers.get( event );
        
        return handler != null ? handler : MtsValue.Nil;
    }
    
    // ========================================
    
    public MtsValue getHandler( String event )
    {
        return _handlers.get( event );
    }
    
    public boolean hasHandlers()
    {
        return _handlers.size() > 0;
    }
    
    public boolean hasHandlerFor( String event )
    {
        return _handlers.containsKey( event );
    }
    
    public boolean hasHandlerFor( MtsString event )
    {
        return hasHandlerFor( event.toJava() );
    }
    
    public MtsVarargs invoke( String event, MtsVarargs args )
    {
        MtsValue handler = _handlers.get( event );
        if ( handler == null )
            return MtsVarargs.Empty;
        
        return handler.call( args );
    }
    
    public MtsVarargs invoke( MtsString event, MtsVarargs args )
    {
        return invoke( event.toJava(), args );
    }
    
    public MtsVarargs invoke( String event, MtsValue... args )
    {
        return invoke( event, MtsVarargs.of( args ) );
    }
    
    public MtsVarargs invoke( MtsString event, MtsValue... args )
    {
        return invoke( event.toJava(), args );
    }
    
    public MtsVarargs invoke( String event )
    {
        return invoke( event, MtsVarargs.Empty );
    }
    
    public MtsVarargs invoke( MtsString event )
    {
        return invoke( event.toJava() );
    }
}
