/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.lib.metatables;

import java.util.Set;

import net.mobtalker.mobtalkerscript.v3.value.MtsTable;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNatives;

public class StringMetatableRegistry extends AbstractMetatableRegistry<String>
{
    public StringMetatableRegistry()
    {}
    
    // ========================================
    
    @Override
    protected MtsTable build( String key )
    {
        Set<Class<?>> libaryClasses = _libraryClasses.get( key );
        MtsTable lib = new MtsTable( 0, 0 );
        
        for ( Class<?> libaryClass : libaryClasses )
        {
            MtsNatives.createLibrary( lib, libaryClass );
        }
        
        return lib;
    }
}
