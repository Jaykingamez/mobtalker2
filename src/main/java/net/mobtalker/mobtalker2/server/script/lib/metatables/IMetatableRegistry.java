/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.lib.metatables;

import net.mobtalker.mobtalkerscript.v3.value.MtsTable;

public interface IMetatableRegistry<T>
{
    void register( T key, Class<?>... libraries );
    
    void build();
    
    boolean isBuilt();
    
    MtsTable get( T key );
}
