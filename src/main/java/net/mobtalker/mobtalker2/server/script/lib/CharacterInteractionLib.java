/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.lib;

import static net.mobtalker.mobtalkerscript.v3.MtsCheck.checkString;

import net.mobtalker.mobtalker2.common.entity.EntityType;
import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;
import net.mobtalker.mobtalker2.server.script.values.*;
import net.mobtalker.mobtalkerscript.v3.value.MtsValue;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNativeFunction;

public class CharacterInteractionLib
{
    private final IInteractionAdapter _adapter;
    
    private final MtsCharacter _creatureCharacter;
    
    // ========================================
    
    public CharacterInteractionLib( IInteractionAdapter adapter )
    {
        _adapter = adapter;
        _creatureCharacter = new MtsEntityCharacter( adapter.getEntity(), adapter.getEntityType() );
    }
    
    // ========================================
    
    /**
     * Modifies the texture path of this interaction's entity character and returns it.
     * 
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Character.ForCreature(...)}.
     * </p>
     * 
     * @param argTexturePath base texture path; if nil, defaults to the {@linkplain IInteractionAdapter#getEntityName() adapter's entity name}
     * @return the character for the entity of this interaction
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Character/ForCreature">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public MtsValue forCreature( MtsValue argTexturePath )
    {
        String texturePath = checkString( argTexturePath, 0, _adapter.getEntityName() );
        
        _creatureCharacter.setTexturePath( texturePath );
        return _creatureCharacter;
    }
    
    /**
     * Creates a new character for this interaction's player and returns it.
     * 
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Character.ForPlayer(...)}.
     * </p>
     * 
     * @param argTexturePath base texture path; if nil, defaults to {@code "Player"}
     * @return a new character for the player of this interaction
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Character/ForPlayer">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public MtsValue forPlayer( MtsValue argTexturePath )
    {
        String texturePath = checkString( argTexturePath, 0, "Player" );
        
        MtsCharacter character = new MtsInteractingPlayerCharacter( _adapter, EntityType.Player );
        character.setTexturePath( texturePath );
        return character;
    }
}
