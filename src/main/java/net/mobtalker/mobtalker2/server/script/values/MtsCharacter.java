/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.values;

import com.google.common.collect.Lists;
import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.village.MerchantRecipe;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import net.mobtalker.mobtalker2.common.entity.EntityType;
import net.mobtalker.mobtalker2.common.entity.VillagerUtil;
import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;
import net.mobtalker.mobtalker2.server.script.lib.EntityReaction;
import net.mobtalker.mobtalker2.server.script.lib.ScriptApiConverter;
import net.mobtalker.mobtalker2.util.*;
import net.mobtalker.mobtalker2.util.builders.LookAtBuilder;
import net.mobtalker.mobtalkerscript.v3.MtsArgumentException;
import net.mobtalker.mobtalkerscript.v3.MtsCheck;
import net.mobtalker.mobtalkerscript.v3.MtsRuntimeException;
import net.mobtalker.mobtalkerscript.v3.value.*;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNativeFunction;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import static net.mobtalker.mobtalker2.server.script.lib.ScriptApiCheck.*;
import static net.mobtalker.mobtalker2.server.script.lib.ScriptApiConstants.*;
import static net.mobtalker.mobtalker2.util.ItemUtil.matchItem;
import static net.mobtalker.mobtalkerscript.v3.MtsCheck.*;
import static net.mobtalker.mobtalkerscript.v3.value.MtsBoolean.False;
import static net.mobtalker.mobtalkerscript.v3.value.MtsBoolean.True;
import static org.apache.commons.lang3.Validate.notNull;

public abstract class MtsCharacter extends MtsValueWithMetaTable
{
    public static MtsCharacter of( EntityPlayer player )
    {
        return of( player, EntityType.Player );
    }
    
    public static MtsCharacter of( EntityLivingBase entity, EntityType type )
    {
        notNull( entity );
        notNull( type );
        return new MtsEntityCharacter( entity, type );
    }
    
    // ========================================
    
    public static final MtsType TYPE = MtsType.forName( "character" );
    
    // ========================================
    
    private String _texturePath;
    
    // ========================================
    
    protected MtsCharacter()
    {
    }
    
    public MtsCharacter( MtsTable metatable )
    {
        setMetaTable( metatable );
    }
    
    // ========================================
    
    public boolean hasEntity()
    {
        return false;
    }
    
    public <T extends EntityLivingBase> T getEntity()
    {
        throw new MtsRuntimeException( "character is not attached to an entity" );
    }
    
    public EntityType getEntityType()
    {
        throw new MtsRuntimeException( "character is not attached to an entity" );
    }
    
    // ========================================
    
    public abstract String getName();
    
    public abstract void setName( String name );
    
    public abstract void clearName();
    
    // ========================================
    
    public String getTexturePath()
    {
        return _texturePath;
    }
    
    public void setTexturePath( String texturePath )
    {
        _texturePath = texturePath;
    }
    
    // ========================================
    
    @Override
    public MtsType getType()
    {
        return TYPE;
    }
    
    // ========================================
    
    @Override
    public String toString()
    {
        return TYPE.getName() + "[" + getName() + "]";
    }
    
    // ========================================
    
    public static class Methods
    {
        /**
         * Returns whether a character is attached to an entity.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :HasEntity()} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to check
         *
         * @return {@link MtsBoolean#True true} if the character is attach to an entity, {@link MtsBoolean#False false} otherwise.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/HasEntity">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean hasEntity( MtsValue argCharacter )
        {
            return MtsBoolean.of( checkCharacter( argCharacter, 0 ).hasEntity() );
        }
        
        // ========================================
        
        /**
         * Returns the name of a character.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetName()} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to get the name for
         *
         * @return The name of {@code argCharacter}.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/GetName">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsString getName( MtsValue argCharacter )
        {
            return MtsString.of( checkCharacter( argCharacter, 0 ).getName() );
        }
        
        /**
         * Sets a custom name for a character.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :SetName()} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to set the name for
         * @param argName      The name that should be set
         *
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/SetName">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void setName( MtsValue argCharacter, MtsValue argName )
        {
            MtsCharacter character = checkCharacter( argCharacter, 0 );
            
            if ( argName.isNil() )
            {
                character.clearName();
            }
            else
            {
                character.setName( checkString( argName, 0 ) );
            }
        }
        
        // ========================================
        
        /**
         * Returns the entity type of a character.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetType()} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to get the entity type for
         *
         * @return A string which represents the key of the character's entity type.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/GetType">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsString getType( MtsValue argCharacter )
        {
            return MtsString.of( checkCharacter( argCharacter, 0 ).getEntityType().getKey() );
        }
        
        // ========================================
        
        /**
         * Returns the world a character is currently in.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetWorld()} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to get the current world for
         *
         * @return The world the character is in.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/GetWorld">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsWorld getWorld( MtsValue argCharacter )
        {
            EntityLivingBase entity = checkCharacter( argCharacter, 0 ).getEntity();
            return MtsWorld.of( entity.worldObj );
        }
        
        /**
         * Returns the biome a character is currently in.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetBiome()} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to get the biome for
         *
         * @return The biome the character is in.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/GetWorld">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBiome getBiome( MtsValue argCharacter )
        {
            EntityLivingBase entity = checkCharacter( argCharacter, 0 ).getEntity();
            World world = entity.worldObj;
            return MtsBiome.of( MtsWorld.of( world ), world.getBiomeGenForCoords( entity.getPosition() ) );
        }
        
        // ========================================
        
        /**
         * Returns information on a character's health.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetHealth()} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to get health information on
         *
         * @return A varargs value. The first value (index 0) is the character's current health. The second value (index 1) is the character's maximum health.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/GetHealth">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVarargs getHealth( MtsValue argCharacter )
        {
            EntityLivingBase entity = checkCharacter( argCharacter, 0 ).getEntity();
            return MtsVarargs.of( MtsNumber.of( entity.getHealth() ),
                                  MtsNumber.of( entity.getMaxHealth() ) );
        }
        
        /**
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Character/Heal">http://mobtalker.net/wiki/Script_API/Types/Character/Heal</a>
         *
         * Heals a given amount of half hearts on a character.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :Heal(...)} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to heal
         * @param argAmount    amount of half hearts to heal; if nil, defaults to 1
         *
         * @return The new health of the character.
         * @see EntityLivingBase#heal(float)
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/Heal">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber heal( MtsValue argCharacter, MtsValue argAmount )
        {
            EntityLivingBase entity = checkCharacter( argCharacter, 0 ).getEntity();
            entity.heal( checkIntegerWithMinimum( argAmount, 1, 0, 1 ) );
            return MtsNumber.of( entity.getHealth() );
        }
        
        /**
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Character/Damage">http://mobtalker.net/wiki/Script_API/Types/Character/Damage</a>
         *
         * Deals a given amount of damage on a character.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :Damage(...)} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to damage
         * @param argAmount    amount of damage to deal, in half hearts; if nil, defaults to 1
         *
         * @return A varargs value. The first value (index 0) is the new health of the character. The second value (index 1) is either {@code true} or {@code
         * false}, depending on if the character could be damaged or not.
         * @see EntityLivingBase#attackEntityFrom(DamageSource, float)
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/Damage">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVarargs damage( MtsValue argCharacter, MtsValue argAmount )
        {
            EntityLivingBase entity = checkCharacter( argCharacter, 0 ).getEntity();
            boolean success = entity.attackEntityFrom( DamageSource.generic,
                                                       checkIntegerWithMinimum( argAmount, 1, 0, 1 ) );
            return MtsVarargs.of( MtsNumber.of( entity.getHealth() ), MtsBoolean.of( success ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Character/Explode">wiki article</a>
         * </p>
         *
         * Creates a new explosion at the current location of this character, caused by this character.
         *
         * <p>
         * This <i>method</i> can be used by appending {@code :Explode(...)} to a {@code Character} value
         * </p>
         *
         * @param argCharacter character at whose position an explosion should be created
         * @param argStrength  strength of the explosion
         * @param argOptions   (optional) additional options
         *
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Character/Explode">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void explode( MtsValue argCharacter, MtsValue argStrength, MtsValue argOptions )
        {
            EntityLivingBase entity = checkEntity( argCharacter, 0 );
            World world = entity.getEntityWorld();
            float strength = (float) checkNumber( argStrength, 1 );
            boolean flaming = false;
            boolean smoking = false;
            
            if ( !argOptions.isNil() )
            {
                flaming = checkOptionAndApply( argOptions, 2, KEY_EXPLOSION_FLAMING, MtsValue::isTrue );
                smoking = checkOptionAndApply( argOptions, 2, KEY_EXPLOSION_SMOKING, MtsValue::isTrue );
            }
            
            world.newExplosion( entity, entity.posX, entity.posY, entity.posZ, strength, flaming, smoking );
        }
        
        // ========================================
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Character/GetPosition">wiki</a>
         * </p>
         *
         * Returns a character's location in the world it's in.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetPosition()} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to get the current location for
         *
         * @return A vector which represents the character's position.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/GetPosition">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVector getPosition( MtsValue argCharacter )
        {
            EntityLivingBase entity = checkCharacter( argCharacter, 0 ).getEntity();
            return MtsVector.of( entity.getPositionVector() );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Character/GetEyePosition">wiki article</a>
         * </p>
         *
         * Returns the position of a character's eyes.
         *
         * <p>
         * This <i>method</i> can be used in scrips by appending {@code :GetEyePosition()} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character whose eye position should be determined
         *
         * @return A vector which represents the position of the character's eyes.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Character/GetEyePosition">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVector getEyePosition( MtsValue argCharacter )
        {
            EntityLivingBase entity = checkCharacter( argCharacter, 0 ).getEntity();
            return MtsVector.of( entity.getPositionVector().addVector( 0, entity.getEyeHeight(), 0 ) );
        }
        
        // ========================================
        
        /**
         * Returns whether a character is riding on another entity.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :IsRiding()} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to check
         *
         * @return {@link MtsBoolean#True true} if the character is riding on another entity, {@link MtsBoolean#False false} otherwise.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/IsRiding">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean isRiding( MtsValue argCharacter )
        {
            EntityLivingBase entity = checkCharacter( argCharacter, 0 ).getEntity();
            return MtsBoolean.of( entity.ridingEntity != null );
        }
        
        /**
         * Returns whether a character is a child or not.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :IsChild()} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to check
         *
         * @return {@link MtsBoolean#True true} if the character is a child, {@link MtsBoolean#False false} otherwise.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/IsChild">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean isChild( MtsValue argCharacter )
        {
            EntityLivingBase entity = checkCharacter( argCharacter, 0 ).getEntity();
            return MtsBoolean.of( ( entity instanceof EntityAgeable ) && ( (EntityAgeable) entity ).isChild() );
        }
        
        // ========================================
        
        /**
         * Returns a list of active potion effects on a character.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetEffects()} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to get the potion effects for
         *
         * @return A table listing detailed information on all active potion effects.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/GetEffects">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsTable getEffects( MtsValue argCharacter )
        {
            EntityLivingBase entity = checkCharacter( argCharacter, 0 ).getEntity();
            
            @SuppressWarnings( "unchecked" )
            Collection<PotionEffect> effects = entity.getActivePotionEffects();
            
            MtsTable result = new MtsTable( effects.size(), 0 );
            for ( PotionEffect effect : effects )
            {
                result.list().add( ScriptApiConverter.asTable( effect ) );
            }
            
            return result;
        }
        
        /**
         * Applies a potion effect to a character.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :ApplyEffect(...)} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to apply the potion effect on
         * @param argEffect    string which equals the potion effect's name
         * @param argDuration  duration of the effect, in ticks
         * @param argAmplifier amplifier of the effect; if nil, defaults to 0
         *
         * @return {@link MtsBoolean#True true} if the effect could be applied, {@link MtsBoolean#False false} otherwise.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/ApplyEffect">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean applyEffect( MtsValue argCharacter,
                                              MtsValue argEffect,
                                              MtsValue argDuration,
                                              MtsValue argAmplifier )
        {
            EntityLivingBase entity = checkCharacter( argCharacter, 0 ).getEntity();
            int duration = checkIntegerWithMinimum( argDuration, 1, 0 );
            int amplifier = checkIntegerWithMinimum( argAmplifier, 2, 0, 0 );
            String name = checkString( argEffect, 0 );
            if ( PotionUtil.getForName( name ) == null )
            {
                throw new MtsArgumentException( 0, "'%s' is not a valid potion effect", name );
            }
            
            PotionEffect potionEffect = PotionUtil.getEffect( name, duration, amplifier );
            
            if ( !entity.isPotionApplicable( potionEffect ) )
            {
                return MtsBoolean.False;
            }
            
            entity.addPotionEffect( potionEffect );
            return MtsBoolean.True;
        }
        
        /**
         * Removes a potion effect from a character.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :RemoveEffect(...)} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to remove the potion effect from
         * @param argEffect    string which equals the potion effect's name or number which equals the potion effect's ID
         *
         * @return {@code MtsBoolean#True true} if the effect was active and could be removed, {@code MtsBoolean#False false} otherwise.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/RemoveEffect">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean removeEffect( MtsValue argCharacter, MtsValue argEffect )
        {
            EntityLivingBase entity = checkCharacter( argCharacter, 0 ).getEntity();
            String name = checkString( argEffect, 0 );
            if ( PotionUtil.getForName( name ) == null )
            {
                throw new MtsArgumentException( 0, "'%s' is not a valid potion effect", name );
            }
            
            Potion potion = PotionUtil.getForName( name );
            
            if ( !entity.isPotionActive( potion ) )
            {
                return MtsBoolean.False;
            }
            
            entity.removePotionEffect( potion.getId() );
            return MtsBoolean.True;
        }
        
        /**
         * Removes all active potion effects from a character.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :RemoveAllEffects(...)} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to remove the potion effects from
         *
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/RemoveAllEffects">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void removeAllEffects( MtsValue argCharacter )
        {
            EntityLivingBase entity = checkCharacter( argCharacter, 0 ).getEntity();
            
            @SuppressWarnings( "unchecked" )
            List<PotionEffect> effects = Lists.newArrayList( entity.getActivePotionEffects() );
            for ( PotionEffect effect : effects )
            {
                entity.removePotionEffect( effect.getPotionID() );
            }
        }
        
        // ========================================
        
        /**
         * Returns the equipment of a character.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetEquipment(...)} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to get the equipment for
         * @param argSlot      Minecraft slot to query
         *
         * @return If {@code argSlot} is nil, a table listing key-value pairs of all equipment slots and detailed information on their respective items.
         * Otherwise, a table listing detailed information on the equipped item in the given slot or nil if the slot is empty.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/GetEquipment">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsValue getEquipment( MtsValue argCharacter, MtsValue argSlot )
        {
            EntityLivingBase entity = checkCharacter( argCharacter, 0 ).getEntity();
            
            if ( argSlot.isNil() )
            {
                MtsTable result = new MtsTable( 0, EquipmentSlot.values().length );
                for ( EquipmentSlot slot : EquipmentSlot.values() )
                {
                    ItemStack stack = entity.getEquipmentInSlot( slot.getID() );
                    if ( stack == null )
                    {
                        continue;
                    }
                    
                    result.set( slot.getKey(), ScriptApiConverter.asTable( stack ) );
                }
                
                return result;
            }
            
            String slotName = MtsCheck.checkString( argSlot, 0 );
            EquipmentSlot slot = EquipmentSlot.forName( slotName );
            if ( slot == null )
            {
                throw new MtsArgumentException( 0, "'%s' is not a valid equipment slot", slotName );
            }
            
            ItemStack stack = entity.getEquipmentInSlot( slot.getID() );
            if ( stack == null )
            {
                return Nil;
            }
            
            return ScriptApiConverter.asTable( stack );
        }
        
        /**
         * Returns detailed information on the held item of a character.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetHeldItem()} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to get the held item for
         *
         * @return A table which maps detailed information on the held item or nil if the character is not holding any item.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/GetHeldItem">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsValue getHeldItem( MtsValue argCharacter )
        {
            EntityLivingBase entity = checkCharacter( argCharacter, 0 ).getEntity();
            
            ItemStack stack = entity.getHeldItem();
            if ( stack == null )
            {
                return Nil;
            }
            
            return ScriptApiConverter.asTable( stack );
        }
        
        // ========================================
        
        /**
         * Returns the unique ID of a character. These so-called UUIDs (<b>u</b>niversally <b>u</b>nique <b>id</b>entifiers) can be used to identify an entity
         * and are more reliable than e.g. a player's name, because UUIDs can't change.
         *
         * <p>
         * UUIDs have the following format:
         * <pre><code>xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx</code></pre>
         * Each of the Xes above represents a hexadecimal digit, which has to be either a digit or a character from A to F.
         * </p>
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetUniqueID()} to a {@code character} value.
         * </p>
         *
         * @param character The character for which the UUID should be returned
         *
         * @return A string representation of the character entity's UUID.
         * @see UUID#toString()
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/Character/GetUniqueID">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsString getUniqueID( MtsValue character )
        {
            EntityLivingBase entity = checkCharacter( character, 0 ).getEntity();
            return MtsString.of( entity.getUniqueID().toString() );
        }
    }
    
    // ========================================
    
    public static class CreatureMethods
    {
        /**
         * Sets the equipment of a character in a given equipment slot.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :SetEquipment(...)} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to set the equipment for
         * @param argSlot      slot into which the item should be put
         * @param argItemName  fully qualified name of the item that should be used
         * @param argMeta      metadata/damage value of the item that should be used; if nil, defaults to 0
         *
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/SetEquipment">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void setEquipment( MtsValue argCharacter, MtsValue argSlot, MtsValue argItemName,
                                         MtsValue argMeta )
        {
            EntityLiving entity = checkCharacter( argCharacter, 0 ).getEntity();
            String slotName = checkString( argSlot, 1 );
            EquipmentSlot slot = EquipmentSlot.forName( slotName );
            if ( slot == null )
            {
                throw new MtsArgumentException( 0, "'%s' is not a valid equipment slot", slotName );
            }
            
            String itemName = checkString( argItemName, 2 );
            if ( !ItemUtil.isValidItem( itemName ) )
            {
                throw new MtsArgumentException( 1, "unknown item name '%s'", itemName );
            }
            
            int meta = checkIntegerWithMinimum( argMeta, 2, 0, 0 );
            
            Item item = ItemUtil.getItemForName( itemName );
            ItemStack itemStack = new ItemStack( item, 1, meta );
            
            entity.setCurrentItemOrArmor( slot.getID(), itemStack );
        }
        
        // ========================================
        
        /**
         * Returns the reaction of a character towards a character or entity type.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetReaction(...)} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to determine the reaction from
         * @param argTarget    character or string representing an entity type towards which the reaction should be determined
         *
         * @return A string representation of the character's reaction.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/GetReaction">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsValue getReaction( MtsValue argCharacter, MtsValue argTarget )
        {
            EntityLiving entity = checkCharacter( argCharacter, 0 ).getEntity();
            
            EntityReaction reaction;
            if ( argTarget.is( MtsCharacter.TYPE ) )
            {
                IInteractionAdapter adapter = checkAdapter( entity, false );
                reaction = adapter.getReaction( ( (MtsCharacter) argTarget ).getEntity() );
            }
            else if ( argTarget.isString() )
            {
                String key = argTarget.asString().toJava();
                if ( key.startsWith( ENTITYKEY_PREFIX_TYPE ) )
                {
                    String typeString = key.substring( ENTITYKEY_PREFIX_TYPE.length() );
                    EntityType type = checkEntityType( typeString, 1 );
                    
                    IInteractionAdapter adapter = checkAdapter( entity, false );
                    reaction = adapter.getReaction( type );
                }
                else if ( key.equals( ENTITYKEY_PLAYER ) )
                {
                    IInteractionAdapter adapter = checkAdapter( entity, true );
                    reaction = adapter.getReaction( adapter.getPartner() );
                }
                else
                {
                    throw new MtsArgumentException( 1, "'%s' is not a valid identifier", key );
                }
            }
            else
            {
                throw new MtsArgumentException( 1, MtsType.STRING, MtsCharacter.TYPE, argTarget.getType() );
            }
            
            return MtsString.of( reaction.getName() );
        }
        
        /**
         * Sets the reaction of a character towards a character or entity type.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :SetReaction(...)} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to set the reaction for
         * @param argTarget    character or string representing an entity type towards which the reaction should be set
         * @param argReaction  reaction which should be set
         *
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/SetReaction">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void setReaction( MtsValue argCharacter, MtsValue argTarget, MtsValue argReaction )
        {
            EntityLiving entity = checkCharacter( argCharacter, 0 ).getEntity();
            EntityReaction reaction = checkReaction( argReaction, 2 );
            
            if ( argTarget.is( MtsCharacter.TYPE ) )
            {
                IInteractionAdapter adapter = checkAdapter( entity, false );
                MtsCharacter target = (MtsCharacter) argTarget;
                EntityType type = target.getEntityType();
                if ( ( reaction == EntityReaction.HOSTILE ) && ( EntityType.Creeper.equals( type ) || EntityType.Ghast.equals(
                        type ) ) )
                {
                    throw new MtsArgumentException( "Can not set reaction towards '%s' to hostile", type.getKey() );
                }
                
                adapter.setReaction( reaction, target.getEntity() );
            }
            else if ( argTarget.isString() )
            {
                String key = argTarget.asString().toJava();
                if ( key.startsWith( ENTITYKEY_PREFIX_TYPE ) )
                {
                    String typeString = key.substring( ENTITYKEY_PREFIX_TYPE.length() );
                    EntityType type = checkEntityType( typeString, 1 );
                    
                    if ( ( reaction == EntityReaction.HOSTILE ) && ( EntityType.Creeper.equals( type ) || EntityType.Ghast
                                                                                                                  .equals( type ) ) )
                    {
                        throw new MtsArgumentException( "Can not set reaction towards '%s' to hostile", type.getKey() );
                    }
                    
                    IInteractionAdapter adapter = checkAdapter( entity, false );
                    adapter.setReaction( reaction, type );
                }
                else if ( key.equals( ENTITYKEY_PLAYER ) )
                {
                    IInteractionAdapter adapter = checkAdapter( entity, true );
                    adapter.setReaction( reaction, adapter.getPartner() );
                }
                else
                {
                    throw new MtsArgumentException( 1, "'%s' is not a valid identifier", key );
                }
            }
            else
            {
                throw new MtsArgumentException( 1, MtsType.STRING, MtsCharacter.TYPE, argTarget.getType() );
            }
        }
        
        /**
         * Resets the reaction of a character towards a character or entity type.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :ResetReaction(...)} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character to reset the reaction for
         * @param argTarget    character or string representing an entity type towards which the reaction should be reset
         *
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/ResetReaction">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void resetReaction( MtsValue argCharacter, MtsValue argTarget )
        {
            EntityLiving entity = checkCharacter( argCharacter, 0 ).getEntity();
            
            if ( argTarget.is( MtsCharacter.TYPE ) )
            {
                IInteractionAdapter adapter = checkAdapter( entity, false );
                adapter.resetReaction( ( (MtsCharacter) argTarget ).getEntity() );
            }
            else if ( argTarget.isString() )
            {
                String key = argTarget.asString().toJava();
                if ( key.startsWith( ENTITYKEY_PREFIX_TYPE ) )
                {
                    String typeString = key.substring( ENTITYKEY_PREFIX_TYPE.length() );
                    EntityType type = checkEntityType( typeString, 1 );
                    
                    IInteractionAdapter adapter = checkAdapter( entity, false );
                    adapter.resetReaction( type );
                }
                else if ( key.equals( ENTITYKEY_PLAYER ) )
                {
                    IInteractionAdapter adapter = checkAdapter( entity, true );
                    adapter.resetReaction( adapter.getPartner() );
                }
                else
                {
                    throw new MtsArgumentException( 1, "'%s' is not a valid identifier", key );
                }
            }
            else
            {
                throw new MtsArgumentException( 1, MtsType.STRING, MtsCharacter.TYPE, argTarget.getType() );
            }
        }
        
        // ========================================
        
        /**
         * Makes a character follow an entity.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :Follow(...)} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character that should follow
         * @param argTarget    character or string representing the entity to follow
         *
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/Follow">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void follow( MtsValue argCharacter, MtsValue argTarget )
        {
            EntityLiving entity = checkCharacter( argCharacter, 0 ).getEntity();
            
            if ( argTarget.isString() )
            {
                String key = argTarget.asString().toJava().toLowerCase();
                if ( !key.equals( ENTITYKEY_PLAYER ) )
                {
                    throw new MtsArgumentException( 1, "'%s' is not a valid identifier", key );
                }
                
                IInteractionAdapter adapter = checkAdapter( entity, true );
                adapter.setFollowedEntity( adapter.getPartner() );
            }
            else if ( argTarget.is( MtsCharacter.TYPE ) )
            {
                EntityLivingBase target = ( (MtsCharacter) argTarget ).getEntity();
                checkAdapter( entity, false ).setFollowedEntity( target );
            }
        }
        
        /**
         * Makes a character stop following an entity.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :StopFollowing()} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character that should stop following
         *
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/StopFollowing">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void stopFollowing( MtsValue argCharacter )
        {
            EntityLiving entity = checkCharacter( argCharacter, 0 ).getEntity();
            IInteractionAdapter adapter = checkAdapter( entity, false );
            adapter.clearFollowedEntity();
        }
        
        // ========================================
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Character/LookAt">wiki article</a>
         * </p>
         *
         * Makes a character look at a position or entity.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :LookAt(...)} to a {@code Character} value.
         * </p>
         *
         * @param args a varargs argument for which the allowed argument types are:
         *             <ul>
         *             <li><code>&lt;character&gt; &lt;character&gt; [ &lt;number&gt; [number] ]</code></li>
         *             <li><code>&lt;character&gt; &lt;vector&gt; [ &lt;number&gt; [number] ]</code></li>
         *             <li><code>&lt;character&gt; &lt;table&gt; [ &lt;number&gt; [number] ]</code></li>
         *             <li><code>&lt;character&gt; &lt;number&gt; &lt;number&gt; &lt;number&gt; [ &lt;number&gt; [number] ]</code></li>
         *             </ul>
         *
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Character/LookAt">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void lookAt( MtsVarargs args )
        {
            Counter c = new Counter( 0 );
            EntityLiving entity = checkCharacter( args, c.postIncrement() ).getEntity();
            LookAtBuilder lab = new LookAtBuilder( entity.getLookHelper() );
            
            if ( args.get( c.value() ).is( MtsCharacter.TYPE ) )
            {
                lab.withEntityTarget( ( (MtsCharacter) args.get( c.postIncrement() ) ).getEntity() );
            }
            else
            {
                lab.withVectorTarget( checkVector( args, c, true, false ) );
            }
            
            if ( !args.get( c.value() ).isNil() )
            {
                lab.withYawDelta( (float) checkNumber( args, c.postIncrement() ) );
                if ( !args.get( c.value() ).isNil() )
                {
                    lab.withPitchDelta( (float) checkNumber( args, c.postIncrement() ) );
                }
            }
            
            lab.run();
        }
    }
    
    // ========================================
    
    public static class CreeperMethods
    {
        /**
         * Returns whether a character is charged.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :IsCharged()} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character which should be checked
         *
         * @return {@link MtsBoolean#True true} if the character is charged, {@link MtsBoolean#False false} otherwise.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/IsCharged">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean isCharged( MtsValue argCharacter )
        {
            EntityCreeper entity = checkCharacter( argCharacter, 0 ).getEntity();
            return MtsBoolean.of( entity.getPowered() );
        }
    }
    
    // ========================================
    
    public static class PlayerMethods
    {
        /**
         * Returns the game mode of a character.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetGameMode()} to a {@code Character} value.
         * </p>
         *
         * @param character character to get the game mode for
         *
         * @return A string representation of the character's current game mode
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/GetGameMode">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsString getGameMode( MtsCharacter character )
        {
            EntityPlayerMP player = checkCharacter( character, 0 ).getEntity();
            return MtsString.of( player.theItemInWorldManager.getGameType().getName() );
        }
        
        // ========================================
        
        /**
         * Returns the total armor value of the character.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetArmor()} to a {@code Character} value.
         * </p>
         *
         * @param character character to get the armor value for
         *
         * @return A number which represents the character's current armor value
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/GetArmor">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber getArmor( MtsCharacter character )
        {
            EntityPlayer player = checkCharacter( character, 0 ).getEntity();
            return MtsNumber.of( ForgeHooks.getTotalArmorValue( player ) );
        }
        
        // ========================================
        
        /**
         * Returns information on a character's levl progress.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetExperience()} to a {@code Character} value.
         * </p>
         *
         * @param character character to get the experience information for
         *
         * @return A varargs value. The first value (index 0) is the current experience level of the character. The second value (index 1) is the current
         * progress towards the next level. The third value (index 2) is the amount of experience needed to proceed to gain another level.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/GetExperience">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVarargs getExperience( MtsCharacter character )
        {
            EntityPlayer player = checkCharacter( character, 0 ).getEntity();
            return MtsVarargs.of( MtsNumber.of( player.experienceLevel ),
                                  MtsNumber.of( MathHelper.floor_float( player.experience * player.xpBarCap() ) ),
                                  MtsNumber.of( player.xpBarCap() ) );
        }
        
        /**
         * Gives a character a given amount of experience.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GiveExperience(...)} to a {@code Character} value.
         * </p>
         *
         * @param character character which should receive the experience points
         * @param argAmount amount of experience that should be given to the character
         *
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/GiveExperience">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void giveExperience( MtsCharacter character, MtsValue argAmount )
        {
            EntityPlayer player = checkCharacter( character, 0 ).getEntity();
            int amount = checkInteger( argAmount, 1 );
            player.addExperience( amount );
        }
        
        /**
         * Takes a given amount of experience from a character.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :TakeExperience(...)} to a {@code Character} value.
         * </p>
         *
         * @param character character from who the experience should be taken from
         * @param argAmount amount of experience that should be taken from the character
         *
         * @return {@link MtsBoolean#True true} if the experience could be taken from the character, {@link MtsBoolean#False false} otherwise.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/TakeExperience">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean takeExperience( MtsCharacter character, MtsValue argAmount )
        {
            EntityPlayer player = checkCharacter( character, 0 ).getEntity();
            int amount = checkInteger( argAmount, 0 );
            
            if ( amount > player.experienceTotal )
            {
                return MtsBoolean.False;
            }
            
            player.addExperience( -amount );
            return MtsBoolean.True;
        }
        
        // ========================================
        
        /**
         * Returns the amount of a specific item in a character's inventory.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :CountItem(...)} to a {@code Character} value.
         * </p>
         *
         * @param character   character whose inventory should be queried
         * @param argItemName fully qualified name of the item that should be counted
         * @param argItemMeta metadata/damage value of the item that should be counted; if nil, any metadata/damage value will be counted
         *
         * @return The amount of the specified item in the character's inventory.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/CountItem">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber countItem( MtsCharacter character, MtsValue argItemName, MtsValue argItemMeta )
        {
            EntityPlayer player = checkCharacter( character, 0 ).getEntity();
            String itemName = checkItemName( argItemName, 1 );
            int itemMeta = checkInteger( argItemMeta, 1, -1 );
            
            return MtsNumber.of( InventoryUtil.getItemCount( player.inventory.mainInventory, itemName, itemMeta ) );
        }
        
        /**
         * Gives an item to a character.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GiveItem(...)} to a {@code Character} value.
         * </p>
         *
         * @param character    character who should be given the item(s)
         * @param argItemName  fully qualified name of the item that should be given
         * @param argItemCount count of items that should be given; if nil, defaults to 1
         * @param argItemMeta  metadata/damage value of the item that should be given; if nil, defaults to 0
         *
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/GiveItem">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void giveItem( MtsCharacter character,
                                     MtsValue argItemName,
                                     MtsValue argItemCount,
                                     MtsValue argItemMeta )
        {
            EntityPlayer player = checkCharacter( character, 0 ).getEntity();
            String itemName = checkItemName( argItemName, 1 );
            int amount = checkIntegerWithMinimum( argItemCount, 2, 1, 1 );
            int itemMeta = checkIntegerWithMinimum( argItemMeta, 3, 0, -1 );
            
            Item item = ItemUtil.getItemForName( itemName );
            while ( amount > 0 )
            {
                ItemStack itemStack = new ItemStack( item, 1, itemMeta );
                
                int stackSize = Math.min( amount, itemStack.getMaxStackSize() );
                if ( stackSize == 0 )
                {
                    throw new MtsRuntimeException( "cannot drop zero size item stacks" );
                }
                
                itemStack.stackSize = stackSize;
                amount -= stackSize;
                
                EntityItem itemEntity = player.entityDropItem( itemStack, 0.0F );
                itemEntity.setNoPickupDelay();
            }
        }
        
        /**
         * Takes an item from a character.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :TakeItem(...)} to a {@code Character} value.
         * </p>
         *
         * @param character    character whose inventory the item(s) should be taken from
         * @param argItemName  fully qualified name of the item that should be taken
         * @param argItemCount count of items that should be given; if nil, defaults to 1
         * @param argItemMeta  metadata/damage value of the item that should be taken; if nil, any metadata/damage value will be taken
         *
         * @return {@link MtsBoolean#True true} if the item could be taken from the inventory, {@link MtsBoolean#False false} otherwise
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/TakeItem">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean takeItem( MtsCharacter character,
                                           MtsValue argItemName,
                                           MtsValue argItemCount,
                                           MtsValue argItemMeta )
        {
            EntityPlayer player = checkCharacter( character, 0 ).getEntity();
            String itemName = checkString( argItemName, 0 );
            int amount = checkIntegerWithMinimum( argItemCount, 2, 1, 1 );
            int itemMeta = checkIntegerWithMinimum( argItemMeta, 3, 0, -1 );
            
            int remaining = InventoryUtil.getItemCount( player.inventory.mainInventory, itemName, itemMeta );
            if ( remaining < amount )
            {
                return False;
            }
            
            ItemStack[] slots = player.inventory.mainInventory;
            for ( int i = 0; ( i < slots.length ) && ( remaining > 0 ); i++ )
            {
                ItemStack stack = slots[i];
                if ( stack == null )
                {
                    continue;
                }
                
                if ( matchItem( stack, itemName, itemMeta ) )
                {
                    int toRemove = Math.min( stack.stackSize, remaining );
                    stack.stackSize -= toRemove;
                    remaining -= toRemove;
                    
                    assert stack.stackSize >= 0;
                    
                    if ( stack.stackSize <= 0 )
                    {
                        slots[i] = null;
                    }
                }
            }
            
            player.inventory.markDirty();
            
            return True;
        }
        
        /**
         * Returns the inventory of a character.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetInventory()} to a {@code Character} value.
         * </p>
         *
         * @param character character to get the inventory for
         *
         * @return A table listing detailed information on the items contained in the character's inventory.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/GetItem">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsTable getInventory( MtsCharacter character )
        {
            EntityPlayer player = checkCharacter( character, 0 ).getEntity();
            
            Map<ItemStack, Integer> itemCounts = InventoryUtil.getTotalItemCounts( player.inventory.mainInventory );
            
            MtsTable result = new MtsTable( itemCounts.size(), 0 );
            for ( Entry<ItemStack, Integer> entry : itemCounts.entrySet() )
            {
                ItemStack item = entry.getKey();
                
                MtsTable info = new MtsTable( 0, 3 );
                info.set( KEY_ITEM_NAME, MtsString.of( ItemUtil.getNameForItem( item ) ) );
                info.set( KEY_ITEM_META, MtsNumber.of( item.getItemDamage() ) );
                info.set( KEY_ITEM_COUNT, MtsNumber.of( entry.getValue() ) );
                
                result.list().add( info );
            }
            
            return result;
        }
    }
    
    // ========================================
    
    public static class VillagerMethods
    {
        /**
         * Returns the profession and career of a character.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetProfession()} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character for which the profession and career should be determined
         *
         * @return A varargs value. The first value (index 0) will contain the string representation of the character's profession. The second value (index 1)
         * will contain the string representation of the character's career.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/GetProfession">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVarargs getProfession( MtsValue argCharacter )
        {
            EntityVillager villager = checkCharacter( argCharacter, 0 ).getEntity();
            int professionId = villager.getProfession();
            int careerId = villager.careerId;
            
            return MtsVarargs.of( MtsString.of( VillagerUtil.getProfessionName( professionId ) ),
                                  MtsString.of( VillagerUtil.getCareerName( professionId, careerId ) ) );
        }
        
        /**
         * Returns all trades which a character has to offer.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetRecipes()} to a {@code Character} value.
         * </p>
         *
         * @param argCharacter character for which the trade offers should be retrieved
         *
         * @return A table listing all trades of the character.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Character/GetRecipes">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsTable getRecipes( MtsValue argCharacter )
        {
            EntityVillager villager = checkCharacter( argCharacter, 0 ).getEntity();
            
            @SuppressWarnings( "unchecked" )
            List<MerchantRecipe> recipes = villager.getRecipes( null );
            
            MtsTable result = new MtsTable( recipes.size(), 0 );
            for ( MerchantRecipe recipe : recipes )
            {
                result.add( ScriptApiConverter.asTable( recipe ) );
            }
            
            return result;
        }
    }
}
