/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.values;

import net.minecraft.entity.EntityLivingBase;
import net.mobtalker.mobtalker2.common.entity.EntityType;
import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;
import net.mobtalker.mobtalker2.server.script.lib.metatables.Metatables;
import net.mobtalker.mobtalker2.util.EntityUtil;
import net.mobtalker.mobtalkerscript.v3.MtsRuntimeException;

public class MtsInteractingPlayerCharacter extends MtsCharacter
{
    private final IInteractionAdapter _adapter;
    
    // ========================================
    
    public MtsInteractingPlayerCharacter( IInteractionAdapter adapter, EntityType type )
    {
        _adapter = adapter;
        setMetaTable( Metatables.get( type ) );
    }
    
    // ========================================
    
    @Override
    public boolean hasEntity()
    {
        return true;
    }
    
    @SuppressWarnings( "unchecked" )
    @Override
    public <T extends EntityLivingBase> T getEntity()
    {
        return (T) _adapter.getPartner();
    }
    
    @Override
    public EntityType getEntityType()
    {
        return EntityType.Player;
    }
    
    // ========================================
    
    @Override
    public String getName()
    {
        return EntityUtil.getDisplayName( _adapter.getPartner() );
    }
    
    @Override
    public void setName( String name )
    {
        throw new MtsRuntimeException( "cannot set name of a player" );
    }
    
    @Override
    public void clearName()
    {
        throw new MtsRuntimeException( "cannot clear name of a player" );
    }
}
