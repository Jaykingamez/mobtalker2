/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.lib;

import net.mobtalker.mobtalkerscript.v3.value.MtsString;

public class ScriptApiConstants
{
    public static final MtsString KEY_ITEM_NAME = MtsString.of( "name" );
    public static final MtsString KEY_ITEM_COUNT = MtsString.of( "count" );
    public static final MtsString KEY_ITEM_META = MtsString.of( "meta" );
    
    public static final MtsString KEY_EFFECT_NAME = MtsString.of( "name" );
    public static final MtsString KEY_EFFECT_DURATION = MtsString.of( "duration" );
    public static final MtsString KEY_EFFECT_AMPLIFIER = MtsString.of( "amplifier" );
    
    public static final MtsString KEY_SCOREBOARD_TEAM_COLOR = MtsString.of( "color" );
    public static final MtsString KEY_SCOREBOARD_TEAM_FRIENDLYFIRE = MtsString.of( "friendlyFire" );
    public static final MtsString KEY_SCOREBOARD_TEAM_SEEFIRENDLYINVISIBLES = MtsString.of( "seeFriendlyInvisibles" );
    
    public static final MtsString KEY_VILLAGER_RECIPE_OFFER = MtsString.of( "offer" );
    public static final MtsString KEY_VILLAGER_RECIPE_PRICE = MtsString.of( "price" );
    
    @Deprecated
    public static final MtsString KEY_BLOCKPOS_X = MtsString.of( "x" );
    @Deprecated
    public static final MtsString KEY_BLOCKPOS_Y = MtsString.of( "y" );
    @Deprecated
    public static final MtsString KEY_BLOCKPOS_Z = MtsString.of( "z" );
    
    public static final MtsString KEY_VECTOR_X = MtsString.of( "x" );
    public static final MtsString KEY_VECTOR_Y = MtsString.of( "y" );
    public static final MtsString KEY_VECTOR_Z = MtsString.of( "z" );
    
    public static final MtsString KEY_PARTICLE_LONGDISTANCE = MtsString.of( "longDistance" );
    public static final MtsString KEY_PARTICLE_SPEED = MtsString.of( "speed" );
    public static final MtsString KEY_PARTICLE_COUNT = MtsString.of( "count" );
    public static final MtsString KEY_PARTICLE_ARGUMENTS = MtsString.of( "arguments" );
    
    public static final MtsString KEY_EXPLOSION_FLAMING = MtsString.of( "flaming" );
    public static final MtsString KEY_EXPLOSION_SMOKING = MtsString.of( "smoking" );
    public static final MtsString KEY_EXPLOSION_CAUSEDBY = MtsString.of( "causedBy" );
    
    public static final String ENTITYKEY_PLAYER = "player";
    public static final String ENTITYKEY_PREFIX_TYPE = "type:";
    
    // ========================================
    
    private ScriptApiConstants()
    {
    }
}
