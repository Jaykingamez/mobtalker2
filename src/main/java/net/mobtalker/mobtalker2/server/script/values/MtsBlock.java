/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.values;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.common.registry.GameData;
import net.mobtalker.mobtalker2.server.script.lib.metatables.Metatables;
import net.mobtalker.mobtalkerscript.v3.MtsArgumentException;
import net.mobtalker.mobtalkerscript.v3.value.*;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNativeFunction;

import java.util.Objects;

import static net.mobtalker.mobtalker2.server.script.lib.ScriptApiCheck.checkBlock;
import static net.mobtalker.mobtalker2.server.script.lib.ScriptApiCheck.checkBlockState;
import static net.mobtalker.mobtalkerscript.v3.MtsCheck.checkInteger;
import static org.apache.commons.lang3.Validate.notNull;

/**
 * Mutable data structure representing a block in a world which is modified as the underlying world is modified.
 */
public class MtsBlock extends MtsValueWithMetaTable
{
    public static MtsBlock of( MtsWorld world, BlockPos position )
    {
        return new MtsBlock( Metatables.get( TYPE ), world, position );
    }
    
    // ========================================
    
    public static final MtsType TYPE = MtsType.forName( "block" );
    
    // ========================================
    
    private MtsWorld _world;
    private BlockPos _position;
    
    // ========================================
    
    private MtsBlock( MtsTable metatable, MtsWorld world, BlockPos position )
    {
        _position = notNull( position );
        _world = notNull( world );
        setMetaTable( notNull( metatable ) );
    }
    
    // ========================================
    
    @Deprecated
    public Block getBlock()
    {
        return toBlock();
    }
    
    public Block toBlock()
    {
        return toBlockState().getBlock();
    }
    
    @Deprecated
    public IBlockState getBlockState()
    {
        return toBlockState();
    }
    
    public IBlockState toBlockState()
    {
        return getWorld().getBlockStateAt( getPosition() );
    }
    
    // ========================================
    
    public BlockPos getPosition()
    {
        return _position;
    }
    
    public MtsWorld getWorld()
    {
        return _world;
    }
    
    // ========================================
    
    public String getName()
    {
        return GameData.getBlockRegistry().getNameForObject( toBlock() ).toString();
    }
    
    public MtsMaterial getMaterial()
    {
        return MtsMaterial.of( toBlock() );
    }
    
    // ========================================
    
    public MtsBlockState getState()
    {
        return MtsBlockState.of( toBlockState() );
    }
    
    public boolean setState( MtsBlockState state )
    {
        return getWorld().setBlockStateAt( getPosition(), state );
    }
    
    // ========================================
    
    public void breakBlock( boolean drop )
    {
        getWorld().breakBlock( getPosition(), drop );
    }
    
    // ========================================
    
    @Override
    public MtsType getType()
    {
        return TYPE;
    }
    
    // ========================================
    
    @Override
    public int hashCode()
    {
        return Objects.hash( _world, _position );
    }
    
    @Override
    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        if ( o == null )
        {
            return false;
        }
        if ( !( o instanceof MtsBlock ) )
        {
            return false;
        }
        
        MtsBlock other = (MtsBlock) o;
        return Objects.equals( _world, other._world ) && Objects.equals( _position, other._position );
    }
    
    // ========================================
    
    @Override
    public String toString()
    {
        BlockPos pos = getPosition();
        return String.format( "%s[%s,(%d|%d|%d)]", TYPE.getName(), getWorld().getName(), pos.getX(), pos.getY(),
                              pos.getZ() );
    }
    
    // ========================================
    
    public static class Methods
    {
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Block/GetName">wiki</a>
         * </p>
         *
         * Returns the unique identifier for a block. This is effectively a shorthand for {@code :GetMaterial():GetName()}.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetName()} to a {@code Block} value.
         * </p>
         *
         * @param argBlock block to get the name of
         *
         * @return The unique identifier of the block.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Block/GetName">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsString getName( MtsValue argBlock )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            return MtsString.of( block.getMaterial().getName() );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Block/GetMetadata">wiki</a>
         * </p>
         *
         * Returns the metadata for a block.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetMetadata()} to a {@code Block} value.
         * </p>
         *
         * @param argBlock block to get the metadata for
         *
         * @return The metadata of the block.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Block/GetMetadata">MobTalker2 wiki</a>
         * @deprecated as of 0.7.2. Please use {@link #getState(MtsValue) GetState} instead.
         */
        @Deprecated
        @MtsNativeFunction
        public static MtsNumber getMetadata( MtsValue argBlock )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            return MtsNumber.of( block.toBlock().getMetaFromState( block.toBlockState() ) );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Block/SetMetadata">wiki</a>
         * </p>
         *
         * Sets the metadata for a block. From the technical side, the metadata is converted into a block state and then the world is told to set that block
         * state at the block's position.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :SetMetadata()} to a {@code Block} value.
         * </p>
         *
         * @param argBlock    block to set the metadata for
         * @param argMetadata metadata value which should be set; if nil, defaults to 0
         *
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Block/SetMetadata">wiki</a>
         * @deprecated as of 0.7.2. Please use {@link #setState(MtsValue, MtsValue) SetState} instead.
         */
        @Deprecated
        @MtsNativeFunction
        public static void setMetadata( MtsValue argBlock, MtsValue argMetadata )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            int meta = checkInteger( argMetadata, 1, 0 );
            IBlockState state = block.toBlock().getStateFromMeta( meta );
            
            if ( state == null )
            {
                throw new MtsArgumentException( "invalid metadata value %d", meta );
            }
            
            block.getWorld().toWorld().setBlockState( block.getPosition(), state, 0b01 | 0b10 );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Block/IsValidMetadata">wiki</a>
         * </p>
         *
         * Returns whether a given metadata value is valid for a block. This is effectively a shorthand for {@code :GetMaterial():IsValidMetadata(...)}.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :IsValidMetadata(...)} to a {@code Block} value.
         * </p>
         *
         * @param argBlock    block which should be checked
         * @param argMetadata metadata value which should be checked
         *
         * @return {@link MtsBoolean#True true} if the metadata value is valid, {@code MtsBoolean#False false} otherwise.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Block/IsValidMetadata">wiki</a>
         * @deprecated as of 0.7.2.
         */
        @Deprecated
        @MtsNativeFunction
        public static MtsBoolean isValidMetadata( MtsValue argBlock, MtsValue argMetadata )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            return MtsMaterial.Methods.isValidMetadata( block.getMaterial(), argMetadata );
        }
        
        /**
         * Returns the current state of a block. The returned value is immutable, so any changes to it won't modify the block's state.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetState()} to a {@code Block} value.
         * </p>
         *
         * @param argBlock The block for which the current state should be returned.
         *
         * @return A {@linkplain MtsBlockState block state} value which represents the state of the given block at the time this method was called.
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/Block/GetState">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsBlockState getState( MtsValue argBlock )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            return MtsBlockState.of( block.toBlockState() );
        }
        
        /**
         * Modifies the current state of a block. As block states are immutable, any changes that are made to the state will have to be applied by using {@code
         * SetState} in order for them to update the block.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :SetState(...)} to a {@code Block} value.
         * </p>
         *
         * @param argBlock The block whose state should be updated.
         * @param argState A {@linkplain MtsBlockState block state} value which should be applied to the block.
         *
         * @return {@link MtsBoolean#True true} if applying the block state worked and caused a change, {@link MtsBoolean#False false} otherwise.
         * @see WorldServer#setBlockState(BlockPos, IBlockState)
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/Block/SetState">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsBoolean setState( MtsValue argBlock, MtsValue argState )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            MtsBlockState state = checkBlockState( argState, 1 );
            return MtsBoolean.of( block.setState( state ) );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Block/GetPosition">wiki</a>
         * </p>
         *
         * Returns the position of a block.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetPosition()} to a {@code Block} value.
         * </p>
         *
         * @param argBlock block to get the position for
         *
         * @return A varargs value. The first value (index 0) equals the X component of the block's position. The second value (index 1) equals the Y component.
         * The third value (index 2) equals the Z component.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Block/GetPosition">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVector getPosition( MtsValue argBlock )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            return MtsVector.of( block.getPosition() );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Block/GetMaterial">wiki</a>
         * </p>
         *
         * Returns a block's material.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetMaterial()} to a {@code Block} value.
         * </p>
         *
         * @param argBlock block to get the material for
         *
         * @return The material of the block.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Block/GetMaterial">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsValue getMaterial( MtsValue argBlock )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            return block.getMaterial();
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Block/GetBiome">wiki</a>
         * </p>
         *
         * Returns the biome at a block's position.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetBiome()} to a {@code Block} value.
         * </p>
         *
         * @param argBlock block to get the biome for
         *
         * @return The biome at the block's position.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Block/GetBiome">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsValue getBiome( MtsValue argBlock )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            MtsWorld world = block.getWorld();
            return world.getBiomeAt( block.getPosition() );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Block/GetWorld">wiki</a>
         * </p>
         *
         * Returns the world of a block.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetWorld()} to a {@code Block} value.
         * </p>
         *
         * @param argBlock block to get the world for
         *
         * @return The block's world.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Block/GetWorld">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsValue getWorld( MtsValue argBlock )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            return block.getWorld();
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Block/IsPowered">wiki</a>
         * </p>
         *
         * Returns whether a block is powered by redstone.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :IsPowered()} to a {@code Block} value.
         * </p>
         *
         * @param argBlock block which should be inspected
         *
         * @return {@link MtsBoolean#True true} if the block is powered by redstone, {@link MtsBoolean#False false} otherwise.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Block/IsPowered">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean isPowered( MtsValue argBlock )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            WorldServer world = block.getWorld().toWorld();
            BlockPos position = block.getPosition();
            return MtsBoolean.of( world.isBlockPowered( position ) );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Block/GetPower">wiki</a>
         * </p>
         *
         * Returns the redstone power value of a block.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetPower()} to a {@code Block} value.
         * </p>
         *
         * @param argBlock block to get the redstone power value for
         *
         * @return The block's redstone power value.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Block/GetPower">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber getPower( MtsValue argBlock )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            BlockPos position = block.getPosition();
            WorldServer world = block.getWorld().toWorld();
            return MtsNumber.of( getRedstonePower( world, position ) );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Block/GetLightLevel">wiki</a>
         * </p>
         *
         * Returns the light level on a block.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetLightLevel()} to a {@code Block} value.
         * </p>
         *
         * @param argBlock block to get the light level for
         *
         * @return The block's light level.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Block/GetLightLevel">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber getLightLevel( MtsValue argBlock )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            WorldServer world = block.getWorld().toWorld();
            BlockPos position = block.getPosition();
            return MtsNumber.of( world.getLight( position ) );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Block/GetHardness">wiki</a>
         * </p>
         *
         * Returns the hardness of a block.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetHardness()} to a {@code Block} value.
         * </p>
         *
         * @param argBlock block to get the hardness for
         *
         * @return The hardness of the block.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Block/GetHardness">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber getHardness( MtsValue argBlock )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            WorldServer world = block.getWorld().toWorld();
            BlockPos position = block.getPosition();
            return MtsNumber.of( block.toBlock().getBlockHardness( world, position ) );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Block/GetHarvestLevel">wiki</a>
         * </p>
         *
         * Returns the harvest level of a block.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetHarvestLevel()} to a {@code Block} value.
         * </p>
         *
         * @param argBlock block to get the harvest level for
         *
         * @return The harvest level of the block.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Block/GetHarvestLevel">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber getHarvestLevel( MtsValue argBlock )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            return MtsNumber.of( block.toBlock().getHarvestLevel( block.toBlockState() ) );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Block/IsAir">wiki</a>
         * </p>
         *
         * Returns whether a block is considered to be air.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :IsAir()} to a {@code Block} value.
         * </p>
         *
         * @param argBlock block which should be inspected
         *
         * @return {@link MtsBoolean#True true} if the block is considered to be air, {@link MtsBoolean#False false} otherwise.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Block/IsAir">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean isAir( MtsValue argBlock )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            WorldServer world = block.getWorld().toWorld();
            BlockPos position = block.getPosition();
            return MtsBoolean.of( block.toBlock().isAir( world, position ) );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Block/IsLiquid">wiki</a>
         * </p>
         *
         * Returns whether a block is a liquid. This is effectively a shorthand for {@code :GetMaterial():IsLiquid()}.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :IsLiquid()} to a {@code Block} value.
         * </p>
         *
         * @param argBlock block which should be inspected
         *
         * @return {@link MtsBoolean#True true} if the block is a liquid, {@link MtsBoolean#False false} otherwise.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Block/IsLiquid">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean isLiquid( MtsValue argBlock )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            return MtsMaterial.Methods.isLiquid( block.getMaterial() );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Block/IsSolid">wiki</a>
         * </p>
         *
         * Returns whether a block is solid. This is effectively a shorthand for {@code :GetMaterial():IsSolid()}.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :IsSolid()} to a {@code Block} value.
         * </p>
         *
         * @param argBlock block which should be inspected
         *
         * @return {@link MtsBoolean#True true} if the block is a liquid, {@link MtsBoolean#False false} otherwise.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Block/IsSolid">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean isSolid( MtsValue argBlock )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            return MtsMaterial.Methods.isSolid( block.getMaterial() );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Block/IsFertile">wiki</a>
         * </p>
         *
         * Returns whether a block is fertile.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :IsFertile()} to a {@code Block} value.
         * </p>
         *
         * @param argBlock block which should be inspected
         *
         * @return {@link MtsBoolean#True true} if the block is fertile, {@link MtsBoolean#False false} otherwise.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Block/IsFertile">wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean isFertile( MtsValue argBlock )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            WorldServer world = block.getWorld().toWorld();
            BlockPos position = block.getPosition();
            return MtsBoolean.of( block.toBlock().isFertile( world, position ) );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Block/Harvest">wiki</a>
         * </p>
         *
         * Breaks a block and spawns its drops at the block's location.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :Harvest(...)} to a {@code Block} value.
         * </p>
         *
         * @param argBlock block which should be broken
         *
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Block/Harvest">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void harvest( MtsValue argBlock )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            block.breakBlock( true );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Block/Destroy">wiki</a>
         * </p>
         *
         * Breaks a block without spawning any drops.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :Destroy()} to a {@code Block} value.
         * </p>
         *
         * @param argBlock block which should be broken
         *
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Block/Destroy">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void destroy( MtsValue argBlock )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            block.breakBlock( false );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Block/CanFreeze">wiki article</a>
         * </p>
         *
         * Returns whether a block can freeze.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :CanFreeze(...)} to a {@code Block} value.
         * </p>
         *
         * @param argBlock         block which should be inspected.
         * @param argWaterAdjacent (optional) determines if the surrounding blocks must be non-water blocks. If
         *                         <code>nil</code> or not specified, defaults to <code>false</code>.
         *
         * @return {@link MtsBoolean#True true} if the block can freeze, {@link MtsBoolean#False false} otherwise.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Block/CanFreeze">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean canFreeze( MtsValue argBlock, MtsValue argWaterAdjacent )
        {
            MtsBlock block = checkBlock( argBlock, 0 );
            WorldServer world = block.getWorld().toWorld();
            return MtsBoolean.of( world.canBlockFreeze( block.getPosition(), !argWaterAdjacent.isTrue() ) );
        }
        
        // ========================================
        
        private static int getRedstonePower( WorldServer world, BlockPos position )
        {
            int power = 0;
            EnumFacing[] facings = EnumFacing.values();
            for ( EnumFacing facing : facings )
            {
                power = Math.max( power, world.getRedstonePower( position.offset( facing ), facing.getOpposite() ) );
            }
            return power;
        }
    }
}
