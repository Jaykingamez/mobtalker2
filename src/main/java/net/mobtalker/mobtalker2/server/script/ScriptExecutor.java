/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script;

import static com.google.common.base.Preconditions.*;

import java.util.concurrent.*;

import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

public class ScriptExecutor
{
    private static final MobTalkerLog LOG = new MobTalkerLog( "ScriptExecutor" );
    
    // ========================================
    
    private static ScriptExecutor instance;
    
    // ========================================
    
    public static ScriptExecutor instance()
    {
        checkState( instance != null, "Not loaded" );
        return instance;
    }
    
    public static boolean isLoaded()
    {
        return instance != null;
    }
    
    public static void load()
    {
        checkState( instance == null, "Already loaded" );
        
        LOG.debug( "Loading" );
        
        instance = new ScriptExecutor();
    }
    
    public static void unload()
    {
        checkState( instance != null, "Not loaded" );
        
        LOG.debug( "Unloading" );
        
        instance.shutdown();
        instance = null;
    }
    
    // ========================================
    
    private final ExecutorService _executor;
    
    // ========================================
    
    private ScriptExecutor()
    {
        _executor = createExecutor();
    }
    
    private static ExecutorService createExecutor()
    {
        ThreadFactory factory = new ThreadFactoryBuilder().setNameFormat( "MobTalker2 ScriptThread #%d" )
                                                          .setPriority( Thread.NORM_PRIORITY )
                                                          .setDaemon( true )
                                                          .build();
        ThreadPoolExecutor executor = new ThreadPoolExecutor( 1, Integer.MAX_VALUE,
                                                              1L, TimeUnit.MINUTES,
                                                              new SynchronousQueue<Runnable>(),
                                                              factory );
        executor.prestartCoreThread();
        
        return executor;
    }
    
    // ========================================
    
    public void submit( Runnable task )
    {
        _executor.submit( task );
    }
    
    public void shutdown()
    {
        _executor.shutdownNow();
    }
}
