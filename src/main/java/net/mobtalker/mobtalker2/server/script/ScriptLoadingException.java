/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script;

public class ScriptLoadingException extends RuntimeException
{
    public ScriptLoadingException( String msg )
    {
        super( msg );
    }
    
    public ScriptLoadingException( Exception parent )
    {
        super( parent );
    }
    
    public ScriptLoadingException( String msg, Object... args )
    {
        super( String.format( msg, args ) );
    }
    
    public ScriptLoadingException( String msg, Throwable parent, Object... args )
    {
        super( String.format( msg, args ), parent );
    }
}
