/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.lib;

import static net.mobtalker.mobtalkerscript.v3.MtsCheck.*;

import java.util.List;
import java.util.concurrent.CancellationException;

import com.google.common.base.Throwables;
import com.google.common.collect.Lists;

import net.mobtalker.mobtalker2.common.network.ServerMessageHandler;
import net.mobtalker.mobtalker2.server.interaction.*;
import net.mobtalker.mobtalker2.server.script.values.MtsCharacter;
import net.mobtalker.mobtalkerscript.v3.*;
import net.mobtalker.mobtalkerscript.v3.value.*;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNativeFunction;

public class InteractionCommandLib
{
    private final IInteractionAdapter _adapter;
    
    // ========================================
    
    public InteractionCommandLib( IInteractionAdapter adapter )
    {
        _adapter = adapter;
    }
    
    // ========================================
    
    /**
     * Sends text to be displayed to the interacting player and blocks script execution until clicked away.
     * 
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code ShowText(...)}.
     * </p>
     * 
     * <p>
     * This <i>command</i> can be used in scripts by using {@code say ... [as conclusion]}.
     * </p>
     * 
     * @param argWho character or string which is displayed as a name
     * @param argText text that should be displayed to the interacting player
     * @param argIsLast if the conversation will stop after this part; if {@link MtsBoolean#True true}, a square will be displayed instead of a triangle
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Interaction/ShowText">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public void showText( MtsValue argWho, MtsValue argText, MtsValue argIsLast )
    {
        String name;
        if ( argWho.isString() )
        {
            name = argWho.asString().toJava();
        }
        else if ( argWho.is( MtsCharacter.TYPE ) )
        {
            name = ( (MtsCharacter) argWho ).getName();
        }
        else
            throw new MtsArgumentException( 0, "string or character expected, got " + argWho.getType() );
        
        showText( name, checkString( argText, 1 ), argIsLast.isTrue() );
    }
    
    /**
     * Sends options to be displayed to the interacting player and blocks script execution until clicked away. At least one option must be provided.
     * 
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code ShowMenu(...)}.
     * </p>
     * 
     * <p>
     * This <i>command</i> can be used in scripts by using {@code menu ... option ... end}.
     * </p>
     * 
     * @param args list of arguments; first value is considered to be a caption for the menu
     * @return the 1-based index of the option that was chosen by the interacting player
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Interaction/ShowMenu">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public MtsValue showMenu( MtsVarargs args )
    {
        String caption = checkString( args, 0, "" );
        MtsValue arg1 = args.get( 1 );
        
        List<String> options;
        if ( arg1.isTable() )
        {
            MtsTable argOptions = arg1.asTable();
            if ( argOptions.isEmpty() )
                throw new MtsRuntimeException( "must provide at least one option" );
            
            options = Lists.newArrayListWithCapacity( argOptions.list().size() );
            for ( MtsValue arg : argOptions.list() )
            {
                options.add( checkString( arg ) );
            }
        }
        else
        {
            if ( arg1.isNil() )
                throw new MtsRuntimeException( "must provide at least one option" );
            
            options = Lists.newArrayListWithCapacity( args.count() - 1 );
            for ( int i = 1; i < args.count(); i++ )
            {
                options.add( checkString( args, i ) );
            }
        }
        
        return MtsNumber.of( showMenu( caption, options ) );
    }
    
    /**
     * Shows a sprite to the interacting player. This method does not block script execution.
     * 
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code ShowSprite(...)}.
     * </p>
     * 
     * <p>
     * This <i>command</i> can be used in scripts by using {@code show ... [at ...] [offset ...]}.
     * </p>
     * 
     * @param argGroup character or string which identifies the sprite group
     * @param argSubPath sub-path to the sprite that should be used
     * @param argPosition string which identifies the screen location where the sprite should be displayed; if nil, defaults to {@code "bottom"}
     * @param argOffsetX horizontal offset from the specified screen location, in pixels; if nil, defaults to {@code 0}
     * @param argOffsetY vertical offset from the specified screen location, in pixels; if nil, defaults to {@code 0}
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Interaction/ShowSprite">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public void showSprite( MtsValue argGroup, MtsValue argSubPath,
                            MtsValue argPosition, MtsValue argOffsetX, MtsValue argOffsetY )
    {
        String group;
        if ( argGroup.isString() )
        {
            group = argGroup.asString().toJava();
        }
        else if ( argGroup.is( MtsCharacter.TYPE ) )
        {
            group = ( (MtsCharacter) argGroup ).getTexturePath();
        }
        else
            throw new MtsArgumentException( 0, "string or character expected, got " + argGroup.getType() );
        
        showSprite( group,
                    checkString( argSubPath, 1 ),
                    checkString( argPosition, 2, "bottom" ),
                    checkInteger( argOffsetX, 3, 0 ),
                    checkInteger( argOffsetY, 4, 0 ) );
    }
    
    /**
     * Shows a scene/background to the interacting player. This method does not block script execution.
     * 
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code ShowScene(...)}.
     * </p>
     * 
     * <p>
     * This <i>command</i> can be used in scripts by using {@code scene ... [as ...]}.
     * </p>
     * 
     * @param argPath path to the texture which should be displayed as the background image
     * @param argMode how the texture should be displayed; if nil, defaults to {@code "fill"}
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Interaction/ShowScene">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public void showScene( MtsValue argPath, MtsValue argMode )
    {
        showScene( checkString( argPath, 0 ),
                   checkString( argMode, 1, "fill" ) );
    }
    
    /**
     * Hides a texture group. This method does not block script execution.
     * 
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code HideTexture(...)}.
     * </p>
     * 
     * <p>
     * This <i>command</i> can be used in scripts by using {@code hide ...}.
     * </p>
     * 
     * @param argGroup character or string which identifies the texture group that should be hidden
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Interaction/HideTexture">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public void hideTexture( MtsValue argGroup )
    {
        String group;
        if ( argGroup.isString() )
        {
            group = argGroup.asString().toJava();
        }
        else if ( argGroup.is( MtsCharacter.TYPE ) )
        {
            group = ( (MtsCharacter) argGroup ).getTexturePath();
        }
        else
            throw new MtsArgumentException( 0, "string or character expected, got " + argGroup.getType() );
        
        hideTexture( group );
    }
    
    // ========================================
    
    private void showText( String name, String text, boolean isLast )
    {
        try
        {
            ServerMessageHandler.instance().sendShowText( _adapter.getPartner(), name, text, isLast );
        }
        catch ( CancellationException | InterruptedException ex )
        {
            throw new InteractionCanceledException();
        }
        catch ( Exception ex )
        {
            throw Throwables.propagate( ex );
        }
    }
    
    private int showMenu( String caption, List<String> options )
    {
        try
        {
            // Caution! Convert 0-based to 1-based.
            return 1 + ServerMessageHandler.instance().sendShowMenu( _adapter.getPartner(), caption, options );
        }
        catch ( CancellationException | InterruptedException ex )
        {
            throw new InteractionCanceledException();
        }
        catch ( Exception ex )
        {
            throw Throwables.propagate( ex );
        }
    }
    
    private void showSprite( String group, String path, String position, int offsetX, int offsetY )
    {
        ServerMessageHandler.sendShowSprite( _adapter.getPartner(), group, path, position, offsetX, offsetY );
    }
    
    private void showScene( String path, String mode )
    {
        ServerMessageHandler.sendShowScene( _adapter.getPartner(), path, mode );
    }
    
    private void hideTexture( String group )
    {
        ServerMessageHandler.sendHideTexture( _adapter.getPartner(), group );
    }
}
