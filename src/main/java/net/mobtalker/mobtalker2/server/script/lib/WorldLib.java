/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.lib;

import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.server.FMLServerHandler;
import net.mobtalker.mobtalker2.server.script.values.MtsWorld;
import net.mobtalker.mobtalkerscript.v3.value.*;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNativeFunction;

public class WorldLib
{
    /**
     * Returns a list of all available worlds.
     * 
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code World.GetWorlds()}.
     * </p>
     * 
     * @return a table listing key-value pairs of a world's name and its respective world object
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/World/GetWorlds">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public static MtsTable getWorlds()
    {
        WorldServer[] worlds = FMLServerHandler.instance().getServer().worldServers;
        MtsTable result = new MtsTable( worlds.length, 0 );
        
        for ( WorldServer world : worlds )
        {
            MtsString name = MtsString.of( world.provider.getDimensionName() );
            MtsWorld mtsWorld = MtsWorld.of( world );
            
            result.set( name, mtsWorld );
        }
        
        return result;
    }
}
