/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script;

import net.mobtalker.mobtalkerscript.v3.value.MtsValue;

public interface IInteractionEventListener
{
    void onEvent( String event, MtsValue... args );
}
