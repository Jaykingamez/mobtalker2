/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.values;

import static net.mobtalker.mobtalker2.server.script.lib.ScriptApiCheck.*;
import static net.mobtalker.mobtalker2.util.CollectionUtil.generify;
import static net.mobtalker.mobtalkerscript.v3.MtsCheck.checkString;
import static org.apache.commons.lang3.Validate.notNull;

import java.util.*;

import net.minecraft.scoreboard.ScorePlayerTeam;
import net.mobtalker.mobtalker2.server.script.lib.metatables.Metatables;
import net.mobtalker.mobtalkerscript.v3.MtsArgumentException;
import net.mobtalker.mobtalkerscript.v3.value.*;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNativeFunction;

/**
 * @since 0.7.2
 */
public class MtsTeam extends MtsValueWithMetaTable
{
    public static MtsTeam of( MtsScoreboard scoreboard, ScorePlayerTeam team )
    {
        return new MtsTeam( Metatables.get( TYPE ), scoreboard, team );
    }
    
    // ========================================
    
    public static final MtsType TYPE = MtsType.forName( "team" );
    
    // ========================================
    
    private final MtsScoreboard _scoreboard;
    private final ScorePlayerTeam _team;
    
    // ========================================
    
    private MtsTeam( MtsTable metatable, MtsScoreboard scoreboard, ScorePlayerTeam team )
    {
        _scoreboard = notNull( scoreboard );
        _team = notNull( team );
        setMetaTable( metatable );
    }
    
    // ========================================
    
    public ScorePlayerTeam toTeam()
    {
        return _team;
    }
    
    // ========================================
    
    public MtsScoreboard getScoreboard()
    {
        return _scoreboard;
    }
    
    // ========================================
    
    public String getName()
    {
        return toTeam().getRegisteredName();
    }
    
    public String getNamePrefix()
    {
        return toTeam().getColorPrefix();
    }
    
    public void setNamePrefix( String string )
    {
        toTeam().setNamePrefix( string );
    }
    
    public String getNameSuffix()
    {
        return toTeam().getColorSuffix();
    }
    
    public void setNameSuffix( String string )
    {
        toTeam().setNameSuffix( string );
    }
    
    // ========================================
    
    public List<String> getMembers()
    {
        Collection<?> members = toTeam().getMembershipCollection();
        return generify( members );
    }
    
    public void addMember( String name )
    {
        getScoreboard().toScoreboard().addPlayerToTeam( name, toTeam().getRegisteredName() );
    }
    
    public void removeMember( String name )
    {
        getScoreboard().toScoreboard().removePlayerFromTeam( name, toTeam() );
    }
    
    public boolean hasMember( String name )
    {
        return toTeam().getMembershipCollection().contains( name );
    }
    
    // ========================================
    
    public boolean isFriendlyFireEnabled()
    {
        return toTeam().getAllowFriendlyFire();
    }
    
    public void setFriendlyFireEnabled( boolean friendlyFire )
    {
        toTeam().setAllowFriendlyFire( friendlyFire );
    }
    
    public boolean canSeeFriendlyInvisibles()
    {
        return toTeam().getSeeFriendlyInvisiblesEnabled();
    }
    
    public void setCanSeeFriendlyInvisibles( boolean canSee )
    {
        toTeam().setSeeFriendlyInvisiblesEnabled( canSee );
    }
    
    // ========================================
    
    @Override
    public MtsType getType()
    {
        return TYPE;
    }
    
    // ========================================
    
    @Override
    public int hashCode()
    {
        return Objects.hashCode( _team );
    }
    
    @Override
    public boolean equals( Object obj )
    {
        if ( this == obj ) return true;
        if ( obj == null ) return false;
        if ( !( obj instanceof MtsTeam ) ) return false;
        
        MtsTeam other = (MtsTeam) obj;
        return Objects.equals( _team, other.toTeam() );
    }
    
    // ========================================
    
    @Override
    public String toString()
    {
        return String.format( "%s[%s]", getType().getName(), toTeam().getRegisteredName() );
    }
    
    // ========================================
    
    /**
     * <p>
     * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team">wiki article</a>
     * </p>
     *
     * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team">MobTalker2 wiki</a>
     */
    public static final class Methods
    {
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/GetScoreboard">wiki article</a>
         * </p>
         *
         * Returns a team's associated scoreboard.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetScoreboard} to a {@code team} value.
         * </p>
         *
         * @param argTeam team whose associated scoreboard should be returned
         * @return The scoreboard associated with the given team.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/GetScoreboard">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsScoreboard getScoreboard( MtsValue argTeam )
        {
            MtsTeam team = checkTeam( argTeam, 0 );
            return team.getScoreboard();
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/GetName">wiki article</a>
         * </p>
         *
         * Returns the registered name of a team.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetName()} to a {@code team} value.
         * </p>
         *
         * @param argTeam team whose registered name should be returned
         * @return The registered name of the given team.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/GetName">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsString getName( MtsValue argTeam )
        {
            MtsTeam team = checkTeam( argTeam, 0 );
            return MtsString.of( team.getName() );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/GetNamePrefix">wiki article</a>
         * </p>
         *
         * Returns the name prefix of a team.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetNamePrefix()} to a {@code team} value.
         * </p>
         *
         * @param argTeam team whose name prefix should be returned
         * @return The name prefix of the given team.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/GetNamePrefix">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsString getNamePrefix( MtsValue argTeam )
        {
            MtsTeam team = checkTeam( argTeam, 0 );
            return MtsString.of( team.getNamePrefix() );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/SetNamePrefix">wiki article</a>
         * </p>
         *
         * Sets the name prefix of a team.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :SetNamePrefix(...)} to a {@code team} value.
         * </p>
         *
         * @param argTeam team whose name prefix should be set
         * @param argPrefix new name prefix for {@code argTeam}
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/SetNamePrefix">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void setNamePrefix( MtsValue argTeam, MtsValue argPrefix )
        {
            MtsTeam team = checkTeam( argTeam, 0 );
            team.setNamePrefix( checkString( argPrefix, 1 ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/GetNameSuffix">wiki article</a>
         * </p>
         *
         * Returns the name suffix of a team.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetNameSuffix()} to a {@code team} value.
         * </p>
         *
         * @param argTeam team whose name suffix should be returned
         * @return The name suffix of the given team.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/GetNameSuffix">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsString getNameSuffix( MtsValue argTeam )
        {
            MtsTeam team = checkTeam( argTeam, 0 );
            return MtsString.of( team.getNameSuffix() );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/SetNameSuffix">wiki article</a>
         * </p>
         *
         * Sets the name suffix of a team.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :SetNameSuffix(...)} to a {@code team} value.
         * </p>
         *
         * @param argTeam team whose name suffix should be set
         * @param argSuffix new name suffix for {@code argTeam}
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/SetNameSuffix">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void setNameSuffix( MtsValue argTeam, MtsValue argSuffix )
        {
            MtsTeam team = checkTeam( argTeam, 0 );
            team.setNameSuffix( checkString( argSuffix, 1 ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/IsFriendlyFireEnabled">wiki article</a>
         * </p>
         *
         * Checks if friendly fire is enabled for a team.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :IsFriendlyFireEnabled()} to a {@code team}
         * value.
         * </p>
         *
         * @param argTeam team to check
         * @return {@link MtsBoolean#True true} if friendly fire is enabled for the given team,
         * {@link MtsBoolean#False false} otherwise.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/IsFriendlyFireEnabled">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean isFriendlyFireEnabled( MtsValue argTeam )
        {
            return MtsBoolean.of( checkTeam( argTeam, 0 ).isFriendlyFireEnabled() );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/SetFriendlyFireEnabled">wiki article</a>
         * </p>
         *
         * Enables or disables friendly fire for a team.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :SetFriendlyFireEnabled(...)} to a {@code team}
         * value.
         * </p>
         *
         * @param argTeam team to enable/disable friendly fire for
         * @param argEnabled {@link MtsBoolean#True true} to enable friendly fire, {@link MtsBoolean#False false} to
         * disable friendly fire
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/SetFriendlyFireEnabled">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void setFriendlyFireEnabled( MtsValue argTeam, MtsValue argEnabled )
        {
            checkTeam( argTeam, 0 ).setFriendlyFireEnabled( argEnabled.isTrue() );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/CanSeeFriendlyInvisibles">wiki articles</a>
         * </p>
         *
         * Checks if invisible members of a team can be seen by other team members.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :CanSeeFriendlyInvisibles()} to a {@code team}
         * value.
         * </p>
         *
         * @param argTeam team to check
         * @return {@link MtsBoolean#True true} if invisible members of the given team can be seen by other team members,
         * {@link MtsBoolean#False false} otherwise.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/CanSeeFriendlyInvisibles">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean canSeeFriendlyInvisibles( MtsValue argTeam )
        {
            return MtsBoolean.of( checkTeam( argTeam, 0 ).canSeeFriendlyInvisibles() );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/SetCanSeeFriendlyInvisibles">wiki article</a>
         * </p>
         *
         * Enables or disables the ability of members of a team to see other invisible members.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :SetCanSeeFriendlyInvisibles(...)} to a
         * {@code team} value.
         * </p>
         *
         * @param argTeam team to enable/disable the ability to see friendly invisible team members for
         * @param argEnabled {@link MtsBoolean#True true} to make friendly invisibles visible to team members,
         * {@link MtsBoolean#False} to make them stay invisible
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/SetCanSeeFriendlyInvisibles">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void setCanSeeFriendlyInvisibles( MtsValue argTeam, MtsValue argEnabled )
        {
            checkTeam( argTeam, 0 ).setCanSeeFriendlyInvisibles( argEnabled.isTrue() );
        }
        
        // ========================================
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/GetMembers">wiki article</a>
         * </p>
         *
         * Returns all members of a team.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetMembers()} to a {@code team} value.
         * </p>
         *
         * @param argTeam team whose members should be returned
         * @return A table listing all members of the given team.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/GetMembers">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsTable getMembers( MtsValue argTeam )
        {
            MtsTeam team = checkTeam( argTeam, 0 );
            List<String> members = team.getMembers();
            MtsTable table = new MtsTable( members.size(), 0 );
            MtsTableList list = table.list();
            
            for ( String s : members )
            {
                list.add( MtsString.of( s ) );
            }
            
            return table;
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/AddMember">wiki article</a>
         * </p>
         *
         * Adds a member to a team.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :AddMember(...)} to a {@code team} value.
         * </p>
         *
         * @param argTeam team to which a member should be added
         * @param argPlayer member that should be added
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/AddMember">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void addMember( MtsValue argTeam, MtsValue argPlayer )
        {
            MtsTeam team = checkTeam( argTeam, 0 );
            String player;
            
            if ( argPlayer.isString() )
            {
                player = checkString( argPlayer, 1 );
            }
            else if ( argPlayer.is( MtsCharacter.TYPE ) )
            {
                player = checkCharacter( argPlayer, 1 ).getName();
            }
            else
            {
                throw new MtsArgumentException( 1, MtsType.STRING, MtsCharacter.TYPE, argPlayer.getType() );
            }
            
            team.addMember( player );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/RemoveMember">wiki article</a>
         * </p>
         *
         * Removes a member from a team.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :RemoveMember(...)} to a {@code team} value.
         * </p>
         *
         * @param argTeam team to remove a member from
         * @param argPlayer member that should be removed
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/RemoveMember">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void removeMember( MtsValue argTeam, MtsValue argPlayer )
        {
            MtsTeam team = checkTeam( argTeam, 0 );
            String player;
            
            if ( argPlayer.isString() )
            {
                player = checkString( argPlayer, 1 );
            }
            else if ( argPlayer.is( MtsCharacter.TYPE ) )
            {
                player = checkCharacter( argPlayer, 1 ).getName();
            }
            else
            {
                throw new MtsArgumentException( 1, MtsType.STRING, MtsCharacter.TYPE, argPlayer.getType() );
            }
            
            team.removeMember( player );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/HasMember">wiki article</a>
         * </p>
         *
         * Checks if a team has a given member.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :HasMember(...)} to a {@code team} value.
         * </p>
         *
         * @param argTeam team to check
         * @param argPlayer member to check for
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Team/HasMember">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean hasMember( MtsValue argTeam, MtsValue argPlayer )
        {
            MtsTeam team = checkTeam( argTeam, 0 );
            String player;
            
            if ( argPlayer.isString() )
            {
                player = checkString( argPlayer, 1 );
            }
            else if ( argPlayer.is( MtsCharacter.TYPE ) )
            {
                player = checkCharacter( argPlayer, 1 ).getName();
            }
            else
            {
                throw new MtsArgumentException( 1, MtsType.STRING, MtsCharacter.TYPE, argPlayer.getType() );
            }
            
            return MtsBoolean.of( team.hasMember( player ) );
        }
    }
}
