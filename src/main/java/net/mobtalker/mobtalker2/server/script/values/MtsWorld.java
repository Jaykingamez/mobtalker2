/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.values;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.*;
import net.minecraft.world.GameRules.ValueType;
import net.minecraft.world.storage.WorldInfo;
import net.mobtalker.mobtalker2.server.script.lib.ScriptApiCheck;
import net.mobtalker.mobtalker2.server.script.lib.metatables.Metatables;
import net.mobtalker.mobtalker2.util.Counter;
import net.mobtalker.mobtalker2.util.ParticleUtil;
import net.mobtalker.mobtalker2.util.builders.SpawnParticleBuilder;
import net.mobtalker.mobtalkerscript.v3.MtsArgumentException;
import net.mobtalker.mobtalkerscript.v3.MtsCheck;
import net.mobtalker.mobtalkerscript.v3.value.*;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNativeFunction;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.WeakHashMap;

import static net.mobtalker.mobtalker2.server.script.lib.ScriptApiCheck.*;
import static net.mobtalker.mobtalker2.server.script.lib.ScriptApiConstants.*;
import static net.mobtalker.mobtalker2.util.LambdaUtil.combine;
import static net.mobtalker.mobtalkerscript.v3.MtsCheck.*;

public class MtsWorld extends MtsValueWithMetaTable
{
    private static WeakHashMap<WorldServer, MtsWorld> _cache = new WeakHashMap<>( 4 );
    
    public static MtsWorld of( World world )
    {
        assert world instanceof WorldServer;
        return _cache.computeIfAbsent( (WorldServer) world, w -> new MtsWorld( Metatables.get( TYPE ), w ) );
    }
    
    // ========================================
    
    public static final MtsType TYPE = MtsType.forName( "world" );
    
    // ========================================
    
    private final WorldServer _world;
    private MtsScoreboard _scoreboard = null;
    
    // ========================================
    
    private MtsWorld( MtsTable metatable, WorldServer world )
    {
        _world = world;
        setMetaTable( metatable );
    }
    
    // ========================================
    
    @Deprecated
    public WorldServer getWorld()
    {
        return toWorld();
    }
    
    public WorldServer toWorld()
    {
        return _world;
    }
    
    // ========================================
    
    public String getName()
    {
        return toWorld().getWorldInfo().getWorldName();
    }
    
    public String getDimensionName()
    {
        return toWorld().provider.getDimensionName();
    }
    
    public int getDimensionId()
    {
        return toWorld().provider.getDimensionId();
    }
    
    public String getWorldType()
    {
        return StringUtils.remove( toWorld().getWorldType().getWorldTypeName(), ' ' ).toLowerCase();
    }
    
    public WorldInfo getWorldInfo()
    {
        return toWorld().getWorldInfo();
    }
    
    public long getSeed()
    {
        return toWorld().getSeed();
    }
    
    // ========================================
    
    public MtsVector getSpawnPoint()
    {
        return MtsVector.of( toWorld().getSpawnPoint() );
    }
    
    public void setSpawnPoint( BlockPos pos )
    {
        toWorld().setSpawnPoint( pos );
    }
    
    public void setSpawnPoint( MtsVector vec )
    {
        setSpawnPoint( vec.toBlockPos() );
    }
    
    public void resetSpawnPoint()
    {
        toWorld().setInitialSpawnLocation();
    }
    
    // ========================================
    
    public long getWorldTime()
    {
        return toWorld().getWorldTime();
    }
    
    public boolean isDaytime()
    {
        return toWorld().isDaytime();
    }
    
    public int getMoonPhase()
    {
        return toWorld().getMoonPhase();
    }
    
    // ========================================
    
    public EnumDifficulty getDifficulty()
    {
        return toWorld().getDifficulty();
    }
    
    public String getDifficultyName()
    {
        return getDifficulty().name().toLowerCase();
    }
    
    public boolean isHardcore()
    {
        return getWorldInfo().isHardcoreModeEnabled();
    }
    
    // ========================================
    
    protected IBlockState getBlockStateAt( BlockPos pos )
    {
        return _world.getBlockState( pos );
    }
    
    protected IBlockState getBlockStateAt( MtsVector vec )
    {
        return getBlockStateAt( vec.toBlockPos() );
    }
    
    /**
     * @param pos   position at which the block state should be set
     * @param state block state to be set
     *
     * @return {@code true} if the placement was successful and the world was changed, {@code false} otherwise.
     */
    protected boolean setBlockStateAt( BlockPos pos, IBlockState state )
    {
        return _world.setBlockState( pos, state );
    }
    
    protected boolean setBlockStateAt( BlockPos pos, MtsBlockState state )
    {
        return setBlockStateAt( pos, state.toBlockState() );
    }
    
    /**
     * @param vec   position at which the block state should be set
     * @param state block state to be set
     *
     * @return {@code true} if the placement was successful and the world was changed, {@code false} otherwise.
     */
    protected boolean setBlockStateAt( MtsVector vec, IBlockState state )
    {
        return setBlockStateAt( vec.toBlockPos(), state );
    }
    
    protected boolean setBlockStateAt( MtsVector vec, MtsBlockState state )
    {
        return setBlockStateAt( vec, state.toBlockState() );
    }
    
    // ========================================
    
    public MtsBlock getBlockAt( BlockPos pos )
    {
        return MtsBlock.of( this, pos );
    }
    
    public MtsBlock getBlockAt( MtsVector vec )
    {
        return getBlockAt( vec.toBlockPos() );
    }
    
    public void breakBlock( BlockPos pos, boolean drop )
    {
        toWorld().destroyBlock( pos, drop );
    }
    
    public void breakBlock( MtsVector vec, boolean drop )
    {
        breakBlock( vec.toBlockPos(), drop );
    }
    
    // ========================================
    
    public MtsBiome getBiomeAt( BlockPos pos )
    {
        return MtsBiome.of( this, _world.getBiomeGenForCoords( pos ) );
    }
    
    public MtsBiome getBiomeAt( MtsVector vec )
    {
        return getBiomeAt( vec.toBlockPos() );
    }
    
    // ========================================
    
    public MtsScoreboard getScoreboard()
    {
        if ( _scoreboard == null )
        {
            _scoreboard = MtsScoreboard.of( toWorld().getScoreboard() );
        }
        return _scoreboard;
    }
    
    // ========================================
    
    @Override
    public MtsType getType()
    {
        return TYPE;
    }
    
    // ========================================
    
    @Override
    public int hashCode()
    {
        return Objects.hashCode( _world );
    }
    
    @Override
    public boolean equals( Object obj )
    {
        if ( this == obj )
        {
            return true;
        }
        if ( obj == this )
        {
            return false;
        }
        if ( !( obj instanceof MtsWorld ) )
        {
            return false;
        }
        
        MtsWorld other = (MtsWorld) obj;
        return Objects.equals( _world, other.toWorld() );
    }
    
    // ========================================
    
    @Override
    public String toString()
    {
        return TYPE.getName() + "[" + getDimensionId() + ":" + getDimensionName() + "]";
    }
    
    // ========================================
    
    public static class Methods
    {
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/World/GetName">wiki</a>
         * </p>
         *
         * Returns the save name of a world.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetName()} to a {@code World} value.
         * </p>
         *
         * @param argWorld world to get the save name of
         *
         * @return The save name of the world
         * @see WorldInfo#getWorldName()
         * @see <a href="http://mobtalker.net/Script_API/Types/World/GetName">wiki</a>
         */
        @MtsNativeFunction
        public static MtsString getName( MtsValue argWorld )
        {
            MtsWorld world = checkWorld( argWorld, 0 );
            return MtsString.of( world.getName() );
        }
        
        @Deprecated
        @MtsNativeFunction
        public static MtsNumber getID( MtsValue argWorld )
        {
            return getDimensionId( argWorld );
        }
        
        /**
         * Returns the dimension ID of a World value.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetDimensionId()} to a {@code World} value.
         * </p>
         *
         * @param argWorld world to get the dimension ID of
         *
         * @return The dimension ID of {@code argWorld}.
         * @see WorldProvider#getDimensionId()
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/World/GetDimensionId">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber getDimensionId( MtsValue argWorld )
        {
            MtsWorld world = checkWorld( argWorld, 0 );
            return MtsNumber.of( world.getDimensionId() );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/World/GetDimensionName">wiki</a>
         * </p>
         *
         * Returns the dimension name of a World value.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetDimensionName()} to a {@code World} value.
         * </p>
         *
         * @param argWorld world to get the dimension name of
         *
         * @return The dimension name of {@code argWorld}.
         * @see WorldProvider#getDimensionName()
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/World/GetName">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsString getDimensionName( MtsValue argWorld )
        {
            MtsWorld world = checkWorld( argWorld, 0 );
            return MtsString.of( world.getDimensionName() );
        }
        
        /**
         * Returns the world type name of a World value.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetType()} to a {@code World} value.
         * </p>
         *
         * @param argWorld world to get the world type name of
         *
         * @return The world type name of {@code argWorld}.
         * @see WorldType#getWorldTypeName()
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/World/GetType">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsString getType( MtsValue argWorld )
        {
            MtsWorld world = checkWorld( argWorld, 0 );
            return MtsString.of( world.getWorldType() );
        }
        
        /**
         * Returns the seed (i. e. initial random number generator state) used to generate a world. Worlds that have the same seed will look the same as long as
         * no modifications are made to the remaining settings of the world generator.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetSeed()} to a {@code world} value.
         * </p>
         *
         * @param argWorld The world for which the seed should be returned
         *
         * @return The seed used to generate the given world, represented as a number.
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/World/GetSeed">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsNumber getSeed( MtsValue argWorld )
        {
            return MtsNumber.of( checkWorld( argWorld, 0 ).getSeed() );
        }
        
        // ========================================
        
        /**
         * Returns the spawn point of a world. Spawn points are the locations at which players who do not have a custom spawn point yet (e. g. because they are
         * new to the world) will be spawned.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetSpawnPoint()} to a {@code world} value.
         * </p>
         *
         * @param argWorld World to get the spawn point for.
         *
         * @return The current spawn point of the world, represented as a {@link MtsVector vector} value.
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/World/GetSpawnPoint">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsVector getSpawnPoint( MtsValue argWorld )
        {
            return checkWorld( argWorld, 0 ).getSpawnPoint();
        }
        
        /**
         * Modifies the spawn point of a world.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :SetSpawnPoint(...)} to a {@code world} value.
         * </p>
         *
         * @param args A varargs value with the following restrictions:
         *             <ul>
         *             <li>The first parameter must be a {@code world} value which describes the world for which the spawn point should be set.</li>
         *             <li>After that, either a {@link MtsVector vector}, {@link MtsTable table}, {@link MtsCharacter character} or three {@link MtsNumber
         *             numbers} must be given and will be interpreted as coordinates where the spawn point should be placed.</li>
         *             </ul>
         *
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/World/SetSpawnPoint">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static void setSpawnPoint( MtsVarargs args )
        {
            checkWorld( args, 0 ).setSpawnPoint( checkVector( args, 1, true, true ) );
        }
        
        /**
         * Resets the spawn point of a world to its initial location.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :ResetSpawnPoint()} to a {@code world} value.
         * </p>
         *
         * @param argWorld The world to reset the spawn point for
         *
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/World/ResetSpawnPoint">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static void resetSpawnPoint( MtsValue argWorld )
        {
            checkWorld( argWorld, 0 ).resetSpawnPoint();
        }
        
        // ========================================
        
        /**
         * Returns the current time of a world, in ticks.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetTime()} to a {@code World} value.
         * </p>
         *
         * @param argWorld world to get the current time of
         *
         * @return The current time of {@code argWorld}, in ticks.
         * @see World#getWorldTime()
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/World/GetTime">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber getTime( MtsValue argWorld )
        {
            MtsWorld world = checkWorld( argWorld, 0 );
            return MtsNumber.of( world.getWorldTime() );
        }
        
        /**
         * Returns if it's currently daytime in a world.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :IsDaytime()} to a {@code World} value.
         * </p>
         *
         * @param argWorld world to check if it's currently daytime in
         *
         * @return {@link MtsBoolean#True true} if it's daytime in {@code argWorld}, {@link MtsBoolean#False false} otherwise.
         * @see World#isDaytime()
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/World/IsDaytime">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean isDaytime( MtsValue argWorld )
        {
            MtsWorld world = checkWorld( argWorld, 0 );
            return MtsBoolean.of( world.isDaytime() );
        }
        
        /**
         * Returns the current moon phase of a world. The moon phase is represented as a &lt;number&gt; and will always be in the range from 0 (full moon) to 7
         * (&frac34; waxing moon).
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetMoonphase()} to a {@code World} value.
         * </p>
         *
         * @param argWorld world to get the moon phase for
         *
         * @return The integer representation of the current moon phase.
         * @see World#getMoonPhase()
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/World/GetMoonphase">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber getMoonphase( MtsValue argWorld )
        {
            MtsWorld world = checkWorld( argWorld, 0 );
            return MtsNumber.of( world.getMoonPhase() );
        }
        
        /**
         * Returns the difficulty of a world and whether hardcore mode is enabled.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetDifficulty()} to a {@code World} value.
         * </p>
         *
         * @param argWorld world to get the difficulty information for
         *
         * @return A {@link MtsVarargs varargs} value whose first value (index 0) is the difficulty's name and second value (index 1) is a boolean which
         * describes if hardcore mode is enabled.
         * @see WorldInfo#getDifficulty()
         * @see WorldInfo#isHardcoreModeEnabled()
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/World/GetDifficulty">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVarargs getDifficulty( MtsValue argWorld )
        {
            MtsWorld world = checkWorld( argWorld, 0 );
            return MtsVarargs.of( MtsString.of( world.getDifficultyName() ), MtsBoolean.of( world.isHardcore() ) );
        }
        
        /**
         * Returns the current value of a selected game rule.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetGameRule(...)} to a {@code World} value.
         * </p>
         *
         * @param argWorld    world to get the value of a certain game rule for
         * @param argRuleName name of the game rule
         *
         * @return The value of the game rule. Depending on the value type of that game rule, that value may be either a boolean, &lt;number&gt; or string.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/World/GetGameRule">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsValue getGameRule( MtsValue argWorld, MtsValue argRuleName )
        {
            WorldServer world = checkWorld( argWorld, 0 ).toWorld();
            String ruleName = checkString( argRuleName, 1 );
            
            GameRules rules = world.getGameRules();
            if ( !rules.hasRule( ruleName ) )
            {
                throw new MtsArgumentException( 1, "unknown game rule '%s'", ruleName );
            }
            
            if ( rules.areSameType( ruleName, ValueType.BOOLEAN_VALUE ) )
            {
                return MtsBoolean.of( rules.getGameRuleBooleanValue( ruleName ) );
            }
            else if ( rules.areSameType( ruleName, ValueType.NUMERICAL_VALUE ) )
            {
                return MtsNumber.of( rules.getInt( ruleName ) );
            }
            else
            {
                return MtsString.of( rules.getGameRuleStringValue( ruleName ) );
            }
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/World/GetBiomeAt">wiki</a>
         * </p>
         *
         * Returns the biome at a given location.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetBiomeAt(...)} to a {@code World} value.
         * </p>
         *
         * @param args location or character to get the biome for; first parameter is considered to be the world in which to search for a biome
         *
         * @return The biome at the given location.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/World/GetBiomeAt">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsValue getBiomeAt( MtsVarargs args )
        {
            return checkWorld( args, 0 ).getBiomeAt( check2DVector( args, 1, true, true ) );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/World/GetBlockAt">wiki</a>
         * </p>
         *
         * Returns the block at a given position.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetBlockAt(...)} to a {@code World} value.
         * </p>
         *
         * @param args location to to get the block at; first parameter is considered to be the world from which to retrieve the block
         *
         * @return The block at the specified position.
         * @see <a href="http://mobtalker.net/Script_API/Types/World/GetBlockAt">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsValue getBlockAt( MtsVarargs args )
        {
            return checkWorld( args, 0 ).getBlockAt( checkVector( args, 1, true ) );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/World/SetBlockAt">wiki</a>
         * </p>
         *
         * Sets the block at a given position.
         *
         * TODO switch from metadata to block states
         *
         * </p>
         * This <i>method</i> can be used in scripts by appending {@code :SetBlockAt(...)} to a {@code World} value.
         * </p>
         *
         * @param args location to set the block at, material and metadata for the block that should be set; first parameter is considered to be the world in
         *             which the block should be set
         *
         * @return The block at the specified position.
         * @see <a href="http://mobtalker.net/Script_API/Types/World/SetBlockAt">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsValue setBlockAt( MtsVarargs args )
        {
            Counter c = new Counter( 0 );
            MtsWorld world = checkWorld( args, c.postIncrement() );
            BlockPos position = checkVector( args, c, true ).toBlockPos();
            MtsBlockState state = checkBlockState( args, c.postIncrement() );
            
            boolean success = world.setBlockStateAt( position, state );
            
            return success ? MtsBlock.of( world, position ) : MtsValue.Nil;
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/World/CanSetBlockAt">wiki</a>
         * </p>
         *
         * Checks if a block at a given position can be replaced.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :CanSetBlockAt(...)} to a {@code World} value.
         * </p>
         *
         * @param args location to check, material and metadata for the block that should be checked; first parameter is considered to be the world which should
         *             be checked
         *
         * @return {@link MtsBoolean#True true} if the material can replace the block at the given location, {@link MtsBoolean#False false} otherwise.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/World/CanSetBlockAt">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean canSetBlockAt( MtsVarargs args )
        {
            Counter c = new Counter( 0 );
            WorldServer world = checkWorld( args, c.postIncrement() ).toWorld();
            BlockPos position = checkVector( args, c, true ).toBlockPos();
            Block block = checkMaterial( args, c.postIncrement() ).toBlock();
            return MtsBoolean.of( block.canPlaceBlockAt( world, position ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/World/SpawnParticle">wiki</a>
         * </p>
         *
         * Spawns particles at a location.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :SpawnParticle(...)} to a {@code World} value.
         * </p>
         *
         * @param args a varargs argument for which the allowed argument types are:
         *             <ul>
         *             <li><code>&lt;world&gt;</code></li>
         *             <li><code>&lt;vector&gt;</code>, <code>&lt;character&gt;</code>, <code>&lt;table&gt;</code> or
         *             <code>&lt;number&gt; &lt;number&gt; &lt;number&gt;</code></li>
         *             <li><code>&lt;vector&gt;</code>, <code>&lt;table&gt;</code> or <code>&lt;number&gt; &lt;number&gt; &lt;number&gt;</code></li>
         *             <li>(optional) <code>&lt;table&gt;</code></li>
         *             </ul>
         *             Any further arguments will be ignored.
         *
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/World/SpawnParticle">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void spawnParticle( MtsVarargs args )
        {
            Counter c = new Counter( 0 );
            WorldServer world = checkWorld( args, c.postIncrement() ).toWorld();
            EnumParticleTypes particleType = ParticleUtil.getParticleTypeIdentifiedBy( args, c.postIncrement() );
            MtsVector pos = checkVector( args, c, true, true );
            MtsVector offset = checkVector( args, c, true );
            MtsTable options = args.count() >= c.value() ? checkTable( args, c.postIncrement() ) : null;
            
            SpawnParticleBuilder spb = new SpawnParticleBuilder( world, particleType );
            spb.withPosition( pos );
            spb.withOffset( offset );
            
            if ( options != null )
            {
                int index = c.value() - 1;
                
                // arguments
                checkOptionAndConsume( options, index, KEY_PARTICLE_ARGUMENTS,
                                       combine( Methods::checkIntVarargs, spb::withArguments ) );
                
                // count
                checkOptionAndConsume( options, index, KEY_PARTICLE_COUNT,
                                       combine( MtsCheck::checkInteger, spb::withCount ) );
                
                // longDistance
                checkOptionAndConsume( options, index, KEY_PARTICLE_LONGDISTANCE,
                                       combine( MtsValue::isTrue, spb::withLongDistanceEnabled ) );
                
                // speed
                checkOptionAndConsume( options, index, KEY_PARTICLE_SPEED,
                                       combine( MtsCheck::checkNumber, spb::withSpeed ) );
            }
            
            spb.run();
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/World/CanSeeSkyFrom">wiki article</a>
         * </p>
         *
         * Checks whether it is possible to see the sky from a given position.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :CanSeeSkyFrom(...)} to a {@code World} value.
         * </p>
         *
         * @param args a varargs argument for which the allowed argument types are:
         *             <ul>
         *             <li><code>&lt;world&gt;</code></li>
         *             <li><code>&lt;vector&gt;</code>, <code>&lt;character&gt;</code>, <code>&lt;table&gt;</code> or
         *             <code>&lt;number&gt; &lt;number&gt; &lt;number&gt;</code></li>
         *             </ul>
         *             All further arguments will be ignored.
         *
         * @return {@link MtsBoolean#True true} if the block is located above Y=62 or - if the block is at or below Y=62 - if all blocks that are above it and
         * below Y=63 are not opaque for light, {@link MtsBoolean#False false} otherwise.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/World/CanSeeSkyFrom">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean canSeeSkyFrom( MtsVarargs args )
        {
            WorldServer world = checkWorld( args, 0 ).toWorld();
            MtsVector vec = checkVector( args, 1, true, true );
            return MtsBoolean.of( world.canBlockSeeSky( vec.toBlockPos() ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/World/CanLightningStrikeAt">wiki article</a>
         * </p>
         *
         * Checks whether a lightning can strike at a given position.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :CanLightningStrike(...)} to a {@code World} value.
         * </p>
         *
         * @param args a varargs argument for which the allowed argument types are:
         *             <ul>
         *             <li><code>&lt;world&gt;</code></li>
         *             <li><code>&lt;vector&gt;</code>, <code>&lt;character&gt;</code>, <code>&lt;table&gt;</code> or
         *             <code>&lt;number&gt; &lt;number&gt; &lt;number&gt;</code></li>
         *             </ul>
         *             All further arguments will be ignored.
         *
         * @return {@link MtsBoolean#True true} if it is possible for a lightning to strike at the given position, {@link MtsBoolean#False false} otherwise.
         */
        @MtsNativeFunction
        public static MtsBoolean canLightningStrikeAt( MtsVarargs args )
        {
            WorldServer world = checkWorld( args, 0 ).toWorld();
            MtsVector vec = checkVector( args, 1, true, true );
            return MtsBoolean.of( world.canLightningStrike( vec.toBlockPos() ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/World/CanSnowAccumulateAt">wiki article</a>
         * </p>
         *
         * Checks whether snow layers can accumulate at a given position.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :CanSnowAccumulateAt(...)} to a {@code World} value.
         * </p>
         *
         * @param args a varargs argument for which the allowed argument types are:
         *             <ul>
         *             <li><code>&lt;world&gt;</code></li>
         *             <li><code>&lt;vector&gt;</code>, <code>&lt;table&gt;</code> or
         *             <code>&lt;number&gt; &lt;number&gt; &lt;number&gt;</code></li>
         *             </ul>
         *             All further arguments will be ignored.
         *
         * @return By default, if all of the following conditions are fulfilled, {@link MtsBoolean#True true} is returned, otherwise {@link MtsBoolean#False
         * false} is returned:
         * <ul>
         * <li>biome's temperature must be less than or equal to 0.15 (cold)</li>
         * <li>position must be between 0 and 255 (both inclusive)</li>
         * <li>light level must be less than 10</li>
         * <li>block at position must be air</li>
         * <li>it must be possible to place a snow layer at position</li>
         * </ul>
         * Modded worlds may have different conditions.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/World/CanSnowAccumulateAt">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean canSnowAccumulateAt( MtsVarargs args )
        {
            WorldServer world = checkWorld( args, 0 ).toWorld();
            MtsVector vec = checkVector( args, 1, true );
            return MtsBoolean.of( world.canSnowAt( vec.toBlockPos(), true ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/World/CreateExplosionAt">wiki article</a>
         * </p>
         *
         * Creates an explosion at a given position.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :CreateExplosionAt(...)} to a {@code World} value.
         * </p>
         *
         * @param args a varargs argument for which the allowed argument types are:
         *             <ul>
         *             <li><code>&lt;world&gt;</code></li>
         *             <li><code>&lt;vector&gt;</code>, <code>&lt;character&gt;</code>, <code>&lt;table&gt;</code> or
         *             <code>&lt;number&gt; &lt;number&gt; &lt;number&gt;</code></li>
         *             <li><code>&lt;number&gt;</code></li>
         *             <li>(optional) <code>&lt;boolean&gt;</code></li>
         *             <li>(optional) <code>&lt;boolean&gt;</code></li>
         *             </ul>
         *
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/World/CreateExplosionAt">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void createExplosionAt( MtsVarargs args )
        {
            Counter c = new Counter( 0 );
            WorldServer world = checkWorld( args, c.postIncrement() ).toWorld();
            MtsVector vec = checkVector( args, c, true, true );
            float strength = (float) checkNumber( args, c.postIncrement() );
            MtsValue options = args.get( c.value() );
            boolean flaming = false;
            boolean smoking = false;
            EntityLivingBase causedBy = null;
            
            if ( !options.isNil() )
            {
                flaming = checkOptionAndApply( options, c.value(), KEY_EXPLOSION_FLAMING, MtsValue::isTrue );
                smoking = checkOptionAndApply( options, c.value(), KEY_EXPLOSION_SMOKING, MtsValue::isTrue );
                causedBy = checkOptionAndApply( options, c.value(), KEY_EXPLOSION_CAUSEDBY, false,
                                                ScriptApiCheck::checkEntity );
            }
            
            world.newExplosion( causedBy, vec.getX(), vec.getY(), vec.getZ(), strength, flaming, smoking );
        }
        
        // ========================================
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Scrip_API/Types/World/GetClosestPlayerTo">wiki article</a>
         * </p>
         *
         * Returns the player that is closest to a given position or character. Note that this method only returs online players.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetClosestPlayerTo(...)} to a {@code World} value.
         * </p>
         *
         * @param args a varargs argument for which the allowed argument types are:
         *             <ul>
         *             <li><code>&lt;world&gt;</code></li>
         *             <li><code>&lt;vector&gt;</code>, <code>&lt;character&gt;</code>, <code>&lt;table&gt;</code> or
         *             <code>&lt;number&gt; &lt;number&gt; &lt;number&gt;</code></li>
         *             <li>(optional) <code>&lt;number&gt;</code></li>
         *             </ul>
         *
         * @return A character that represents the closest player. If a positive distance was specified and no player found within that range, {@code nil} is
         * returned.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/World/GetClosestPlayerTo">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsValue getClosestPlayerTo( MtsVarargs args )
        {
            Counter c = new Counter( 0 );
            WorldServer world = checkWorld( args, c.postIncrement() ).toWorld();
            MtsVector pos = checkVector( args, c, true, true );
            double distance = args.get( c.value() ).isNil() ? -1 : checkNumber( args, c.postIncrement() );
            EntityPlayer player = world.getClosestPlayer( pos.getX(), pos.getY(), pos.getZ(), distance );
            
            if ( player == null )
            {
                return MtsValue.Nil;
            }
            return MtsCharacter.of( player );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/World/GetPlayerByName">wiki article</a>
         * </p>
         *
         * Returns the player with a given name. Note that this method only returs online players.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetPlayerByName(...)} to a {@code World} value.
         * </p>
         *
         * @param argWorld world in which the player should be found
         * @param argName  player name
         *
         * @return A character representing the player or {@link MtsValue#Nil nil} if no player with a matching name could be found.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/World/GetPlayerByName">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsValue getPlayerByName( MtsValue argWorld, MtsValue argName )
        {
            WorldServer world = checkWorld( argWorld, 0 ).toWorld();
            String name = checkString( argName, 1 );
            EntityPlayer player = world.getPlayerEntityByName( name );
            
            if ( player == null )
            {
                return MtsValue.Nil;
            }
            return MtsCharacter.of( player );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/World/GetPlayerByUuid">wiki article</a>
         * </p>
         *
         * Returns the player with a given UUID. Note that this method only returs online players.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetPlayerByUuid(...)} to a {@code World} value.
         * </p>
         *
         * @param argWorld world in which the player should be found
         * @param argUuid  player name
         *
         * @return A character representing the player or {@link MtsValue#Nil nil} if no player with a matching UUID could be found.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/World/GetPlayerByUuid">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsValue getPlayerByUuid( MtsValue argWorld, MtsValue argUuid )
        {
            WorldServer world = checkWorld( argWorld, 0 ).toWorld();
            String id = checkString( argUuid, 1 );
            EntityPlayer player;
            
            try
            {
                UUID uuid = UUID.fromString( id );
                player = world.getPlayerEntityByUUID( uuid );
            }
            catch ( IllegalArgumentException e )
            {
                throw new MtsArgumentException( 1, "not a UUID string" );
            }
            
            if ( player == null )
            {
                return MtsValue.Nil;
            }
            return MtsCharacter.of( player );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/World/GetPlayers">wiki article</a>
         * </p>
         *
         * Returns a list of all players in a world. Note that this method only returs online players.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetPlayers()} to a {@code world} value.
         * </p>
         *
         * @param argWorld world from which all players should be returned
         *
         * @return A list of characters with each representing a player.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/World/GetPlayers">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsTable getPlayers( MtsValue argWorld )
        {
            WorldServer world = checkWorld( argWorld, 0 ).toWorld();
            List<?> players = world.getPlayers( EntityPlayer.class, p -> true );
            MtsTable table = new MtsTable( players.size(), 0 );
            MtsTableList list = table.list();
            
            for ( Object o : players )
            {
                // according to decompiled code from Forge, this should not be an issue
                // see also: net.minecraft.world.World.playerEntities
                EntityPlayer player = (EntityPlayer) o;
                list.add( MtsCharacter.of( player ) );
            }
            
            return table;
        }
        
        // ========================================
        
        /**
         * Returns the scoreboard for a world.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetScoreboard()} to a {@code world} value.
         * </p>
         *
         * @param argWorld The world to get the scoreboard for.
         *
         * @return The {@link MtsScoreboard scoreboard} value that is assigned to the given world.
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/World/GetScoreboard">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsScoreboard getScoreboard( MtsValue argWorld )
        {
            return checkWorld( argWorld, 0 ).getScoreboard();
        }
        
        // ========================================
        
        private static int[] checkIntVarargs( MtsValue value )
        {
            if ( value.isNil() )
            {
                return null;
            }
            if ( value.isTable() )
            {
                MtsTableList list = value.asTable().list();
                int[] array = new int[list.size()];
                for ( int i = 0; i < list.size(); i++ )
                {
                    array[i] = checkInteger( list.get( i ) );
                }
                return array;
            }
            if ( value.isNumber() )
            {
                return new int[] { checkInteger( value ) };
            }
            
            throw new MtsArgumentException( "bad argument (%s, %s or %s expected, got %s)", MtsType.NIL, MtsType.NUMBER,
                                            MtsType.TABLE, value.getType() );
        }
        
        // TODO WorldServer.getTopSolidOrLiquidBlock GetHighestBlockAt
        // TODO WorldServer.makeFireworks CreateFirework
    }
}
