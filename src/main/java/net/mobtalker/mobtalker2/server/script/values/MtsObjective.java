/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.values;

import static net.mobtalker.mobtalker2.server.script.lib.ScriptApiCheck.checkObjective;
import static net.mobtalker.mobtalker2.server.script.values.MtsScoreboard.checkPlayer;
import static net.mobtalker.mobtalkerscript.v3.MtsCheck.*;

import java.util.Objects;

import net.minecraft.scoreboard.*;
import net.mobtalker.mobtalker2.server.script.lib.metatables.Metatables;
import net.mobtalker.mobtalkerscript.v3.MtsArgumentException;
import net.mobtalker.mobtalkerscript.v3.value.*;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNativeFunction;

/**
 * @since 0.7.2
 */
public class MtsObjective extends MtsValueWithMetaTable
{
    public static MtsObjective of( MtsScoreboard scoreboard, ScoreObjective objective )
    {
        return new MtsObjective( Metatables.get( "objective" ), scoreboard, objective );
    }
    
    // ========================================
    
    public static final MtsType TYPE = MtsType.forName( "objective" );
    
    // ========================================
    
    private final MtsScoreboard _scoreboard;
    private final ScoreObjective _objective;
    
    // ========================================
    
    private MtsObjective( MtsTable metatable, MtsScoreboard scoreboard, ScoreObjective objective )
    {
        _scoreboard = scoreboard;
        _objective = objective;
        setMetaTable( metatable );
    }
    
    // ========================================
    
    public ScoreObjective toObjective()
    {
        return _objective;
    }
    
    // ========================================
    
    public MtsScoreboard getScoreboard()
    {
        return _scoreboard;
    }
    
    // ========================================
    
    public String getName()
    {
        return toObjective().getName();
    }
    
    public String getDisplayName()
    {
        return toObjective().getDisplayName();
    }
    
    public void setDisplayName( String name )
    {
        toObjective().setDisplayName( name );
    }
    
    public String getCriteria()
    {
        return toObjective().getCriteria().getName();
    }
    
    public boolean isReadOnly()
    {
        return toObjective().getCriteria().isReadOnly();
    }
    
    // ========================================
    
    private Score getInternalScore( String playerName )
    {
        return getScoreboard().getScore( playerName, this );
    }
    
    public int getScore( String playerName )
    {
        return getInternalScore( playerName ).getScorePoints();
    }
    
    public void setScore( String playerName, int score )
    {
        getInternalScore( playerName ).setScorePoints( score );
    }
    
    public int increaseScore( String playerName, int amount )
    {
        Score score = getInternalScore( playerName );
        score.increseScore( amount );
        return score.getScorePoints();
    }
    
    public int decreaseScore( String playerName, int amount )
    {
        Score score = getInternalScore( playerName );
        score.decreaseScore( amount );
        return score.getScorePoints();
    }
    
    // ========================================
    
    @Override
    public MtsType getType()
    {
        return TYPE;
    }
    
    // ========================================
    
    @Override
    public int hashCode()
    {
        return Objects.hashCode( _objective );
    }
    
    @Override
    public boolean equals( Object obj )
    {
        if ( this == obj ) return true;
        if ( obj == null ) return false;
        if ( !( obj instanceof MtsObjective ) ) return false;
        
        MtsObjective other = (MtsObjective) obj;
        return Objects.equals( _objective, other.toObjective() );
    }
    
    // ========================================
    
    @Override
    public String toString()
    {
        return String.format( "%s[%s]", getType().getName(), getName() );
    }
    
    // ========================================
    
    public static final class Methods
    {
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Objective/GetName">wiki article</a>
         * </p>
         *
         * Returns the registered name of an objective.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetName()} to an {@code objective} value.
         * </p>
         *
         * @param argObjective objective to get the name from
         * @return The registered name of the given objective.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Objective/GetName">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsString getName( MtsValue argObjective )
        {
            return MtsString.of( checkObjective( argObjective, 0 ).getName() );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Objective/GetDisplayName">wiki article</a>
         * </p>
         *
         * Returns the display name of an objective.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetDisplayName()} to an {@code objective}
         * value.
         * </p>
         *
         * @param argObjective objective to get the display name for
         * @return The display name of the given objective.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Objective/GetDisplayName">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsString getDisplayName( MtsValue argObjective )
        {
            return MtsString.of( checkObjective( argObjective, 0 ).getDisplayName() );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Objective/SetDisplayName">wiki article</a>
         * </p>
         *
         * Sets the display name of an objective.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :SetDisplayName(...)} to an {@code objective}
         * value.
         * </p>
         *
         * @param argObjective objective to set the display name for
         * @param argDisplayName new display name of the objective
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Objective/SetDisplayName">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void setDisplayName( MtsValue argObjective, MtsValue argDisplayName )
        {
            checkObjective( argObjective, 0 ).setDisplayName( checkString( argDisplayName, 1 ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Objective/GetCriteria">wiki article</a>
         * </p>
         *
         * Returns the criteria of an objective.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetCriteria()} to an {@code objective} value.
         * </p>
         *
         * @param argObjective objective to get the criteria for
         * @return A {@code string} that represents the criteria of the given objective.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Objective/GetCriteria">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsString getCriteria( MtsValue argObjective )
        {
            return MtsString.of( checkObjective( argObjective, 0 ).getCriteria() );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Objective/IsReadOnly">wiki article</a>
         * </p>
         *
         * Returns whether an objective is read-only.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :IsReadOnly()} to an {@code objective} value.
         * </p>
         *
         * @param argObjective objective to return the read-only status for
         * @return {@link MtsBoolean#True true} if the given objective is read-only, {@link MtsBoolean#False false}
         * otherwise.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Objective/IsReadOnly">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean isReadOnly( MtsValue argObjective )
        {
            return MtsBoolean.of( checkObjective( argObjective, 0 ).isReadOnly() );
        }
        
        // ========================================
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Objective/GetScore">wiki article</a>
         * </p>
         *
         * Returns a player's score on an objective.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetScore(...)} to an {@code objective} value.
         * </p>
         *
         * @param argObjective objective to return the score of
         * @param argPlayer player whose score should be returned
         * @return A {@code number} representing the score of the given player
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Objective/GetScore">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber getScore( MtsValue argObjective, MtsValue argPlayer )
        {
            MtsObjective objective = checkObjective( argObjective, 0 );
            String player = checkPlayer( argPlayer, 1 );
            return MtsNumber.of( objective.getScore( player ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Objective/SetScore">wiki article</a>
         * </p>
         *
         * Sets the score of a player on an objective.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :SetScore(...)} to an {@code objective} value.
         * </p>
         *
         * @param argObjective objective to set the score on
         * @param argPlayer player to set the score for
         * @param argScore score to set
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Objective/SetScore">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void setScore( MtsValue argObjective, MtsValue argPlayer, MtsValue argScore )
        {
            MtsObjective objective = checkObjective( argObjective, 0 );
            
            // TODO check if this if-statement is really necessary
            if ( objective.isReadOnly() )
                throw new MtsArgumentException( 0, "objective '%s' is read-only", objective.getName() );
            
            String player = checkPlayer( argPlayer, 1 );
            int score = checkInteger( argScore, 2 );
            objective.setScore( player, score );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Objective/IncreaseScore">wiki article</a>
         * </p>
         *
         * Increases the score of a player on an objective and returns it.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :IncreaseScore(...)} to an {@code objective}
         * value.
         * </p>
         *
         * @param argObjective objective to increase the score on
         * @param argPlayer player to increase the score for
         * @param argAmount score given to the player; if {@code nil} or not specified, defaults to 1.
         * @return A {@code number} which represents the new score of the player.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Objective/IncreaseScore">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber increaseScore( MtsValue argObjective, MtsValue argPlayer, MtsValue argAmount )
        {
            MtsObjective objective = checkObjective( argObjective, 0 );
            String player = checkPlayer( argPlayer, 1 );
            int amount = checkIntegerWithMinimum( argAmount, 2, 0, 1 );
            return MtsNumber.of( objective.increaseScore( player, amount ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Objective/DecreaseScore">wiki article</a>
         * </p>
         *
         * Increases the score of a player on an objective and returns it.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :DecreaseScore(...)} to an {@code objective}
         * value.
         * </p>
         *
         * @param argObjective objective to decrease the score on
         * @param argPlayer player to decrease the score for
         * @param argAmount score taken from the player; if {@code nil} or not specified, defaults to 1.
         * @return A {@code number} which represents the new score of the player.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Objective/DecreaseScore">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber decreaseScore( MtsValue argObjective, MtsValue argPlayer, MtsValue argAmount )
        {
            MtsObjective objective = checkObjective( argObjective, 0 );
            String player = checkPlayer( argPlayer, 1 );
            int amount = checkIntegerWithMinimum( argAmount, 2, 0, 1 );
            return MtsNumber.of( objective.decreaseScore( player, amount ) );
        }
    }
}
