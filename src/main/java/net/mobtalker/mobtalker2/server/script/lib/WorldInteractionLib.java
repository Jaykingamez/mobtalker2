/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.lib;

import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;
import net.mobtalker.mobtalker2.server.script.values.MtsWorld;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNativeFunction;

public class WorldInteractionLib
{
    private final IInteractionAdapter _adapter;
    
    // ========================================
    
    public WorldInteractionLib( IInteractionAdapter adapter )
    {
        _adapter = adapter;
    }
    
    // ========================================
    
    /**
     * Returns the world in which the current interaction is taking place.
     * 
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code World.GetCurrent()}.
     * </p>
     * 
     * @return a world object which represents the current world
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/World/GetCurrent">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public MtsWorld getCurrent()
    {
        return MtsWorld.of( _adapter.getEntity().worldObj );
    }
}
