/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.values;

import static net.mobtalker.mobtalker2.server.script.lib.ScriptApiCheck.*;
import static net.mobtalker.mobtalker2.util.CollectionUtil.generify;
import static net.mobtalker.mobtalkerscript.v3.MtsCheck.*;
import static org.apache.commons.lang3.Validate.notNull;

import java.util.*;
import java.util.Map.Entry;

import net.minecraft.scoreboard.*;
import net.mobtalker.mobtalker2.server.script.lib.metatables.Metatables;
import net.mobtalker.mobtalker2.util.CriteriaUtil;
import net.mobtalker.mobtalkerscript.v3.MtsArgumentException;
import net.mobtalker.mobtalkerscript.v3.value.*;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNativeFunction;

import joptsimple.internal.Strings;

/**
 * @since 0.7.2
 */
public class MtsScoreboard extends MtsValueWithMetaTable
{
    public static MtsScoreboard of( Scoreboard scoreboard )
    {
        return new MtsScoreboard( Metatables.get( TYPE ), scoreboard );
    }
    
    // ========================================
    
    public static final MtsType TYPE = MtsType.forName( "scoreboard" );
    
    // ========================================
    
    private final Scoreboard _scoreboard;
    
    // ========================================
    
    private MtsScoreboard( MtsTable metatable, Scoreboard scoreboard )
    {
        _scoreboard = notNull( scoreboard );
        setMetaTable( notNull( metatable ) );
    }
    
    // ========================================
    
    public Scoreboard toScoreboard()
    {
        return _scoreboard;
    }
    
    // ========================================
    
    protected Score getScore( String playerName, MtsObjective objective )
    {
        return toScoreboard().getValueFromObjective( playerName, objective.toObjective() );
    }
    
    public Map<MtsObjective, Score> getScores( String playerName )
    {
        Map<?, ?> scores = toScoreboard().getObjectivesForEntity( playerName );
        return generify( scores, e -> MtsObjective.of( this, (ScoreObjective) e ), e -> (Score) e );
    }
    
    public MtsObjective getObjective( String name )
    {
        ScoreObjective objective = toScoreboard().getObjective( name );
        if ( objective == null ) return null;
        return MtsObjective.of( this, objective );
    }
    
    public List<MtsObjective> getObjectives()
    {
        Collection<?> objectives = toScoreboard().getScoreObjectives();
        return generify( objectives, e -> MtsObjective.of( this, (ScoreObjective) e ) );
    }
    
    public boolean hasObjective( String name )
    {
        return getObjective( name ) != null;
    }
    
    public MtsObjective newObjective( String name, IScoreObjectiveCriteria criteria, String displayName )
    {
        ScoreObjective objective = toScoreboard().addScoreObjective( name, criteria );
        if ( displayName != null ) objective.setDisplayName( displayName );
        return MtsObjective.of( this, objective );
    }
    
    public void removeObjective( MtsObjective objective )
    {
        toScoreboard().removeObjective( objective.toObjective() );
    }
    
    public MtsObjective getDisplayedObjective( int slot )
    {
        ScoreObjective objective = toScoreboard().getObjectiveInDisplaySlot( slot );
        if ( objective == null ) return null;
        return MtsObjective.of( this, objective );
    }
    
    public void setDisplayedObjective( int slot, MtsObjective objective )
    {
        toScoreboard().setObjectiveInDisplaySlot( slot, objective.toObjective() );
    }
    
    public void clearDisplayedObjective( int slot )
    {
        toScoreboard().setObjectiveInDisplaySlot( slot, null );
    }
    
    public MtsTeam getTeam( String name )
    {
        ScorePlayerTeam team = toScoreboard().getTeam( name );
        if ( team == null ) return null;
        return MtsTeam.of( this, team );
    }
    
    public List<MtsTeam> getTeams()
    {
        Collection<?> teams = toScoreboard().getTeams();
        return generify( teams, e -> MtsTeam.of( this, (ScorePlayerTeam) e ) );
    }
    
    public boolean hasTeam( String name )
    {
        return getTeam( name ) != null;
    }
    
    public MtsTeam newTeam( String name )
    {
        return MtsTeam.of( this, toScoreboard().createTeam( notNull( name ) ) );
    }
    
    public void removeTeam( MtsTeam team )
    {
        toScoreboard().removeTeam( team.toTeam() );
    }
    
    public List<String> getPlayerNames()
    {
        Collection<?> players = toScoreboard().getObjectiveNames();
        return generify( players );
    }
    
    // ========================================
    // utility
    
    @Deprecated
    private String checkNewTeamName( MtsValue name )
    {
        String s = checkString( name );
        
        if ( hasTeam( s ) )
            throw new MtsArgumentException( "bad argument (team with name '%s' already exists)", s );
        
        return s;
    }
    
    @Deprecated
    private String checkNewTeamName( MtsValue name, int index )
    {
        String s = checkString( name );
        
        if ( hasTeam( s ) )
            throw new MtsArgumentException( index, "team with name '%s' already exists", s );
        
        return s;
    }
    
    // ========================================
    
    @Override
    public MtsType getType()
    {
        return TYPE;
    }
    
    // ========================================
    
    @Override
    public int hashCode()
    {
        return Objects.hashCode( _scoreboard );
    }
    
    @Override
    public boolean equals( Object obj )
    {
        if ( this == obj ) return true;
        if ( obj == null ) return false;
        if ( !( obj instanceof MtsScoreboard ) ) return false;
        
        MtsScoreboard other = (MtsScoreboard) obj;
        return Objects.equals( _scoreboard, other.toScoreboard() );
    }
    
    // ========================================
    
    @Override
    public String toString()
    {
        return String.format( "%s@%h", getType().getName(), this );
    }
    
    // ========================================
    
    public static String checkPlayer( MtsValue value, int index )
    {
        if ( value.isString() ) return checkString( value, index );
        if ( value.is( MtsCharacter.TYPE ) ) return checkCharacter( value, index ).getName();
        
        throw new MtsArgumentException( index, MtsType.STRING, MtsCharacter.TYPE );
    }
    
    // ========================================
    
    public static final class Methods
    {
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/SetDisplayedObjective">wiki article</a>
         * </p>
         *
         * Sets the displayed objective in a display slot.
         *
         * <p>
         * This <i>method</i> can be used by appending {@code :SetDisplayedObjective(...)} to a {@code scoreboard}
         * value.
         * </p>
         *
         * @param argScoreboard scoreboard to set the displayed objective for
         * @param argSlot slot to display the objective in
         * @param argObjective objective to display in the given slot
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/SetDisplayedObjective">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void setDisplayedObjective( MtsValue argScoreboard, MtsValue argSlot, MtsValue argObjective )
        {
            MtsScoreboard scoreboard = checkScoreboard( argScoreboard, 0 );
            int slot = checkDisplaySlot( argSlot, 1 );
            MtsObjective objective = checkObjective( argObjective, 2 );
            scoreboard.toScoreboard().setObjectiveInDisplaySlot( slot, objective.toObjective() );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/GetDisplayedObjective">wiki article</a>
         * </p>
         *
         * Returns the objective that is currently displayed in a display slot.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetDisplayedObjective(...)} to a
         * {@code scoreboard} value.
         * </p>
         *
         * @param argScoreboard scoreboard to check
         * @param argSlot display slot that should be inspected
         * @return An {@code objective} value which represents the objective that is being displayed or
         * {@link MtsValue#Nil nil} if no objective is currently being displayed in the given display slot
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/GetDisplayedObjective">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsValue getDisplayedObjective( MtsValue argScoreboard, MtsValue argSlot )
        {
            MtsScoreboard scoreboard = checkScoreboard( argScoreboard, 0 );
            int slot = checkDisplaySlot( argSlot, 1 );
            ScoreObjective objective = scoreboard.toScoreboard().getObjectiveInDisplaySlot( slot );
            if ( objective == null ) return MtsValue.Nil;
            return MtsObjective.of( scoreboard, objective );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/ClearDisplayedObjective">wiki article</a>
         * </p>
         *
         * Clears the displayed objective in a display slot.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :ClearDisplayedObjective(...)} to a
         * {@code scoreboard} value.
         * </p>
         *
         * @param argScoreboard scoreboard to clear the displayed objective for
         * @param argSlot slot which should be cleared
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/ClearDisplayedObjective">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void clearDisplayedObjective( MtsValue argScoreboard, MtsValue argSlot )
        {
            checkScoreboard( argScoreboard, 0 ).clearDisplayedObjective( checkDisplaySlot( argSlot, 1 ) );
        }
        
        // ========================================
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/GetObjective">wiki article</a>
         * </p>
         *
         * Returns an objective with a certain name from the list of objectives.
         *
         * <p>
         * This <i>method</i> can be used by appending {@code :GetObjective(...)} to a {@code scoreboard} value.
         * </p>
         *
         * @param argScoreboard scoreboard to query
         * @param argName name of the objective that should be returned
         * @return An {@code objective} value representing the objective with the given name if it exists,
         * {@link MtsValue#Nil nil} otherwise.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/GetObjective">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsValue getObjective( MtsValue argScoreboard, MtsValue argName )
        {
            MtsScoreboard scoreboard = checkScoreboard( argScoreboard, 0 );
            String name = checkString( argName, 1 );
            MtsObjective objective = scoreboard.getObjective( name );
            if ( objective == null ) return MtsValue.Nil;
            return objective;
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/GetObjectives">wiki article</a>
         * </p>
         *
         * Returns a list of all available objectives.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetObjectives()} to a {@code scoreboard} value.
         * </p>
         *
         * @param argScoreboard scoreboard to query
         * @return A {@code table} which contains a list of all objectives of the given scoreboard.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/GetObjectives">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsTable getObjectives( MtsValue argScoreboard )
        {
            MtsScoreboard scoreboard = checkScoreboard( argScoreboard, 0 );
            List<MtsObjective> objectives = scoreboard.getObjectives();
            MtsTable table = new MtsTable( objectives.size(), 0 );
            table.list().addAll( objectives );
            return table;
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/HasObjective">wiki article</a>
         * </p>
         *
         * Checks whether a scoreboard has an objective with a certain name.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :HasObjective(...)} to a {@code scoreboard}
         * value.
         * </p>
         *
         * @param argScoreboard scoreboard to query
         * @param argName name of the objective
         * @return {@link MtsBoolean#True true} if the given scoreboard contains an objective with the given name,
         * {@link MtsBoolean#False false} otherwise.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/HasObjective">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean hasObjective( MtsValue argScoreboard, MtsValue argName )
        {
            MtsScoreboard scoreboard = checkScoreboard( argScoreboard, 0 );
            String name = checkString( argName, 1 );
            return MtsBoolean.of( scoreboard.hasObjective( name ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/NewObjective">wiki article</a>
         * </p>
         *
         * Creates a new objective for a scoreboard.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :NewObjective(...)} to a {@code scoreboard}
         * value.
         * </p>
         *
         * @param argScoreboard scoreboard to create the new objective on
         * @param argName name of the new objective; must not be nil, empty or longer than 16 characters
         * @param argCriteria criteria for the new objective
         * @param argDisplayName (optional) display name of the new objective; must not be longer than 32 characters
         * @return An {@code objective} value representing the newly created objective.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/NewObjective">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsObjective newObjective( MtsValue argScoreboard, MtsValue argName, MtsValue argCriteria,
                                                 MtsValue argDisplayName )
        {
            MtsScoreboard scoreboard = checkScoreboard( argScoreboard, 0 );
            String name = checkString( argName, 1 );
            
            if ( Strings.isNullOrEmpty( name ) )
                throw new MtsArgumentException( 1, "name is empty" );
            if ( name.length() > 16 )
                throw new MtsArgumentException( 1, "name must be at most 16 characters long" );
            if ( scoreboard.hasObjective( name ) )
                throw new MtsArgumentException( 1, "objective with name '%s' already exists", name );
            
            IScoreObjectiveCriteria criteria = checkCriteria( argCriteria, 2 );
            String displayName;
            
            if ( argDisplayName.isNil() )
            {
                displayName = null;
            }
            else
            {
                displayName = checkString( argDisplayName, 3 );
            }
            
            return scoreboard.newObjective( name, criteria, displayName );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/RemoveObjective">wiki article</a>
         * </p>
         *
         * Removes an objective from a scoreboard.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :RemoveObjective(...)} to a {@code scoreboard}
         * value.
         * </p>
         *
         * @param argScoreboard scoreboard from which the objective should be removed from
         * @param argObjective objective to be removed
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/RemoveObjective">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void removeObjective( MtsValue argScoreboard, MtsValue argObjective )
        {
            checkScoreboard( argScoreboard, 0 ).removeObjective( checkObjective( argObjective, 1 ) );
        }
        
        // ========================================
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/GetPlayerNames">wiki article</a>
         * </p>
         *
         * Returns a list containing the names of all players of a scoreboard.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetPlayerNames()} to a {@code scoreboard} value.
         * </p>
         *
         * @param argScoreboard scoreboard to query
         * @return A {@code table} containing the names of all players of the given scoreboard.
         */
        @MtsNativeFunction
        public static MtsTable getPlayerNames( MtsValue argScoreboard )
        {
            MtsScoreboard scoreboard = checkScoreboard( argScoreboard, 0 );
            List<String> players = scoreboard.getPlayerNames();
            MtsTable table = new MtsTable( players.size(), 0 );
            MtsTableList list = table.list();
            
            for ( String s : players )
            {
                list.add( MtsString.of( s ) );
            }
            
            return table;
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/GetAllScoresOf">wiki article</a>
         * </p>
         *
         * Returns the scores for all objective a player has a score on.
         *
         * <p>
         * This <i>method</i> can be used by appending {@code :GetAllScoresOf(...)} to a {@code scoreboard} value.
         * </p>
         *
         * @param argScoreboard scoreboard to query
         * @param argPlayer player's name or a {@code character} representing a player
         * @return A {@code table} which maps {@code objective}s to the score the given player has achieved.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/GetAllScoresOf">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsTable getScoresOf( MtsValue argScoreboard, MtsValue argPlayer )
        {
            MtsScoreboard scoreboard = checkScoreboard( argScoreboard, 0 );
            String player = checkPlayer( argPlayer, 1 );
            Map<MtsObjective, Score> map = scoreboard.getScores( player );
            MtsTable table = new MtsTable( 0, map.size() );
            MtsTableMap m = table.map();
            
            for ( Entry<MtsObjective, Score> entry : map.entrySet() )
            {
                m.put( entry.getKey(), MtsNumber.of( entry.getValue().getScorePoints() ) );
            }
            
            return table;
        }
        
        // ========================================
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/GetTeam">wiki article</a>
         * </p>
         *
         * Returns a team with a certain name from the list of teams.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetTeam(...)} to a {@code scoreboard} value.
         * </p>
         *
         * @param argScoreboard scoreboard to query
         * @param argName name of the team that should be returned
         * @return A {@code team} representing the team with the given name if it exists, {@link MtsValue#Nil nil}
         * otherwise.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/GetTeam">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsValue getTeam( MtsValue argScoreboard, MtsValue argName )
        {
            MtsScoreboard scoreboard = checkScoreboard( argScoreboard, 0 );
            String name = checkString( argName, 1 );
            MtsTeam team = scoreboard.getTeam( name );
            if ( team == null ) return MtsValue.Nil;
            return team;
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/GetTeams">wiki article</a>
         * </p>
         *
         * Returns a list of all available teams.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetTeams()} to a {@code scoreboard} value.
         * </p>
         *
         * @param argScoreboard scoreboard to query
         * @return A {@code table} which contains a list of all teams of the given scoreboard.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/GetTeams">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsTable getTeams( MtsValue argScoreboard )
        {
            MtsScoreboard scoreboard = checkScoreboard( argScoreboard, 0 );
            List<MtsTeam> teams = scoreboard.getTeams();
            MtsTable table = new MtsTable( teams.size(), 0 );
            table.list().addAll( teams );
            return table;
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/HasTeam">wiki article</a>
         * </p>
         *
         * Checks whether a scoreboard has a team with a given name.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :HasTeam(...)} to a {@code scoreboard} value.
         * </p>
         *
         * @param argScoreboard scoreboard to query
         * @param argName name of the team
         * @return {@link MtsBoolean#True true} if the given scoreboard contains a team with the given name,
         * {@link MtsBoolean#False false} otherwise.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/HasTeam">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean hasTeam( MtsValue argScoreboard, MtsValue argName )
        {
            return MtsBoolean.of( checkScoreboard( argScoreboard, 0 ).hasTeam( checkString( argName, 1 ) ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/NewTeam">wiki article</a>
         * </p>
         *
         * Creates a new team for a scoreboard.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :NewTeam(...)} to a {@code scoreboard} value.
         * </p>
         *
         * @param argScoreboard scoreboard to create the new team on
         * @param argName name of the new team
         * @return A {@code team} value representing the newly created team.
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/NewTeam">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsTeam newTeam( MtsValue argScoreboard, MtsValue argName )
        {
            MtsScoreboard scoreboard = checkScoreboard( argScoreboard, 0 );
            return scoreboard.newTeam( checkString( argName, 1 ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/RemoveTeam">wiki article</a>
         * </p>
         *
         * Removes a team from a scoreboard.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :RemoveTeam(...)} to a {@code scoreboard} value.
         * </p>
         *
         * @param argScoreboard scoreboard to remove the team from
         * @param argTeam team to be removed
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Scoreboard/RemoveTeam">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static void removeTeam( MtsValue argScoreboard, MtsValue argTeam )
        {
            checkScoreboard( argScoreboard, 0 ).removeTeam( checkTeam( argTeam, 1 ) );
        }
        
        // ========================================
        
        private static int checkDisplaySlot( MtsValue value, int index )
        {
            int slot;
            if ( value.isString() )
            {
                slot = Scoreboard.getObjectiveDisplaySlotNumber( checkString( value, index ) );
            }
            else if ( value.isNumber() )
            {
                slot = checkInteger( value, index );
                if ( Scoreboard.getObjectiveDisplaySlot( slot ) == null ) slot = -1;
            }
            else
            {
                throw new MtsArgumentException( index, MtsType.STRING, MtsType.NUMBER, value.getType() );
            }
            
            if ( slot < 0 )
                throw new MtsArgumentException( index, "unknown display slot '%s'", value );
            
            return slot;
        }
        
        private static IScoreObjectiveCriteria checkCriteria( MtsValue name, int index )
        {
            String key = checkString( name, index );
            IScoreObjectiveCriteria criteria = CriteriaUtil.getCriteria( key );
            
            if ( criteria == null )
                throw new MtsArgumentException( index, "unknown criteria '%s'", key );
            
            return criteria;
        }
    }
}
