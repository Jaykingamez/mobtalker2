/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.lib;

import static net.mobtalker.mobtalkerscript.v3.MtsCheck.checkString;

import java.util.*;

import com.google.common.collect.Lists;

import net.minecraft.command.*;
import net.minecraft.command.CommandResultStats.Type;
import net.minecraft.entity.Entity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.*;
import net.minecraft.world.World;
import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;
import net.mobtalker.mobtalker2.server.script.ScriptFactory;
import net.mobtalker.mobtalkerscript.v3.MtsArgumentException;
import net.mobtalker.mobtalkerscript.v3.value.*;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNativeFunction;

import org.apache.commons.lang3.StringUtils;

public class CommandLib implements ICommandSender
{
    public static MtsTable create( IInteractionAdapter adapter, Collection<String> commands )
    {
        CommandLib lib = new CommandLib( adapter );
        MtsTable t = new MtsTable( 0, 1 + commands.size() );
        
        t.set( "Execute", new Execute( lib ) );
        for ( String command : commands )
        {
            MtsString key = MtsString.of( command );
            t.set( key, new CommandWrapper( lib, key ) );
        }
        
        return t;
    }
    
    // ========================================
    
    private final IInteractionAdapter _adapter;
    
    // ========================================
    
    private CommandLib( IInteractionAdapter adapter )
    {
        _adapter = adapter;
    }
    
    // ========================================
    
    /**
     * Executes a Minecraft command with administrative privileges via this interaction's entity.
     * 
     * @param args arguments for the command; first value is considered to be the command itself
     * @return A varargs value. The first value (index 0) is either {@code true} or {@code false}, depending on if the command execution was successful or not.
     *         The second value (index 1) is the number of the command's executions. If the command execution wasn't successful, this equals 0.
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Command/Execute">MobTalker2 Wiki</a>
     */
    public MtsVarargs execute( MtsVarargs args )
    {
        String command = checkString( args, 0 );
        
        List<String> arguments;
        int nArguments = args.count() - 1;
        
        if ( nArguments > 0 )
        {
            arguments = Lists.newArrayListWithCapacity( nArguments );
            for ( int i = 1; i <= nArguments; i++ )
            {
                arguments.add( checkString( args.get( i ), i ) );
            }
        }
        else
        {
            arguments = Collections.emptyList();
        }
        
        ICommandManager manager = MinecraftServer.getServer().getCommandManager();
        if ( !manager.getCommands().containsKey( command ) )
            throw new MtsArgumentException( "unknown command '%s'", command );
        
        int executions = arguments.isEmpty() ? manager.executeCommand( this, command ) : manager.executeCommand( this, command + " " + StringUtils.join(
                                                                                                                                                         arguments,
                                                                                                                                                         " " ) );
        
        return MtsVarargs.of( MtsBoolean.of( executions > 0 ), MtsNumber.of( executions ) );
    }
    
    /**
     * <p>
     * TODO <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Command/IsAvailable">MobTalker2 Wiki</a>
     * </p>
     * 
     * Checks if a Minecraft command is available through the script API.
     * 
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Command.IsAvailable(...)}.
     * </p>
     * 
     * @param argCommand command to check
     * @return {@link MtsBoolean#True true} if the command is available, {@link MtsBoolean#False false} otherwise.
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Command/IsAvailable">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public MtsValue isAvailable( MtsValue argCommand )
    {
        String command = checkString( argCommand, 0 );
        
        return MtsBoolean.of( ScriptFactory.getAvailableMinecraftCommands().contains( command ) );
    }
    
    // ========================================
    
    @Override
    public String getCommandSenderName()
    {
        return "[MobTalker2]";
    }
    
    @Override
    public IChatComponent getDisplayName()
    {
        return new ChatComponentText( getCommandSenderName() );
    }
    
    @Override
    public Entity getCommandSenderEntity()
    {
        return _adapter.getEntity();
    }
    
    @Override
    public void addChatMessage( IChatComponent msg )
    {
        // TODO
    }
    
    @Override
    public boolean canCommandSenderUseCommand( int i, String s )
    {
        return true;
    }
    
    @Override
    public BlockPos getPosition()
    {
        return getCommandSenderEntity().getPosition();
    }
    
    @Override
    public Vec3 getPositionVector()
    {
        return getCommandSenderEntity().getPositionVector();
    }
    
    @Override
    public World getEntityWorld()
    {
        return getCommandSenderEntity().worldObj;
    }
    
    @Override
    public void setCommandStat( Type type, int amount )
    {
        // TODO
    }
    
    @Override
    public boolean sendCommandFeedback()
    {
        return false;
    }
    
    // ========================================
    
    private static final class CommandWrapper extends MtsFunction
    {
        private final CommandLib _lib;
        private final MtsString _command;
        
        public CommandWrapper( CommandLib lib, MtsString command )
        {
            _lib = lib;
            _command = command;
        }
        
        @Override
        public MtsVarargs call( MtsVarargs args )
        {
            return _lib.execute( MtsVarargs.of( _command, args ) );
        }
    }
    
    private static final class Execute extends MtsFunction
    {
        private final CommandLib _lib;
        
        public Execute( CommandLib lib )
        {
            _lib = lib;
        }
        
        @Override
        public MtsVarargs call( MtsVarargs args )
        {
            return _lib.execute( args );
        }
    }
}
