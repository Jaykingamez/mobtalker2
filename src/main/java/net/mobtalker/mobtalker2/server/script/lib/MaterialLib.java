/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.lib;

import static net.mobtalker.mobtalkerscript.v3.MtsCheck.checkString;

import java.util.Set;

import net.minecraft.block.Block;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.*;
import net.mobtalker.mobtalker2.server.script.values.MtsMaterial;
import net.mobtalker.mobtalkerscript.v3.MtsRuntimeException;
import net.mobtalker.mobtalkerscript.v3.value.*;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNativeFunction;

public class MaterialLib
{
    @MtsNativeFunction
    public static MtsBoolean isAvailable( MtsValue argMaterial )
    {
        ResourceLocation materialName = new ResourceLocation( checkString( argMaterial, 1 ) );
        boolean available = GameData.getBlockRegistry().containsKey( materialName );
        return MtsBoolean.of( available );
    }
    
    @MtsNativeFunction
    public static MtsTable getMaterials()
    {
        FMLControlledNamespacedRegistry<Block> registry = GameData.getBlockRegistry();
        Set<?> keys = registry.getKeys();
        MtsTable table = new MtsTable( keys.size(), 0 );
        for ( Object key : keys )
        {
            table.list().add( MtsString.of( key.toString() ) );
        }
        return table;
    }
    
    @MtsNativeFunction
    public static MtsValue getMaterial( MtsValue argMaterial )
    {
        ResourceLocation materialName = new ResourceLocation( checkString( argMaterial, 1 ) );
        if ( !GameData.getBlockRegistry().containsKey( materialName ) )
            throw new MtsRuntimeException( "requested material \"%s\" is not available", argMaterial );
        Block block = GameData.getBlockRegistry().getObject( materialName );
        return MtsMaterial.of( block );
    }
}
