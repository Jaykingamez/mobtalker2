/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.resources.scriptpack;

import static net.minecraftforge.fml.relauncher.Side.*;

import java.nio.file.Path;
import java.util.List;

import net.minecraftforge.fml.common.versioning.ArtifactVersion;
import net.minecraftforge.fml.relauncher.SideOnly;

public interface IScriptPack
{
    public String getTitle();
    
    public String getDescription();
    
    public String getVersion();
    
    public Path getPath();
    
    public ArtifactVersion getApiVersion();
    
    public List<String> getAuthors();
    
    public List<ScriptPackEntry> getScripts();
    
    @SideOnly( CLIENT )
    public boolean hasResources();
    
    public boolean hasDownloadableResources();
    
    /**
     * If {@link #hasDownloadableResources()} is {@code true}, returns the path
     * to the file containing the resources.
     * 
     * @return file which contains the resources
     */
    public Path getDownloadableResourcesFile();
    
    public void close();
}
