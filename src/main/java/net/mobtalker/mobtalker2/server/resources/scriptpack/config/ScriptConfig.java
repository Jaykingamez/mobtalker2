/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.resources.scriptpack.config;

import net.mobtalker.mobtalker2.util.json.JsonObject;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;

import java.util.List;
import java.util.Set;

public abstract class ScriptConfig
{
    protected static final MobTalkerLog LOG = new MobTalkerLog( "ScriptConfig" );
    
    public static final String KEY_TYPE = "Type";
    public static final String KEY_IDENTIFIER = "Identifier";
    public static final String KEY_SAVEDVARIABLESPERENTITY = "SavedVariablesPerEntity";
    public static final String KEY_FILES = "Files";
    
    /** @deprecated Will be replaced by {@link #KEY_SAVEDVARIABLESPERENTITY} in a future version. */
    @Deprecated public static final String KEY_SAVEDVARIABLES = "SavedVariables";
    
    // ========================================
    
    protected static <T extends ScriptConfig> T deserialize( T config, JsonObject json )
    {
        config.setIdentifier( json.getString( KEY_IDENTIFIER, "" ) );
        config.setScriptPaths( json.getStringList( KEY_FILES ) );
        config.setSavedVariablesPerEntity( json.getStringSet( KEY_SAVEDVARIABLESPERENTITY,
                                                              json.getStringSet( KEY_SAVEDVARIABLES ) ) );
        
        if ( json.has( KEY_SAVEDVARIABLES ) )
        {
            LOG.warn( "Option \"%s\" is deprecated and will be removed in a future version. " +
                      "Consider switching to \"%s\" instead.", KEY_SAVEDVARIABLES, KEY_SAVEDVARIABLESPERENTITY );
        }
        
        return config;
    }
    
    // ========================================
    
    private String _identifier;
    private List<String> _scriptPaths;
    
    /** List of variables whose values should be saved on the entity, and are accessible regardless of the interacting player */
    private Set<String> _savedVariablesPerEntity;
    
    // ========================================
    
    public String getIdentifier()
    {
        return _identifier;
    }
    
    public void setIdentifier( String identifier )
    {
        _identifier = identifier;
    }
    
    public Set<String> getSavedVariablesPerEntity()
    {
        return _savedVariablesPerEntity;
    }
    
    public void setSavedVariablesPerEntity( Set<String> savedVariablesPerEntity )
    {
        _savedVariablesPerEntity = savedVariablesPerEntity;
    }
    
    public List<String> getScriptPaths()
    {
        return _scriptPaths;
    }
    
    public void setScriptPaths( List<String> scriptPaths )
    {
        _scriptPaths = scriptPaths;
    }
}
