/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.resources.scriptpack;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import net.mobtalker.mobtalker2.common.entity.EntityType;
import net.mobtalker.mobtalker2.util.PathUtil;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;
import org.apache.logging.log4j.core.helpers.Strings;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.Map.Entry;

import static com.google.common.base.Preconditions.checkState;

public class ScriptPackRepository
{
    private static final MobTalkerLog LOG = new MobTalkerLog( "ScriptPackRepository" );
    
    // ========================================
    
    private static ScriptPackRepository instance;
    
    // ========================================
    
    public static ScriptPackRepository instance()
    {
        checkState( isLoaded(), "No ScriptPackRepository is currently loaded" );
        
        return instance;
    }
    
    public static boolean isLoaded()
    {
        return instance != null;
    }
    
    public static void load( List<Path> repositoryPaths )
    {
        checkState( !isLoaded(), "already loaded" );
        
        LOG.debug( "Loading ScriptPackRepository" );
        
        instance = new ScriptPackRepository();
        
        for ( Path path : repositoryPaths )
        {
            if ( !Files.exists( path ) )
            {
                try
                {
                    Files.createDirectories( path );
                    continue;
                }
                catch ( IOException ex )
                {
                    LOG.error( ex,
                               "unable to create script repository with path '%s'",
                               PathUtil.relativizeMinecraftPath( path ) );
                }
            }
            
            LOG.debug( "Scanning '%s' for script packs", PathUtil.relativizeMinecraftPath( path ) );
            
            ScriptPackDiscoverer discoverer = new ScriptPackDiscoverer();
            try
            {
                for ( IScriptPack scriptPack : discoverer.discover( path ) )
                {
                    instance.addScriptPack( scriptPack );
                }
            }
            catch ( IOException ex )
            {
                LOG.error( ex, "Could not read script pack repository path" );
            }
            
            instance.addWarnings( discoverer.getWarnings() );
        }
        
        instance.loadScriptPackEntries();
    }
    
    public static void unload()
    {
        checkState( isLoaded(), "no ScriptPackRepository is currently loaded" );
        
        instance.clear();
        instance = null;
    }
    
    // ========================================
    
    /**
     * All ScriptPacks.
     */
    private final List<IScriptPack> _scriptPacks;
    
    /**
     * Active Script Packs. Used to determine which resources need to be loaded/downloaded.
     */
    private final Set<IScriptPack> _activePacks;
    
    private final Set<InteractionScriptEntry> _activeInteractionScripts;
    private final Map<String, InteractionScriptEntry> _interactionScriptsPerName;
    private final Map<EntityType, InteractionScriptEntry> _interactionScriptsPerType;
    private InteractionScriptEntry _catchAllInteractionScript;
    
    /**
     * List of warnings that occurred while discovering packs.
     */
    private final List<String> _warnings;
    
    // ========================================
    
    public ScriptPackRepository()
    {
        _scriptPacks = Lists.newArrayListWithExpectedSize( 1 );
        _activePacks = Sets.newHashSetWithExpectedSize( 1 );
        
        _activeInteractionScripts = Sets.newHashSetWithExpectedSize( 1 );
        _interactionScriptsPerName = Maps.newHashMapWithExpectedSize( 1 );
        _interactionScriptsPerType = Maps.newHashMapWithExpectedSize( 1 );
        
        _warnings = Lists.newArrayListWithExpectedSize( 1 );
    }
    
    // ========================================
    
    public boolean hasScripts()
    {
        return !_activeInteractionScripts.isEmpty();
    }
    
    // ========================================
    
    /**
     * A view on all registered ScriptPacks in this repository.
     */
    public List<IScriptPack> getScriptPacks()
    {
        return Collections.unmodifiableList( _scriptPacks );
    }
    
    /**
     * A view on which ScriptPacks have at least one active entry.
     */
    public Set<IScriptPack> getActiveScriptPacks()
    {
        return Collections.unmodifiableSet( _activePacks );
    }
    
    /**
     * A view on the active script entries in this repository.
     */
    public Set<InteractionScriptEntry> getActiveInteractionScripts()
    {
        return Collections.unmodifiableSet( _activeInteractionScripts );
    }
    
    public InteractionScriptEntry getScriptForName( String name )
    {
        return _interactionScriptsPerName.get( name );
    }
    
    public InteractionScriptEntry getScriptForType( EntityType type )
    {
        InteractionScriptEntry entry = _interactionScriptsPerType.get( type );
        if ( entry == null )
        {
            return _catchAllInteractionScript;
        }
        return entry;
    }
    
    // ========================================
    
    public void addWarnings( Collection<String> warnings )
    {
        _warnings.addAll( warnings );
    }
    
    public List<String> getWarnings()
    {
        return _warnings;
    }
    
    // ========================================
    
    private void clear()
    {
        for ( IScriptPack pack : _scriptPacks )
        {
            pack.close();
        }
        
        _scriptPacks.clear();
    }
    
    public void addScriptPack( IScriptPack pack )
    {
        if ( _scriptPacks.contains( pack ) )
        {
            LOG.warn( "Tried to add script pack '%s' twice", pack.getTitle() );
            return;
        }
        
        _scriptPacks.add( pack );
    }
    
    public void loadScriptPackEntries()
    {
        Map<String, InteractionScriptEntry> perNameEntry = Maps.newHashMap();
        Map<EntityType, InteractionScriptEntry> perTypeEntry = Maps.newHashMap();
        
        for ( IScriptPack pack : _scriptPacks )
        {
            LOG.debug( "Reading ScriptPack entries for %s", pack.getTitle() );
            
            for ( ScriptPackEntry entry : pack.getScripts() )
            {
                LOG.debug( "Found script pack entry of type '%s' from '%s'", entry.getClass().getSimpleName(), entry.getScriptPack().getTitle() );
                if ( entry instanceof InteractionScriptEntry )
                {
                    InteractionScriptEntry script = (InteractionScriptEntry) entry;
                    if ( Strings.isNotEmpty( script.getName() ) )
                    {
                        String name = script.getName();
                        if ( perNameEntry.containsKey( name ) )
                        {
                            LOG.warn( "Script pack \"%s\" is overriding interaction script \"%s\" from \"%s\"", pack.getTitle(), name,
                                      perNameEntry.get( name ).getScriptPack().getTitle() );
                        }
                        
                        perNameEntry.put( name, script );
                        LOG.debug( "Added script '%s' from '%s' to the map of scripts by name", name, script.getScriptPack().getTitle() );
                    }
                    
                    for ( String typeName : script.getEntities() )
                    {
                        EntityType type = EntityType.forName( typeName );
                        if ( perTypeEntry.containsKey( type ) )
                        {
                            LOG.warn( "Script pack \"%s\" is overriding an interaction script for \"%s\" from \"%s\"", pack.getTitle(), type,
                                      perTypeEntry.get( type ).getScriptPack().getTitle() );
                        }
                        
                        perTypeEntry.put( type, script );
                        LOG.debug( "Added script for '%s' from '%s' to the map of scripts by type", type, script.getScriptPack().getTitle() );
                    }
                }
            }
        }
        
        // Clean up from previous runs, if any
        _activePacks.clear();
        
        _activeInteractionScripts.clear();
        _interactionScriptsPerName.clear();
        _interactionScriptsPerType.clear();
        
        for ( Entry<String, InteractionScriptEntry> e : perNameEntry.entrySet() )
        {
            String name = e.getKey();
            InteractionScriptEntry entry = e.getValue();
            
            _interactionScriptsPerName.put( name, entry );
            _activeInteractionScripts.add( entry );
            _activePacks.add( entry.getScriptPack() );
            
            LOG.info( "Activated interaction script '%s' from '%s'", name, entry.getScriptPack().getTitle() );
        }
        
        for ( Entry<EntityType, InteractionScriptEntry> e : perTypeEntry.entrySet() )
        {
            EntityType type = e.getKey();
            InteractionScriptEntry entry = e.getValue();
            
            if ( type == EntityType.All )
            {
                _catchAllInteractionScript = entry;
                LOG.debug( "Set script '%s' for type '%s' from '%s' as fallback/catchall script", entry.getName(), type, entry.getScriptPack().getTitle() );
            }
            else
            {
                _interactionScriptsPerType.put( type, entry );
            }
            _activeInteractionScripts.add( entry );
            _activePacks.add( entry.getScriptPack() );
            
            LOG.info( "Activated interaction script for '%s' from '%s'", type, entry.getScriptPack().getTitle() );
        }
    }
}
