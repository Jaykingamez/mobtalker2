/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.resources.scriptpack.config;

import net.mobtalker.mobtalker2.util.json.JsonObject;

import java.util.Set;

public final class InteractionScriptConfig extends ScriptConfig
{
    public static final String TYPE = "Interaction";
    public static final String KEY_NAME = "Name";
    public static final String KEY_ENTITIES = "Entities";
    public static final String KEY_SAVEDVARIABLESPERPLAYER = "SavedVariablesPerPlayer";
    public static final String KEY_SAVEDVARIABLESPERPLAYERANDENTITY = "SavedVariablesPerPlayerAndEntity";
    public static final String KEY_GUI = "GUI";
    
    // ========================================
    
    public static InteractionScriptConfig deserialize( JsonObject json )
    {
        InteractionScriptConfig ret = ScriptConfig.deserialize( new InteractionScriptConfig(), json );
        
        ret.setName( json.getString( KEY_NAME, "" ) );
        ret.setEntities( json.getStringSet( KEY_ENTITIES ) );
        ret.setGuiConfigPath( json.getString( KEY_GUI, null ) );
        
        if ( json.has( KEY_SAVEDVARIABLES )
             || ( !json.has( KEY_SAVEDVARIABLESPERENTITY ) && !json.has( KEY_SAVEDVARIABLESPERPLAYERANDENTITY ) ) )
        {
            ret.setSavedVariablesPerPlayerAndEntity( json.getStringSet( KEY_SAVEDVARIABLESPERPLAYER ) );
            LOG.warn( "Option \"%s\" will be renamed to \"%s\" in a future version. Please consider " +
                      "switching to \"%2$s\" instead.", KEY_SAVEDVARIABLESPERPLAYER, KEY_SAVEDVARIABLESPERPLAYERANDENTITY );
        }
        else
        {
            ret.setSavedVariablesPerPlayer( json.getStringSet( KEY_SAVEDVARIABLESPERPLAYER ) );
            ret.setSavedVariablesPerPlayerAndEntity( json.getStringSet( KEY_SAVEDVARIABLESPERPLAYERANDENTITY ) );
        }
        
        return ret;
    }
    
    // ========================================
    
    private String _name;
    private Set<String> _entities;
    private String _guiConfigPath;
    
    /** List of variables whose values should be saved on the interacting player */
    private Set<String> _savedVariablesPerPlayer;
    
    /** List of variables whose values should be saved on the entity, and vary depending on the interacting player */
    private Set<String> _savedVariablesPerPlayerAndEntity;
    
    // ========================================
    
    public String getName()
    {
        return _name;
    }
    
    public void setName( String name )
    {
        _name = name;
    }
    
    public Set<String> getEntities()
    {
        return _entities;
    }
    
    public void setEntities( Set<String> entities )
    {
        _entities = entities;
    }
    
    public String getGuiConfigPath()
    {
        return _guiConfigPath;
    }
    
    public void setGuiConfigPath( String guiConfigPath )
    {
        _guiConfigPath = guiConfigPath;
    }
    
    public Set<String> getSavedVariablesPerPlayer()
    {
        return _savedVariablesPerPlayer;
    }
    
    public void setSavedVariablesPerPlayer( Set<String> savedVariablesPerPlayer )
    {
        _savedVariablesPerPlayer = savedVariablesPerPlayer;
    }
    
    public Set<String> getSavedVariablesPerPlayerAndEntity()
    {
        return _savedVariablesPerPlayerAndEntity;
    }
    
    public void setSavedVariablesPerPlayerAndEntity( Set<String> savedVariablesPerPlayerMob )
    {
        _savedVariablesPerPlayerAndEntity = savedVariablesPerPlayerMob;
    }
}
