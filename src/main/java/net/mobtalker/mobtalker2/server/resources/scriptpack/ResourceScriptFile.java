/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.resources.scriptpack;

import java.io.*;

import org.apache.commons.lang3.Validate;

import net.minecraft.util.ResourceLocation;

public final class ResourceScriptFile extends AbstractScriptFile
{
    private final ResourceLocation _location;
    
    // ========================================
    
    public ResourceScriptFile( IScriptPack pack, ResourceLocation location )
    {
        super( pack );
        _location = Validate.notNull( location, "resource location mut not be null" );
    }
    
    // ========================================
    
    @Override
    public String getSourceName()
    {
        return _location.toString();
    }
    
    @Override
    public InputStream getStream()
        throws IOException
    {
        String resourcePath = "/assets/" + _location.getResourceDomain() + "/" + _location.getResourcePath();
        return getClass().getClassLoader().getResourceAsStream( resourcePath );
    }
    
    // ========================================
    
    @Override
    public int hashCode()
    {
        return _location.hashCode();
    }
    
    @Override
    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        if ( o == null )
        {
            return false;
        }
        if ( o instanceof ResourceLocation )
        {
            return _location.equals( o );
        }
        if ( o instanceof ResourceScriptFile )
        {
            return _location.equals( ( (ResourceScriptFile) o )._location );
        }
        return false;
    }
    
    @Override
    public String toString()
    {
        return _location.toString();
    }
}
