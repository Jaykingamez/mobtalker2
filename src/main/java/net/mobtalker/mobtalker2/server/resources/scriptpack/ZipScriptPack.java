/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.resources.scriptpack;

import java.io.*;
import java.nio.file.*;
import java.util.List;
import java.util.zip.*;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;

import net.mobtalker.mobtalker2.server.resources.scriptpack.config.*;

public final class ZipScriptPack extends ConfigurableScriptPack
{
    private static final PathMatcher MATCHER = FileSystems.getDefault().getPathMatcher( "glob:**.zip" );
    
    // ========================================
    
    private ZipFile _zipFile;
    
    // ========================================
    
    public ZipScriptPack( Path path )
        throws IOException
    {
        super( path );
    }
    
    // ========================================
    
    @Override
    protected ScriptPackConfig readConfiguration()
        throws IOException
    {
        _zipFile = new ZipFile( getPath().toFile() );
        ZipEntry configFileEntry = _zipFile.getEntry( SCRIPT_PACK_CONFIG_NAME );
        
        try (
            InputStream stream = _zipFile.getInputStream( configFileEntry );
            InputStreamReader reader = new InputStreamReader( stream, Charsets.UTF_8 ) )
        {
            return ScriptPackConfig.read( reader );
        }
    }
    
    @Override
    protected List<IScriptFile> readFiles( List<String> stringPaths )
    {
        List<IScriptFile> files = Lists.newArrayListWithExpectedSize( stringPaths.size() );
        for ( String stringPath : stringPaths )
        {
            stringPath = StringUtils.replaceChars( stringPath, '\\', '/' );
            
            ZipEntry entry = _zipFile.getEntry( stringPath );
            if ( entry == null )
            {
                throw new ScriptPackConfigException( "script path \"%s\" does not exist", stringPath );
            }
            
            files.add( new ZipScriptFile( this, getPath(), _zipFile, entry ) );
        }
        
        return files;
    }
    
    @Override
    protected String readGuiConfig( String stringPath )
        throws IOException
    {
        stringPath = StringUtils.replaceChars( stringPath, '\\', '/' );
        ZipEntry entry = _zipFile.getEntry( stringPath );
        if ( entry == null )
        {
            throw new ScriptPackConfigException( "GUI config path \"%s\" does not exist", stringPath );
        }
        
        StringBuilder sb = new StringBuilder( 1024 );
        try (
            InputStream stream = _zipFile.getInputStream( entry );
            InputStreamReader reader = new InputStreamReader( stream, Charsets.UTF_8 ) )
        {
            int c;
            while ( ( c = reader.read() ) != -1 )
            {
                sb.append( (char) c );
            }
        }
        
        return sb.toString();
    }
    
    @Override
    protected boolean scanForResources()
    {
        ZipEntry entry = _zipFile.getEntry( "resources" );
        return ( entry != null ) && entry.isDirectory();
    }
    
    @Override
    public void close()
    {
        try
        {
            _zipFile.close();
            _zipFile = null;
        }
        catch ( IOException e )
        {}
    }
    
    public static boolean isValid( Path path )
    {
        if ( !Files.isRegularFile( path ) || !MATCHER.matches( path ) )
        {
            return false;
        }
        
        try (
            ZipFile zipFile = new ZipFile( path.toFile() ) )
        {
            return zipFile.getEntry( SCRIPT_PACK_CONFIG_NAME ) != null;
        }
        catch ( IOException e )
        {
            return false;
        }
    }
}
