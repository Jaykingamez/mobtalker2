/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.resources.scriptpack;

import org.apache.commons.lang3.Validate;

public abstract class AbstractScriptFile implements IScriptFile
{
    private final IScriptPack _pack;
    
    // ========================================
    
    public AbstractScriptFile( IScriptPack pack )
    {
        _pack = Validate.notNull( pack, "script pack must not be null" );
    }
    
    // ========================================
    
    @Override
    public IScriptPack getScriptPack()
    {
        return _pack;
    }
    
    // ========================================
    
    @Override
    public String toString()
    {
        return _pack.getTitle() + ":" + getSourceName();
    }
}
