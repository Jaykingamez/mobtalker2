/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.resources.scriptpack;

import java.io.*;
import java.nio.file.*;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;

import net.mobtalker.mobtalker2.server.resources.scriptpack.config.*;

public final class FolderScriptPack extends ConfigurableScriptPack
{
    public FolderScriptPack( Path path )
        throws IOException
    {
        super( path );
    }
    
    // ========================================
    
    @Override
    protected ScriptPackConfig readConfiguration()
        throws IOException
    {
        Path path = getPath().resolve( SCRIPT_PACK_CONFIG_NAME );
        try (
            InputStream stream = Files.newInputStream( path, StandardOpenOption.READ );
            InputStreamReader reader = new InputStreamReader( stream, Charsets.UTF_8 ) )
        {
            return ScriptPackConfig.read( reader );
        }
    }
    
    @Override
    protected boolean scanForResources()
        throws IOException
    {
        return Files.isDirectory( getPath().resolve( "resources" ) );
    }
    
    @Override
    protected List<IScriptFile> readFiles( List<String> stringPaths )
        throws IOException
    {
        List<IScriptFile> files = Lists.newArrayListWithExpectedSize( stringPaths.size() );
        for ( String stringPath : stringPaths )
        {
            stringPath = StringUtils.replaceChars( stringPath, '\\', '/' );
            
            Path path = getPath().resolve( stringPath ).toAbsolutePath().normalize();
            if ( !Files.isRegularFile( path ) )
            {
                throw new ScriptPackConfigException( "script path \"%s\" does not exist, is not a valid file or can not be read",
                                                     stringPath );
            }
            
            // prevent usage of files outside the script pack
            if ( !path.startsWith( getPath() ) )
            {
                throw new ScriptPackConfigException( "script path \"%s\" is not a valid sub-path of its script pack path",
                                                     stringPath );
            }
            
            files.add( new RegularScriptFile( this, path ) );
        }
        
        return files;
    }
    
    @Override
    protected String readGuiConfig( String stringPath )
        throws IOException
    {
        stringPath = StringUtils.replaceChars( stringPath, '\\', '/' );
        
        Path path = getPath().resolve( stringPath ).toAbsolutePath().normalize();
        if ( !Files.isRegularFile( path ) )
        {
            throw new ScriptPackConfigException( "GUI config path \"%s\" does not exist, is not a valid file or can not be read",
                                                 stringPath );
        }
        
        // prevent usage of files outside the script pack
        if ( !path.startsWith( getPath() ) )
        {
            throw new ScriptPackConfigException( "GUI config path \"%s\" is not a valid sub-path of its script pack path",
                                                 stringPath );
        }
        
        StringBuilder sb = new StringBuilder( 1024 );
        try (
            BufferedReader reader = Files.newBufferedReader( path, Charsets.UTF_8 ) )
        {
            int c;
            while ( ( c = reader.read() ) != -1 )
            {
                sb.append( (char) c );
            }
        }
        
        return sb.toString();
    }
    
    public static boolean isValid( Path path )
    {
        if ( !Files.isDirectory( path ) )
        {
            return false;
        }
        
        Path configFile = path.resolve( SCRIPT_PACK_CONFIG_NAME );
        return Files.isRegularFile( configFile );
    }
}
