/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.resources.scriptpack;

import java.io.*;
import java.nio.file.*;

import org.apache.commons.lang3.Validate;

public final class RegularScriptFile extends AbstractScriptFile
{
    private final Path _path;
    
    // ========================================
    
    public RegularScriptFile( IScriptPack pack, Path path )
    {
        super( pack );
        _path = Validate.notNull( path, "path must not be null" );
    }
    
    // ========================================
    
    public Path getPath()
    {
        return _path;
    }
    
    @Override
    public String getSourceName()
    {
        return getScriptPack().getPath().relativize( _path ).toString();
    }
    
    @Override
    public InputStream getStream()
        throws IOException
    {
        return Files.newInputStream( _path );
    }
    
    // ========================================
    
    @Override
    public int hashCode()
    {
        return _path.hashCode();
    }
    
    @Override
    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        if ( o == null )
        {
            return false;
        }
        if ( o instanceof Path )
        {
            return _path.equals( o );
        }
        if ( o instanceof RegularScriptFile )
        {
            return _path.equals( ( (RegularScriptFile) o ).getPath() );
        }
        return false;
    }
}
