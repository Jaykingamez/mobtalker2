/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.resources.scriptpack.config;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import net.mobtalker.mobtalker2.util.json.JsonArray;
import net.mobtalker.mobtalker2.util.json.JsonObject;
import net.mobtalker.mobtalker2.util.json.JsonValue;

import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.stream.Collectors;

public final class ScriptPackConfig
{
    public static final String KEY_TITLE = "Title";
    public static final String KEY_API = "API";
    public static final String KEY_DESCRIPTION = "Description";
    public static final String KEY_VERSION = "Version";
    public static final String KEY_AUTHORS = "Authors";
    public static final String KEY_SCRIPTS = "Scripts";
    
    // ========================================
    
    private String _title;
    private String _apiVersion;
    private String _description;
    private String _version;
    private List<String> _authors;
    private transient List<ScriptConfig> _scripts;
    
    // ========================================
    
    private static ScriptPackConfig deserialize( JsonObject json )
            throws ScriptPackConfigException
    {
        ScriptPackConfig ret = new ScriptPackConfig();
        
        ret.setTitle( json.getString( KEY_TITLE ) );
        ret.setApiVersion( json.getString( KEY_API ) );
        ret.setDescription( json.getString( KEY_DESCRIPTION, "" ) );
        ret.setVersion( json.getString( KEY_VERSION, "" ) );
        ret.setAuthors( json.getStringList( KEY_AUTHORS ) );
        
        List<ScriptConfig> scripts = Lists.newArrayList();
        JsonArray scriptsJson = json.get( KEY_SCRIPTS ).asArray();
        for ( JsonValue valueJson : scriptsJson )
        {
            JsonObject scriptJson = valueJson.asObject();
            String type = scriptJson.getString( ScriptConfig.KEY_TYPE );
            ScriptConfig script;
            if ( InteractionScriptConfig.TYPE.equalsIgnoreCase( type ) )
            {
                script = InteractionScriptConfig.deserialize( scriptJson );
            }
            else
            {
                throw new ScriptPackConfigException( "Invalid script type \"%s\"", type );
            }
            
            // TODO Change identifiers to no longer be optional once current SavedVariables/future SavedVariablesPerMob will be migrated
            // Give scripts an identifier if they don't have one.
            // Needs to be possible to rebuild, otherwise variables saved per player won't be persistent.
            if ( Strings.isNullOrEmpty( script.getIdentifier() ) )
            {
                // Use the name of the script pack and append the used entity types.
                //
                // This should pose no problems to existing script packs (judging by the Script Marketplace at the time of writing)
                // and, technically, shouldn't be an issue at all, as it doesn't make sense to make multiple scripts in the same
                // script pack available for the same mobs.
                //
                // Only change here will be that variables saved per-player will be available for other mobs of the same kind as well,
                // so "resetting" the story needs to be handled by the story or a Minecraft command.
                String identifier = ret.getTitle();
                identifier += "_" + ( (InteractionScriptConfig) script ).getEntities().stream().sorted().collect( Collectors.joining() );
                
                script.setIdentifier( identifier );
            }
            
            scripts.add( script );
        }
        ret.setScripts( scripts );
        
        return ret;
    }
    // ========================================
    
    public String getTitle()
    {
        return _title;
    }
    
    public void setTitle( String title )
    {
        _title = title;
    }
    
    public String getApiVersion()
    {
        return _apiVersion;
    }
    
    public void setApiVersion( String apiVersion )
    {
        _apiVersion = apiVersion;
    }
    
    public String getDescription()
    {
        return _description;
    }
    
    public void setDescription( String description )
    {
        _description = description;
    }
    
    public String getVersion()
    {
        return _version;
    }
    
    public void setVersion( String version )
    {
        _version = version;
    }
    
    public List<String> getAuthors()
    {
        return _authors;
    }
    
    public void setAuthors( List<String> authors )
    {
        _authors = authors;
    }
    
    public List<ScriptConfig> getScripts()
    {
        return _scripts;
    }
    
    public void setScripts( List<ScriptConfig> scripts )
    {
        _scripts = scripts;
    }
    
    // ========================================
    
    public static ScriptPackConfig read( Reader reader )
            throws IOException
    {
        try
        {
            return deserialize( JsonObject.readFrom( reader ) );
        }
        catch ( IOException e )
        {
            throw e;
        }
        catch ( Exception e )
        {
            throw new ScriptPackConfigException( e );
        }
    }
}
