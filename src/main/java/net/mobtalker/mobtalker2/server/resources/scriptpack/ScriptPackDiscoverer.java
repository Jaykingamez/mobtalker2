/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.resources.scriptpack;

import java.io.IOException;
import java.nio.file.*;
import java.util.*;

import com.google.common.collect.Lists;

import net.minecraftforge.fml.common.versioning.*;
import net.mobtalker.mobtalker2.MobTalker2;
import net.mobtalker.mobtalker2.server.resources.scriptpack.config.ScriptPackConfigException;
import net.mobtalker.mobtalker2.util.PathUtil;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;

public class ScriptPackDiscoverer
{
    private static final MobTalkerLog LOG = new MobTalkerLog( "ScriptPackDiscovery" );
    
    // ========================================
    
    private final List<String> _warnings;
    
    // ========================================
    
    public ScriptPackDiscoverer()
    {
        _warnings = Lists.newArrayList();
    }
    
    // ========================================
    
    public List<IScriptPack> discover( Path searchPath )
        throws IOException
    {
        List<IScriptPack> filePacks = Lists.newArrayList();
        List<IScriptPack> folderPacks = Lists.newArrayList();
        
        // Folder script packs
        try (
            DirectoryStream<Path> stream = Files.newDirectoryStream( searchPath ) )
        {
            for ( Path path : stream )
            {
                try
                {
                    if ( FolderScriptPack.isValid( path ) )
                    {
                        LOG.debug( "reading FolderScriptPack '%s'", PathUtil.relativizeMinecraftPath( path ) );
                        folderPacks.add( new FolderScriptPack( path ) );
                    }
                    else if ( ZipScriptPack.isValid( path ) )
                    {
                        LOG.debug( "reading ZipScriptPack '%s'", PathUtil.relativizeMinecraftPath( path ) );
                        filePacks.add( new ZipScriptPack( path ) );
                    }
                }
                catch ( ScriptPackConfigException ex )
                {
                    String msg = String.format( "Invalid script pack configuration for '%s': %s",
                                                PathUtil.relativizeMinecraftPath( path ),
                                                ex.getMessage() );
                    
                    LOG.warn( ex, msg );
                    _warnings.add( msg );
                }
                catch ( IOException ex )
                {
                    String msg = String.format( "Error while reading script pack '%s': %s",
                                                PathUtil.relativizeMinecraftPath( path ),
                                                ex.getMessage() );
                    
                    LOG.warn( ex, msg );
                    _warnings.add( msg );
                }
            }
        }
        
        List<IScriptPack> packs = Lists.newArrayListWithExpectedSize( filePacks.size() + folderPacks.size() );
        packs.addAll( filePacks );
        packs.addAll( folderPacks );
        
        for ( Iterator<IScriptPack> it = packs.iterator(); it.hasNext(); )
        {
            IScriptPack pack = it.next();
            VersionRange range = MobTalker2.SCRIPT_API_VERSIONRANGE;
            ArtifactVersion version = pack.getApiVersion();
            
            if ( !range.containsVersion( version ) )
            {
                it.remove();
                
                String msg = String.format( "Invalid script pack '%s': API version %s is incompatible with required version %s",
                                            pack.getTitle(), MobTalker2.SCRIPT_API_VERSION, version );
                LOG.warn( msg );
                _warnings.add( msg );
            }
        }
        
        return packs;
    }
    
    public List<String> getWarnings()
    {
        return _warnings;
    }
}
