/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.interaction;

import static net.minecraftforge.fml.relauncher.Side.*;

import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.*;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.mobtalker.mobtalker2.client.gui.MobTalkerInteractionGui;
import net.mobtalker.mobtalker2.client.network.ClientMessageHandler;
import net.mobtalker.mobtalker2.client.resources.MobTalkerResourceLocation;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;

import com.google.common.base.Strings;

import de.rummeltsoftware.mc.mgt.*;

@SideOnly( CLIENT )
public class ClientInteractionHandler extends Container
{
    private static final MobTalkerLog LOG = new MobTalkerLog( "InteractionHandler" );
    
    // ========================================
    
    protected final EntityPlayer _player;
    protected final MobTalkerInteractionGui _gui;
    
    // ========================================
    
    public ClientInteractionHandler( EntityPlayer player )
    {
        _player = player;
        _gui = new MobTalkerInteractionGui( this );
        _gui.setVisible( false );
        
        // Needed because Container does not expect to have no slots and causes an exception otherwise.
        addSlotToContainer( new Slot( new InventoryBasic( "FakeInventory", false, 1 ), 0, 9001, 9001 ) );
    }
    
    // ========================================
    
    private Minecraft getMinecraft()
    {
        return FMLClientHandler.instance().getClient();
    }
    
    // ========================================
    
    public EntityPlayer getPlayer()
    {
        return _player;
    }
    
    public MobTalkerInteractionGui getGui()
    {
        return _gui;
    }
    
    // ========================================
    
    public void onChoiceMade( int choice )
    {
        LOG.debug( "Choice made: %s", choice );
        
        _gui.hideChoiceMenu();
        ClientMessageHandler.instance().sendChoiceMade( choice );
    }
    
    public void onKeyTyped( char par1, int keyCode )
    {
        LOG.debug( "Typed key '%s' (%s)", par1, keyCode );
        
        if ( ( keyCode == 1 ) || ( keyCode == getMinecraft().gameSettings.keyBindInventory.getKeyCode() ) )
        {
            ClientMessageHandler.instance().sendCancelInteraction();
        }
        else
        {
            // TODO Implement client text input
        }
    }
    
    public void onTextFinished()
    {
        ClientMessageHandler.instance().sendTextFinished();
    }
    
    // ========================================
    
    public void showSprite( String textureGroup, String texturePath, String position, Point offset )
    {
        textureGroup = textureGroup.toLowerCase();
        texturePath = texturePath.toLowerCase();
        
        String pathStr = "sprites/" + textureGroup + "/" + texturePath + ".png";
        
        ResourceLocation textureLocation = new MobTalkerResourceLocation( pathStr );
        
        _gui.setVisible( true );
        _gui.showSprite( textureGroup, textureLocation, position, offset );
    }
    
    public void showScene( String texturePath, String mode )
    {
        texturePath = texturePath.toLowerCase();
        
        String pathStr = "scenes/" + texturePath + ".png";
        FullscreenMode fsMode = FullscreenMode.valueOf( mode.toUpperCase() );
        
        MobTalkerResourceLocation textureLocation = new MobTalkerResourceLocation( pathStr );
        
        _gui.setVisible( true );
        _gui.showScene( textureLocation, fsMode );
    }
    
    public void hideTexture( String textureGroup )
    {
        textureGroup = textureGroup.toLowerCase();
        
        _gui.setVisible( true );
        _gui.hideTexture( textureGroup );
    }
    
    public void displayName( String name )
    {
        _gui.setCharacterName( name, AlignmentX.LEFT );
    }
    
    public void displayText( String text, boolean isConclusion )
    {
        _gui.clearDialogeText();
        _gui.setVisible( true );
        _gui.setDialogeText( text, isConclusion );
    }
    
    public void displayChoice( String caption, List<String> choices )
    {
        if ( !Strings.isNullOrEmpty( caption ) )
        {
            _gui.setCharacterName( "", AlignmentX.LEFT );
            _gui.clearDialogeText();
            _gui.setDialogeText( caption, false );
        }
        
        _gui.setVisible( true );
        _gui.displayChoiceMenu( choices );
    }
    
    // ========================================
    
    @Override
    public final boolean canInteractWith( EntityPlayer player )
    {
        return _player == player;
    }
    
    @Override
    public final void onContainerClosed( EntityPlayer player )
    {
        if ( player.isEntityAlive() )
            throw new AssertionError( "Should not call this if player is still alive" );
        
        super.onContainerClosed( player );
    }
}
