/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.config.gui;

import static net.minecraftforge.fml.relauncher.Side.*;

import java.util.List;

import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.*;
import net.minecraftforge.fml.client.config.*;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.mobtalker.mobtalker2.*;
import net.mobtalker.mobtalker2.common.MobTalkerConfig;

import com.google.common.collect.Lists;

@SideOnly( CLIENT )
public class MobTalkerGuiConfig extends GuiConfig
{
    public MobTalkerGuiConfig( GuiScreen parentScreen )
    {
        super( parentScreen,
               getConfigElements( MobTalkerConfig.getConfiguration() ),
               MobTalker2.ID, false, false,
               GuiConfig.getAbridgedConfigPath( MobTalkerConfig.getConfiguration().toString() ) );
    }
    
    private static List<IConfigElement> getConfigElements( Configuration config )
    {
        List<IConfigElement> result = Lists.newArrayList();
        
        for ( String categoryName : config.getCategoryNames() )
        {
            result.add( new ConfigElement( config.getCategory( categoryName ) ) );
        }
        
        return result;
    }
}
