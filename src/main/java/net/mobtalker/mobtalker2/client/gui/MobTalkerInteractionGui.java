/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.gui;

import static net.minecraftforge.fml.relauncher.Side.*;

import java.util.*;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.mobtalker.mobtalker2.client.gui.settings.*;
import net.mobtalker.mobtalker2.client.interaction.ClientInteractionHandler;
import net.mobtalker.mobtalker2.client.resources.MobTalkerResourceLocation;
import net.mobtalker.mobtalker2.client.util.SoundHelper;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;

import de.rummeltsoftware.mc.mgt.*;

@SideOnly( CLIENT )
public class MobTalkerInteractionGui extends MGContainer
{
    private static final MobTalkerLog LOG = new MobTalkerLog( "GUI" );
    
    // ========================================
    
    private final ClientInteractionHandler _handler;
    
    private MobTalkerChoiceGui _choiceGui;
    private MobTalkerDialogTextArea _textArea;
    private MGFrame _dialogFrame;
    private MGTextArea _characterName;
    
    private final Map<String, MGTexture> _textures;
    
//    private boolean _hideHotbar = false;
    
    // ========================================
    
    public MobTalkerInteractionGui( ClientInteractionHandler handler )
    {
        _handler = handler;
        _textures = Maps.newHashMap();
        
        enableMouseEvents( true );
        
        _textArea = new MobTalkerDialogTextArea();
        _textArea.setName( "DialogText" );
        _textArea.setLevel( 0 );
        _textArea.setEndOfTextListener( this::onEndOfTextReached );
        add( _textArea );
        
        _dialogFrame = new MGFrame();
        _dialogFrame.setName( "DialogFrame" );
        _dialogFrame.setParent( _textArea );
        _dialogFrame.setLevel( -1 );
        add( _dialogFrame );
        
        _characterName = new MGTextArea();
        _characterName.setName( "CharacterName" );
        _characterName.setLevel( 0 );
        add( _characterName );
        
        _choiceGui = new MobTalkerChoiceGui();
        _choiceGui.setName( "ChoiceGui" );
        _choiceGui.setVisible( false );
        _choiceGui.setChoiceListener( this::onChoiceMade );
        add( _choiceGui );
        
//        MinecraftForge.EVENT_BUS.register( this );
    }
    
    @Override
    public void init()
    {
        ClientUiSettings settings = null;
        try
        {
            settings = ClientUiSettings.getCurrent();
        }
        catch ( Exception ex )
        {
            LOG.error( ex, "Error while reading GUI config. Falling back to default." );
        }
        
        if ( settings == null )
        {
            settings = ClientUiSettings.getDefault();
        }
        
        try
        {
            applySettings( settings );
        }
        catch ( Exception ex )
        {
            LOG.error( ex, "Error while applying GUI config." );
        }
    }
    
    private void applySettings( ClientUiSettings settings )
    {
        configureDialogTextArea( settings.DialogText );
        configureCharacterName( settings.CharacterName );
        configureChoiceGui( settings.ChoiceFrame );
    }
    
    /**
     * TODO Ugly switchy-casy
     */
    private MGComponent getComponentForName( String name )
    {
        switch ( name )
        {
            case "DialogText":
                return _textArea;
            case "CharacterName":
                return _characterName;
            case "ChoiceGui":
                return _choiceGui;
            case "UI":
            default:
                return null;
        }
    }
    
    private void configureDialogTextArea( DialogTextSettings settings )
    {
        _textArea.setParent( getComponentForName( settings.Parent ) );
        _textArea.setWidth( settings.Width );
        
        _textArea.clearAllAnchors();
        for ( AnchorSettings anchor : settings.Anchors )
        {
            _textArea.addAnchor( anchor.From, anchor.To, getComponentForName( anchor.Relative ), anchor.OffsetX, anchor.OffsetY );
        }
        
        _textArea.setTextAlignment( settings.Text.Align.X );
        _textArea.setLineCount( settings.Text.MaxLines );
        _textArea.setLineSpacing( settings.Text.Spacing );
        _textArea.setDropShadow( settings.Text.DropShadow );
        
        Margin padding = Margin.parse( settings.Background.Padding );
        _dialogFrame.addAnchor( Alignment.TOPLEFT, Alignment.TOPLEFT, _textArea, -padding.getLeft(), -padding.getTop() );
        _dialogFrame.addAnchor( Alignment.BOTTOMRIGHT, Alignment.BOTTOMRIGHT, _textArea, padding.getRight(), padding.getBottom() );
        
        Margin insets = Margin.parse( settings.Background.Insets );
        _dialogFrame.setBackgroundPadding( insets.getLeft(), insets.getTop(), insets.getRight(), insets.getBottom() );
        
        if ( !Strings.isNullOrEmpty( settings.Background.Texture ) )
            _dialogFrame.setBackgroundTexture( new MobTalkerResourceLocation( settings.Background.Texture ) );
        else
            _dialogFrame.setBackgroundColor( Color.fromHexString( settings.Background.Color ) );
        
        _dialogFrame.setEdgeTexture( new MobTalkerResourceLocation( settings.Border.Texture ) );
        _dialogFrame.setEdgeSize( settings.Border.Size );
        
    }
    
    private void configureCharacterName( CharacterNameSettings settings )
    {
        _characterName.setParent( getComponentForName( settings.Parent ) );
        _characterName.setWidth( settings.Width );
        
        _characterName.clearAllAnchors();
        for ( AnchorSettings anchor : settings.Anchors )
        {
            _characterName.addAnchor( anchor.From, anchor.To, getComponentForName( anchor.Relative ), anchor.OffsetX, anchor.OffsetY );
        }
        
        _textArea.setTextAlignment( settings.Text.Align.X );
        _textArea.setDropShadow( settings.Text.DropShadow );
    }
    
    private void configureChoiceGui( ChoiceFrameSettings settings )
    {
        _choiceGui.setParent( getComponentForName( settings.Parent ) );
        
        _choiceGui.clearAllAnchors();
        for ( AnchorSettings anchor : settings.Anchors )
        {
            _choiceGui.addAnchor( anchor.From, anchor.To, getComponentForName( anchor.Relative ), anchor.OffsetX, anchor.OffsetY );
        }
        
        _choiceGui.setChoicesPerPage( settings.Choices.PerPage );
        _choiceGui.setMinWidth( settings.Choices.MinWidth );
        
        _choiceGui.setPageBackgroundSettings( settings.Background );
        _choiceGui.setPageBorderSettings( settings.Border );
    }
    
    // ========================================
    
//    @SubscribeEvent( priority = EventPriority.HIGHEST )
//    public void onRenderIngame( RenderGameOverlayEvent.Pre event )
//    {
//        if ( !isVisible() )
//            return;
//
//        if ( event.type != ElementType.HOTBAR )
//            return;
//
//        if ( _hideHotbar )
//        {
//            event.setCanceled( true );
//        }
//    }
    
    @Override
    public void doLayout( Dimensions screenDim )
    {
        LOG.debug( "ScreenDim: %s x %s", screenDim.getWidth(), screenDim.getHeight() );
        
        setSize( screenDim );
        super.doLayout( screenDim );
    }
    
    // ========================================
    
    public void showSprite( String textureGroup, ResourceLocation textureLocation, String position, Point offset )
    {
        Alignment alignment = Alignment.forName( position.toUpperCase() );
        
        if ( alignment == null )
        {
            LOG.fatal( "Recieved request to show sprite '%s' in texture group '%s' at position '%s' that does not exist!",
                       textureLocation.getResourcePath(), textureGroup, position );
            return;
        }
        
        MGTexture texture = _textures.get( textureGroup );
        
        if ( texture == null )
        {
            texture = new CharacterSprite();
            texture.setName( "Sprite[" + textureGroup + "]" );
            texture.setLevel( -100 );
            
            _textures.put( textureGroup, texture );
            add( texture );
            
            LOG.debug( "Created texture group for '%s'", textureGroup );
        }
        
        LOG.debug( "Showing sprite '%s' in group '%s' at '%s' with offset '%s, %s'",
                   textureLocation.getResourcePath(), textureGroup,
                   position, offset.X, offset.Y );
        
        texture.setTexture( textureLocation );
        texture.clearAllAnchors();
        texture.addAnchor( alignment, alignment, null, offset );
        texture.setVisible( true );
        
        invalidate();
    }
    
    public void showScene( MobTalkerResourceLocation textureLocation, FullscreenMode mode )
    {
        MGFullscreenTexture texture;
        try
        {
            texture = (MGFullscreenTexture) _textures.get( "scene" );
        }
        catch ( ClassCastException ex )
        {
            LOG.error( "Tried to show scene '%s' in group 'scene', but this group already exists and is not a scene group!",
                       textureLocation.getResourcePath() );
            return;
        }
        
        if ( texture == null )
        {
            texture = new MGFullscreenTexture();
            texture.setName( "Scene" );
            texture.setLevel( -1000 );
            
            _textures.put( "scene", texture );
            add( texture );
        }
        
        LOG.debug( "Showing scene '%s' as '%s'",
                   textureLocation.getResourcePath(), mode );
        
        texture.setTexture( textureLocation );
        texture.addAnchor( Alignment.CENTER, Alignment.CENTER, null, Point.Zero );
        texture.setMode( mode );
        texture.setVisible( true );
        
        invalidate();
    }
    
    public void hideTexture( String textureGroup )
    {
        MGTexture texture = _textures.get( textureGroup );
        
        LOG.debug( "Hiding texture group '%s'", textureGroup );
        
        if ( texture == null )
        {
            LOG.warn( "Texture group '%s' does not exist!", textureGroup );
            return;
        }
        
        texture.setVisible( false );
        remove( texture );
        _textures.remove( textureGroup );
        
        invalidate();
    }
    
    public void setCharacterName( String name, AlignmentX side )
    {
        _characterName.setText( name );
//        _characterName.setTextAlignment( side );
        
        _characterName.setVisible( !Strings.isNullOrEmpty( name ) );
    }
    
    public void clearDialogeText()
    {
        _textArea.clearText();
    }
    
    public void setDialogeText( String text, boolean isConclusion )
    {
        _textArea.setText( text, isConclusion );
    }
    
    public void displayChoiceMenu( List<String> choices )
    {
        _choiceGui.setChoices( choices );
        _choiceGui.setVisible( true );
    }
    
    public void hideChoiceMenu()
    {
        _choiceGui.setVisible( false );
        _choiceGui.clearChoices();
    }
    
    // ========================================
    
    @Override
    public boolean onKeyPressed( char eventChar, int eventKey )
    {
        _handler.onKeyTyped( eventChar, eventKey );
        return false;
    }
    
    @Override
    public void onClosed()
    {
//        setVisible( false );

//        MinecraftForge.EVENT_BUS.unregister( this );
        // GuiIngameForge.renderHotbar = true;
    }
    
    // ========================================
    
    public void onChoiceMade( int choice )
    {
        _handler.onChoiceMade( choice );
    }
    
    public void onEndOfTextReached()
    {
        _handler.onTextFinished();
    }
    
    @Override
    public boolean onMouseClicked( int x, int y, int button )
    {
//        return super.onMouseClicked( x, y, button );
        if ( super.onMouseClicked( x, y, button ) )
            return true;
        
        if ( button == 0 )
        {
            if ( _textArea.isTextScrolling() )
            {
                _textArea.finishTextScrolling();
                SoundHelper.playSoundFX( "gui.button.press", 1.0F );
            }
            else if ( !_choiceGui.isVisible() )
            {
                _textArea.advanceText();
                SoundHelper.playSoundFX( "gui.button.press", 1.0F );
            }
        }
        
        return true;
    }
}
