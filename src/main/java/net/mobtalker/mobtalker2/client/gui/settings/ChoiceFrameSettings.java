/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.gui.settings;

import java.util.List;

import org.simpleframework.xml.*;

public class ChoiceFrameSettings
{
    @Attribute( name = "parent" )
    public String Parent;
    
    @ElementList( name = "Anchors" )
    public List<AnchorSettings> Anchors;
    
    @Element( name = "Choices" )
    public ChoicesSettings Choices;
    
    @Element( name = "Background" )
    public BackgroundSettings Background;
    
    @Element( name = "Border" )
    public BorderSettings Border;
    
    // ========================================
    
    public static class ChoicesSettings
    {
        @Attribute( name = "perPage" )
        public int PerPage;
        
        @Attribute( name = "minWidth", required = false )
        public int MinWidth;
    }
    
    // ========================================
    
    public static class BackgroundSettings
    {
        @Attribute( name = "color", required = false )
        public String Color;
        
        @Attribute( name = "texture", required = false )
        public String Texture;
        
        // TODO
//        @Attribute( name = "padding" )
//        public String Padding;
        
        @Attribute( name = "insets" )
        public String Insets;
    }
    
    // ========================================
    
    public static class BorderSettings
    {
        @Attribute( name = "texture" )
        public String Texture;
        
        @Attribute( name = "size" )
        public int Size;
    }
}
