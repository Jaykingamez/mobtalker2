/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.gui.settings;

import java.util.List;

import org.simpleframework.xml.*;

import de.rummeltsoftware.mc.mgt.Alignment;

public class DialogTextSettings
{
    @Attribute( name = "parent" )
    public String Parent;
    
    @Attribute( name = "width", required = false )
    public int Width;
    
    @ElementList( name = "Anchors" )
    public List<AnchorSettings> Anchors;
    
    @Element( name = "Text" )
    public TextSettings Text;
    
    @Element( name = "Background" )
    public BackgroundSettings Background;
    
    @Element( name = "Border" )
    public BorderSettings Border;
    
    // ========================================
    
    public static class TextSettings
    {
        @Attribute( name = "align" )
        public Alignment Align;
        
        @Attribute( name = "spacing" )
        public int Spacing;
        
        @Attribute( name = "maxLines" )
        public int MaxLines;
        
        @Attribute( name = "color" )
        public String Color;
        
        @Attribute( name = "dropShadow" )
        public boolean DropShadow;
    }
    
    // ========================================
    
    public static class BackgroundSettings
    {
        @Attribute( name = "color", required = false )
        public String Color;
        
        @Attribute( name = "texture", required = false )
        public String Texture;
        
        @Attribute( name = "padding" )
        public String Padding;
        
        @Attribute( name = "insets" )
        public String Insets;
    }
    
    // ========================================
    
    public static class BorderSettings
    {
        @Attribute( name = "texture" )
        public String Texture;
        
        @Attribute( name = "size" )
        public int Size;
    }
}
