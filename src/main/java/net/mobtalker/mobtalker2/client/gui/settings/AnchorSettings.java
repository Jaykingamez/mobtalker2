/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.gui.settings;

import org.simpleframework.xml.Attribute;

import de.rummeltsoftware.mc.mgt.Alignment;

public class AnchorSettings
{
    @Attribute( name = "from" )
    public Alignment From;
    
    @Attribute( name = "to" )
    public Alignment To;
    
    @Attribute( name = "relative", required = false )
    public String Relative;
    
    @Attribute( name = "x" )
    public int OffsetX;
    
    @Attribute( name = "y" )
    public int OffsetY;
    
    // ========================================
    
    public AnchorSettings()
    {}
    
    public AnchorSettings( Alignment from, Alignment to, String relative, int offsetX, int offsetY )
    {
        From = from;
        To = to;
        Relative = relative;
        OffsetX = offsetX;
        OffsetY = offsetY;
    }
    
    // ========================================
    
    @Override
    public String toString()
    {
        return "Anchor [From=" + From + ", To=" + To + ", Relative=" + Relative + ", X=" + OffsetX + ", Y=" + OffsetY + "]";
    }
}
