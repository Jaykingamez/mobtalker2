/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.gui.settings;

import static net.mobtalker.mobtalker2.util.AssetUtil.*;

import java.io.InputStream;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.simpleframework.xml.*;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.stream.Format;

import com.google.common.base.*;

@Root( name = "UI", strict = false )
@Namespace( reference = "http://www.mobtalker.net/mt2/ui" )
public class ClientUiSettings
{
    private static ClientUiSettings _default;
    
    private static String _configString;
    private static ClientUiSettings _current;
    
    // ========================================
    
    public static void set( String config )
        throws Exception
    {
        _configString = config;
        _current = null;
    }
    
    public static ClientUiSettings getCurrent()
        throws Exception
    {
        if ( Strings.isNullOrEmpty( _configString ) )
            return null;
        
        if ( _current == null ) // This is always true in the current use-case
        {
            // We read it here so we don't have parsing exceptions within the network handler where we can't find them.
            Persister persister = new Persister();
            _current = persister.read( ClientUiSettings.class, _configString );
        }
        
        return _current;
    }
    
    public static ClientUiSettings getDefault()
    {
        if ( _default == null )
        {
            try (
                InputStream stream = getAsset( "ui/default.xml" ) )
            {
                _default = read( stream );
            }
            catch ( Exception ex )
            {
                throw new RuntimeException( "Unable to read GUI defaults", ex );
            }
        }
        
        return _default;
    }
    
    // ========================================
    
    public static ClientUiSettings read( InputStream input )
        throws Exception
    {
        Persister persister = new Persister();
        return persister.read( ClientUiSettings.class, input );
    }
    
    // ========================================
    
    @Element( name = "DialogText" )
    public DialogTextSettings DialogText;
    
    @Element( name = "CharacterName" )
    public CharacterNameSettings CharacterName;
    
    @Element( name = "ChoiceFrame" )
    public ChoiceFrameSettings ChoiceFrame;
    
    // ========================================
    
    public String write()
        throws Exception
    {
        Persister persister = new Persister( new Format( 0 ) );
        
        byte[] bytes;
        try (
            ByteArrayOutputStream stream = new ByteArrayOutputStream(); )
        {
            persister.write( this, stream );
            bytes = stream.toByteArray();
        }
        
        return new String( bytes, Charsets.UTF_8 );
    }
}
