/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.gui;

import static com.google.common.base.Preconditions.*;
import static net.minecraftforge.fml.relauncher.Side.*;

import java.util.*;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.mobtalker.mobtalker2.client.gui.settings.*;
import net.mobtalker.mobtalker2.client.resources.MobTalkerResourceLocation;

import com.google.common.base.Strings;

import de.rummeltsoftware.mc.mgt.*;

@SideOnly( CLIENT )
public class MobTalkerChoiceGui extends MGContainer
{
    private int _choicesPerPage;
    private int _minWidth;
    
    private ChoiceFrameSettings.BackgroundSettings _pageBackgroundSettings;
    private ChoiceFrameSettings.BorderSettings _pageBorderSettings;
    
    private final Map<Integer, MGContainer> _pages;
    private int _currentPage;
    
    private final MGButton _buttonPrevPage;
    private final MGButton _buttonNextPage;
    
    private IChoiceMadeListener choiceListener;
    
    // ========================================
    
    public MobTalkerChoiceGui()
    {
        _choicesPerPage = 1;
        
        _pages = new HashMap<>( 1 );
        _currentPage = -1;
        
        _buttonPrevPage = new MGButton();
        _buttonPrevPage.setName( "PagePrev" );
        _buttonPrevPage.setParent( this );
        _buttonPrevPage.setSize( 20, 20 );
        _buttonPrevPage.setVisible( false );
        _buttonPrevPage.setCaption( "<" );
        _buttonPrevPage.addMouseListener( clickSoundListener );
        _buttonPrevPage.addMouseListener( this::onPageButtonClicked );
        add( _buttonPrevPage );
        
        _buttonNextPage = new MGButton();
        _buttonNextPage.setName( "PageNext" );
        _buttonNextPage.setParent( this );
        _buttonNextPage.setSize( 20, 20 );
        _buttonNextPage.setVisible( false );
        _buttonNextPage.setCaption( ">" );
        _buttonNextPage.addMouseListener( clickSoundListener );
        _buttonNextPage.addMouseListener( this::onPageButtonClicked );
        add( _buttonNextPage );
    }
    
    // ========================================
    
    @Override
    public void doLayout( Dimensions screenDim )
    {
        super.doLayout( screenDim );
    }
    
    // ========================================
    
    public int getChoicesPerPage()
    {
        return _choicesPerPage;
    }
    
    public void setChoicesPerPage( int choicesPerPage )
    {
        checkArgument( choicesPerPage > 0 );
        _choicesPerPage = choicesPerPage;
        invalidate();
    }
    
    public int getMinWidth()
    {
        return _minWidth;
    }
    
    public void setMinWidth( int minWidth )
    {
        _minWidth = minWidth;
    }
    
    public void setPageBackgroundSettings( ChoiceFrameSettings.BackgroundSettings settings )
    {
        _pageBackgroundSettings = settings;
    }
    
    public void setPageBorderSettings( ChoiceFrameSettings.BorderSettings settings )
    {
        _pageBorderSettings = settings;
    }
    
    // ========================================
    
    public void setChoices( List<String> choices )
    {
        clearChoices();
        
        if ( choices.isEmpty() )
            return;
        
        for ( int i = 0, limit = choices.size(), page = 0; i < limit; i += _choicesPerPage, page++ )
        {
            int max = Math.min( i + _choicesPerPage, limit );
            createPage( page, choices.subList( i, max ) );
        }
        
        displayPage( 0 );
    }
    
    private void createPage( int pageIndex, List<String> choices )
    {
        MGFrame page = new MGFrame();
        page.setParent( this );
        page.setVisible( false );
        page.setLevel( -1 );
        
        Margin insets = Margin.parse( _pageBackgroundSettings.Insets );
        page.setBackgroundPadding( insets.getLeft(), insets.getTop(), insets.getRight(), insets.getBottom() );
        
        if ( !Strings.isNullOrEmpty( _pageBackgroundSettings.Texture ) )
            page.setBackgroundTexture( new MobTalkerResourceLocation( _pageBackgroundSettings.Texture ) );
        else
            page.setBackgroundColor( Color.fromHexString( _pageBackgroundSettings.Color ) );
        
        page.setEdgeTexture( new MobTalkerResourceLocation( _pageBorderSettings.Texture ) );
        page.setEdgeSize( _pageBorderSettings.Size );
        
        _pages.put( pageIndex, page );
        
        int maxStringWidth = Math.max( getMaxStringWidth( choices ), _minWidth );
        int buttonHeight = 20;
        int buttonSpacing = 1;
        
        MGButton prevButton = null;
        for ( int i = 0; i < choices.size(); i++ )
        {
            String choice = choices.get( i );
            MGButton button = new MGButton();
            button.setId( i );
            button.setName( "Choice[" + pageIndex + "][" + i + "]" );
            button.setParent( page );
            button.setSize( maxStringWidth + 8, buttonHeight );
            
            if ( i == 0 )
                button.addAnchor( Alignment.TOP, Alignment.TOP, null, new Point( 0, 0 ) );
            else
                button.addAnchor( Alignment.TOP, Alignment.BOTTOM, prevButton, new Point( 0, buttonSpacing ) );
            
            button.setCaption( choice );
            
            button.addMouseListener( clickSoundListener );
            button.addMouseListener( this::onChoiceButtonClicked );
            
            page.add( button );
            prevButton = button;
        }
        
        int totalHeight = ( choices.size() * buttonHeight ) + ( ( choices.size() - 1 ) * buttonSpacing );
        page.setSize( maxStringWidth + 8, totalHeight );
        page.addAnchor( Alignment.BOTTOM, Alignment.BOTTOM, null, Point.Zero );
    }
    
    private static int getMaxStringWidth( List<String> strings )
    {
        return strings.stream()
                      .mapToInt( getFontRenderer()::getStringWidth )
                      .max()
                      .getAsInt();
    }
    
    private void displayPage( int pageIndex )
    {
        if ( pageIndex == _currentPage )
            return;
        
        hideCurrentPage();
        
        MGFrame page = (MGFrame) _pages.get( pageIndex );
        if ( page == null )
            throw new IndexOutOfBoundsException( "page " + pageIndex + " is empty" );
        
        page.setVisible( true );
        
        boolean showPrevButton = pageIndex > 0;
        boolean showNextButton = pageIndex < ( _pages.size() - 1 );
//        boolean showNavigateButton = showPrevButton || showNextButton;
        
        _buttonPrevPage.clearAllAnchors();
        _buttonPrevPage.addAnchor( Alignment.TOPLEFT, Alignment.BOTTOMLEFT, page, new Point( 0, 1 ) );
        _buttonPrevPage.setVisible( showPrevButton );
        
        _buttonNextPage.clearAllAnchors();
        _buttonNextPage.addAnchor( Alignment.TOPRIGHT, Alignment.BOTTOMRIGHT, page, new Point( 0, 1 ) );
        _buttonNextPage.setVisible( showNextButton );
        
        _currentPage = pageIndex;
        
        add( page );
        invalidate();
    }
    
    private void hideCurrentPage()
    {
        if ( _currentPage == -1 )
            return;
        
        MGContainer page = _pages.get( _currentPage );
        page.setVisible( false );
        
        _currentPage = -1;
        
        remove( page );
    }
    
    public void clearChoices()
    {
        hideCurrentPage();
        _pages.clear();
    }
    
    // ========================================
    
    private void onChoiceButtonClicked( MGComponent sender, int x, int y, int button )
    {
        if ( ( button == 0 ) && ( sender.getId() >= 0 ) )
        {
            int choice = ( _currentPage * _choicesPerPage ) + sender.getId();
            choiceClicked( choice );
        }
    }
    
    private void onPageButtonClicked( MGComponent sender, int x, int y, int mouseButton )
    {
        if ( mouseButton != 0 )
            return;
        
        if ( sender == _buttonPrevPage )
        {
            displayPage( _currentPage - 1 );
        }
        else if ( sender == _buttonNextPage )
        {
            displayPage( _currentPage + 1 );
        }
    }
    
    // ========================================
    
    public void setChoiceListener( IChoiceMadeListener choiceListener )
    {
        this.choiceListener = choiceListener;
    }
    
    protected void choiceClicked( int choice )
    {
        if ( choiceListener != null )
        {
            choiceListener.onChoiceMade( choice );
        }
    }
    
    // ========================================
    
    @FunctionalInterface
    public interface IChoiceMadeListener
    {
        void onChoiceMade( int choice );
    }
}
