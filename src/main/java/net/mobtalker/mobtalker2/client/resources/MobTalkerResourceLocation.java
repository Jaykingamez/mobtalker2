/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.resources;

import static net.minecraftforge.fml.relauncher.Side.*;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.mobtalker.mobtalker2.MobTalker2;

@SideOnly( CLIENT )
public class MobTalkerResourceLocation extends ResourceLocation
{
    public MobTalkerResourceLocation( String path )
    {
        super( MobTalker2.DOMAIN, path );
    }
    
}
