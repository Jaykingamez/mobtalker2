/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.resources;

import static com.google.common.base.Preconditions.checkArgument;
import static net.minecraftforge.fml.relauncher.Side.CLIENT;

import java.io.*;
import java.nio.file.*;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.mobtalker.mobtalker2.server.resources.scriptpack.IScriptPack;

@SideOnly( CLIENT )
public abstract class ScriptResources
{
    public static ScriptResources fromScriptPack( IScriptPack pack )
        throws IOException
    {
        checkArgument( pack.hasResources(), "pack has no resources!" );
        
        Path packPath = pack.getPath();
        
        if ( Files.isRegularFile( packPath ) )
            return new ScriptFileResources( pack.getTitle(), packPath, "resources/" );
        
        if ( Files.isDirectory( packPath ) )
            return new ScriptPackResources( pack.getTitle(), packPath );
        
        throw new AssertionError();
    }
    
    // ========================================
    
    protected final String _name;
    protected final Path _path;
    
    // ========================================
    
    public ScriptResources( String name, Path path )
    {
        _name = name;
        _path = path;
    }
    
    // ========================================
    
    public String getPackName()
    {
        return _name;
    }
    
    /**
     * Should start with one of the following: sprites, scenes, music, sfx, ui
     *
     * @param path The path relative to the /resources/ folder
     */
    public abstract boolean hasResource( String path );
    
    /**
     * @param path The path relative to the /resources/ folder
     */
    public abstract InputStream getResourceAsStream( String path )
        throws IOException;
    
    // ========================================
    
    public abstract void close();
    
    // ========================================
    
    @Override
    public String toString()
    {
        return getPackName();
    }
}
