/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.resources;

import com.google.common.collect.Lists;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import net.minecraft.client.resources.IResourcePack;
import net.minecraft.client.resources.data.IMetadataSection;
import net.minecraft.client.resources.data.IMetadataSerializer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.mobtalker.mobtalker2.MobTalker2;
import net.mobtalker.mobtalker2.server.resources.scriptpack.IScriptPack;
import net.mobtalker.mobtalker2.server.resources.scriptpack.ScriptPackRepository;
import net.mobtalker.mobtalker2.util.PathUtil;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;
import org.apache.commons.lang3.StringUtils;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static net.minecraftforge.fml.relauncher.Side.CLIENT;

@SideOnly( CLIENT )
public class ScriptResourceLoader implements IResourcePack
{
    private static final ScriptResourceLoader INSTANCE = new ScriptResourceLoader();
    
    public static ScriptResourceLoader instance()
    {
        return INSTANCE;
    }
    
    // ========================================
    
    private static final MobTalkerLog LOG = new MobTalkerLog( "ResourceLoader" );
    
    // ========================================
    
    public static void refreshResources()
    {
        INSTANCE.unloadResources();
        
        scanScriptPacks();
        scanResourceFolder( PathUtil.getMobTalkerPath( "resources" ) );
    }
    
    private static void scanResourceFolder( Path folderPath )
    {
        try
        {
            PathUtil.ensureDirectory( folderPath );
        }
        catch ( IOException e )
        {
            LOG.fatal( e, "Unable to locate or create resources folder" );
            return;
        }
        
        scanResourceFolderFilePacks( folderPath );
        scanResourceFolderFolderPacks( folderPath );
    }
    
    private static void scanResourceFolderFilePacks( Path folderPath )
    {
        LOG.trace( "Scanning resources folder for file art packs" );
        
        try ( DirectoryStream<Path> stream = Files.newDirectoryStream( folderPath, "*.{zip}" ) )
        {
            for ( Path path : stream )
            {
                String name = path.getFileName().toString();
                LOG.debug( "Found file art pack '%s' in resources folder, adding contents as resources", name );
                
                name = StringUtils.removeEnd( name, ".zip" );
                
                try
                {
                    ScriptResources resources = new ScriptFileResources( name, path, "" );
                    INSTANCE.addResources( resources );
                }
                catch ( IOException e )
                {
                    LOG.warn( e, "Unable to read file art pack at '%s'", path );
                }
            }
        }
        catch ( IOException e )
        {
            LOG.error( e, "Unable to scan resources folder for file art packs" );
        }
    }
    
    private static void scanResourceFolderFolderPacks( Path folderPath )
    {
        LOG.trace( "Scanning resources folder for folder art packs" );
        
        try ( DirectoryStream<Path> stream = Files.newDirectoryStream( folderPath, Files::isDirectory ) )
        {
            for ( Path path : stream )
            {
                String name = path.getFileName().toString();
                LOG.debug( "Found folder art pack '%s' in resources folder, adding contents as resources", name );
                
                ScriptResources resources = new ScriptFolderResources( name, path );
                INSTANCE.addResources( resources );
            }
        }
        catch ( IOException e )
        {
            LOG.error( e, "Unable to scan resources folder for folder art packs" );
        }
    }
    
    private static void scanScriptPacks()
    {
        for ( IScriptPack scriptPack : ScriptPackRepository.instance().getActiveScriptPacks() )
        {
            if ( scriptPack.hasResources() )
            {
                try
                {
                    LOG.debug( "Found integrated art pack in script pack '%s', adding contents as resources", scriptPack.getTitle() );
                    ScriptResources resources = ScriptResources.fromScriptPack( scriptPack );
                    INSTANCE.addResources( resources );
                }
                catch ( IOException ex )
                {
                    LOG.error( ex, "Unable to create resource pack from script pack '%s'", scriptPack.getTitle() );
                }
            }
        }
    }
    
    // ========================================
    
    private List<ScriptResources> _resourcePacks;
    
    // ========================================
    
    private ScriptResourceLoader()
    {
        _resourcePacks = Lists.newArrayList();
    }
    
    // ========================================
    
    public List<ScriptResources> getResourcePacks()
    {
        return _resourcePacks;
    }
    
    public void addResources( ScriptResources resourcePack )
    {
        _resourcePacks.add( resourcePack );
    }
    
    public void unloadResources()
    {
        for ( ScriptResources pack : _resourcePacks )
        {
            pack.close();
        }
        
        _resourcePacks.clear();
    }
    
    public boolean hasResourcePacks()
    {
        return !_resourcePacks.isEmpty();
    }
    
    // ========================================
    
    @Override
    public String getPackName()
    {
        String name = "MobTalker2ResourceLoader";
        
        if ( hasResourcePacks() )
        {
            name += _resourcePacks.toString();
        }
        
        return name;
    }
    
    @Override
    public InputStream getInputStream( ResourceLocation location )
            throws IOException
    {
        if ( !hasResourcePacks() )
        {
            return null;
        }
        if ( !getResourceDomains().contains( location.getResourceDomain() ) )
        {
            return null;
        }
        
        String resourcePath = location.getResourcePath();
        for ( ScriptResources pack : _resourcePacks )
        {
            if ( pack.hasResource( resourcePath ) )
            {
                LOG.debug( "Found requested script resource: '%s'", location );
                return pack.getResourceAsStream( resourcePath );
            }
        }
        
        return null;
    }
    
    @Override
    public boolean resourceExists( ResourceLocation location )
    {
        if ( !hasResourcePacks() )
        {
            return false;
        }
        if ( !location.getResourceDomain().equals( MobTalker2.DOMAIN ) )
        {
            return false;
        }
        
        String resourcePath = location.getResourcePath();
        for ( ScriptResources pack : _resourcePacks )
        {
            if ( pack.hasResource( resourcePath ) )
            {
                return true;
            }
        }
        
        return false;
    }
    
    @Override
    public Set<String> getResourceDomains()
    {
        return Collections.singleton( MobTalker2.DOMAIN );
    }
    
    @Override
    public IMetadataSection getPackMetadata( IMetadataSerializer serializer, String type )
            throws IOException
    {
        JsonObject json = null;
        try
        {
            json = new JsonParser().parse( "{\"pack\":{\"description\":\"" + getPackName() + "\",\"pack_format\":1}}" )
                                   .getAsJsonObject();
        }
        catch ( Exception ex )
        {
            throw new JsonParseException( ex );
        }
        
        return serializer.parseMetadataSection( type, json );
    }
    
    @Override
    public BufferedImage getPackImage()
            throws IOException
    {
        return null; // None
    }
}
