/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

import net.minecraft.client.resources.IResourcePack;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.event.*;
import net.minecraftforge.fml.relauncher.*;
import net.mobtalker.mobtalker2.*;
import net.mobtalker.mobtalker2.client.gui.MobTalkerInteractionGui;
import net.mobtalker.mobtalker2.client.interaction.ClientInteractionHandler;
import net.mobtalker.mobtalker2.client.resources.ScriptResourceLoader;
import net.mobtalker.mobtalker2.client.util.EntityPathDebugger;
import net.mobtalker.mobtalker2.common.*;
import net.mobtalker.mobtalker2.common.launch.FingerprintViolationException;
import net.mobtalker.mobtalker2.util.PathUtil;

import org.lwjgl.opengl.Display;

import com.google.common.collect.ImmutableList;

import de.rummeltsoftware.mc.mgt.MGGuiScreenAdapter;

@SideOnly( Side.CLIENT )
public class MobTalkerClientProxy extends MobTalkerCommonProxy
{
    @Override
    public void onPreInit( FMLPreInitializationEvent event )
    {
        super.onPreInit( event );
        injectArtPackRepository();
        
        if ( MobTalkerConfig.isDebugEnabled() )
        {
            Display.setLocation( Display.getX(), 0 );
        }
    }
    
    private static void injectArtPackRepository()
    {
        List<IResourcePack> resourcePackList = getResourcePackList();
        resourcePackList.add( ScriptResourceLoader.instance() );
    }
    
    private static List<IResourcePack> getResourcePackList()
    {
        return ReflectionHelper.getPrivateValue( FMLClientHandler.class,
                                                 FMLClientHandler.instance(),
                                                 "resourcePackList" );
    }
    
    @Override
    public void onInit( FMLInitializationEvent event )
    {
        super.onInit( event );
        
        MobTalkerItems.registerRender();
        
        if ( MobTalkerConfig.isDebugEnabled() )
        {
            EntityPathDebugger.enable();
        }
    }
    
    // ========================================
    
    @Override
    protected List<Path> getScriptPackPaths()
    {
        try
        {
            return ImmutableList.of( PathUtil.getMobTalkerPath( "scripts" ),
                                     getWorldSavePath() );
        }
        catch ( IOException ex )
        {
            LOG.error( ex, "Unable to get script repository paths" );
            return Collections.emptyList();
        }
    }
    
    @Override
    public void onServerStarted( FMLServerStartedEvent event )
    {
        super.onServerStarted( event );
        
        ScriptResourceLoader.refreshResources();
    }
    
    // ========================================
    
    @Override
    public void onFingerprintViolation( FMLFingerprintViolationEvent event )
    {
        throw new FingerprintViolationException();
    }
    
    // ========================================
    
    @Override
    public Object getClientGuiElement( int ID, EntityPlayer player, World world, int x, int y, int z )
    {
        ClientInteractionHandler handler = new ClientInteractionHandler( player );
        
        player.openContainer = handler;
        
        MobTalkerInteractionGui gui = handler.getGui();
        MGGuiScreenAdapter adapter = new MGGuiScreenAdapter( gui, false );
        
        return adapter;
    }
    
    // ========================================
    
    @Override
    protected Path getWorldSavePath()
        throws IOException
    {
        MinecraftServer server = MinecraftServer.getServer();
        Path repoPath = PathUtil.getMinecraftPath( "saves", server.getFolderName(), MobTalker2.ID, "scripts" );
        PathUtil.ensureDirectory( repoPath );
        
        return repoPath;
    }
}
