/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatStyle;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IChatComponent;
import net.mobtalker.mobtalkerscript.v3.value.MtsValue;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static net.minecraft.util.EnumChatFormatting.*;

public class ChatMessageHelper
{
    /** @since 0.7.5 */
    private static IChatComponent formatMtsValue( MtsValue value, String indent )
    {
        if ( value.isNumber() || value.isBoolean() || value.isNil() )
        {
            return coloredText( value.toString(), BLUE );
        }
        
        if ( value.isTable() )
        {
            IChatComponent component = new ChatComponentText( "{" );
            final IChatComponent newline = new ChatComponentText( "\n" + indent );
            
            Map<MtsValue, MtsValue> map = value.asTable().map();
            List<MtsValue> keys = map.keySet().stream().sorted( ( a, b ) -> a.toString().compareToIgnoreCase( b.toString() ) ).collect( Collectors.toList() );
            for ( MtsValue key : keys )
            {
                component.appendSibling( newline );
                component.appendText( "  [" );
                component.appendSibling( formatMtsValue( key, indent + "  " ) );
                component.appendText( "] = " );
                component.appendSibling( formatMtsValue( map.get( key ), indent + "  " ) );
                component.appendText( "," );
            }
            
            if ( !map.isEmpty() )
            {
                component.appendSibling( newline );
            }
            
            component.appendText( "}" );
            return component;
        }
        
        if ( value.isClosure() )
        {
            return coloredText( "<closure>", ITALIC );
        }
        
        if ( value.isFunction() )
        {
            return coloredText( "<function>", ITALIC );
        }
        
        return coloredText( value.toString(), GOLD );
    }
    
    /** @since 0.7.5 */
    public static IChatComponent formatMtsValue( MtsValue value )
    {
        return formatMtsValue( value, "" );
    }
    
    // ========================================
    
    public static void sendMessageToEntity( EntityLivingBase entity, String msg, EnumChatFormatting color, Object... data )
    {
        for ( String line : StringUtils.split( msg, '\n' ) )
        {
            entity.addChatMessage( coloredText( line, color, data ) );
        }
    }
    
    public static void sendMessageToEntity( EntityLivingBase entity, String msg, EnumChatFormatting color )
    {
        for ( String line : StringUtils.split( msg, '\n' ) )
        {
            entity.addChatMessage( coloredText( line, color ) );
        }
    }
    
    // ========================================
    
    public static ChatComponentText coloredText( String msg, EnumChatFormatting color, Object... data )
    {
        return coloredText( String.format( msg, data ), color );
    }
    
    public static ChatComponentText coloredText( String msg, EnumChatFormatting color )
    {
        ChatComponentText cm = new ChatComponentText( msg );
        cm.setChatStyle( new ChatStyle().setColor( color ) );
        
        return cm;
    }
    
    // ========================================
    
    private ChatMessageHelper()
    {}
}
