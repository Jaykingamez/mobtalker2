/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

import java.util.*;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import net.minecraft.scoreboard.*;
import net.minecraft.server.MinecraftServer;
import net.mobtalker.mobtalkerscript.v3.MtsArgumentException;

@Deprecated
public class ScoreboardWrapper
{
    private static int getDefaultDimension()
    {
        return 0;
    }
    
    private static Scoreboard getScoreboardForDimension( int dimension )
    {
        return MinecraftServer.getServer().worldServerForDimension( dimension ).getScoreboard();
    }
    
    private static ScoreObjective getObjective( int dimension, String name, boolean throwOnReadonly )
    {
        ScoreObjective objective = getScoreboardForDimension( dimension ).getObjective( name );
        
        if ( objective == null )
            throw new MtsArgumentException( "unknown objective '%s'", name );
        else if ( throwOnReadonly && objective.getCriteria().isReadOnly() )
            throw new MtsArgumentException( "objective '%s' is read-only", name );
        else
            return objective;
    }
    
    private static IScoreObjectiveCriteria getCriteria( String criteriaName )
    {
        return (IScoreObjectiveCriteria) IScoreObjectiveCriteria.INSTANCES.get( criteriaName );
    }
    
    private static Score getScore( int dimension, String playerName, String objectiveName )
    {
        Scoreboard scoreboard = getScoreboardForDimension( dimension );
        ScoreObjective objective = getObjective( dimension, objectiveName, false );
        
        Score score = scoreboard.getValueFromObjective( playerName, objective );
        return score;
    }
    
    // ========================================
    
    private static ScoreboardObjectiveInfo getInfo( ScoreObjective objective )
    {
        return new ScoreboardObjectiveInfo( objective.getName(),
                                            objective.getCriteria().getName() );
    }
    
    private static ScoreboardScoreInfo getInfo( Score score )
    {
        return new ScoreboardScoreInfo( score.getPlayerName(),
                                        getInfo( score.getObjective() ),
                                        score.getScorePoints() );
    }
    
    private static ScoreboardTeamInfo getInfo( ScorePlayerTeam team )
    {
        return new ScoreboardTeamInfo( team.getRegisteredName(),
                                       ChatFormattingUtil.fromPrefix( team.getColorPrefix() ).getFriendlyName(),
                                       team.getAllowFriendlyFire(),
                                       team.getSeeFriendlyInvisiblesEnabled() );
    }
    
    // ========================================
    
    public static boolean checkObjectiveDisplaySlot( String slot )
    {
        return Scoreboard.getObjectiveDisplaySlotNumber( slot ) > 0;
    }
    
    public static void setDisplayedObjective( String slotName, String objectiveName )
    {
        setDisplayedObjective( getDefaultDimension(), slotName, objectiveName );
    }
    
    public static void setDisplayedObjective( int dimension, String slotName, String objectiveName )
    {
        getScoreboardForDimension( dimension ).setObjectiveInDisplaySlot( Scoreboard.getObjectiveDisplaySlotNumber(
            slotName ),
                                                                          getObjective( dimension,
                                                                                        objectiveName,
                                                                                        false ) );
    }
    
    public static void clearDisplayedObjective( String slotName )
    {
        clearDisplayedObjective( getDefaultDimension(), slotName );
    }
    
    public static void clearDisplayedObjective( int dimension, String slotName )
    {
        getScoreboardForDimension( dimension ).setObjectiveInDisplaySlot( Scoreboard.getObjectiveDisplaySlotNumber(
            slotName ),
                                                                          null );
    }
    
    // ========================================
    
    public static boolean hasObjective( String name )
    {
        return hasObjective( getDefaultDimension(), name );
    }
    
    public static boolean hasObjective( int dimension, String name )
    {
        return getScoreboardForDimension( dimension ).getObjective( name ) != null;
    }
    
    public static boolean hasCriteria( String criteriaName )
    {
        return IScoreObjectiveCriteria.INSTANCES.get( criteriaName ) != null;
    }
    
    public static List<ScoreboardObjectiveInfo> getObjectives()
    {
        return getObjectives( getDefaultDimension() );
    }
    
    public static List<ScoreboardObjectiveInfo> getObjectives( int dimension )
    {
        @SuppressWarnings( "unchecked" )
        Collection<ScoreObjective> objectives = getScoreboardForDimension( dimension ).getScoreObjectives();
        
        List<ScoreboardObjectiveInfo> results = Lists.newArrayListWithCapacity( objectives.size() );
        for ( ScoreObjective objective : objectives )
        {
            results.add( getInfo( objective ) );
        }
        
        return results;
    }

//    @SuppressWarnings( "unchecked" )
//    public static List<String> getObjectiveNames()
//    {
//        return new ArrayList<String>( getScoreboard().getObjectiveNames() );
//    }
    
    public static void addObjective( String objectiveName, String criteriaName, String displayName )
    {
        addObjective( getDefaultDimension(), objectiveName, criteriaName, displayName );
    }
    
    public static void addObjective( int dimension, String objectiveName, String criteriaName, String displayName )
    {
        ScoreObjective objective = getScoreboardForDimension( dimension ).addScoreObjective( objectiveName,
                                                                                             getCriteria( criteriaName ) );
        
        if ( !Strings.isNullOrEmpty( displayName ) )
        {
            objective.setDisplayName( displayName );
        }
    }
    
    public static void removeObjective( String objectiveName )
    {
        removeObjective( getDefaultDimension(), objectiveName );
    }
    
    public static void removeObjective( int dimension, String objectiveName )
    {
        getScoreboardForDimension( dimension ).removeObjective( getObjective( dimension, objectiveName, false ) );
    }
    
    // ========================================
    
    public static List<String> getPlayerNames()
    {
        return getPlayerNames( getDefaultDimension() );
    }
    
    @SuppressWarnings( "unchecked" )
    public static List<String> getPlayerNames( int dimension )
    {
        return Lists.newArrayList( getScoreboardForDimension( dimension ).getObjectiveNames() );
    }
    
    public static List<ScoreboardScoreInfo> getPlayerScores( String playerName )
    {
        return getPlayerScores( getDefaultDimension(), playerName );
    }
    
    public static List<ScoreboardScoreInfo> getPlayerScores( int dimension, String playerName )
    {
        // Map<String, Map<ScoreObjective, Score>>
        @SuppressWarnings( "unchecked" )
        Map<ScoreObjective, Score> scores = getScoreboardForDimension( dimension ).getObjectivesForEntity( playerName );
        
        List<ScoreboardScoreInfo> results = Lists.newArrayListWithCapacity( scores.size() );
        for ( Score score : scores.values() )
        {
            results.add( getInfo( score ) );
        }
        
        return results;
    }
    
    public static ScoreboardScoreInfo getPlayerScore( String playerName, String objectiveName )
    {
        return getPlayerScore( getDefaultDimension(), playerName, objectiveName );
    }
    
    public static ScoreboardScoreInfo getPlayerScore( int dimension, String playerName, String objectiveName )
    {
        return getInfo( getScore( dimension, playerName, objectiveName ) );
    }
    
    public static void setPlayerScore( String playerName, String objectiveName, int value )
    {
        setPlayerScore( getDefaultDimension(), playerName, objectiveName, value );
    }
    
    public static void setPlayerScore( int dimension, String playerName, String objectiveName, int value )
    {
        getScore( dimension, playerName, objectiveName ).setScorePoints( value );
    }
    
    public static int increasePlayerScore( String playerName, String objectiveName, int value )
    {
        return increasePlayerScore( getDefaultDimension(), playerName, objectiveName, value );
    }
    
    public static int increasePlayerScore( int dimension, String playerName, String objectiveName, int value )
    {
        Score score = getScore( dimension, playerName, objectiveName );
        score.increseScore( value );
        return score.getScorePoints();
    }
    
    public static int decreasePlayerScore( String playerName, String objectiveName, int value )
    {
        return decreasePlayerScore( getDefaultDimension(), playerName, objectiveName, value );
    }
    
    public static int decreasePlayerScore( int dimension, String playerName, String objectiveName, int value )
    {
        Score score = getScore( dimension, playerName, objectiveName );
        score.decreaseScore( value );
        return score.getScorePoints();
    }
    
    // ========================================
    
    public static boolean hasTeam( String teamName )
    {
        return hasTeam( getDefaultDimension(), teamName );
    }
    
    public static boolean hasTeam( int dimension, String teamName )
    {
        return getScoreboardForDimension( dimension ).getTeam( teamName ) != null;
    }
    
    public static List<ScoreboardTeamInfo> getTeamInfos()
    {
        return getTeamInfos( getDefaultDimension() );
    }
    
    public static List<ScoreboardTeamInfo> getTeamInfos( int dimension )
    {
        @SuppressWarnings( "unchecked" )
        Collection<ScorePlayerTeam> teams = getScoreboardForDimension( dimension ).getTeams();
        
        List<ScoreboardTeamInfo> results = Lists.newArrayListWithCapacity( teams.size() );
        for ( ScorePlayerTeam team : teams )
        {
            results.add( getInfo( team ) );
        }
        
        return results;
    }
    
    public static List<String> getTeamMembers( String teamName )
    {
        return getTeamMembers( getDefaultDimension(), teamName );
    }
    
    @SuppressWarnings( "unchecked" )
    public static List<String> getTeamMembers( int dimension, String teamName )
    {
        return Lists.newArrayList( getScoreboardForDimension( dimension ).getTeam( teamName )
                                                                         .getMembershipCollection() );
    }
    
    public static void addTeam( String teamName, String displayName )
    {
        addTeam( getDefaultDimension(), teamName, displayName );
    }
    
    public static void addTeam( int dimension, String teamName, String displayName )
    {
        ScorePlayerTeam team = getScoreboardForDimension( dimension ).createTeam( teamName );
        if ( !Strings.isNullOrEmpty( displayName ) )
        {
            team.setTeamName( displayName );
        }
    }
    
    public static void removeTeam( String teamName )
    {
        removeTeam( getDefaultDimension(), teamName );
    }
    
    public static void removeTeam( int dimension, String teamName )
    {
        Scoreboard scoreboard = getScoreboardForDimension( dimension );
        scoreboard.removeTeam( scoreboard.getTeam( teamName ) );
    }
    
    public static void addTeamMember( String teamName, String playerName )
    {
        addTeamMember( getDefaultDimension(), teamName, playerName );
    }
    
    public static void addTeamMember( int dimension, String teamName, String playerName )
    {
        getScoreboardForDimension( dimension ).addPlayerToTeam( playerName, teamName );
    }
    
    public static void removeTeamMember( String teamName, String playerName )
    {
        removeTeamMember( getDefaultDimension(), teamName, playerName );
    }
    
    public static void removeTeamMember( int dimension, String teamName, String playerName )
    {
        Scoreboard scoreboard = getScoreboardForDimension( dimension );
        scoreboard.removePlayerFromTeam( playerName, scoreboard.getTeam( teamName ) );
    }
}
