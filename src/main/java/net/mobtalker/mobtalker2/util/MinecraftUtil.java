/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

import net.minecraft.world.*;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class MinecraftUtil
{
    /**
     * Calls {@link FMLCommonHandler#getEffectiveSide()}
     */
    public static boolean isServer()
    {
        return FMLCommonHandler.instance().getEffectiveSide().isServer();
    }
    
    public static boolean isOverworld( World world )
    {
        WorldServer worldDimension0 = DimensionManager.getWorld( 0 );
        return worldDimension0.equals( world );
    }
}
