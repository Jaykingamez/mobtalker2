/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

import java.util.HashMap;

import net.mobtalker.mobtalkerscript.v3.value.MtsString;

import com.google.common.collect.Maps;

public enum EquipmentSlot
{
 HEAD( "head", 1 ),
 CHEST( "chest", 2 ),
 LEGS( "legs", 3 ),
 FEET( "feet", 4 ),
    
    ; // ========================================
    
    private static final HashMap<String, EquipmentSlot> _values;
    
    static
    {
        _values = Maps.newHashMapWithExpectedSize( values().length );
        for ( EquipmentSlot value : values() )
        {
            _values.put( value.getName(), value );
        }
    }
    
    public static EquipmentSlot forName( String name )
    {
        return _values.get( name );
    }
    
    // ========================================
    
    private final String _name;
    private final int _id;
    private final MtsString _key;
    
    private EquipmentSlot( String name, int id )
    {
        _name = name;
        _id = id;
        _key = MtsString.of( name );
    }
    
    public String getName()
    {
        return _name;
    }
    
    public int getID()
    {
        return _id;
    }
    
    public MtsString getKey()
    {
        return _key;
    }
}
