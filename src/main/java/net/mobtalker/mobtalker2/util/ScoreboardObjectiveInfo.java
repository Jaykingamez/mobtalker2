/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

import static org.apache.commons.lang3.Validate.notNull;

import net.minecraft.scoreboard.ScoreObjective;

@Deprecated
public class ScoreboardObjectiveInfo
{
    public final String Name;
    public final String Criteria;
    
    // ========================================
    
    public ScoreboardObjectiveInfo( String name, String criteria )
    {
        Name = name;
        Criteria = criteria;
    }
    
    public ScoreboardObjectiveInfo( ScoreObjective objective )
    {
        notNull( objective );
        Name = objective.getName();
        Criteria = objective.getCriteria().getName();
    }
}
