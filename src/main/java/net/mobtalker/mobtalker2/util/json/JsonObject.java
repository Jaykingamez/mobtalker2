/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util.json;

import java.io.*;
import java.util.*;

import net.mobtalker.mobtalker2.util.json.JsonObject.Member;

/**
 * Represents a JSON object, i.e. an unordered set of name/value pairs, where the names are strings
 * and the values are JSON values.
 * <p>
 * Members can be added using the <code>add(String, ...)</code> methods which accept instances of {@link JsonValue}, strings,
 * primitive numbers, and boolean values. To modify certain values of an object, use the <code>set(String, ...)</code> methods.
 * Please note that the <code>add</code> methods are faster than <code>set</code> as they do not search for existing members. On
 * the other hand, the <code>add</code> methods do not prevent adding multiple members with the same name. Duplicate names are
 * discouraged but not prohibited by JSON.
 * </p>
 * <p>
 * Members can be accessed by their name using {@link #get(String)}. A list of all names can be obtained from the method
 * {@link #names()}. This class also supports iterating over the members in document order using an {@link #iterator()} or an
 * enhanced for loop:
 * </p>
 *
 * <pre>
 * for( Member member : jsonObject ) {
 *   String name = member.getName();
 *   JsonValue value = member.getValue();
 *   ...
 * }
 * </pre>
 * <p>
 * Even though JSON objects are unordered by definition, instances of this class preserve the order of members to allow
 * processing in document order and to guarantee a predictable output.
 * </p>
 * <p>
 * Note that this class is <strong>not thread-safe</strong>. If multiple threads access a <code>JsonObject</code> instance
 * concurrently, while at least one of these threads modifies the contents of this object, access to the instance must be
 * synchronized externally. Failure to do so may lead to an inconsistent state.
 * </p>
 * <p>
 * This class is <strong>not supposed to be extended</strong> by clients.
 * </p>
 */
@SuppressWarnings( "serial" )
// use default serial UID
public class JsonObject extends JsonValue implements Iterable<Member>
{
    
    private final List<String> _names;
    private final List<JsonValue> _values;
    private transient HashIndexTable _table;
    
    /**
     * Creates a new empty JsonObject.
     */
    public JsonObject()
    {
        _names = new ArrayList<String>();
        _values = new ArrayList<JsonValue>();
        _table = new HashIndexTable();
    }
    
    /**
     * Creates a new JsonObject, initialized with the contents of the specified JSON object.
     *
     * @param object
     *            the JSON object to get the initial contents from, must not be <code>null</code>
     */
    public JsonObject( JsonObject object )
    {
        this( object, false );
    }
    
    private JsonObject( JsonObject object, boolean unmodifiable )
    {
        if ( object == null )
            throw new NullPointerException( "object is null" );
        
        if ( unmodifiable )
        {
            _names = Collections.unmodifiableList( object._names );
            _values = Collections.unmodifiableList( object._values );
        }
        else
        {
            _names = new ArrayList<String>( object._names );
            _values = new ArrayList<JsonValue>( object._values );
        }
        _table = new HashIndexTable();
        updateHashIndex();
    }
    
    /**
     * Reads a JSON object from the given reader.
     * <p>
     * Characters are read in chunks and buffered internally, therefore wrapping an existing reader in an additional
     * <code>BufferedReader</code> does <strong>not</strong> improve reading performance.
     * </p>
     *
     * @param reader
     *            the reader to read the JSON object from
     * @return the JSON object that has been read
     * @throws IOException
     *             if an I/O error occurs in the reader
     * @throws ParseException
     *             if the input is not valid JSON
     * @throws UnsupportedOperationException
     *             if the input does not contain a JSON object
     */
    public static JsonObject readFrom( Reader reader )
        throws IOException
    {
        return JsonValue.readFrom( reader ).asObject();
    }
    
    /**
     * Reads a JSON object from the given string.
     *
     * @param string
     *            the string that contains the JSON object
     * @return the JSON object that has been read
     * @throws ParseException
     *             if the input is not valid JSON
     * @throws UnsupportedOperationException
     *             if the input does not contain a JSON object
     */
    public static JsonObject readFrom( String string )
    {
        return JsonValue.readFrom( string ).asObject();
    }
    
    /**
     * Returns an unmodifiable JsonObject for the specified one. This method allows to provide
     * read-only access to a JsonObject.
     * <p>
     * The returned JsonObject is backed by the given object and reflect changes that happen to it. Attempts to modify the
     * returned JsonObject result in an <code>UnsupportedOperationException</code>.
     * </p>
     *
     * @param object
     *            the JsonObject for which an unmodifiable JsonObject is to be returned
     * @return an unmodifiable view of the specified JsonObject
     */
    public static JsonObject unmodifiableObject( JsonObject object )
    {
        return new JsonObject( object, true );
    }
    
    /**
     * Appends a new member to the end of this object, with the specified name and the JSON
     * representation of the specified <code>int</code> value.
     * <p>
     * This method <strong>does not prevent duplicate names</strong>. Calling this method with a name that already exists in the
     * object will append another member with the same name. In order to replace existing members, use the method
     * <code>set(name, value)</code> instead. However, <strong> <em>add</em> is much faster than <em>set</em></strong> (because
     * it does not need to search for existing members). Therefore <em>add</em> should be preferred when constructing new
     * objects.
     * </p>
     *
     * @param name
     *            the name of the member to add
     * @param value
     *            the value of the member to add
     * @return the object itself, to enable method chaining
     */
    public JsonObject add( String name, int value )
    {
        add( name, valueOf( value ) );
        return this;
    }
    
    /**
     * Appends a new member to the end of this object, with the specified name and the JSON
     * representation of the specified <code>long</code> value.
     * <p>
     * This method <strong>does not prevent duplicate names</strong>. Calling this method with a name that already exists in the
     * object will append another member with the same name. In order to replace existing members, use the method
     * <code>set(name, value)</code> instead. However, <strong> <em>add</em> is much faster than <em>set</em></strong> (because
     * it does not need to search for existing members). Therefore <em>add</em> should be preferred when constructing new
     * objects.
     * </p>
     *
     * @param name
     *            the name of the member to add
     * @param value
     *            the value of the member to add
     * @return the object itself, to enable method chaining
     */
    public JsonObject add( String name, long value )
    {
        add( name, valueOf( value ) );
        return this;
    }
    
    /**
     * Appends a new member to the end of this object, with the specified name and the JSON
     * representation of the specified <code>float</code> value.
     * <p>
     * This method <strong>does not prevent duplicate names</strong>. Calling this method with a name that already exists in the
     * object will append another member with the same name. In order to replace existing members, use the method
     * <code>set(name, value)</code> instead. However, <strong> <em>add</em> is much faster than <em>set</em></strong> (because
     * it does not need to search for existing members). Therefore <em>add</em> should be preferred when constructing new
     * objects.
     * </p>
     *
     * @param name
     *            the name of the member to add
     * @param value
     *            the value of the member to add
     * @return the object itself, to enable method chaining
     */
    public JsonObject add( String name, float value )
    {
        add( name, valueOf( value ) );
        return this;
    }
    
    /**
     * Appends a new member to the end of this object, with the specified name and the JSON
     * representation of the specified <code>double</code> value.
     * <p>
     * This method <strong>does not prevent duplicate names</strong>. Calling this method with a name that already exists in the
     * object will append another member with the same name. In order to replace existing members, use the method
     * <code>set(name, value)</code> instead. However, <strong> <em>add</em> is much faster than <em>set</em></strong> (because
     * it does not need to search for existing members). Therefore <em>add</em> should be preferred when constructing new
     * objects.
     * </p>
     *
     * @param name
     *            the name of the member to add
     * @param value
     *            the value of the member to add
     * @return the object itself, to enable method chaining
     */
    public JsonObject add( String name, double value )
    {
        add( name, valueOf( value ) );
        return this;
    }
    
    /**
     * Appends a new member to the end of this object, with the specified name and the JSON
     * representation of the specified <code>boolean</code> value.
     * <p>
     * This method <strong>does not prevent duplicate names</strong>. Calling this method with a name that already exists in the
     * object will append another member with the same name. In order to replace existing members, use the method
     * <code>set(name, value)</code> instead. However, <strong> <em>add</em> is much faster than <em>set</em></strong> (because
     * it does not need to search for existing members). Therefore <em>add</em> should be preferred when constructing new
     * objects.
     * </p>
     *
     * @param name
     *            the name of the member to add
     * @param value
     *            the value of the member to add
     * @return the object itself, to enable method chaining
     */
    public JsonObject add( String name, boolean value )
    {
        add( name, valueOf( value ) );
        return this;
    }
    
    /**
     * Appends a new member to the end of this object, with the specified name and the JSON
     * representation of the specified string.
     * <p>
     * This method <strong>does not prevent duplicate names</strong>. Calling this method with a name that already exists in the
     * object will append another member with the same name. In order to replace existing members, use the method
     * <code>set(name, value)</code> instead. However, <strong> <em>add</em> is much faster than <em>set</em></strong> (because
     * it does not need to search for existing members). Therefore <em>add</em> should be preferred when constructing new
     * objects.
     * </p>
     *
     * @param name
     *            the name of the member to add
     * @param value
     *            the value of the member to add
     * @return the object itself, to enable method chaining
     */
    public JsonObject add( String name, String value )
    {
        add( name, valueOf( value ) );
        return this;
    }
    
    /**
     * Appends a new member to the end of this object, with the specified name and the specified JSON
     * value.
     * <p>
     * This method <strong>does not prevent duplicate names</strong>. Calling this method with a name that already exists in the
     * object will append another member with the same name. In order to replace existing members, use the method
     * <code>set(name, value)</code> instead. However, <strong> <em>add</em> is much faster than <em>set</em></strong> (because
     * it does not need to search for existing members). Therefore <em>add</em> should be preferred when constructing new
     * objects.
     * </p>
     *
     * @param name
     *            the name of the member to add
     * @param value
     *            the value of the member to add, must not be <code>null</code>
     * @return the object itself, to enable method chaining
     */
    public JsonObject add( String name, JsonValue value )
    {
        if ( name == null )
            throw new NullPointerException( "name is null" );
        if ( value == null )
            throw new NullPointerException( "value is null" );
        
        _table.add( name, _names.size() );
        _names.add( name );
        _values.add( value );
        return this;
    }
    
    /**
     * Sets the value of the member with the specified name to the JSON representation of the
     * specified <code>int</code> value. If this object does not contain a member with this name, a
     * new member is added at the end of the object. If this object contains multiple members with
     * this name, only the last one is changed.
     * <p>
     * This method should <strong>only be used to modify existing objects</strong>. To fill a new object with members, the
     * method <code>add(name, value)</code> should be preferred which is much faster (as it does not need to search for existing
     * members).
     * </p>
     *
     * @param name
     *            the name of the member to replace
     * @param value
     *            the value to set to the member
     * @return the object itself, to enable method chaining
     */
    public JsonObject set( String name, int value )
    {
        set( name, valueOf( value ) );
        return this;
    }
    
    /**
     * Sets the value of the member with the specified name to the JSON representation of the
     * specified <code>long</code> value. If this object does not contain a member with this name, a
     * new member is added at the end of the object. If this object contains multiple members with
     * this name, only the last one is changed.
     * <p>
     * This method should <strong>only be used to modify existing objects</strong>. To fill a new object with members, the
     * method <code>add(name, value)</code> should be preferred which is much faster (as it does not need to search for existing
     * members).
     * </p>
     *
     * @param name
     *            the name of the member to replace
     * @param value
     *            the value to set to the member
     * @return the object itself, to enable method chaining
     */
    public JsonObject set( String name, long value )
    {
        set( name, valueOf( value ) );
        return this;
    }
    
    /**
     * Sets the value of the member with the specified name to the JSON representation of the
     * specified <code>float</code> value. If this object does not contain a member with this name, a
     * new member is added at the end of the object. If this object contains multiple members with
     * this name, only the last one is changed.
     * <p>
     * This method should <strong>only be used to modify existing objects</strong>. To fill a new object with members, the
     * method <code>add(name, value)</code> should be preferred which is much faster (as it does not need to search for existing
     * members).
     * </p>
     *
     * @param name
     *            the name of the member to add
     * @param value
     *            the value of the member to add
     * @return the object itself, to enable method chaining
     */
    public JsonObject set( String name, float value )
    {
        set( name, valueOf( value ) );
        return this;
    }
    
    /**
     * Sets the value of the member with the specified name to the JSON representation of the
     * specified <code>double</code> value. If this object does not contain a member with this name, a
     * new member is added at the end of the object. If this object contains multiple members with
     * this name, only the last one is changed.
     * <p>
     * This method should <strong>only be used to modify existing objects</strong>. To fill a new object with members, the
     * method <code>add(name, value)</code> should be preferred which is much faster (as it does not need to search for existing
     * members).
     * </p>
     *
     * @param name
     *            the name of the member to add
     * @param value
     *            the value of the member to add
     * @return the object itself, to enable method chaining
     */
    public JsonObject set( String name, double value )
    {
        set( name, valueOf( value ) );
        return this;
    }
    
    /**
     * Sets the value of the member with the specified name to the JSON representation of the
     * specified <code>boolean</code> value. If this object does not contain a member with this name,
     * a new member is added at the end of the object. If this object contains multiple members with
     * this name, only the last one is changed.
     * <p>
     * This method should <strong>only be used to modify existing objects</strong>. To fill a new object with members, the
     * method <code>add(name, value)</code> should be preferred which is much faster (as it does not need to search for existing
     * members).
     * </p>
     *
     * @param name
     *            the name of the member to add
     * @param value
     *            the value of the member to add
     * @return the object itself, to enable method chaining
     */
    public JsonObject set( String name, boolean value )
    {
        set( name, valueOf( value ) );
        return this;
    }
    
    /**
     * Sets the value of the member with the specified name to the JSON representation of the
     * specified string. If this object does not contain a member with this name, a new member is
     * added at the end of the object. If this object contains multiple members with this name, only
     * the last one is changed.
     * <p>
     * This method should <strong>only be used to modify existing objects</strong>. To fill a new object with members, the
     * method <code>add(name, value)</code> should be preferred which is much faster (as it does not need to search for existing
     * members).
     * </p>
     *
     * @param name
     *            the name of the member to add
     * @param value
     *            the value of the member to add
     * @return the object itself, to enable method chaining
     */
    public JsonObject set( String name, String value )
    {
        set( name, valueOf( value ) );
        return this;
    }
    
    /**
     * Sets the value of the member with the specified name to the specified JSON value. If this
     * object does not contain a member with this name, a new member is added at the end of the
     * object. If this object contains multiple members with this name, only the last one is changed.
     * <p>
     * This method should <strong>only be used to modify existing objects</strong>. To fill a new object with members, the
     * method <code>add(name, value)</code> should be preferred which is much faster (as it does not need to search for existing
     * members).
     * </p>
     *
     * @param name
     *            the name of the member to add
     * @param value
     *            the value of the member to add, must not be <code>null</code>
     * @return the object itself, to enable method chaining
     */
    public JsonObject set( String name, JsonValue value )
    {
        if ( name == null )
            throw new NullPointerException( "name is null" );
        if ( value == null )
            throw new NullPointerException( "value is null" );
        
        int index = indexOf( name );
        if ( index != -1 )
        {
            _values.set( index, value );
        }
        else
        {
            _table.add( name, _names.size() );
            _names.add( name );
            _values.add( value );
        }
        return this;
    }
    
    /**
     * Removes a member with the specified name from this object. If this object contains multiple
     * members with the given name, only the last one is removed. If this object does not contain a
     * member with the specified name, the object is not modified.
     *
     * @param name
     *            the name of the member to remove
     * @return the object itself, to enable method chaining
     */
    public JsonObject remove( String name )
    {
        if ( name == null )
            throw new NullPointerException( "name is null" );
        
        int index = indexOf( name );
        if ( index != -1 )
        {
            _table.remove( index );
            _names.remove( index );
            _values.remove( index );
        }
        return this;
    }
    
    /**
     * Determines if this object contains at least one member with the specified name.
     *
     * @param name
     *            the name of the member to look for
     * @return if this object contains a member with the specified name
     */
    public boolean has( String name )
    {
        if ( name == null )
            throw new NullPointerException( "name is null" );
        
        return indexOf( name ) != -1;
    }
    
    /**
     * Returns the value of the member with the specified name in this object. If this object contains
     * multiple members with the given name, this method will return the last one.
     *
     * @param name
     *            the name of the member whose value is to be returned
     * @return the value of the last member with the specified name, or <code>null</code> if this
     *         object does not contain a member with that name
     */
    public JsonValue get( String name )
    {
        if ( name == null )
            throw new NullPointerException( "name is null" );
        
        int index = indexOf( name );
        
        if ( index != -1 )
            return _values.get( index );
        
        throw new MissingMemberException( name );
    }
    
    public String getString( String name )
    {
        return get( name ).asString();
    }
    
    public String getString( String name, String fallback )
    {
        if ( name == null )
            throw new NullPointerException( "name is null" );
        
        int index = indexOf( name );
        
        if ( index != -1 )
            return _values.get( index ).asString();
        
        return fallback;
    }
    
    public List<String> getStringList( String name )
    {
        if ( name == null )
            throw new NullPointerException( "name is null" );
        
        int index = indexOf( name );
        
        if ( index == -1 )
            return Collections.emptyList();
        
        JsonArray jsonArray = _values.get( index ).asArray();
        ArrayList<String> result = new ArrayList<>( index );
        for ( JsonValue jsonValue : jsonArray )
        {
            result.add( jsonValue.asString() );
        }
        
        return result;
    }
    
    public Set<String> getStringSet( String name )
    {
        return getStringSet( name, Collections.emptySet() );
    }
    
    public Set<String> getStringSet( String name, Set<String> fallback )
    {
        if ( name == null )
            throw new NullPointerException( "name is null" );
        
        int index = indexOf( name );
        
        if ( index == -1 )
            return fallback;
        
        JsonArray jsonArray = _values.get( index ).asArray();
        HashSet<String> result = new HashSet<>( index );
        for ( JsonValue jsonValue : jsonArray )
        {
            result.add( jsonValue.asString() );
        }
        
        return result;
    }
    
    /**
     * Returns the number of members (i.e. name/value pairs) in this object.
     *
     * @return the number of members in this object
     */
    public int size()
    {
        return _names.size();
    }
    
    /**
     * Returns <code>true</code> if this object contains no members.
     *
     * @return <code>true</code> if this object contains no members
     */
    public boolean isEmpty()
    {
        return _names.isEmpty();
    }
    
    /**
     * Returns a list of the names in this object in document order. The returned list is backed by
     * this object and will reflect subsequent changes. It cannot be used to modify this object.
     * Attempts to modify the returned list will result in an exception.
     *
     * @returns a list of the names in this object
     */
    public List<String> names()
    {
        return Collections.unmodifiableList( _names );
    }
    
    /**
     * Returns an iterator over the members of this object in document order. The returned iterator
     * cannot be used to modify this object.
     *
     * @return an iterator over the members of this object
     */
    @Override
    public Iterator<Member> iterator()
    {
        final Iterator<String> namesIterator = _names.iterator();
        final Iterator<JsonValue> valuesIterator = _values.iterator();
        return new Iterator<JsonObject.Member>()
        {
            @Override
            public boolean hasNext()
            {
                return namesIterator.hasNext();
            }
            
            @Override
            public Member next()
            {
                String name = namesIterator.next();
                JsonValue value = valuesIterator.next();
                return new Member( name, value );
            }
            
            @Override
            public void remove()
            {
                throw new UnsupportedOperationException();
            }
        };
    }
    
    @Override
    protected void write( JsonWriter writer )
        throws IOException
    {
        writer.writeObject( this );
    }
    
    @Override
    public boolean isObject()
    {
        return true;
    }
    
    @Override
    public JsonObject asObject()
    {
        return this;
    }
    
    @Override
    public int hashCode()
    {
        int result = 1;
        result = ( 31 * result ) + _names.hashCode();
        result = ( 31 * result ) + _values.hashCode();
        return result;
    }
    
    @Override
    public boolean equals( Object obj )
    {
        if ( this == obj )
        {
            return true;
        }
        if ( obj == null )
        {
            return false;
        }
        if ( getClass() != obj.getClass() )
        {
            return false;
        }
        JsonObject other = (JsonObject) obj;
        return _names.equals( other._names ) && _values.equals( other._values );
    }
    
    int indexOf( String name )
    {
        int index = _table.get( name );
        if ( ( index != -1 ) && name.equals( _names.get( index ) ) )
        {
            return index;
        }
        return _names.lastIndexOf( name );
    }
    
    private synchronized void readObject( ObjectInputStream inputStream )
        throws IOException,
        ClassNotFoundException
    {
        inputStream.defaultReadObject();
        _table = new HashIndexTable();
        updateHashIndex();
    }
    
    private void updateHashIndex()
    {
        int size = _names.size();
        for ( int i = 0; i < size; i++ )
        {
            _table.add( _names.get( i ), i );
        }
    }
    
    /**
     * Represents a member of a JSON object, i.e. a pair of name and value.
     */
    public static class Member
    {
        
        private final String _name;
        private final JsonValue _value;
        
        Member( String name, JsonValue value )
        {
            _name = name;
            _value = value;
        }
        
        /**
         * Returns the name of this member.
         *
         * @return the name of this member, never <code>null</code>
         */
        public String getName()
        {
            return _name;
        }
        
        /**
         * Returns the value of this member.
         *
         * @return the value of this member, never <code>null</code>
         */
        public JsonValue getValue()
        {
            return _value;
        }
        
        @Override
        public int hashCode()
        {
            int result = 1;
            result = ( 31 * result ) + _name.hashCode();
            result = ( 31 * result ) + _value.hashCode();
            return result;
        }
        
        @Override
        public boolean equals( Object obj )
        {
            if ( this == obj )
                return true;
            if ( obj == null )
                return false;
            if ( getClass() != obj.getClass() )
                return false;
            
            Member other = (Member) obj;
            return _name.equals( other._name ) && _value.equals( other._value );
        }
        
    }
    
    static class HashIndexTable
    {
        
        private final byte[] _hashTable = new byte[32]; // must be a power of two
        
        public HashIndexTable()
        {}
        
        public HashIndexTable( HashIndexTable original )
        {
            System.arraycopy( original._hashTable, 0, _hashTable, 0, _hashTable.length );
        }
        
        void add( String name, int index )
        {
            int slot = hashSlotFor( name );
            if ( index < 0xff )
            {
                // increment by 1, 0 stands for empty
                _hashTable[slot] = (byte) ( index + 1 );
            }
            else
            {
                _hashTable[slot] = 0;
            }
        }
        
        void remove( int index )
        {
            for ( int i = 0; i < _hashTable.length; i++ )
            {
                if ( _hashTable[i] == ( index + 1 ) )
                {
                    _hashTable[i] = 0;
                }
                else if ( _hashTable[i] > ( index + 1 ) )
                {
                    _hashTable[i]--;
                }
            }
        }
        
        int get( Object name )
        {
            int slot = hashSlotFor( name );
            // subtract 1, 0 stands for empty
            return ( _hashTable[slot] & 0xff ) - 1;
        }
        
        private int hashSlotFor( Object element )
        {
            return element.hashCode() & ( _hashTable.length - 1 );
        }
        
    }
    
}
