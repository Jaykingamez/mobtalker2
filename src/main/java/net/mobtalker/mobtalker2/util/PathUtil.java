/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

import java.io.IOException;
import java.nio.file.*;

import net.mobtalker.mobtalker2.MobTalker2;

public class PathUtil
{
    private static final Path PATH_MINECRAFT;
    private static final Path PATH_MOBTALKER;
    
    // ========================================
    
    static
    {
        PATH_MINECRAFT = Paths.get( "." ).toAbsolutePath().normalize();
        PATH_MOBTALKER = PATH_MINECRAFT.resolve( MobTalker2.ID ).toAbsolutePath().normalize();
    }
    
    // ========================================
    
    public static Path getMinecraftPath( String... others )
    {
        return resolve( PATH_MINECRAFT, others );
    }
    
    public static Path getMobTalkerPath( String... others )
    {
        return resolve( PATH_MOBTALKER, others );
    }
    
    public static Path relativizeMinecraftPath( Path other )
    {
        return PATH_MINECRAFT.relativize( other );
    }
    
    public static Path relativizeMobTalkerPath( Path other )
    {
        return PATH_MOBTALKER.relativize( other );
    }
    
    // ========================================
    
    public static void ensureDirectory( Path path )
        throws IOException
    {
        if ( !Files.isDirectory( path ) )
        {
            if ( Files.exists( path ) )
            {
                Files.delete( path );
            }
            
            Files.createDirectories( path );
        }
    }
    
    public static Path resolve( Path base, String... others )
    {
        
        if ( ( others == null ) || ( others.length == 0 ) )
            return base;
        
        Path result = base;
        for ( String other : others )
        {
            if ( other != null )
                result = result.resolve( other );
        }
        
        return result;
    }
}
