/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util.fp;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static org.apache.commons.lang3.Validate.notNull;

public abstract class Either<L, R>
{
    // Prevent inheritance outside this file
    private Either() {}
    
    // ========================================
    
    public boolean isLeft()
    {
        return false;
    }
    
    public LeftView<L, R> left()
    {
        return new LeftView<>( this );
    }
    
    // ========================================
    
    public boolean isRight()
    {
        return false;
    }
    
    public RightView<L, R> right()
    {
        return new RightView<>( this );
    }
    
    // ========================================
    
    public abstract <A, B> Either<A, B> bimap( Function<? super L, ? extends A> left,
                                               Function<? super R, ? extends B> right );
    
    public abstract Either<R, L> swap();
    
    public Option<Either<L, R>> filter( Predicate<? super L> left, Predicate<? super R> right )
    {
        notNull( left );
        notNull( right );
        if ( isLeft() )
        {
            return left().filter( left );
        }
        return right().filter( right );
    }
    
    // ========================================
    
    public static <L, R> Either<L, R> left( L value )
    {
        return new Left<>( value );
    }
    
    public static <L, R> Either<L, R> right( R value )
    {
        return new Right<>( value );
    }
    
    // ========================================
    
    public static final class Left<L, R> extends Either<L, R>
    {
        private final L _value;
        
        // ========================================
        
        private Left( L value )
        {
            _value = value;
        }
        
        // ========================================
        
        protected L value()
        {
            return _value;
        }
        
        // ========================================
        
        @Override
        public boolean isLeft()
        {
            return true;
        }
        
        // ========================================
        
        @Override
        public <A, B> Either<A, B> bimap( Function<? super L, ? extends A> left,
                                          Function<? super R, ? extends B> right )
        {
            notNull( left );
            notNull( right );
            A result = left.apply( value() );
            return Either.left( result );
        }
        
        @Override
        public Either<R, L> swap()
        {
            return Either.right( value() );
        }
        
        // ========================================
        
        @Override
        public String toString()
        {
            return new ToStringBuilder( this )
                           .append( "_value", _value )
                           .toString();
        }
        
        @Override
        public boolean equals( Object o )
        {
            if ( this == o )
            {
                return true;
            }
            if ( o == null )
            {
                return false;
            }
            if ( !o.getClass().equals( getClass() ) )
            {
                return false;
            }
            
            Left<?, ?> other = (Left<?, ?>) o;
            return new EqualsBuilder()
                           .append( _value, other._value )
                           .isEquals();
        }
        
        @Override
        public int hashCode()
        {
            return new HashCodeBuilder()
                           .append( getClass() )
                           .append( _value )
                           .toHashCode();
        }
    }
    
    // ========================================
    
    public static final class Right<L, R> extends Either<L, R>
    {
        private final R _value;
        
        // ========================================
        
        private Right( R value )
        {
            _value = value;
        }
        
        // ========================================
        
        protected R value()
        {
            return _value;
        }
        
        // ========================================
        
        @Override
        public boolean isRight()
        {
            return true;
        }
        
        // ========================================
        
        @Override
        public <A, B> Either<A, B> bimap( Function<? super L, ? extends A> left,
                                          Function<? super R, ? extends B> right )
        {
            notNull( left );
            notNull( right );
            B result = right.apply( value() );
            return Either.right( result );
        }
        
        @Override
        public Either<R, L> swap()
        {
            return Either.left( value() );
        }
        
        // ========================================
        
        @Override
        public String toString()
        {
            return new ToStringBuilder( this )
                           .append( "_value", _value )
                           .toString();
        }
        
        @Override
        public boolean equals( Object o )
        {
            if ( this == o )
            {
                return true;
            }
            if ( o == null )
            {
                return false;
            }
            if ( !o.getClass().equals( getClass() ) )
            {
                return false;
            }
            
            Right<?, ?> other = (Right<?, ?>) o;
            return new EqualsBuilder()
                           .append( _value, other._value )
                           .isEquals();
        }
        
        @Override
        public int hashCode()
        {
            return new HashCodeBuilder()
                           .append( getClass() )
                           .append( _value )
                           .toHashCode();
        }
    }
    
    // ========================================
    
    public static final class LeftView<L, R>
    {
        private final Either<L, R> _either;
        
        // ========================================
        
        private LeftView( Either<L, R> either )
        {
            notNull( either );
            _either = either;
        }
        
        // ========================================
        
        public Either<L, R> either()
        {
            return _either;
        }
        
        // ========================================
        
        public L get()
        {
            return getOrThrow();
        }
        
        public L getOrThrow()
        {
            return getOrThrow( "Either is not left" );
        }
        
        public L getOrThrow( String errorMessage )
        {
            return getOrThrow( () -> errorMessage );
        }
        
        public L getOrThrow( Supplier<String> errorMessage )
        {
            notNull( errorMessage );
            if ( either().isLeft() )
            {
                return ( (Left<L, R>) either() ).value();
            }
            throw new IllegalStateException( errorMessage.get() );
        }
        
        // ========================================
        
        public L getOrElse( L fallback )
        {
            return getOrElse( () -> fallback );
        }
        
        public L getOrElse( Supplier<? extends L> fallback )
        {
            notNull( fallback );
            if ( either().isLeft() )
            {
                return ( (Left<L, R>) either() ).value();
            }
            return fallback.get();
        }
        
        // ========================================
        
        public <T> Either<T, R> map( Function<? super L, ? extends T> mapping )
        {
            notNull( mapping );
            if ( either().isLeft() )
            {
                return new Left<>( mapping.apply( get() ) );
            }
            return new Right<>( either().right().get() );
        }
        
        public boolean exists( Predicate<? super L> condition )
        {
            notNull( condition );
            return either().isLeft() && condition.test( get() );
        }
        
        public <T> Option<Either<L, T>> filter( Predicate<? super L> condition )
        {
            notNull( condition );
            if ( either().isLeft() && condition.test( get() ) )
            {
                return Option.of( new Left<>( get() ) );
            }
            return Option.empty();
        }
        
        public void ifLeft( Consumer<? super L> action )
        {
            notNull( action );
            if ( either().isLeft() )
            {
                action.accept( get() );
            }
        }
        
        // ========================================
        
        public Option<L> toOption()
        {
            if ( either().isLeft() )
            {
                return Option.of( get() );
            }
            return Option.empty();
        }
    }
    
    // ========================================
    
    public static final class RightView<L, R>
    {
        private final Either<L, R> _either;
        
        // ========================================
        
        private RightView( Either<L, R> either )
        {
            notNull( either );
            _either = either;
        }
        
        // ========================================
        
        // ========================================
        
        public Either<L, R> either()
        {
            return _either;
        }
        
        // ========================================
        
        public R get()
        {
            return getOrThrow();
        }
        
        public R getOrThrow()
        {
            return getOrThrow( "Either is not left" );
        }
        
        public R getOrThrow( String errorMessage )
        {
            return getOrThrow( () -> errorMessage );
        }
        
        public R getOrThrow( Supplier<String> errorMessage )
        {
            notNull( errorMessage );
            if ( either().isRight() )
            {
                return ( (Right<L, R>) either() ).value();
            }
            throw new IllegalStateException( errorMessage.get() );
        }
        
        // ========================================
        
        public R getOrElse( R fallback )
        {
            return getOrElse( () -> fallback );
        }
        
        public R getOrElse( Supplier<? extends R> fallback )
        {
            notNull( fallback );
            if ( either().isLeft() )
            {
                return ( (Right<L, R>) either() ).value();
            }
            return fallback.get();
        }
        
        // ========================================
        
        public <T> Either<L, T> map( Function<? super R, ? extends T> mapping )
        {
            notNull( mapping );
            if ( either().isRight() )
            {
                return new Right<>( mapping.apply( get() ) );
            }
            return new Left<>( either().left().get() );
        }
        
        public boolean exists( Predicate<? super R> condition )
        {
            notNull( condition );
            return either().isRight() && condition.test( get() );
        }
        
        public <T> Option<Either<T, R>> filter( Predicate<? super R> condition )
        {
            notNull( condition );
            if ( either().isRight() && condition.test( get() ) )
            {
                return Option.of( new Right<>( get() ) );
            }
            return Option.empty();
        }
        
        public void ifRight( Consumer<? super R> action )
        {
            notNull( action );
            if ( either().isRight() )
            {
                action.accept( get() );
            }
        }
        
        // ========================================
        
        public Option<R> toOption()
        {
            if ( either().isRight() )
            {
                return Option.of( get() );
            }
            return Option.empty();
        }
    }
}
