/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

import net.minecraft.scoreboard.IScoreObjectiveCriteria;

public final class CriteriaUtil
{
    private static Object getRawCriteria( String name )
    {
        return IScoreObjectiveCriteria.INSTANCES.get( name );
    }
    
    public static IScoreObjectiveCriteria getCriteria( String name )
    {
        return (IScoreObjectiveCriteria) getRawCriteria( name );
    }
    
    public static boolean hasCriteria( String name )
    {
        return getRawCriteria( name ) != null;
    }
}
