/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

import java.util.Map;

import net.minecraft.util.EnumChatFormatting;

import com.google.common.collect.Maps;

public class ChatFormattingUtil
{
    private static final Map<String, EnumChatFormatting> _reverseLookup;
    
    static
    {
        _reverseLookup = Maps.newHashMap();
        
        for ( EnumChatFormatting v : EnumChatFormatting.values() )
        {
            _reverseLookup.put( v.toString(), v );
        }
    }
    
    public static EnumChatFormatting fromPrefix( String prefix )
    {
        return _reverseLookup.get( prefix );
    }
    
}
