/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

import java.util.*;
import java.util.Map.Entry;
import java.util.function.*;
import java.util.stream.Collectors;

import com.google.common.collect.*;

public class CollectionUtil
{
    public static <T> List<T> asListWithoutNull( T[] array )
    {
        ImmutableList.Builder<T> builder = ImmutableList.builder();
        for ( T value : array )
        {
            if ( value != null ) builder.add( value );
        }
        return builder.build();
    }
    
    public static <T> Set<T> asSetWithoutNull( T[] array )
    {
        ImmutableSet.Builder<T> builder = ImmutableSet.builder();
        for ( T value : array )
        {
            if ( value != null ) builder.add( value );
        }
        return builder.build();
    }
    
    public static <T extends Collection<U>, U> T addIf( T collection, U value, Predicate<U> condition )
    {
        if ( condition.test( value ) ) collection.add( value );
        return collection;
    }
    
    public static <T extends Collection<U>, U> T addIf( T collection, U value, boolean condition )
    {
        if ( condition ) collection.add( value );
        return collection;
    }
    
    public static String join( Collection<?> collection )
    {
        return join( collection, Objects::toString );
    }
    
    public static String join( Collection<?> collection, String delimiter )
    {
        return join( collection, Objects::toString, delimiter );
    }
    
    public static String join( Collection<?> collection, String delimiter, String prefix,
                               String suffix )
    {
        return join( collection, Objects::toString, delimiter, prefix, suffix );
    }
    
    public static <T> String join( Collection<T> collection, Function<? super T, ? extends CharSequence> toString )
    {
        return collection.stream().map( toString ).collect( Collectors.joining() );
    }
    
    public static <T> String join( Collection<T> collection, Function<? super T, ? extends CharSequence> toString,
                                   String delimiter )
    {
        return collection.stream().map( toString ).collect( Collectors.joining( delimiter ) );
    }
    
    public static <T> String join( Collection<T> collection, Function<? super T, ? extends CharSequence> toString,
                                   String delimiter, String prefix, String suffix )
    {
        return collection.stream().map( toString ).collect( Collectors.joining( delimiter ) );
    }
    
    @SuppressWarnings( "unchecked" )
    public static <T> List<T> generify( Collection<?> collection )
    {
        return generify( collection, e -> (T) e );
    }
    
    public static <T, U> List<T> generify( Collection<U> collection, Function<? super U, ? extends T> converter )
    {
        if ( collection == null ) return null;
        return collection.stream().map( converter ).collect( Collectors.toList() );
    }
    
    @SuppressWarnings( "unchecked" )
    public static <K, V> Map<K, V> generify( Map<?, ?> map )
    {
        return generify( map, k -> (K) k, v -> (V) v );
    }
    
    public static <K1, K2, V1, V2> Map<K2, V2> generify( Map<K1, V1> map, Function<? super K1, ? extends K2> convertKey,
                                                         Function<? super V1, ? extends V2> convertValue )
    {
        if ( map == null ) return null;
        Map<K2, V2> m = Maps.newHashMapWithExpectedSize( map.size() );
        for ( Entry<K1, V1> entry : map.entrySet() )
        {
            m.put( convertKey.apply( entry.getKey() ), convertValue.apply( entry.getValue() ) );
        }
        return m;
    }
}
