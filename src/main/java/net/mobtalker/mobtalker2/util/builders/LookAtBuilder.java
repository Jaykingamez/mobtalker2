/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util.builders;

import net.minecraft.entity.Entity;
import net.minecraft.entity.ai.EntityLookHelper;
import net.mobtalker.mobtalker2.server.script.values.MtsVector;

public final class LookAtBuilder implements Runnable
{
    private LookAtData _data;
    private final EntityLookHelper _helper;
    
    public LookAtBuilder( EntityLookHelper helper )
    {
        _helper = helper;
    }
    
    private LookAtData getData()
    {
        if ( _data == null ) _data = new SimpleLookAtData();
        return _data;
    }
    
    private void setData( LookAtData data )
    {
        data.copyFrom( _data );
        _data = data;
    }
    
    public LookAtBuilder withYawDelta( float yawDelta )
    {
        getData().YawDelta = yawDelta;
        return this;
    }
    
    public LookAtBuilder withPitchDelta( float pitchDelta )
    {
        getData().PitchDelta = pitchDelta;
        return this;
    }
    
    public LookAtBuilder withVectorTarget( MtsVector vector )
    {
        setData( new VectorLookAtData( vector ) );
        return this;
    }
    
    public LookAtBuilder withVectorTarget( double x, double y, double z )
    {
        setData( new VectorLookAtData( x, y, z ) );
        return this;
    }
    
    public LookAtBuilder withEntityTarget( Entity entity )
    {
        setData( new EntityLookAtData( entity ) );
        return this;
    }
    
    public Runnable build()
    {
        if ( getData() instanceof SimpleLookAtData ) throw new UnsupportedOperationException();
        return this;
    }
    
    @Override
    public void run()
    {
        getData().run( _helper );
    }
    
    private static abstract class LookAtData
    {
        public float YawDelta;
        public float PitchDelta;
        
        public abstract void run( EntityLookHelper helper );
        
        public void copyFrom( LookAtData other )
        {
            if ( other == null ) return;
            YawDelta = other.YawDelta;
            PitchDelta = other.PitchDelta;
        }
    }
    
    private static final class SimpleLookAtData extends LookAtData
    {
        @Override
        public final void run( EntityLookHelper helper )
        {
            throw new UnsupportedOperationException();
        }
    }
    
    private static final class VectorLookAtData extends LookAtData
    {
        public double X;
        public double Y;
        public double Z;
        
        private VectorLookAtData( MtsVector vec )
        {
            X = vec.getX();
            Y = vec.getY();
            Z = vec.getZ();
        }
        
        private VectorLookAtData( double x, double y, double z )
        {
            X = x;
            Y = y;
            Z = z;
        }
        
        @Override
        public void run( EntityLookHelper helper )
        {
            helper.setLookPosition( X, Y, Z, YawDelta, PitchDelta );
        }
    }
    
    private static final class EntityLookAtData extends LookAtData
    {
        public Entity Entity;
        
        private EntityLookAtData( Entity entity )
        {
            Entity = entity;
        }
        
        @Override
        public void run( EntityLookHelper helper )
        {
            helper.setLookPositionWithEntity( Entity, YawDelta, PitchDelta );
        }
    }
}
