/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util.builders;

import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.WorldServer;
import net.mobtalker.mobtalker2.server.script.values.MtsVector;
import net.mobtalker.mobtalkerscript.v3.MtsArgumentException;

public class SpawnParticleBuilder implements Runnable
{
    private SpawnParticleData _data;
    private final WorldServer _world;
    private final EnumParticleTypes _type;
    
    public SpawnParticleBuilder( WorldServer world, EnumParticleTypes type )
    {
        _world = world;
        _type = type;
    }
    
    private SpawnParticleData getData()
    {
        if ( _data == null ) _data = new SimpleSpawnParticleData();
        return _data;
    }
    
    private ExtendedSpawnParticleData getExtendedData()
    {
        if ( !( _data instanceof ExtendedSpawnParticleData ) ) setData( new ExtendedSpawnParticleData() );
        return (ExtendedSpawnParticleData) _data;
    }
    
    private void setData( SpawnParticleData data )
    {
        if ( _data != null ) data.copyFrom( _data );
        _data = data;
    }
    
    public SpawnParticleBuilder withLongDistanceEnabled( boolean longDistance )
    {
        getData().LongDistance = longDistance;
        return this;
    }
    
    public SpawnParticleBuilder withPosition( MtsVector vector )
    {
        return withPosition( vector.getX(), vector.getY(), vector.getZ() );
    }
    
    public SpawnParticleBuilder withPosition( double x, double y, double z )
    {
        SpawnParticleData data = getData();
        data.PosX = x;
        data.PosY = y;
        data.PosZ = z;
        return this;
    }
    
    public SpawnParticleBuilder withOffset( MtsVector vector )
    {
        return withOffset( vector.getX(), vector.getY(), vector.getZ() );
    }
    
    public SpawnParticleBuilder withOffset( double x, double y, double z )
    {
        SpawnParticleData data = getData();
        data.OffsetX = x;
        data.OffsetY = y;
        data.OffsetZ = z;
        return this;
    }
    
    public SpawnParticleBuilder withArguments( int... args )
    {
        getData().Arguments = args;
        return this;
    }
    
    public SpawnParticleBuilder withCount( int count )
    {
        getExtendedData().Count = count;
        return this;
    }
    
    public SpawnParticleBuilder withSpeed( double speed )
    {
        getExtendedData().Speed = speed;
        return this;
    }
    
    public Runnable build()
    {
        return this;
    }
    
    @Override
    public void run()
    {
        getData().run( _world, _type );
    }
    
    private static abstract class SpawnParticleData
    {
        public boolean LongDistance;
        public double PosX;
        public double PosY;
        public double PosZ;
        public double OffsetX;
        public double OffsetY;
        public double OffsetZ;
        public int[] Arguments;
        
        private static final String _errorMessage = "invalid particle argument count: expected %d, got %d";
        
        protected final void checkArgumentCount( EnumParticleTypes type )
        {
            if ( Arguments == null && type.getArgumentCount() != 0 )
                throw new MtsArgumentException( _errorMessage, type.getArgumentCount(), 0 );
            if ( Arguments != null && Arguments.length != type.getArgumentCount() )
                throw new MtsArgumentException( _errorMessage, type.getArgumentCount(), Arguments.length );
        }
        
        public int[] getArguments()
        {
            return Arguments == null ? new int[0] : Arguments;
        }
        
        public abstract void run( WorldServer world, EnumParticleTypes type );
        
        public void copyFrom( SpawnParticleData other )
        {
            if ( other == null ) return;
            LongDistance = other.LongDistance;
            PosX = other.PosX;
            PosY = other.PosY;
            PosZ = other.PosZ;
            OffsetX = other.OffsetX;
            OffsetY = other.OffsetY;
            OffsetZ = other.OffsetZ;
            Arguments = other.Arguments;
        }
    }
    
    private static final class SimpleSpawnParticleData extends SpawnParticleData
    {
        @Override
        public void run( WorldServer world, EnumParticleTypes type )
        {
            checkArgumentCount( type );
            if ( LongDistance )
            {
                // TODO check if this works even though the method is annotated with @SideOnly(Side.CLIENT)
                world.spawnParticle( type, true, PosX, PosY, PosZ, OffsetX, OffsetY, OffsetZ, getArguments() );
            }
            else
            {
                world.spawnParticle( type, PosX, PosY, PosZ, OffsetX, OffsetY, OffsetZ, getArguments() );
            }
        }
    }
    
    private static final class ExtendedSpawnParticleData extends SpawnParticleData
    {
        public int Count = 1;
        public double Speed;
        
        @Override
        public void run( WorldServer world, EnumParticleTypes type )
        {
            checkArgumentCount( type );
            boolean longDistance = type.getShouldIgnoreRange() | LongDistance;
            world.spawnParticle( type, longDistance, PosX, PosY, PosZ, Count, OffsetX, OffsetY, OffsetZ, Speed,
                                 getArguments() );
        }
    }
}
