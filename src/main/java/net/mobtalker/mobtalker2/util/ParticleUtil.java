/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

import java.util.*;

import net.minecraft.util.EnumParticleTypes;
import net.mobtalker.mobtalkerscript.v3.MtsArgumentException;
import net.mobtalker.mobtalkerscript.v3.value.*;

public final class ParticleUtil
{
    private static final Map<String, EnumParticleTypes> _particleByName;
    //private static final Map<EnumParticleTypes, String> _nameByParticle;
    
    static
    {
        EnumParticleTypes[] types = EnumParticleTypes.values();
        _particleByName = new WeakHashMap<>( types.length );
        //_nameByParticle = new WeakHashMap<>( types.length );
        for ( EnumParticleTypes type : types )
        {
            _particleByName.put( type.getParticleName(), type );
            //_nameByParticle.put( type, type.getParticleName() );
        }
    }
    
    private ParticleUtil()
    {
    }
    
    public static EnumParticleTypes getParticleTypeIdentifiedBy( MtsValue val, int index )
    {
        if ( val.isNumber() )
        {
            int id = val.asNumber().toJavaInt();
            EnumParticleTypes type = EnumParticleTypes.getParticleFromId( id );
            if ( type == null ) throw new MtsArgumentException( index, "invalid particle id '%d'", id );
            return type;
        }
        
        if ( val.isString() )
        {
            String name = val.asString().toJava();
            EnumParticleTypes type = _particleByName.get( name );
            if ( type == null ) throw new MtsArgumentException( index, "invalid particle name '%s'", name );
            return type;
        }
        
        throw new MtsArgumentException( index, MtsType.NUMBER, MtsType.STRING, val.getType() );
    }
    
    public static EnumParticleTypes getParticleTypeIdentifiedBy( MtsVarargs args, int index )
    {
        return getParticleTypeIdentifiedBy( args.get( index ), index );
    }
}
