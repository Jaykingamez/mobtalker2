/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util.logging;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;

public class MobTalkerLogPrintStream extends PrintStream
{
    private final MobTalkerLog _log;
    private final Level _level;
    
    // ========================================
    
    public MobTalkerLogPrintStream( MobTalkerLog log, Level level, PrintStream original )
    {
        super( original );
        _log = log;
        _level = level;
    }
    
    // ========================================
    
    @Override
    public void println( Object o )
    {
        _log.log( _level, o );
    }
    
    @Override
    public void println( String s )
    {
        _log.log( _level, s );
    }
}
