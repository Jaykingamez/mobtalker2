/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

import java.util.*;
import java.util.Map.Entry;

import net.minecraft.potion.*;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.ReflectionHelper;

public class PotionUtil
{
    private static final Map<ResourceLocation, Potion> _resourceToPotionMap;
    private static final Map<Potion, ResourceLocation> _potionToResourceMap;
    
    static
    {
        _resourceToPotionMap = ReflectionHelper.getPrivateValue( Potion.class, null, "field_180150_I" );
        _potionToResourceMap = new HashMap<>( _resourceToPotionMap.size() );
        
        for ( Entry<ResourceLocation, Potion> entry : _resourceToPotionMap.entrySet() )
        {
            _potionToResourceMap.put( entry.getValue(), entry.getKey() );
        }
    }
    
    public static Potion getForName( String name )
    {
        return _resourceToPotionMap.get( new ResourceLocation( name ) );
    }
    
    public static PotionEffect getEffect( String name, int duration, int amplifier )
    {
        Potion potion = getForName( name );
        return new PotionEffect( potion.getId(), duration, amplifier, false, true );
    }
    
    public static String getName( PotionEffect effect )
    {
        Potion potion = Potion.potionTypes[effect.getPotionID()];
        return _potionToResourceMap.get( potion ).toString();
    }
}
