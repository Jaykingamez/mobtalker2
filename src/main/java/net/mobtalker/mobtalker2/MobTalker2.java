/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2;

import java.io.IOException;

import net.minecraft.launchwrapper.Launch;
import net.minecraftforge.fml.common.*;
import net.minecraftforge.fml.common.event.*;
import net.minecraftforge.fml.common.versioning.*;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;

@Mod( modid = MobTalker2.ID,
      name = MobTalker2.NAME,
      version = MobTalker2.VERSION,
      dependencies = "required-after:Forge@[11.14.1,11.15);",
      certificateFingerprint = MobTalker2.FINGERPRINT,
      guiFactory = "net.mobtalker.mobtalker2.client.config.gui.MobTalkerGuiConfigFactory",
      modLanguage = "java",
      useMetadata = true )
public class MobTalker2
{
    public static final MobTalkerLog LOG = new MobTalkerLog();
    
    // ========================================
    
    public static final String ID = "mobtalker2";
    public static final String NAME = "MobTalker2";
    public static final String VERSION = "@@VERSION@@";
    public static final String DOMAIN = "mobtalker2";
    
    public static final String FINGERPRINT = "@@FINGERPRINT@@";
    
    public static final String CHANNEL = "mobtalker2";
    
    public static final ComparableVersion SCRIPT_API_VERSION = new ComparableVersion( "1.2" );
    
    /**
     * Used to check if scripts target the same major script API version,
     * in which case we can load them even if minor version doesn't match
     */
    public static final VersionRange SCRIPT_API_VERSIONRANGE;
    
    // ========================================
    
    static
    {
        try
        {
            SCRIPT_API_VERSIONRANGE = VersionRange.createFromVersionSpec( "[1.0,2.0)" );
        }
        catch ( InvalidVersionSpecificationException ex )
        {
            throw new AssertionError( ex );
        }
    }
    
    // ========================================
    
    @Mod.Instance( ID )
    public static MobTalker2 instance;
    
    @SidedProxy( clientSide = "net.mobtalker.mobtalker2.client.MobTalkerClientProxy",
                 serverSide = "net.mobtalker.mobtalker2.server.MobTalkerServerProxy" )
    public static IMobTalkerProxy proxy;
    
    // ========================================
    
    @Mod.InstanceFactory
    public static MobTalker2 construct()
    {
        return new MobTalker2();
    }
    
    // ========================================
    
    @Mod.EventHandler
    public void onPreInit( FMLPreInitializationEvent event )
        throws IOException
    {
        LOG.trace( "onPreInit" );
        proxy.onPreInit( event );
    }
    
    @Mod.EventHandler
    public void onInit( FMLInitializationEvent event )
    {
        LOG.trace( "onInit" );
        proxy.onInit( event );
    }
    
    @Mod.EventHandler
    public void onPostInit( FMLPostInitializationEvent event )
    {
        LOG.trace( "onPostInit" );
        proxy.onPostInit( event );
    }
    
    // ========================================
    
    @Mod.EventHandler
    public void onServerAboutToStart( FMLServerAboutToStartEvent event )
    {
        LOG.trace( "onServerAboutToStart" );
        proxy.onServerAboutToStart( event );
    }
    
    @Mod.EventHandler
    public void onServerStarting( FMLServerStartingEvent event )
    {
        LOG.trace( "onServerStarting" );
        proxy.onServerStarting( event );
    }
    
    @Mod.EventHandler
    public void onServerStarted( FMLServerStartedEvent event )
    {
        LOG.trace( "onServerStarted" );
        proxy.onServerStarted( event );
    }
    
    @Mod.EventHandler
    public void onServerStopping( FMLServerStoppingEvent event )
    {
        LOG.trace( "onServerStopping" );
        proxy.onServerStopping( event );
    }
    
    @Mod.EventHandler
    public void onServerStopped( FMLServerStoppedEvent event )
    {
        LOG.trace( "onServerStopped" );
        proxy.onServerStopped( event );
    }
    
    // ========================================
    
    @Mod.EventHandler
    public void onFingerprintViolation( FMLFingerprintViolationEvent event )
    {
        // Only raise violation in obfuscated environment
        if ( Launch.blackboard.get( "fml.deobfuscatedEnvironment" ) == null )
        {
            proxy.onFingerprintViolation( event );
        }
    }
}
